﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoldRaven.PrintFolder
{
    public partial class PrintRdlc_Form : Form
    {
        public PrintRdlc_Form()
        {
            InitializeComponent();
        
        }
   
        private static List<Stream> m_streams;
        private static int m_currentPageIndex = 0;
        private DataTable dataT = new DataTable();
        public string reportId;


        public string passingvalue
        {

            get { return reportId; }
            set { reportId = value; }
        }

        private void PrintRdlc_Form_Load(object sender, EventArgs e)
        {
           
            
        }

        public void getPrintData(string Id)
        {
           Database databaseObject = new Database();
            ;
            string query = " SELECT * FROM reportTable INNER JOIN customerTable ON reportTable.customerId = customerTable.id INNER JOIN reportDetailTable ON reportTable.id = reportDetailTable.reportId  WHERE reportTable.id = @ReportId ";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                command.Parameters.Add(new SQLiteParameter("@ReportId", Id.ToString()));
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                Data_PrintReport dt = new Data_PrintReport();
                sda.Fill(dt, "DataTable_PrintReport");
                ReportDataSource dataSource = new ReportDataSource("DataSet_ReportPrint", dt.Tables[0]);
                var dataTablepass = new DataTable();
                sda.Fill(dataTablepass);
                dataT = dataTablepass;
               
                //this.reportViewer1.LocalReport.DataSources.Clear();
               // this.reportViewer1.LocalReport.DataSources.Add(dataSource);
               // this.reportViewer1.RefreshReport();

                databaseObject.CloseConnection();
               

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            } 
        }

        public void calldatda(string Id)
        {
            try
            {
                this.getPrintData(Id);
                LocalReport printReport = new LocalReport();
                string path = Path.GetDirectoryName(Application.ExecutablePath);
                string fullPath = Path.GetDirectoryName(Application.ExecutablePath)+ @"\PrintFolder\Print_ReportRdlc.rdlc";
                printReport.ReportPath = fullPath;
               // For Debug Purpose The Below line;  
                //MessageBox.Show(path + fullPath);
                printReport.DataSources.Add(new ReportDataSource("DataSet_ReportPrint", dataT));
                PrintToPrinter(printReport);
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
        public static void PrintToPrinter(LocalReport report)
        {
            Export(report);

        }

        public static void Export(LocalReport report, bool print = true)
        {
            string deviceInfo =
             @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                <PageWidth>8.27in</PageWidth>
                <PageHeight>11.7in</PageHeight>
                <MarginTop>0in</MarginTop>
                <MarginLeft>0in</MarginLeft>
                <MarginRight>0in</MarginRight>
                <MarginBottom>0in</MarginBottom>
            </DeviceInfo>";
            Warning[] warnings;
            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream, out warnings);
            foreach (Stream stream in m_streams)
                stream.Position = 0;

            if (print)
            {
                Print();
            }
        }


        public static void Print()
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");
            PrintDocument printDoc = new PrintDocument();
            if (!printDoc.PrinterSettings.IsValid)
            {
                throw new Exception("Error: cannot find the default printer.");
            }
            else
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                m_currentPageIndex = 0;
                //THIS BELOW CODE IS FOR SHOW PRINTER THEN PRINT 
                /*   PrintDialog printReportDialog = new PrintDialog();
                   printReportDialog.Document = printDoc;
                   DialogResult result = printReportDialog.ShowDialog(); 


                   if (result == DialogResult.OK)
                   {
                       printDoc.Print();
                   }
                */
                //AND //THIS BELOW CODE IS FOR SHOW PRINTER THEN PRINT 

                printDoc.Print();

            }
        }

        public static Stream CreateStream(string name, string fileNameExtension, Encoding encoding, string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }

        public static void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(m_streams[m_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            Rectangle adjustedRect = new Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        public static void DisposePrint()
        {
            if (m_streams != null)
            {
                foreach (Stream stream in m_streams)
                    stream.Close();
                m_streams = null;
            }
        }
    }
}
