﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoldRaven.PrintTestingReport
{
    public partial class PrintTestingForm : Form
    {
        public PrintTestingForm()
        {
            InitializeComponent();
        }
        private static List<Stream> m_streams;
        private static int m_currentPageIndex = 0;
        private DataTable dataTest = new DataTable();
        public string testingId;

        public string passingvalue_Two
        {

            get { return testingId; }
            set { testingId = value; }
        }
        private void PrintTestingForm_Load(object sender, EventArgs e)
        {

            //this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }

        public void getTestingPrintData(string Id)
        {
            Database databaseObject = new Database();
            ;
            string query = "SELECT * FROM testingTable INNER JOIN customerTable ON testingTable.testcustId = customerTable.id  WHERE testingTable.id = @testingId";

            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                command.Parameters.Add(new SQLiteParameter("@testingId", Id.ToString()));
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataSetTesting1 dt = new DataSetTesting1();
                sda.Fill(dt, "DataTestingTable1");
                ReportDataSource dataSource = new ReportDataSource("DataSet_Testing", dt.Tables[0]);
                var dataTablepass = new DataTable();
                sda.Fill(dataTablepass);
                dataTest = dataTablepass;

                //this.reportViewer1.LocalReport.DataSources.Clear();
                // this.reportViewer1.LocalReport.DataSources.Add(dataSource);
                // this.reportViewer1.RefreshReport();

                databaseObject.CloseConnection();


            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

        public void callTestingdata(string Id)
        {
       
            try
            {
                this.getTestingPrintData(Id);
                LocalReport printTesting = new LocalReport();
                string path = Path.GetDirectoryName(Application.ExecutablePath);
                string fullPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\PrintTestingReport\Print_Testing1.rdlc";
                printTesting.ReportPath = fullPath;
                printTesting.DataSources.Add(new ReportDataSource("DataSet_Testing", dataTest));
                PrintToPrinter(printTesting);
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

     
        public static void PrintToPrinter(LocalReport reportTest)
        {
            Export(reportTest);

        }

        public static void Export(LocalReport reportTest2, bool print = true)
        {
            string deviceInfo =
             @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                   <PageWidth>3.53in</PageWidth>
                <PageHeight>5in</PageHeight>
                <MarginTop>0in</MarginTop>
                <MarginLeft>0in</MarginLeft>
                <MarginRight>0in</MarginRight>
                <MarginBottom>0in</MarginBottom>
            </DeviceInfo>";
            Warning[] warnings;
            m_streams = new List<Stream>();
            reportTest2.Render("Image", deviceInfo, CreateStream, out warnings);
            foreach (Stream stream in m_streams)
                stream.Position = 0;

            if (print)
            {
                Print();
            }
        }


        public static void Print()
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");
            PrintDocument printDoc = new PrintDocument();
            if (!printDoc.PrinterSettings.IsValid)
            {
                throw new Exception("Error: cannot find the default printer.");
            }
            else
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                m_currentPageIndex = 0;
                PrintDialog printReportDialog = new PrintDialog();
                printReportDialog.Document = printDoc;
                //  printDoc.DefaultPageSettings.PaperSize = new System.Drawing.Printing.PaperSize("pprnm", 285, 600);
                DialogResult result = printReportDialog.ShowDialog();


                if (result == DialogResult.OK)
                {
                    printDoc.Print();
                }
                
            }
        }

        public static Stream CreateStream(string name, string fileNameExtension, Encoding encoding, string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }

        public static void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(m_streams[m_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            Rectangle adjustedRect = new Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        public static void DisposePrint()
        {
            if (m_streams != null)
            {
                foreach (Stream stream in m_streams)
                    stream.Close();
                m_streams = null;
            }
        }
    }
}

