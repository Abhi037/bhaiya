﻿
namespace GoldRaven
{
    partial class ReportModel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportModel));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges9 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties17 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties18 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges10 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties19 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties20 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges11 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties21 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties22 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges12 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties23 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties24 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.label4 = new System.Windows.Forms.Label();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuButton5 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton6 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.reportDetailSaveButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.reportDetailPrintButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(16, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 32);
            this.label4.TabIndex = 146;
            this.label4.Text = "Report Detail";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuButton5
            // 
            this.bunifuButton5.AllowToggling = false;
            this.bunifuButton5.AnimationSpeed = 200;
            this.bunifuButton5.AutoGenerateColors = false;
            this.bunifuButton5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton5.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.bunifuButton5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton5.BackgroundImage")));
            this.bunifuButton5.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton5.ButtonText = "X";
            this.bunifuButton5.ButtonTextMarginLeft = 0;
            this.bunifuButton5.ColorContrastOnClick = 45;
            this.bunifuButton5.ColorContrastOnHover = 45;
            this.bunifuButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges9.BottomLeft = true;
            borderEdges9.BottomRight = true;
            borderEdges9.TopLeft = true;
            borderEdges9.TopRight = true;
            this.bunifuButton5.CustomizableEdges = borderEdges9;
            this.bunifuButton5.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton5.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton5.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton5.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton5.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton5.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuButton5.ForeColor = System.Drawing.Color.White;
            this.bunifuButton5.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton5.IconMarginLeft = 11;
            this.bunifuButton5.IconPadding = 10;
            this.bunifuButton5.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton5.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.bunifuButton5.IdleBorderRadius = 3;
            this.bunifuButton5.IdleBorderThickness = 1;
            this.bunifuButton5.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.bunifuButton5.IdleIconLeftImage = null;
            this.bunifuButton5.IdleIconRightImage = null;
            this.bunifuButton5.IndicateFocus = false;
            this.bunifuButton5.Location = new System.Drawing.Point(929, 27);
            this.bunifuButton5.Name = "bunifuButton5";
            stateProperties17.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties17.BorderRadius = 3;
            stateProperties17.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties17.BorderThickness = 1;
            stateProperties17.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties17.ForeColor = System.Drawing.Color.White;
            stateProperties17.IconLeftImage = null;
            stateProperties17.IconRightImage = null;
            this.bunifuButton5.onHoverState = stateProperties17;
            stateProperties18.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties18.BorderRadius = 3;
            stateProperties18.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties18.BorderThickness = 1;
            stateProperties18.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties18.ForeColor = System.Drawing.Color.White;
            stateProperties18.IconLeftImage = null;
            stateProperties18.IconRightImage = null;
            this.bunifuButton5.OnPressedState = stateProperties18;
            this.bunifuButton5.Size = new System.Drawing.Size(28, 24);
            this.bunifuButton5.TabIndex = 152;
            this.bunifuButton5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton5.TextMarginLeft = 0;
            this.bunifuButton5.UseDefaultRadiusAndThickness = true;
            this.bunifuButton5.Click += new System.EventHandler(this.bunifuButton5_Click);
            // 
            // bunifuButton6
            // 
            this.bunifuButton6.AllowToggling = false;
            this.bunifuButton6.AnimationSpeed = 200;
            this.bunifuButton6.AutoGenerateColors = false;
            this.bunifuButton6.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton6.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton6.BackgroundImage")));
            this.bunifuButton6.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton6.ButtonText = "-";
            this.bunifuButton6.ButtonTextMarginLeft = 0;
            this.bunifuButton6.ColorContrastOnClick = 45;
            this.bunifuButton6.ColorContrastOnHover = 45;
            this.bunifuButton6.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges10.BottomLeft = true;
            borderEdges10.BottomRight = true;
            borderEdges10.TopLeft = true;
            borderEdges10.TopRight = true;
            this.bunifuButton6.CustomizableEdges = borderEdges10;
            this.bunifuButton6.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton6.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton6.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton6.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton6.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton6.Font = new System.Drawing.Font("Segoe UI Semibold", 15F, System.Drawing.FontStyle.Bold);
            this.bunifuButton6.ForeColor = System.Drawing.Color.White;
            this.bunifuButton6.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton6.IconMarginLeft = 11;
            this.bunifuButton6.IconPadding = 10;
            this.bunifuButton6.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton6.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton6.IdleBorderRadius = 3;
            this.bunifuButton6.IdleBorderThickness = 1;
            this.bunifuButton6.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton6.IdleIconLeftImage = null;
            this.bunifuButton6.IdleIconRightImage = null;
            this.bunifuButton6.IndicateFocus = false;
            this.bunifuButton6.Location = new System.Drawing.Point(895, 27);
            this.bunifuButton6.Name = "bunifuButton6";
            stateProperties19.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties19.BorderRadius = 3;
            stateProperties19.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties19.BorderThickness = 1;
            stateProperties19.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties19.ForeColor = System.Drawing.Color.White;
            stateProperties19.IconLeftImage = null;
            stateProperties19.IconRightImage = null;
            this.bunifuButton6.onHoverState = stateProperties19;
            stateProperties20.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties20.BorderRadius = 3;
            stateProperties20.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties20.BorderThickness = 1;
            stateProperties20.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties20.ForeColor = System.Drawing.Color.White;
            stateProperties20.IconLeftImage = null;
            stateProperties20.IconRightImage = null;
            this.bunifuButton6.OnPressedState = stateProperties20;
            this.bunifuButton6.Size = new System.Drawing.Size(28, 24);
            this.bunifuButton6.TabIndex = 151;
            this.bunifuButton6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton6.TextMarginLeft = 0;
            this.bunifuButton6.UseDefaultRadiusAndThickness = true;
            // 
            // reportDetailSaveButton
            // 
            this.reportDetailSaveButton.AllowToggling = false;
            this.reportDetailSaveButton.AnimationSpeed = 200;
            this.reportDetailSaveButton.AutoGenerateColors = false;
            this.reportDetailSaveButton.BackColor = System.Drawing.Color.Transparent;
            this.reportDetailSaveButton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.reportDetailSaveButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("reportDetailSaveButton.BackgroundImage")));
            this.reportDetailSaveButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.reportDetailSaveButton.ButtonText = "Save";
            this.reportDetailSaveButton.ButtonTextMarginLeft = 0;
            this.reportDetailSaveButton.ColorContrastOnClick = 45;
            this.reportDetailSaveButton.ColorContrastOnHover = 45;
            this.reportDetailSaveButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges11.BottomLeft = true;
            borderEdges11.BottomRight = true;
            borderEdges11.TopLeft = true;
            borderEdges11.TopRight = true;
            this.reportDetailSaveButton.CustomizableEdges = borderEdges11;
            this.reportDetailSaveButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.reportDetailSaveButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.reportDetailSaveButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.reportDetailSaveButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.reportDetailSaveButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.reportDetailSaveButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.reportDetailSaveButton.ForeColor = System.Drawing.Color.White;
            this.reportDetailSaveButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.reportDetailSaveButton.IconMarginLeft = 11;
            this.reportDetailSaveButton.IconPadding = 10;
            this.reportDetailSaveButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.reportDetailSaveButton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.reportDetailSaveButton.IdleBorderRadius = 3;
            this.reportDetailSaveButton.IdleBorderThickness = 1;
            this.reportDetailSaveButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.reportDetailSaveButton.IdleIconLeftImage = null;
            this.reportDetailSaveButton.IdleIconRightImage = null;
            this.reportDetailSaveButton.IndicateFocus = false;
            this.reportDetailSaveButton.Location = new System.Drawing.Point(568, 512);
            this.reportDetailSaveButton.Name = "reportDetailSaveButton";
            stateProperties21.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties21.BorderRadius = 3;
            stateProperties21.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties21.BorderThickness = 1;
            stateProperties21.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties21.ForeColor = System.Drawing.Color.White;
            stateProperties21.IconLeftImage = null;
            stateProperties21.IconRightImage = null;
            this.reportDetailSaveButton.onHoverState = stateProperties21;
            stateProperties22.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties22.BorderRadius = 3;
            stateProperties22.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties22.BorderThickness = 1;
            stateProperties22.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties22.ForeColor = System.Drawing.Color.White;
            stateProperties22.IconLeftImage = null;
            stateProperties22.IconRightImage = null;
            this.reportDetailSaveButton.OnPressedState = stateProperties22;
            this.reportDetailSaveButton.Size = new System.Drawing.Size(140, 33);
            this.reportDetailSaveButton.TabIndex = 150;
            this.reportDetailSaveButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reportDetailSaveButton.TextMarginLeft = 0;
            this.reportDetailSaveButton.UseDefaultRadiusAndThickness = true;
            // 
            // reportDetailPrintButton
            // 
            this.reportDetailPrintButton.AllowToggling = false;
            this.reportDetailPrintButton.AnimationSpeed = 200;
            this.reportDetailPrintButton.AutoGenerateColors = false;
            this.reportDetailPrintButton.BackColor = System.Drawing.Color.Transparent;
            this.reportDetailPrintButton.BackColor1 = System.Drawing.Color.Goldenrod;
            this.reportDetailPrintButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("reportDetailPrintButton.BackgroundImage")));
            this.reportDetailPrintButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.reportDetailPrintButton.ButtonText = "Print";
            this.reportDetailPrintButton.ButtonTextMarginLeft = 0;
            this.reportDetailPrintButton.ColorContrastOnClick = 45;
            this.reportDetailPrintButton.ColorContrastOnHover = 45;
            this.reportDetailPrintButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges12.BottomLeft = true;
            borderEdges12.BottomRight = true;
            borderEdges12.TopLeft = true;
            borderEdges12.TopRight = true;
            this.reportDetailPrintButton.CustomizableEdges = borderEdges12;
            this.reportDetailPrintButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.reportDetailPrintButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.reportDetailPrintButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.reportDetailPrintButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.reportDetailPrintButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.reportDetailPrintButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.reportDetailPrintButton.ForeColor = System.Drawing.Color.White;
            this.reportDetailPrintButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.reportDetailPrintButton.IconMarginLeft = 11;
            this.reportDetailPrintButton.IconPadding = 10;
            this.reportDetailPrintButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.reportDetailPrintButton.IdleBorderColor = System.Drawing.Color.Goldenrod;
            this.reportDetailPrintButton.IdleBorderRadius = 3;
            this.reportDetailPrintButton.IdleBorderThickness = 1;
            this.reportDetailPrintButton.IdleFillColor = System.Drawing.Color.Goldenrod;
            this.reportDetailPrintButton.IdleIconLeftImage = null;
            this.reportDetailPrintButton.IdleIconRightImage = null;
            this.reportDetailPrintButton.IndicateFocus = false;
            this.reportDetailPrintButton.Location = new System.Drawing.Point(724, 512);
            this.reportDetailPrintButton.Name = "reportDetailPrintButton";
            stateProperties23.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties23.BorderRadius = 3;
            stateProperties23.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties23.BorderThickness = 1;
            stateProperties23.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties23.ForeColor = System.Drawing.Color.White;
            stateProperties23.IconLeftImage = null;
            stateProperties23.IconRightImage = null;
            this.reportDetailPrintButton.onHoverState = stateProperties23;
            stateProperties24.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties24.BorderRadius = 3;
            stateProperties24.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties24.BorderThickness = 1;
            stateProperties24.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties24.ForeColor = System.Drawing.Color.White;
            stateProperties24.IconLeftImage = null;
            stateProperties24.IconRightImage = null;
            this.reportDetailPrintButton.OnPressedState = stateProperties24;
            this.reportDetailPrintButton.Size = new System.Drawing.Size(141, 33);
            this.reportDetailPrintButton.TabIndex = 149;
            this.reportDetailPrintButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reportDetailPrintButton.TextMarginLeft = 0;
            this.reportDetailPrintButton.UseDefaultRadiusAndThickness = true;
            this.reportDetailPrintButton.Click += new System.EventHandler(this.reportDetailPrintButton_Click);
            // 
            // ReportModel
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(79)))));
            this.ClientSize = new System.Drawing.Size(982, 600);
            this.Controls.Add(this.bunifuButton5);
            this.Controls.Add(this.bunifuButton6);
            this.Controls.Add(this.reportDetailSaveButton);
            this.Controls.Add(this.reportDetailPrintButton);
            this.Controls.Add(this.label4);
            this.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReportModel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ReportModel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton reportDetailPrintButton;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton reportDetailSaveButton;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton5;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton6;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
    }
}