﻿
namespace GoldRaven.PrintTokenSlip
{
    partial class Print_tokenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.TokenPrint_Dataset = new GoldRaven.PrintTokenSlip.TokenPrint_Dataset();
            this.TokenPrint_DataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.TokenPrint_Dataset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TokenPrint_DataTableBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // TokenPrint_Dataset
            // 
            this.TokenPrint_Dataset.DataSetName = "TokenPrint_Dataset";
            this.TokenPrint_Dataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // TokenPrint_DataTableBindingSource
            // 
            this.TokenPrint_DataTableBindingSource.DataMember = "TokenPrint_DataTable";
            this.TokenPrint_DataTableBindingSource.DataSource = this.TokenPrint_Dataset;
            // 
            // reportViewer2
            // 
            reportDataSource1.Name = "DataSet_PrintToken";
            reportDataSource1.Value = this.TokenPrint_DataTableBindingSource;
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "GoldRaven.PrintTokenSlip.ReportPrintToken.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(12, 12);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.ServerReport.BearerToken = null;
            this.reportViewer2.Size = new System.Drawing.Size(415, 292);
            this.reportViewer2.TabIndex = 0;
            // 
            // Print_tokenForm
            // 
            this.ClientSize = new System.Drawing.Size(439, 316);
            this.Controls.Add(this.reportViewer2);
            this.Name = "Print_tokenForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Print_tokenForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TokenPrint_Dataset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TokenPrint_DataTableBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource TokenPrint_DataTableBindingSource;
        private TokenPrint_Dataset TokenPrint_Dataset;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer2;
    }
}