﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoldRaven.PrintTokenSlip
{
    public partial class Print_tokenForm : Form
    {
        public Print_tokenForm()
        {
            InitializeComponent();
        }
        private static List<Stream> m_streams;
        private static int m_currentPageIndex = 0;
        private DataTable dataToken = new DataTable();
        public string tokenId;

        public string passingvalue_Two
        {

            get { return tokenId; }
            set { tokenId = value; }
        }
       

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }

        public void getTokenPrintData(string Id)
        {
            Database databaseObject = new Database();
            ;
            string query = "SELECT * FROM tokenTable INNER JOIN customerTable ON tokenTable.customerId = customerTable.id INNER JOIN sampleTable ON tokenTable.id = sampleTable.tokenId  WHERE tokenTable.id = @tokenId";

            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                command.Parameters.Add(new SQLiteParameter("@tokenId", Id.ToString()));
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                TokenPrint_Dataset dt = new TokenPrint_Dataset();
                sda.Fill(dt, "TokenPrint_DataTable");
                ReportDataSource dataSource = new ReportDataSource("DataSet_PrintToken", dt.Tables[0]);
                var dataTableToken = new DataTable();
                sda.Fill(dataTableToken);
                dataToken = dataTableToken;
            

                //this.reportViewer1.LocalReport.DataSources.Clear();
                // this.reportViewer1.LocalReport.DataSources.Add(dataSource);
                // this.reportViewer1.RefreshReport();

                databaseObject.CloseConnection();


            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

        public void callTokendata(string Id)
        {

            try
            {
                this.getTokenPrintData(Id);
                LocalReport printToken = new LocalReport();
                string path = Path.GetDirectoryName(Application.ExecutablePath);
                string fullPath = Path.GetDirectoryName(Application.ExecutablePath)+ @"\PrintTokenSlip\ReportPrintToken.rdlc";
                printToken.ReportPath = fullPath;
                printToken.DataSources.Add(new ReportDataSource("DataSet_PrintToken", dataToken));

                PrintToPrinter(printToken);
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }


        public static void PrintToPrinter(LocalReport reportTest)
        {
            Export(reportTest);

        }

        public static void Export(LocalReport reportTest3, bool print = true)
        {
            string deviceInfo =
             @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                   <PageWidth>3.51in</PageWidth>
                <PageHeight>5in</PageHeight>
                <MarginTop>0in</MarginTop>
                <MarginLeft>0in</MarginLeft>
                <MarginRight>0in</MarginRight>
                <MarginBottom>0in</MarginBottom>
            </DeviceInfo>";
            Warning[] warnings;
            m_streams = new List<Stream>();
            reportTest3.Render("Image", deviceInfo, CreateStream, out warnings);
            foreach (Stream stream in m_streams)
                stream.Position = 0;

            if (print)
            {
                Print();
            }
        }


        public static void Print()
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");
            PrintDocument printDoc = new PrintDocument();
            if (!printDoc.PrinterSettings.IsValid)
            {
                throw new Exception("Error: cannot find the default printer.");
            }
            else
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                m_currentPageIndex = 0;
                PrintDialog printReportDialog = new PrintDialog();
                printReportDialog.Document = printDoc;
              //  printDoc.DefaultPageSettings.PaperSize = new System.Drawing.Printing.PaperSize("pprnm", 285, 600);
                DialogResult result = printReportDialog.ShowDialog();


                if (result == DialogResult.OK)
                {
                    printDoc.Print();
                }

                
            }
        }

        public static Stream CreateStream(string name, string fileNameExtension, Encoding encoding, string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }

        public static void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(m_streams[m_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            Rectangle adjustedRect = new Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        public static void DisposePrint()
        {
            if (m_streams != null)
            {
                foreach (Stream stream in m_streams)
                    stream.Close();
                m_streams = null;
            }
        }

        private void Print_tokenForm_Load(object sender, EventArgs e)
        {

            //this.reportViewer2.RefreshReport();
            //this.reportViewer2.RefreshReport();
        }
    }
   
    
}
