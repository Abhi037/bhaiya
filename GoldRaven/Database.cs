﻿using System.Data.SQLite;
namespace GoldRaven
{
    class Database
    {
        public SQLiteConnection myConn;
        public Database() {
            myConn = new SQLiteConnection("Data Source = db.db") ;
           
        }

        public void OpenConnection() {
           
                           if (myConn.State != System.Data.ConnectionState.Open)
                {
                    myConn.Open();
                }
            
        }

        public void CloseConnection() {
            if (myConn.State != System.Data.ConnectionState.Closed)
            {
                myConn.Close();
            }
        }
    }
}
