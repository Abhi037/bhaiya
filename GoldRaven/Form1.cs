﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoldRaven
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Timer formClose = new Timer();
        private Login loginForm;
        private void Form1_Load(object sender, EventArgs e)
        {
            this.BackgroundImage = Properties.Resources.raven_gold;
            this.ControlBox = false;
            formClose.Interval = 2000;
            formClose.Tick += new EventHandler(timer_tick);
            formClose.Start();
        }
        private void timer_tick(object sender, EventArgs e) {
            Debug.WriteLine("Send to debug output.");
            this.Hide();
            loginForm = new Login();
            loginForm.Show();
            formClose.Stop();

        }
    }
}
