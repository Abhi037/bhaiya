﻿using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace GoldRaven
{
    public partial class Login : Form
    {

        private Dashboard dashboard;
        public Login()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            System.Windows.Forms.Application.Exit();
        }


        private void bunifuLabel1_Click(object sender, EventArgs e)
        {

        }

        private void loginButton_Click_1(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.userNameInput.Text))
            {
                //MessageBox.Show("UserName Cannot Be Empty");
                this.Alert("UserName Cannot Be Empty", Toaster.enmType.Info);

                return;
            }
            if (string.IsNullOrEmpty(this.passwordInput.Text))
            {
            //    MessageBox.Show("Password Cannot Be Empty");
                this.Alert("Password Cannot Be Empty", Toaster.enmType.Info);

                return;
            }
            bool loginsuccess = false;
            Database databaseObject = new Database();
            string query = "SELECT * FROM users WHERE username=@UserName";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            command.Parameters.AddWithValue("@UserName", this.userNameInput.Text.ToString());
            databaseObject.OpenConnection();
            SQLiteDataReader result = command.ExecuteReader();
            if (result.HasRows) {
                while (result.Read()) {
                    string pwd = result["password"].ToString();
                    if (pwd.Equals(this.passwordInput.Text.ToString())) {
                        loginsuccess = true;
                    }
                }
            }
                
            databaseObject.CloseConnection();
            if (loginsuccess)
            {

                this.Hide();
                dashboard = new Dashboard();
                dashboard.Show();

            }
            else
            {
               // MessageBox.Show("Invalid User Name Or Password");
                this.Alert("Invalid User Name Or Password", Toaster.enmType.Warning);
            }

        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
            System.Windows.Forms.Application.Exit();

        }

        private void reportDetailPrintButton_Click(object sender, EventArgs e)
        {
            this.Close();
            System.Windows.Forms.Application.Exit();
        }
        public void Alert(string msg, Toaster.enmType type)
        {
            Toaster frm = new Toaster();
            frm.showAlert(msg, type);
        }

        private void bunifuButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.userNameInput.Text))
            {
                this.Alert("UserName Cannot Be Empty", Toaster.enmType.Warning);
               // MessageBox.Show("UserName Cannot Be Empty");
                return;
            }
            if (string.IsNullOrEmpty(this.passwordInput.Text))
            {
                this.Alert("Password Cannot Be Empty", Toaster.enmType.Warning);
               // MessageBox.Show("Password Cannot Be Empty");
                return;
            }
            bool loginsuccess = false;
            
            Database databaseObject = new Database();
            string query = "SELECT * FROM users WHERE username=@UserName";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            command.Parameters.AddWithValue("@UserName", this.userNameInput.Text.ToString());
            try
            {
                databaseObject.OpenConnection();
                SQLiteDataReader result = command.ExecuteReader();
                if (result.HasRows)
                {
                    while (result.Read())
                    {
                        string validation = result["lastDate"].ToString();
                        DateTime expDate = DateTime.Parse(validation.ToString());
                        DateTime toDate = DateTime.Now;
                        string pwd = result["password"].ToString();
                        if ((pwd.Equals(this.passwordInput.Text.ToString())) && (toDate.Date < expDate.Date))
                        {
                            
                            loginsuccess = true;
                           
                            this.Alert("Login Successfully!", Toaster.enmType.Success);

                        }
                        else if ((pwd.Equals(this.passwordInput.Text.ToString())) && !(toDate.Date < expDate.Date))
                        {
                            this.Alert("Your Software is Expired!", Toaster.enmType.Warning);
                        }
                         else 
                        {
                            this.Alert("Invalid User Name Or Password!", Toaster.enmType.Error);
                        }



                    }
                } else
                {
                    this.Alert("Invalid User Name Or Password!", Toaster.enmType.Error);
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

            databaseObject.CloseConnection();
            if (loginsuccess)
            {

                this.Hide();
                dashboard = new Dashboard();
                dashboard.Show();
               

            }
           // else if (!loginsuccess  )
            //{
              //  MessageBox.Show("Invalid User Name Or Password");
              // this.Alert("Invalid User Name Or Password!", Toaster.enmType.Error);
          
             
           
            
           
        }

        private void Login_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }
    }
}
