﻿namespace GoldRaven
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components3 = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components3 != null))
            {
                components3.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties5 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties6 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges4 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties7 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties8 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges5 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties9 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties10 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges6 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties11 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties12 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges7 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties13 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties14 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges8 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties15 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties16 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges9 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties17 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties18 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Utilities.BunifuPages.BunifuAnimatorNS.Animation animation1 = new Utilities.BunifuPages.BunifuAnimatorNS.Animation();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges10 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties19 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties20 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties21 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties22 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties23 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties24 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges11 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties25 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties26 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges12 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties27 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties28 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges13 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties29 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties30 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges14 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties31 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties32 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges15 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties33 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties34 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges16 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties35 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties36 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges17 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties37 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties38 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties39 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties40 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties41 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties42 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties43 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties44 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties45 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties46 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties47 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties48 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties49 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties50 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties51 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties52 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties53 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties54 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties55 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties56 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties57 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties58 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties59 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties60 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties61 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties62 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges18 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties63 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties64 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges19 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties65 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties66 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges20 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties67 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties68 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges21 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties69 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties70 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges22 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties71 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties72 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges23 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties73 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties74 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges24 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties75 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties76 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges25 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties77 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties78 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges26 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties79 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties80 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges27 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties81 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties82 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties83 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties84 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties85 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties86 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges28 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties87 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties88 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges29 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties89 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties90 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges30 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties91 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties92 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges31 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties93 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties94 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties95 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties96 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties97 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties98 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties99 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties100 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties101 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties102 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties103 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties104 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties105 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties106 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties107 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties108 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties109 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties110 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties111 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties112 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties113 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties114 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties115 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties116 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties117 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties118 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties119 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties120 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties121 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties122 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties123 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties124 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties125 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties126 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges32 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties127 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties128 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges33 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties129 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties130 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges34 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties131 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties132 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges35 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties133 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties134 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties135 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties136 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties137 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties138 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges36 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties139 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties140 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges37 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties141 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties142 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges38 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties143 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties144 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges39 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties145 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties146 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges40 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties147 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties148 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges41 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties149 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties150 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties151 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties152 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties153 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties154 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties155 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties156 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties157 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties158 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties159 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties160 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties161 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties162 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties163 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties164 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties165 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties166 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties167 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties168 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties169 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties170 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties171 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties172 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties173 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties174 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges42 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties175 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties176 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges43 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties177 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties178 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges44 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties179 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties180 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges45 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties181 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties182 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges46 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties183 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties184 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.bunifuShadowPanel1 = new Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.indicator = new System.Windows.Forms.PictureBox();
            this.bunifuButton8 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton15 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton14 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton12 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton11 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton4 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bunifuPictureBox2 = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.pagesForm = new Bunifu.UI.WinForms.BunifuPages();
            this.dashbaordPage = new System.Windows.Forms.TabPage();
            this.bunifuLabel42 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel76 = new Bunifu.UI.WinForms.BunifuLabel();
            this.dashboardEndDatePicker = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.dashboardStartDatePicker = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.bunifuCards11 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.unpaidTestingAmount = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel93 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuCards12 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.totalUnpaidReportAmount = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel95 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuCards8 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.totalUnpaidTesting = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel83 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuCards7 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.totalUnpaidReportText = new Bunifu.UI.WinForms.BunifuLabel();
            this.unpaidAmount = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuCards6 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.totaltestingAmountText = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel69 = new Bunifu.UI.WinForms.BunifuLabel();
            this.getDashboardDataButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuCards5 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.testingCountText = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel87 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuCards4 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.savingText = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel26 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuCards3 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.investAmontText = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel25 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuDatepicker3 = new Bunifu.Framework.UI.BunifuDatepicker();
            this.bunifuCards2 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.saleAmountText = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel24 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuCards1 = new Bunifu.Framework.UI.BunifuCards();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.totalReportText = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel22 = new Bunifu.UI.WinForms.BunifuLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.customerMasterPage = new System.Windows.Forms.TabPage();
            this.custMastRatetextbox = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.customerDataGridView = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bunifuLabel21 = new Bunifu.UI.WinForms.BunifuLabel();
            this.custMastFirmNametextbox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel15 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel16 = new Bunifu.UI.WinForms.BunifuLabel();
            this.custMastMobiletextbox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel17 = new Bunifu.UI.WinForms.BunifuLabel();
            this.custMastAddresstextbox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel20 = new Bunifu.UI.WinForms.BunifuLabel();
            this.custMastNametextbox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.label2 = new System.Windows.Forms.Label();
            this.bunifuButton10 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton9 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.reportDetailSaveButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.reportDetailPrintButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.sellItemPage = new System.Windows.Forms.TabPage();
            this.bunifuLabel19 = new Bunifu.UI.WinForms.BunifuLabel();
            this.serialNoTextBox = new Bunifu.UI.WinForms.BunifuLabel();
            this.panelAddDetail = new System.Windows.Forms.Panel();
            this.bunifuLabel88 = new Bunifu.UI.WinForms.BunifuLabel();
            this.totalPercentageLabel = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel23 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel45 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel46 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailGalliumTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel47 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailRehniumTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel48 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailOsmiumTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel49 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailManganeseTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel50 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailTungustanTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel51 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailRutheniumTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel52 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailTitaniumTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel53 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailIndiumTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel54 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailNickelTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel55 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailBismuthTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel56 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailChromiumTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel57 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailLeadTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel58 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailTinTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel59 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailIronTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel60 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailAntimonyTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel61 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailPalladiumTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel62 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailPlatinumTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel63 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailCobaltTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.reportDetailRhodiumTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel64 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailIridiumTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel65 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailCadmiumTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel66 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailZincTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel67 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailCopperTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel68 = new Bunifu.UI.WinForms.BunifuLabel();
            this.reportDetailSilverTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.reportDataTableGirdView = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.Column21 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column77 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column74 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column67 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column68 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column69 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column70 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column71 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column72 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column73 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sampleTextBox = new System.Windows.Forms.ComboBox();
            this.customerTextBox = new System.Windows.Forms.ComboBox();
            this.currentTimebox = new System.Windows.Forms.DateTimePicker();
            this.bunifuLabel14 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel11 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel13 = new Bunifu.UI.WinForms.BunifuLabel();
            this.customerAddressTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel12 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel10 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel9 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel8 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel7 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel6 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel4 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel3 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel2 = new Bunifu.UI.WinForms.BunifuLabel();
            this.seriesNoTexBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel1 = new Bunifu.UI.WinForms.BunifuLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.bunifuLabel18 = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.customerMobileTextBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuMetroTextbox4 = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuButton13 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.startButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.startStopTimerButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.goldFineTextBox = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.customerRate = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tounchWeightTextBox = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.originalWeightTextBox = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.goldKaratTextBox = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.goldPurityTextBox = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.reportImageArea = new System.Windows.Forms.PictureBox();
            this.reportDatePicker = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.reportSelectButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.reportScanImageButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.adddetailButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.reportPrintButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.reportSaveButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.reportModifyButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.reportNewButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.reportPage = new System.Windows.Forms.TabPage();
            this.rowCountTotal = new System.Windows.Forms.Label();
            this.totalrow = new System.Windows.Forms.Label();
            this.allReportDataGrid = new ADGV.AdvancedDataGridView();
            this.Column22 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column78 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bunifuLabel30 = new Bunifu.UI.WinForms.BunifuLabel();
            this.fullfilterMobileNo = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel29 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel28 = new Bunifu.UI.WinForms.BunifuLabel();
            this.fullcustomerReportBox = new System.Windows.Forms.ComboBox();
            this.bunifuLabel27 = new Bunifu.UI.WinForms.BunifuLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.bunifuButton29 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton30 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton7 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.fullReportEndDatePicker = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.fullReportStartDatepicker = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.tokenPage = new System.Windows.Forms.TabPage();
            this.tokecustcomboBox2 = new System.Windows.Forms.ComboBox();
            this.tokenSeries = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel44 = new Bunifu.UI.WinForms.BunifuLabel();
            this.tokenListDataGrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column76 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bunifuLabel40 = new Bunifu.UI.WinForms.BunifuLabel();
            this.tokenSampleDataGrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label10 = new System.Windows.Forms.Label();
            this.tokenSample = new System.Windows.Forms.ComboBox();
            this.tokenDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.bunifuLabel31 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel32 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel33 = new Bunifu.UI.WinForms.BunifuLabel();
            this.tokenAddress = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel34 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel35 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel36 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel37 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel38 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel39 = new Bunifu.UI.WinForms.BunifuLabel();
            this.tokenSeriesno = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.tokenMobile = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.label6 = new System.Windows.Forms.Label();
            this.tokenTotal = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tokenAddButton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton16 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton18 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.tokenSampleDeletebutton = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.tokenRate = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tokenWeightText = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tokenDatePicker = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.testPage = new System.Windows.Forms.TabPage();
            this.testingDropdown = new Bunifu.UI.WinForms.BunifuDropdown();
            this.bunifuLabel89 = new Bunifu.UI.WinForms.BunifuLabel();
            this.testDatagridView = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column79 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bunifuLabel41 = new Bunifu.UI.WinForms.BunifuLabel();
            this.testId = new Bunifu.UI.WinForms.BunifuLabel();
            this.testSmaplecomboBox = new System.Windows.Forms.ComboBox();
            this.testCustcombo = new System.Windows.Forms.ComboBox();
            this.testTime = new System.Windows.Forms.DateTimePicker();
            this.bunifuLabel43 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel70 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel71 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel72 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel73 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel74 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel75 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel77 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel78 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel79 = new Bunifu.UI.WinForms.BunifuLabel();
            this.testSeries = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel80 = new Bunifu.UI.WinForms.BunifuLabel();
            this.label11 = new System.Windows.Forms.Label();
            this.testgoldFine = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.testAmount = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.testtounchweight = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.testorgweight = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.testKarat = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.testPurity = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.testDate = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.bunifuButton21 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton22 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton23 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton24 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.expenses = new System.Windows.Forms.TabPage();
            this.bunifuLabel86 = new Bunifu.UI.WinForms.BunifuLabel();
            this.expenseId = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.expenseTime = new System.Windows.Forms.DateTimePicker();
            this.bunifuLabel81 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel84 = new Bunifu.UI.WinForms.BunifuLabel();
            this.label7 = new System.Windows.Forms.Label();
            this.expensesDataGridView = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bunifuLabel82 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel85 = new Bunifu.UI.WinForms.BunifuLabel();
            this.expenseRemark = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.expenseDate = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.expensesAmount = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.bunifuButton20 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton25 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton26 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton27 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.backup = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.bunifuPictureBox1 = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bunifuButton28 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton19 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.backUppathtext = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.label12 = new System.Windows.Forms.Label();
            this.sendReport = new System.Windows.Forms.TabPage();
            this.sendMobile = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel91 = new Bunifu.UI.WinForms.BunifuLabel();
            this.label17 = new System.Windows.Forms.Label();
            this.bunifuLabel100 = new Bunifu.UI.WinForms.BunifuLabel();
            this.selectDate = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.sendGoldPurity = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.sendreportDataTable = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column202 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendSample = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel104 = new Bunifu.UI.WinForms.BunifuLabel();
            this.sendCustomer = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel90 = new Bunifu.UI.WinForms.BunifuLabel();
            this.sendSerial = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel92 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel94 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel96 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel97 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel98 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel99 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel101 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel102 = new Bunifu.UI.WinForms.BunifuLabel();
            this.sendseries = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuLabel103 = new Bunifu.UI.WinForms.BunifuLabel();
            this.label16 = new System.Windows.Forms.Label();
            this.sendFine = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.sendAmount = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.sendTweight = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.sendOweight = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.sendKarat = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.sendDate = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.bunifuButton17 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton31 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton33 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuFormDock1 = new Bunifu.UI.WinForms.BunifuFormDock();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.bunifuButton6 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton5 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuShadowPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.indicator)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox2)).BeginInit();
            this.pagesForm.SuspendLayout();
            this.dashbaordPage.SuspendLayout();
            this.bunifuCards11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.bunifuCards12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.bunifuCards8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.bunifuCards7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.bunifuCards6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.bunifuCards5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.bunifuCards4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.bunifuCards3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.bunifuCards2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.bunifuCards1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.customerMasterPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customerDataGridView)).BeginInit();
            this.sellItemPage.SuspendLayout();
            this.panelAddDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataTableGirdView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportImageArea)).BeginInit();
            this.reportPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allReportDataGrid)).BeginInit();
            this.tokenPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tokenListDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tokenSampleDataGrid)).BeginInit();
            this.testPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testDatagridView)).BeginInit();
            this.expenses.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expensesDataGridView)).BeginInit();
            this.backup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox1)).BeginInit();
            this.sendReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sendreportDataTable)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuShadowPanel1
            // 
            this.bunifuShadowPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel1.BorderColor = System.Drawing.Color.Transparent;
            this.bunifuShadowPanel1.Controls.Add(this.panel1);
            this.bunifuShadowPanel1.Location = new System.Drawing.Point(12, 7);
            this.bunifuShadowPanel1.Name = "bunifuShadowPanel1";
            this.bunifuShadowPanel1.PanelColor = System.Drawing.Color.Empty;
            this.bunifuShadowPanel1.ShadowDept = 2;
            this.bunifuShadowPanel1.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel1.Size = new System.Drawing.Size(271, 783);
            this.bunifuShadowPanel1.TabIndex = 0;
            this.bunifuShadowPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.bunifuShadowPanel1_Paint);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.Controls.Add(this.indicator);
            this.panel1.Controls.Add(this.bunifuButton8);
            this.panel1.Controls.Add(this.bunifuButton15);
            this.panel1.Controls.Add(this.bunifuButton14);
            this.panel1.Controls.Add(this.bunifuButton12);
            this.panel1.Controls.Add(this.bunifuButton11);
            this.panel1.Controls.Add(this.bunifuButton4);
            this.panel1.Controls.Add(this.bunifuButton3);
            this.panel1.Controls.Add(this.bunifuButton2);
            this.panel1.Controls.Add(this.bunifuButton1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(268, 783);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // indicator
            // 
            this.indicator.BackColor = System.Drawing.Color.Goldenrod;
            this.indicator.Location = new System.Drawing.Point(3, 184);
            this.indicator.Name = "indicator";
            this.indicator.Size = new System.Drawing.Size(10, 45);
            this.indicator.TabIndex = 2;
            this.indicator.TabStop = false;
            this.indicator.Click += new System.EventHandler(this.indicator_Click);
            // 
            // bunifuButton8
            // 
            this.bunifuButton8.AllowToggling = true;
            this.bunifuButton8.AnimationSpeed = 200;
            this.bunifuButton8.AutoGenerateColors = false;
            this.bunifuButton8.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton8.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton8.BackgroundImage")));
            this.bunifuButton8.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton8.ButtonText = "Backup";
            this.bunifuButton8.ButtonTextMarginLeft = 0;
            this.bunifuButton8.ColorContrastOnClick = 45;
            this.bunifuButton8.ColorContrastOnHover = 45;
            this.bunifuButton8.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges1.BottomLeft = true;
            borderEdges1.BottomRight = true;
            borderEdges1.TopLeft = true;
            borderEdges1.TopRight = true;
            this.bunifuButton8.CustomizableEdges = borderEdges1;
            this.bunifuButton8.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton8.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton8.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton8.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton8.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton8.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuButton8.ForeColor = System.Drawing.Color.White;
            this.bunifuButton8.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton8.IconMarginLeft = 32;
            this.bunifuButton8.IconPadding = 11;
            this.bunifuButton8.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton8.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton8.IdleBorderRadius = 3;
            this.bunifuButton8.IdleBorderThickness = 1;
            this.bunifuButton8.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton8.IdleIconLeftImage = global::GoldRaven.Properties.Resources.icons8_database_view_125px_1;
            this.bunifuButton8.IdleIconRightImage = null;
            this.bunifuButton8.IndicateFocus = true;
            this.bunifuButton8.Location = new System.Drawing.Point(0, 544);
            this.bunifuButton8.Name = "bunifuButton8";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties1.BorderRadius = 3;
            stateProperties1.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties1.BorderThickness = 1;
            stateProperties1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties1.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties1.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_database_view_125px;
            stateProperties1.IconRightImage = null;
            this.bunifuButton8.onHoverState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties2.BorderRadius = 3;
            stateProperties2.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties2.BorderThickness = 1;
            stateProperties2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties2.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties2.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_database_view_125px;
            stateProperties2.IconRightImage = null;
            this.bunifuButton8.OnPressedState = stateProperties2;
            this.bunifuButton8.Size = new System.Drawing.Size(268, 45);
            this.bunifuButton8.TabIndex = 3;
            this.bunifuButton8.TabStop = false;
            this.bunifuButton8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton8.TextMarginLeft = 0;
            this.bunifuButton8.UseDefaultRadiusAndThickness = true;
            this.bunifuButton8.Click += new System.EventHandler(this.bunifuButton8_Click_6);
            // 
            // bunifuButton15
            // 
            this.bunifuButton15.AllowToggling = true;
            this.bunifuButton15.AnimationSpeed = 200;
            this.bunifuButton15.AutoGenerateColors = false;
            this.bunifuButton15.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton15.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton15.BackgroundImage")));
            this.bunifuButton15.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton15.ButtonText = "Send Report";
            this.bunifuButton15.ButtonTextMarginLeft = 15;
            this.bunifuButton15.ColorContrastOnClick = 45;
            this.bunifuButton15.ColorContrastOnHover = 45;
            this.bunifuButton15.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges2.BottomLeft = true;
            borderEdges2.BottomRight = true;
            borderEdges2.TopLeft = true;
            borderEdges2.TopRight = true;
            this.bunifuButton15.CustomizableEdges = borderEdges2;
            this.bunifuButton15.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton15.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton15.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton15.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton15.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton15.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuButton15.ForeColor = System.Drawing.Color.White;
            this.bunifuButton15.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton15.IconMarginLeft = 32;
            this.bunifuButton15.IconPadding = 11;
            this.bunifuButton15.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton15.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton15.IdleBorderRadius = 3;
            this.bunifuButton15.IdleBorderThickness = 1;
            this.bunifuButton15.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton15.IdleIconLeftImage = global::GoldRaven.Properties.Resources.icons8_whatsapp_96px;
            this.bunifuButton15.IdleIconRightImage = null;
            this.bunifuButton15.IndicateFocus = true;
            this.bunifuButton15.Location = new System.Drawing.Point(0, 499);
            this.bunifuButton15.Name = "bunifuButton15";
            stateProperties3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties3.BorderRadius = 3;
            stateProperties3.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties3.BorderThickness = 1;
            stateProperties3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties3.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties3.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_whatsapp_96px_1;
            stateProperties3.IconRightImage = null;
            this.bunifuButton15.onHoverState = stateProperties3;
            stateProperties4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties4.BorderRadius = 3;
            stateProperties4.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties4.BorderThickness = 1;
            stateProperties4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties4.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties4.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_whatsapp_96px_1;
            stateProperties4.IconRightImage = null;
            this.bunifuButton15.OnPressedState = stateProperties4;
            this.bunifuButton15.Size = new System.Drawing.Size(268, 45);
            this.bunifuButton15.TabIndex = 0;
            this.bunifuButton15.TabStop = false;
            this.bunifuButton15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton15.TextMarginLeft = 15;
            this.bunifuButton15.UseDefaultRadiusAndThickness = true;
            this.bunifuButton15.Click += new System.EventHandler(this.bunifuButton15_Click);
            // 
            // bunifuButton14
            // 
            this.bunifuButton14.AllowToggling = true;
            this.bunifuButton14.AnimationSpeed = 200;
            this.bunifuButton14.AutoGenerateColors = false;
            this.bunifuButton14.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton14.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton14.BackgroundImage")));
            this.bunifuButton14.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton14.ButtonText = "Expenses";
            this.bunifuButton14.ButtonTextMarginLeft = 0;
            this.bunifuButton14.ColorContrastOnClick = 45;
            this.bunifuButton14.ColorContrastOnHover = 45;
            this.bunifuButton14.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges3.BottomLeft = true;
            borderEdges3.BottomRight = true;
            borderEdges3.TopLeft = true;
            borderEdges3.TopRight = true;
            this.bunifuButton14.CustomizableEdges = borderEdges3;
            this.bunifuButton14.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton14.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton14.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton14.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton14.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton14.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuButton14.ForeColor = System.Drawing.Color.White;
            this.bunifuButton14.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton14.IconMarginLeft = 32;
            this.bunifuButton14.IconPadding = 11;
            this.bunifuButton14.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton14.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton14.IdleBorderRadius = 3;
            this.bunifuButton14.IdleBorderThickness = 1;
            this.bunifuButton14.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton14.IdleIconLeftImage = global::GoldRaven.Properties.Resources.icons8_reserve_125px;
            this.bunifuButton14.IdleIconRightImage = null;
            this.bunifuButton14.IndicateFocus = true;
            this.bunifuButton14.Location = new System.Drawing.Point(0, 454);
            this.bunifuButton14.Name = "bunifuButton14";
            stateProperties5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties5.BorderRadius = 3;
            stateProperties5.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties5.BorderThickness = 1;
            stateProperties5.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties5.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties5.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_reserve_125px_1;
            stateProperties5.IconRightImage = null;
            this.bunifuButton14.onHoverState = stateProperties5;
            stateProperties6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties6.BorderRadius = 3;
            stateProperties6.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties6.BorderThickness = 1;
            stateProperties6.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties6.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties6.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_reserve_125px_1;
            stateProperties6.IconRightImage = null;
            this.bunifuButton14.OnPressedState = stateProperties6;
            this.bunifuButton14.Size = new System.Drawing.Size(268, 45);
            this.bunifuButton14.TabIndex = 0;
            this.bunifuButton14.TabStop = false;
            this.bunifuButton14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton14.TextMarginLeft = 0;
            this.bunifuButton14.UseDefaultRadiusAndThickness = true;
            this.bunifuButton14.Click += new System.EventHandler(this.bunifuButton14_Click_1);
            // 
            // bunifuButton12
            // 
            this.bunifuButton12.AllowToggling = true;
            this.bunifuButton12.AnimationSpeed = 200;
            this.bunifuButton12.AutoGenerateColors = false;
            this.bunifuButton12.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton12.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton12.BackgroundImage")));
            this.bunifuButton12.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton12.ButtonText = "Testing";
            this.bunifuButton12.ButtonTextMarginLeft = 0;
            this.bunifuButton12.ColorContrastOnClick = 45;
            this.bunifuButton12.ColorContrastOnHover = 45;
            this.bunifuButton12.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges4.BottomLeft = true;
            borderEdges4.BottomRight = true;
            borderEdges4.TopLeft = true;
            borderEdges4.TopRight = true;
            this.bunifuButton12.CustomizableEdges = borderEdges4;
            this.bunifuButton12.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton12.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton12.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton12.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton12.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton12.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuButton12.ForeColor = System.Drawing.Color.White;
            this.bunifuButton12.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton12.IconMarginLeft = 32;
            this.bunifuButton12.IconPadding = 11;
            this.bunifuButton12.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton12.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton12.IdleBorderRadius = 3;
            this.bunifuButton12.IdleBorderThickness = 1;
            this.bunifuButton12.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton12.IdleIconLeftImage = global::GoldRaven.Properties.Resources.icons8_test_passed_96px;
            this.bunifuButton12.IdleIconRightImage = null;
            this.bunifuButton12.IndicateFocus = true;
            this.bunifuButton12.Location = new System.Drawing.Point(0, 409);
            this.bunifuButton12.Name = "bunifuButton12";
            stateProperties7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties7.BorderRadius = 3;
            stateProperties7.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties7.BorderThickness = 1;
            stateProperties7.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties7.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties7.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_test_passed_96px_1;
            stateProperties7.IconRightImage = null;
            this.bunifuButton12.onHoverState = stateProperties7;
            stateProperties8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties8.BorderRadius = 3;
            stateProperties8.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties8.BorderThickness = 1;
            stateProperties8.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties8.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties8.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_test_passed_96px_1;
            stateProperties8.IconRightImage = null;
            this.bunifuButton12.OnPressedState = stateProperties8;
            this.bunifuButton12.Size = new System.Drawing.Size(268, 45);
            this.bunifuButton12.TabIndex = 0;
            this.bunifuButton12.TabStop = false;
            this.bunifuButton12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton12.TextMarginLeft = 0;
            this.bunifuButton12.UseDefaultRadiusAndThickness = true;
            this.bunifuButton12.Click += new System.EventHandler(this.bunifuButton12_Click_1);
            // 
            // bunifuButton11
            // 
            this.bunifuButton11.AllowToggling = true;
            this.bunifuButton11.AnimationSpeed = 200;
            this.bunifuButton11.AutoGenerateColors = false;
            this.bunifuButton11.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton11.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton11.BackgroundImage")));
            this.bunifuButton11.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton11.ButtonText = "Token";
            this.bunifuButton11.ButtonTextMarginLeft = 0;
            this.bunifuButton11.ColorContrastOnClick = 45;
            this.bunifuButton11.ColorContrastOnHover = 45;
            this.bunifuButton11.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges5.BottomLeft = true;
            borderEdges5.BottomRight = true;
            borderEdges5.TopLeft = true;
            borderEdges5.TopRight = true;
            this.bunifuButton11.CustomizableEdges = borderEdges5;
            this.bunifuButton11.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton11.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton11.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton11.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton11.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton11.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuButton11.ForeColor = System.Drawing.Color.White;
            this.bunifuButton11.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton11.IconMarginLeft = 32;
            this.bunifuButton11.IconPadding = 11;
            this.bunifuButton11.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton11.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton11.IdleBorderRadius = 3;
            this.bunifuButton11.IdleBorderThickness = 1;
            this.bunifuButton11.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton11.IdleIconLeftImage = global::GoldRaven.Properties.Resources.icons8_ticket_96px;
            this.bunifuButton11.IdleIconRightImage = null;
            this.bunifuButton11.IndicateFocus = true;
            this.bunifuButton11.Location = new System.Drawing.Point(0, 364);
            this.bunifuButton11.Name = "bunifuButton11";
            stateProperties9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties9.BorderRadius = 3;
            stateProperties9.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties9.BorderThickness = 1;
            stateProperties9.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties9.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties9.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_ticket_96px_1;
            stateProperties9.IconRightImage = null;
            this.bunifuButton11.onHoverState = stateProperties9;
            stateProperties10.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties10.BorderRadius = 3;
            stateProperties10.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties10.BorderThickness = 1;
            stateProperties10.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties10.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties10.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_ticket_96px_1;
            stateProperties10.IconRightImage = null;
            this.bunifuButton11.OnPressedState = stateProperties10;
            this.bunifuButton11.Size = new System.Drawing.Size(268, 45);
            this.bunifuButton11.TabIndex = 0;
            this.bunifuButton11.TabStop = false;
            this.bunifuButton11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton11.TextMarginLeft = 0;
            this.bunifuButton11.UseDefaultRadiusAndThickness = true;
            this.bunifuButton11.Click += new System.EventHandler(this.bunifuButton11_Click_2);
            // 
            // bunifuButton4
            // 
            this.bunifuButton4.AllowToggling = true;
            this.bunifuButton4.AnimationSpeed = 200;
            this.bunifuButton4.AutoGenerateColors = false;
            this.bunifuButton4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton4.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton4.BackgroundImage")));
            this.bunifuButton4.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton4.ButtonText = "All Report";
            this.bunifuButton4.ButtonTextMarginLeft = 0;
            this.bunifuButton4.ColorContrastOnClick = 45;
            this.bunifuButton4.ColorContrastOnHover = 45;
            this.bunifuButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges6.BottomLeft = true;
            borderEdges6.BottomRight = true;
            borderEdges6.TopLeft = true;
            borderEdges6.TopRight = true;
            this.bunifuButton4.CustomizableEdges = borderEdges6;
            this.bunifuButton4.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton4.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton4.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton4.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton4.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton4.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuButton4.ForeColor = System.Drawing.Color.White;
            this.bunifuButton4.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton4.IconMarginLeft = 32;
            this.bunifuButton4.IconPadding = 11;
            this.bunifuButton4.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton4.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton4.IdleBorderRadius = 3;
            this.bunifuButton4.IdleBorderThickness = 1;
            this.bunifuButton4.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton4.IdleIconLeftImage = global::GoldRaven.Properties.Resources.icons8_diploma_96px;
            this.bunifuButton4.IdleIconRightImage = null;
            this.bunifuButton4.IndicateFocus = true;
            this.bunifuButton4.Location = new System.Drawing.Point(0, 319);
            this.bunifuButton4.Name = "bunifuButton4";
            stateProperties11.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties11.BorderRadius = 3;
            stateProperties11.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties11.BorderThickness = 1;
            stateProperties11.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties11.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties11.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_diploma_96px_1;
            stateProperties11.IconRightImage = null;
            this.bunifuButton4.onHoverState = stateProperties11;
            stateProperties12.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties12.BorderRadius = 3;
            stateProperties12.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties12.BorderThickness = 1;
            stateProperties12.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties12.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties12.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_diploma_96px_1;
            stateProperties12.IconRightImage = null;
            this.bunifuButton4.OnPressedState = stateProperties12;
            this.bunifuButton4.Size = new System.Drawing.Size(268, 45);
            this.bunifuButton4.TabIndex = 0;
            this.bunifuButton4.TabStop = false;
            this.bunifuButton4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton4.TextMarginLeft = 0;
            this.bunifuButton4.UseDefaultRadiusAndThickness = true;
            this.bunifuButton4.Click += new System.EventHandler(this.bunifuButton4_Click);
            // 
            // bunifuButton3
            // 
            this.bunifuButton3.AllowToggling = true;
            this.bunifuButton3.AnimationSpeed = 200;
            this.bunifuButton3.AutoGenerateColors = false;
            this.bunifuButton3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton3.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton3.BackgroundImage")));
            this.bunifuButton3.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton3.ButtonText = "Create Report";
            this.bunifuButton3.ButtonTextMarginLeft = 16;
            this.bunifuButton3.ColorContrastOnClick = 45;
            this.bunifuButton3.ColorContrastOnHover = 45;
            this.bunifuButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges7.BottomLeft = true;
            borderEdges7.BottomRight = true;
            borderEdges7.TopLeft = true;
            borderEdges7.TopRight = true;
            this.bunifuButton3.CustomizableEdges = borderEdges7;
            this.bunifuButton3.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton3.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton3.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton3.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton3.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton3.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuButton3.ForeColor = System.Drawing.Color.White;
            this.bunifuButton3.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton3.IconMarginLeft = 32;
            this.bunifuButton3.IconPadding = 11;
            this.bunifuButton3.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton3.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton3.IdleBorderRadius = 3;
            this.bunifuButton3.IdleBorderThickness = 1;
            this.bunifuButton3.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton3.IdleIconLeftImage = global::GoldRaven.Properties.Resources.icons8_create_96px_1;
            this.bunifuButton3.IdleIconRightImage = null;
            this.bunifuButton3.IndicateFocus = true;
            this.bunifuButton3.Location = new System.Drawing.Point(0, 274);
            this.bunifuButton3.Name = "bunifuButton3";
            stateProperties13.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties13.BorderRadius = 3;
            stateProperties13.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties13.BorderThickness = 1;
            stateProperties13.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties13.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties13.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_create_96px;
            stateProperties13.IconRightImage = null;
            this.bunifuButton3.onHoverState = stateProperties13;
            stateProperties14.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties14.BorderRadius = 3;
            stateProperties14.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties14.BorderThickness = 1;
            stateProperties14.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties14.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties14.IconLeftImage = global::GoldRaven.Properties.Resources.icons8_create_96px;
            stateProperties14.IconRightImage = null;
            this.bunifuButton3.OnPressedState = stateProperties14;
            this.bunifuButton3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuButton3.Size = new System.Drawing.Size(268, 45);
            this.bunifuButton3.TabIndex = 0;
            this.bunifuButton3.TabStop = false;
            this.bunifuButton3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton3.TextMarginLeft = 16;
            this.bunifuButton3.UseDefaultRadiusAndThickness = true;
            this.bunifuButton3.Click += new System.EventHandler(this.bunifuButton3_Click);
            // 
            // bunifuButton2
            // 
            this.bunifuButton2.AllowToggling = true;
            this.bunifuButton2.AnimationSpeed = 200;
            this.bunifuButton2.AutoGenerateColors = false;
            this.bunifuButton2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton2.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton2.BackgroundImage")));
            this.bunifuButton2.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton2.ButtonText = "Customer Master";
            this.bunifuButton2.ButtonTextMarginLeft = 20;
            this.bunifuButton2.ColorContrastOnClick = 45;
            this.bunifuButton2.ColorContrastOnHover = 45;
            this.bunifuButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges8.BottomLeft = true;
            borderEdges8.BottomRight = true;
            borderEdges8.TopLeft = true;
            borderEdges8.TopRight = true;
            this.bunifuButton2.CustomizableEdges = borderEdges8;
            this.bunifuButton2.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton2.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton2.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton2.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton2.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuButton2.ForeColor = System.Drawing.Color.White;
            this.bunifuButton2.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton2.IconMarginLeft = 32;
            this.bunifuButton2.IconPadding = 11;
            this.bunifuButton2.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton2.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton2.IdleBorderRadius = 3;
            this.bunifuButton2.IdleBorderThickness = 1;
            this.bunifuButton2.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton2.IdleIconLeftImage = global::GoldRaven.Properties.Resources._1564534_32;
            this.bunifuButton2.IdleIconRightImage = null;
            this.bunifuButton2.IndicateFocus = true;
            this.bunifuButton2.Location = new System.Drawing.Point(0, 229);
            this.bunifuButton2.Name = "bunifuButton2";
            stateProperties15.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties15.BorderRadius = 3;
            stateProperties15.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties15.BorderThickness = 1;
            stateProperties15.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties15.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties15.IconLeftImage = global::GoldRaven.Properties.Resources._1564534_32__1_;
            stateProperties15.IconRightImage = null;
            this.bunifuButton2.onHoverState = stateProperties15;
            stateProperties16.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties16.BorderRadius = 3;
            stateProperties16.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties16.BorderThickness = 1;
            stateProperties16.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties16.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties16.IconLeftImage = global::GoldRaven.Properties.Resources._1564534_32__1_;
            stateProperties16.IconRightImage = null;
            this.bunifuButton2.OnPressedState = stateProperties16;
            this.bunifuButton2.Size = new System.Drawing.Size(268, 45);
            this.bunifuButton2.TabIndex = 0;
            this.bunifuButton2.TabStop = false;
            this.bunifuButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton2.TextMarginLeft = 20;
            this.bunifuButton2.UseDefaultRadiusAndThickness = true;
            this.bunifuButton2.Click += new System.EventHandler(this.bunifuButton2_Click);
            // 
            // bunifuButton1
            // 
            this.bunifuButton1.AllowToggling = true;
            this.bunifuButton1.AnimationSpeed = 200;
            this.bunifuButton1.AutoGenerateColors = false;
            this.bunifuButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton1.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton1.BackgroundImage")));
            this.bunifuButton1.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton1.ButtonText = "Dashboard";
            this.bunifuButton1.ButtonTextMarginLeft = 0;
            this.bunifuButton1.ColorContrastOnClick = 45;
            this.bunifuButton1.ColorContrastOnHover = 45;
            this.bunifuButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges9.BottomLeft = true;
            borderEdges9.BottomRight = true;
            borderEdges9.TopLeft = true;
            borderEdges9.TopRight = true;
            this.bunifuButton1.CustomizableEdges = borderEdges9;
            this.bunifuButton1.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton1.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.bunifuButton1.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton1.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuButton1.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuButton1.ForeColor = System.Drawing.Color.White;
            this.bunifuButton1.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton1.IconMarginLeft = 32;
            this.bunifuButton1.IconPadding = 11;
            this.bunifuButton1.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton1.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton1.IdleBorderRadius = 3;
            this.bunifuButton1.IdleBorderThickness = 1;
            this.bunifuButton1.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.bunifuButton1.IdleIconLeftImage = global::GoldRaven.Properties.Resources._3669363_32__1_;
            this.bunifuButton1.IdleIconRightImage = null;
            this.bunifuButton1.IndicateFocus = true;
            this.bunifuButton1.Location = new System.Drawing.Point(0, 184);
            this.bunifuButton1.Name = "bunifuButton1";
            stateProperties17.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties17.BorderRadius = 3;
            stateProperties17.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties17.BorderThickness = 1;
            stateProperties17.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties17.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties17.IconLeftImage = global::GoldRaven.Properties.Resources._3669363_32__2_;
            stateProperties17.IconRightImage = null;
            this.bunifuButton1.onHoverState = stateProperties17;
            stateProperties18.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties18.BorderRadius = 3;
            stateProperties18.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties18.BorderThickness = 1;
            stateProperties18.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            stateProperties18.ForeColor = System.Drawing.Color.Goldenrod;
            stateProperties18.IconLeftImage = global::GoldRaven.Properties.Resources._3669363_32__2_;
            stateProperties18.IconRightImage = null;
            this.bunifuButton1.OnPressedState = stateProperties18;
            this.bunifuButton1.Size = new System.Drawing.Size(268, 45);
            this.bunifuButton1.TabIndex = 0;
            this.bunifuButton1.TabStop = false;
            this.bunifuButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton1.TextMarginLeft = 0;
            this.bunifuButton1.UseDefaultRadiusAndThickness = true;
            this.bunifuButton1.ForeColorChanged += new System.EventHandler(this.bunifuButton1_ForeColorChanged);
            this.bunifuButton1.Click += new System.EventHandler(this.bunifuButton1_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.bunifuPictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(268, 184);
            this.panel2.TabIndex = 0;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // bunifuPictureBox2
            // 
            this.bunifuPictureBox2.AllowFocused = false;
            this.bunifuPictureBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuPictureBox2.BorderRadius = 12;
            this.bunifuPictureBox2.ErrorImage = null;
            this.bunifuPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuPictureBox2.Image")));
            this.bunifuPictureBox2.InitialImage = null;
            this.bunifuPictureBox2.IsCircle = false;
            this.bunifuPictureBox2.Location = new System.Drawing.Point(61, 15);
            this.bunifuPictureBox2.Margin = new System.Windows.Forms.Padding(0);
            this.bunifuPictureBox2.Name = "bunifuPictureBox2";
            this.bunifuPictureBox2.Size = new System.Drawing.Size(155, 155);
            this.bunifuPictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuPictureBox2.TabIndex = 28;
            this.bunifuPictureBox2.TabStop = false;
            this.bunifuPictureBox2.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Square;
            // 
            // pagesForm
            // 
            this.pagesForm.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.pagesForm.AllowTransitions = true;
            this.pagesForm.Controls.Add(this.dashbaordPage);
            this.pagesForm.Controls.Add(this.customerMasterPage);
            this.pagesForm.Controls.Add(this.sellItemPage);
            this.pagesForm.Controls.Add(this.reportPage);
            this.pagesForm.Controls.Add(this.tokenPage);
            this.pagesForm.Controls.Add(this.testPage);
            this.pagesForm.Controls.Add(this.expenses);
            this.pagesForm.Controls.Add(this.backup);
            this.pagesForm.Controls.Add(this.sendReport);
            this.pagesForm.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pagesForm.Location = new System.Drawing.Point(289, 33);
            this.pagesForm.Multiline = true;
            this.pagesForm.Name = "pagesForm";
            this.pagesForm.Page = this.backup;
            this.pagesForm.PageIndex = 7;
            this.pagesForm.PageName = "backup";
            this.pagesForm.PageTitle = "BackUp";
            this.pagesForm.SelectedIndex = 0;
            this.pagesForm.Size = new System.Drawing.Size(1049, 757);
            this.pagesForm.TabIndex = 1;
            animation1.AnimateOnlyDifferences = false;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.pagesForm.Transition = animation1;
            this.pagesForm.TransitionType = Utilities.BunifuPages.BunifuAnimatorNS.AnimationType.Custom;
            this.pagesForm.SelectedIndexChanged += new System.EventHandler(this.pagesForm_SelectedIndexChanged);
            // 
            // dashbaordPage
            // 
            this.dashbaordPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.dashbaordPage.Controls.Add(this.bunifuLabel42);
            this.dashbaordPage.Controls.Add(this.bunifuLabel76);
            this.dashbaordPage.Controls.Add(this.dashboardEndDatePicker);
            this.dashbaordPage.Controls.Add(this.dashboardStartDatePicker);
            this.dashbaordPage.Controls.Add(this.bunifuCards11);
            this.dashbaordPage.Controls.Add(this.bunifuCards12);
            this.dashbaordPage.Controls.Add(this.bunifuCards8);
            this.dashbaordPage.Controls.Add(this.bunifuCards7);
            this.dashbaordPage.Controls.Add(this.bunifuCards6);
            this.dashbaordPage.Controls.Add(this.getDashboardDataButton);
            this.dashbaordPage.Controls.Add(this.bunifuCards5);
            this.dashbaordPage.Controls.Add(this.bunifuCards4);
            this.dashbaordPage.Controls.Add(this.bunifuCards3);
            this.dashbaordPage.Controls.Add(this.bunifuCards2);
            this.dashbaordPage.Controls.Add(this.bunifuCards1);
            this.dashbaordPage.Controls.Add(this.label1);
            this.dashbaordPage.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.dashbaordPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dashbaordPage.Location = new System.Drawing.Point(4, 4);
            this.dashbaordPage.Name = "dashbaordPage";
            this.dashbaordPage.Padding = new System.Windows.Forms.Padding(3);
            this.dashbaordPage.Size = new System.Drawing.Size(1041, 728);
            this.dashbaordPage.TabIndex = 0;
            this.dashbaordPage.Text = "Dashboard";
            this.dashbaordPage.Click += new System.EventHandler(this.dashbaordPage_Click);
            // 
            // bunifuLabel42
            // 
            this.bunifuLabel42.AutoEllipsis = false;
            this.bunifuLabel42.CursorType = null;
            this.bunifuLabel42.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel42.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel42.Location = new System.Drawing.Point(549, 25);
            this.bunifuLabel42.Name = "bunifuLabel42";
            this.bunifuLabel42.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel42.Size = new System.Drawing.Size(65, 23);
            this.bunifuLabel42.TabIndex = 166;
            this.bunifuLabel42.TabStop = false;
            this.bunifuLabel42.Text = "End Date";
            this.bunifuLabel42.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel42.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel76
            // 
            this.bunifuLabel76.AutoEllipsis = false;
            this.bunifuLabel76.CursorType = null;
            this.bunifuLabel76.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel76.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel76.Location = new System.Drawing.Point(288, 25);
            this.bunifuLabel76.Name = "bunifuLabel76";
            this.bunifuLabel76.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel76.Size = new System.Drawing.Size(72, 23);
            this.bunifuLabel76.TabIndex = 164;
            this.bunifuLabel76.TabStop = false;
            this.bunifuLabel76.Text = "Start Date";
            this.bunifuLabel76.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel76.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // dashboardEndDatePicker
            // 
            this.dashboardEndDatePicker.BackColor = System.Drawing.Color.Transparent;
            this.dashboardEndDatePicker.BorderRadius = 1;
            this.dashboardEndDatePicker.Color = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.dashboardEndDatePicker.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.dashboardEndDatePicker.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Right;
            this.dashboardEndDatePicker.DisabledColor = System.Drawing.Color.Gray;
            this.dashboardEndDatePicker.DisplayWeekNumbers = false;
            this.dashboardEndDatePicker.DPHeight = 0;
            this.dashboardEndDatePicker.FillDatePicker = false;
            this.dashboardEndDatePicker.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.dashboardEndDatePicker.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.dashboardEndDatePicker.Icon = ((System.Drawing.Image)(resources.GetObject("dashboardEndDatePicker.Icon")));
            this.dashboardEndDatePicker.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.dashboardEndDatePicker.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Left;
            this.dashboardEndDatePicker.Location = new System.Drawing.Point(548, 54);
            this.dashboardEndDatePicker.MinimumSize = new System.Drawing.Size(217, 33);
            this.dashboardEndDatePicker.Name = "dashboardEndDatePicker";
            this.dashboardEndDatePicker.Size = new System.Drawing.Size(217, 33);
            this.dashboardEndDatePicker.TabIndex = 2;
            // 
            // dashboardStartDatePicker
            // 
            this.dashboardStartDatePicker.BackColor = System.Drawing.Color.Transparent;
            this.dashboardStartDatePicker.BorderRadius = 1;
            this.dashboardStartDatePicker.Color = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.dashboardStartDatePicker.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.dashboardStartDatePicker.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Right;
            this.dashboardStartDatePicker.DisabledColor = System.Drawing.Color.Gray;
            this.dashboardStartDatePicker.DisplayWeekNumbers = false;
            this.dashboardStartDatePicker.DPHeight = 0;
            this.dashboardStartDatePicker.FillDatePicker = false;
            this.dashboardStartDatePicker.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.dashboardStartDatePicker.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.dashboardStartDatePicker.Icon = ((System.Drawing.Image)(resources.GetObject("dashboardStartDatePicker.Icon")));
            this.dashboardStartDatePicker.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.dashboardStartDatePicker.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Left;
            this.dashboardStartDatePicker.Location = new System.Drawing.Point(288, 54);
            this.dashboardStartDatePicker.MinimumSize = new System.Drawing.Size(213, 33);
            this.dashboardStartDatePicker.Name = "dashboardStartDatePicker";
            this.dashboardStartDatePicker.Size = new System.Drawing.Size(213, 33);
            this.dashboardStartDatePicker.TabIndex = 1;
            // 
            // bunifuCards11
            // 
            this.bunifuCards11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards11.BorderRadius = 5;
            this.bunifuCards11.BottomSahddow = false;
            this.bunifuCards11.color = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards11.Controls.Add(this.pictureBox11);
            this.bunifuCards11.Controls.Add(this.unpaidTestingAmount);
            this.bunifuCards11.Controls.Add(this.bunifuLabel93);
            this.bunifuCards11.LeftSahddow = false;
            this.bunifuCards11.Location = new System.Drawing.Point(548, 514);
            this.bunifuCards11.Name = "bunifuCards11";
            this.bunifuCards11.RightSahddow = false;
            this.bunifuCards11.ShadowDepth = 20;
            this.bunifuCards11.Size = new System.Drawing.Size(217, 143);
            this.bunifuCards11.TabIndex = 40;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(148, 67);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(52, 50);
            this.pictureBox11.TabIndex = 2;
            this.pictureBox11.TabStop = false;
            // 
            // unpaidTestingAmount
            // 
            this.unpaidTestingAmount.AutoEllipsis = false;
            this.unpaidTestingAmount.CursorType = null;
            this.unpaidTestingAmount.Font = new System.Drawing.Font("Segoe UI", 22F, System.Drawing.FontStyle.Bold);
            this.unpaidTestingAmount.ForeColor = System.Drawing.Color.Plum;
            this.unpaidTestingAmount.Location = new System.Drawing.Point(9, 66);
            this.unpaidTestingAmount.Name = "unpaidTestingAmount";
            this.unpaidTestingAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.unpaidTestingAmount.Size = new System.Drawing.Size(129, 52);
            this.unpaidTestingAmount.TabIndex = 3;
            this.unpaidTestingAmount.Text = "000000";
            this.unpaidTestingAmount.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.unpaidTestingAmount.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel93
            // 
            this.bunifuLabel93.AutoEllipsis = false;
            this.bunifuLabel93.CursorType = null;
            this.bunifuLabel93.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel93.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel93.Location = new System.Drawing.Point(11, 6);
            this.bunifuLabel93.Name = "bunifuLabel93";
            this.bunifuLabel93.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel93.Size = new System.Drawing.Size(195, 25);
            this.bunifuLabel93.TabIndex = 1;
            this.bunifuLabel93.Text = "Unpaid Testing Amount";
            this.bunifuLabel93.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel93.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuCards12
            // 
            this.bunifuCards12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards12.BorderRadius = 5;
            this.bunifuCards12.BottomSahddow = false;
            this.bunifuCards12.color = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards12.Controls.Add(this.pictureBox12);
            this.bunifuCards12.Controls.Add(this.totalUnpaidReportAmount);
            this.bunifuCards12.Controls.Add(this.bunifuLabel95);
            this.bunifuCards12.LeftSahddow = false;
            this.bunifuCards12.Location = new System.Drawing.Point(287, 514);
            this.bunifuCards12.Name = "bunifuCards12";
            this.bunifuCards12.RightSahddow = false;
            this.bunifuCards12.ShadowDepth = 20;
            this.bunifuCards12.Size = new System.Drawing.Size(217, 143);
            this.bunifuCards12.TabIndex = 39;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(155, 67);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(52, 50);
            this.pictureBox12.TabIndex = 2;
            this.pictureBox12.TabStop = false;
            // 
            // totalUnpaidReportAmount
            // 
            this.totalUnpaidReportAmount.AutoEllipsis = false;
            this.totalUnpaidReportAmount.CursorType = null;
            this.totalUnpaidReportAmount.Font = new System.Drawing.Font("Segoe UI", 22F, System.Drawing.FontStyle.Bold);
            this.totalUnpaidReportAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.totalUnpaidReportAmount.Location = new System.Drawing.Point(11, 66);
            this.totalUnpaidReportAmount.Name = "totalUnpaidReportAmount";
            this.totalUnpaidReportAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.totalUnpaidReportAmount.Size = new System.Drawing.Size(129, 52);
            this.totalUnpaidReportAmount.TabIndex = 3;
            this.totalUnpaidReportAmount.Text = "000000";
            this.totalUnpaidReportAmount.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.totalUnpaidReportAmount.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel95
            // 
            this.bunifuLabel95.AutoEllipsis = false;
            this.bunifuLabel95.CursorType = null;
            this.bunifuLabel95.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel95.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel95.Location = new System.Drawing.Point(12, 8);
            this.bunifuLabel95.Name = "bunifuLabel95";
            this.bunifuLabel95.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel95.Size = new System.Drawing.Size(190, 25);
            this.bunifuLabel95.TabIndex = 1;
            this.bunifuLabel95.Text = "UnPaid Report Amount ";
            this.bunifuLabel95.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel95.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuCards8
            // 
            this.bunifuCards8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards8.BorderRadius = 5;
            this.bunifuCards8.BottomSahddow = false;
            this.bunifuCards8.color = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards8.Controls.Add(this.pictureBox8);
            this.bunifuCards8.Controls.Add(this.totalUnpaidTesting);
            this.bunifuCards8.Controls.Add(this.bunifuLabel83);
            this.bunifuCards8.LeftSahddow = false;
            this.bunifuCards8.Location = new System.Drawing.Point(809, 306);
            this.bunifuCards8.Name = "bunifuCards8";
            this.bunifuCards8.RightSahddow = false;
            this.bunifuCards8.ShadowDepth = 20;
            this.bunifuCards8.Size = new System.Drawing.Size(217, 143);
            this.bunifuCards8.TabIndex = 38;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(148, 67);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(52, 50);
            this.pictureBox8.TabIndex = 2;
            this.pictureBox8.TabStop = false;
            // 
            // totalUnpaidTesting
            // 
            this.totalUnpaidTesting.AutoEllipsis = false;
            this.totalUnpaidTesting.CursorType = null;
            this.totalUnpaidTesting.Font = new System.Drawing.Font("Segoe UI", 22F, System.Drawing.FontStyle.Bold);
            this.totalUnpaidTesting.ForeColor = System.Drawing.Color.Tomato;
            this.totalUnpaidTesting.Location = new System.Drawing.Point(9, 66);
            this.totalUnpaidTesting.Name = "totalUnpaidTesting";
            this.totalUnpaidTesting.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.totalUnpaidTesting.Size = new System.Drawing.Size(129, 52);
            this.totalUnpaidTesting.TabIndex = 3;
            this.totalUnpaidTesting.Text = "000000";
            this.totalUnpaidTesting.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.totalUnpaidTesting.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel83
            // 
            this.bunifuLabel83.AutoEllipsis = false;
            this.bunifuLabel83.CursorType = null;
            this.bunifuLabel83.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel83.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel83.Location = new System.Drawing.Point(4, 4);
            this.bunifuLabel83.Name = "bunifuLabel83";
            this.bunifuLabel83.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel83.Size = new System.Drawing.Size(201, 30);
            this.bunifuLabel83.TabIndex = 1;
            this.bunifuLabel83.Text = "Total Unpaid Testing";
            this.bunifuLabel83.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel83.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuCards7
            // 
            this.bunifuCards7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards7.BorderRadius = 5;
            this.bunifuCards7.BottomSahddow = false;
            this.bunifuCards7.color = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards7.Controls.Add(this.pictureBox6);
            this.bunifuCards7.Controls.Add(this.totalUnpaidReportText);
            this.bunifuCards7.Controls.Add(this.unpaidAmount);
            this.bunifuCards7.LeftSahddow = false;
            this.bunifuCards7.Location = new System.Drawing.Point(548, 306);
            this.bunifuCards7.Name = "bunifuCards7";
            this.bunifuCards7.RightSahddow = false;
            this.bunifuCards7.ShadowDepth = 20;
            this.bunifuCards7.Size = new System.Drawing.Size(217, 143);
            this.bunifuCards7.TabIndex = 37;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(148, 67);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(52, 50);
            this.pictureBox6.TabIndex = 2;
            this.pictureBox6.TabStop = false;
            // 
            // totalUnpaidReportText
            // 
            this.totalUnpaidReportText.AutoEllipsis = false;
            this.totalUnpaidReportText.CursorType = null;
            this.totalUnpaidReportText.Font = new System.Drawing.Font("Segoe UI", 22F, System.Drawing.FontStyle.Bold);
            this.totalUnpaidReportText.ForeColor = System.Drawing.Color.Goldenrod;
            this.totalUnpaidReportText.Location = new System.Drawing.Point(9, 66);
            this.totalUnpaidReportText.Name = "totalUnpaidReportText";
            this.totalUnpaidReportText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.totalUnpaidReportText.Size = new System.Drawing.Size(129, 52);
            this.totalUnpaidReportText.TabIndex = 3;
            this.totalUnpaidReportText.Text = "000000";
            this.totalUnpaidReportText.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.totalUnpaidReportText.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // unpaidAmount
            // 
            this.unpaidAmount.AutoEllipsis = false;
            this.unpaidAmount.CursorType = null;
            this.unpaidAmount.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.unpaidAmount.ForeColor = System.Drawing.Color.White;
            this.unpaidAmount.Location = new System.Drawing.Point(4, 4);
            this.unpaidAmount.Name = "unpaidAmount";
            this.unpaidAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.unpaidAmount.Size = new System.Drawing.Size(195, 30);
            this.unpaidAmount.TabIndex = 1;
            this.unpaidAmount.Text = "Total Unpaid Report";
            this.unpaidAmount.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.unpaidAmount.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.unpaidAmount.Click += new System.EventHandler(this.bunifuLabel76_Click);
            // 
            // bunifuCards6
            // 
            this.bunifuCards6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards6.BorderRadius = 5;
            this.bunifuCards6.BottomSahddow = false;
            this.bunifuCards6.color = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards6.Controls.Add(this.pictureBox5);
            this.bunifuCards6.Controls.Add(this.totaltestingAmountText);
            this.bunifuCards6.Controls.Add(this.bunifuLabel69);
            this.bunifuCards6.LeftSahddow = false;
            this.bunifuCards6.Location = new System.Drawing.Point(287, 306);
            this.bunifuCards6.Name = "bunifuCards6";
            this.bunifuCards6.RightSahddow = false;
            this.bunifuCards6.ShadowDepth = 20;
            this.bunifuCards6.Size = new System.Drawing.Size(217, 143);
            this.bunifuCards6.TabIndex = 8;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(148, 67);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(52, 50);
            this.pictureBox5.TabIndex = 2;
            this.pictureBox5.TabStop = false;
            // 
            // totaltestingAmountText
            // 
            this.totaltestingAmountText.AutoEllipsis = false;
            this.totaltestingAmountText.CursorType = null;
            this.totaltestingAmountText.Font = new System.Drawing.Font("Segoe UI", 22F, System.Drawing.FontStyle.Bold);
            this.totaltestingAmountText.ForeColor = System.Drawing.Color.RoyalBlue;
            this.totaltestingAmountText.Location = new System.Drawing.Point(9, 66);
            this.totaltestingAmountText.Name = "totaltestingAmountText";
            this.totaltestingAmountText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.totaltestingAmountText.Size = new System.Drawing.Size(129, 52);
            this.totaltestingAmountText.TabIndex = 3;
            this.totaltestingAmountText.Text = "000000";
            this.totaltestingAmountText.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.totaltestingAmountText.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel69
            // 
            this.bunifuLabel69.AutoEllipsis = false;
            this.bunifuLabel69.CursorType = null;
            this.bunifuLabel69.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel69.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel69.Location = new System.Drawing.Point(4, 4);
            this.bunifuLabel69.Name = "bunifuLabel69";
            this.bunifuLabel69.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel69.Size = new System.Drawing.Size(210, 30);
            this.bunifuLabel69.TabIndex = 1;
            this.bunifuLabel69.Text = "Total Testing Amount";
            this.bunifuLabel69.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel69.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // getDashboardDataButton
            // 
            this.getDashboardDataButton.AllowToggling = false;
            this.getDashboardDataButton.AnimationSpeed = 200;
            this.getDashboardDataButton.AutoGenerateColors = false;
            this.getDashboardDataButton.BackColor = System.Drawing.Color.Transparent;
            this.getDashboardDataButton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.getDashboardDataButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("getDashboardDataButton.BackgroundImage")));
            this.getDashboardDataButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.getDashboardDataButton.ButtonText = "Get Data";
            this.getDashboardDataButton.ButtonTextMarginLeft = 0;
            this.getDashboardDataButton.ColorContrastOnClick = 45;
            this.getDashboardDataButton.ColorContrastOnHover = 45;
            this.getDashboardDataButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges10.BottomLeft = true;
            borderEdges10.BottomRight = true;
            borderEdges10.TopLeft = true;
            borderEdges10.TopRight = true;
            this.getDashboardDataButton.CustomizableEdges = borderEdges10;
            this.getDashboardDataButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.getDashboardDataButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.getDashboardDataButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.getDashboardDataButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.getDashboardDataButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.getDashboardDataButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.getDashboardDataButton.ForeColor = System.Drawing.Color.White;
            this.getDashboardDataButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.getDashboardDataButton.IconMarginLeft = 11;
            this.getDashboardDataButton.IconPadding = 10;
            this.getDashboardDataButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.getDashboardDataButton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.getDashboardDataButton.IdleBorderRadius = 3;
            this.getDashboardDataButton.IdleBorderThickness = 1;
            this.getDashboardDataButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.getDashboardDataButton.IdleIconLeftImage = null;
            this.getDashboardDataButton.IdleIconRightImage = null;
            this.getDashboardDataButton.IndicateFocus = false;
            this.getDashboardDataButton.Location = new System.Drawing.Point(809, 54);
            this.getDashboardDataButton.Name = "getDashboardDataButton";
            stateProperties19.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties19.BorderRadius = 3;
            stateProperties19.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties19.BorderThickness = 1;
            stateProperties19.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties19.ForeColor = System.Drawing.Color.White;
            stateProperties19.IconLeftImage = null;
            stateProperties19.IconRightImage = null;
            this.getDashboardDataButton.onHoverState = stateProperties19;
            stateProperties20.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties20.BorderRadius = 3;
            stateProperties20.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties20.BorderThickness = 1;
            stateProperties20.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties20.ForeColor = System.Drawing.Color.White;
            stateProperties20.IconLeftImage = null;
            stateProperties20.IconRightImage = null;
            this.getDashboardDataButton.OnPressedState = stateProperties20;
            this.getDashboardDataButton.Size = new System.Drawing.Size(217, 33);
            this.getDashboardDataButton.TabIndex = 3;
            this.getDashboardDataButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.getDashboardDataButton.TextMarginLeft = 0;
            this.getDashboardDataButton.UseDefaultRadiusAndThickness = true;
            this.getDashboardDataButton.Click += new System.EventHandler(this.bunifuButton31_Click);
            // 
            // bunifuCards5
            // 
            this.bunifuCards5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards5.BorderRadius = 5;
            this.bunifuCards5.BottomSahddow = false;
            this.bunifuCards5.color = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards5.Controls.Add(this.pictureBox7);
            this.bunifuCards5.Controls.Add(this.testingCountText);
            this.bunifuCards5.Controls.Add(this.bunifuLabel87);
            this.bunifuCards5.LeftSahddow = false;
            this.bunifuCards5.Location = new System.Drawing.Point(26, 306);
            this.bunifuCards5.Name = "bunifuCards5";
            this.bunifuCards5.RightSahddow = false;
            this.bunifuCards5.ShadowDepth = 20;
            this.bunifuCards5.Size = new System.Drawing.Size(217, 143);
            this.bunifuCards5.TabIndex = 7;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(155, 67);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(52, 50);
            this.pictureBox7.TabIndex = 2;
            this.pictureBox7.TabStop = false;
            // 
            // testingCountText
            // 
            this.testingCountText.AutoEllipsis = false;
            this.testingCountText.CursorType = null;
            this.testingCountText.Font = new System.Drawing.Font("Segoe UI", 22F, System.Drawing.FontStyle.Bold);
            this.testingCountText.ForeColor = System.Drawing.Color.Fuchsia;
            this.testingCountText.Location = new System.Drawing.Point(11, 66);
            this.testingCountText.Name = "testingCountText";
            this.testingCountText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.testingCountText.Size = new System.Drawing.Size(129, 52);
            this.testingCountText.TabIndex = 3;
            this.testingCountText.Text = "000000";
            this.testingCountText.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.testingCountText.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel87
            // 
            this.bunifuLabel87.AutoEllipsis = false;
            this.bunifuLabel87.CursorType = null;
            this.bunifuLabel87.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel87.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel87.Location = new System.Drawing.Point(11, 4);
            this.bunifuLabel87.Name = "bunifuLabel87";
            this.bunifuLabel87.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel87.Size = new System.Drawing.Size(128, 30);
            this.bunifuLabel87.TabIndex = 1;
            this.bunifuLabel87.Text = "Total Testing";
            this.bunifuLabel87.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel87.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuCards4
            // 
            this.bunifuCards4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards4.BorderRadius = 5;
            this.bunifuCards4.BottomSahddow = false;
            this.bunifuCards4.color = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards4.Controls.Add(this.pictureBox4);
            this.bunifuCards4.Controls.Add(this.savingText);
            this.bunifuCards4.Controls.Add(this.bunifuLabel26);
            this.bunifuCards4.LeftSahddow = false;
            this.bunifuCards4.Location = new System.Drawing.Point(809, 105);
            this.bunifuCards4.Name = "bunifuCards4";
            this.bunifuCards4.RightSahddow = false;
            this.bunifuCards4.ShadowDepth = 20;
            this.bunifuCards4.Size = new System.Drawing.Size(217, 143);
            this.bunifuCards4.TabIndex = 5;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::GoldRaven.Properties.Resources.icons8_stack_of_money_50px;
            this.pictureBox4.Location = new System.Drawing.Point(153, 61);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(52, 50);
            this.pictureBox4.TabIndex = 5;
            this.pictureBox4.TabStop = false;
            // 
            // savingText
            // 
            this.savingText.AutoEllipsis = false;
            this.savingText.CursorType = null;
            this.savingText.Font = new System.Drawing.Font("Segoe UI", 22F, System.Drawing.FontStyle.Bold);
            this.savingText.ForeColor = System.Drawing.Color.Lime;
            this.savingText.Location = new System.Drawing.Point(9, 63);
            this.savingText.Name = "savingText";
            this.savingText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.savingText.Size = new System.Drawing.Size(129, 52);
            this.savingText.TabIndex = 6;
            this.savingText.Text = "000000";
            this.savingText.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.savingText.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel26
            // 
            this.bunifuLabel26.AutoEllipsis = false;
            this.bunifuLabel26.CursorType = null;
            this.bunifuLabel26.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel26.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel26.Location = new System.Drawing.Point(9, 4);
            this.bunifuLabel26.Name = "bunifuLabel26";
            this.bunifuLabel26.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel26.Size = new System.Drawing.Size(121, 30);
            this.bunifuLabel26.TabIndex = 5;
            this.bunifuLabel26.Text = "Total Saving";
            this.bunifuLabel26.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel26.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuCards3
            // 
            this.bunifuCards3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards3.BorderRadius = 5;
            this.bunifuCards3.BottomSahddow = false;
            this.bunifuCards3.color = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards3.Controls.Add(this.pictureBox3);
            this.bunifuCards3.Controls.Add(this.investAmontText);
            this.bunifuCards3.Controls.Add(this.bunifuLabel25);
            this.bunifuCards3.Controls.Add(this.bunifuDatepicker3);
            this.bunifuCards3.LeftSahddow = false;
            this.bunifuCards3.Location = new System.Drawing.Point(548, 105);
            this.bunifuCards3.Name = "bunifuCards3";
            this.bunifuCards3.RightSahddow = false;
            this.bunifuCards3.ShadowDepth = 20;
            this.bunifuCards3.Size = new System.Drawing.Size(217, 143);
            this.bunifuCards3.TabIndex = 4;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::GoldRaven.Properties.Resources.icons8_external_50px;
            this.pictureBox3.Location = new System.Drawing.Point(155, 61);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(52, 50);
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // investAmontText
            // 
            this.investAmontText.AutoEllipsis = false;
            this.investAmontText.CursorType = null;
            this.investAmontText.Font = new System.Drawing.Font("Segoe UI", 22F, System.Drawing.FontStyle.Bold);
            this.investAmontText.ForeColor = System.Drawing.Color.DarkRed;
            this.investAmontText.Location = new System.Drawing.Point(9, 63);
            this.investAmontText.Name = "investAmontText";
            this.investAmontText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.investAmontText.Size = new System.Drawing.Size(129, 52);
            this.investAmontText.TabIndex = 5;
            this.investAmontText.Text = "000000";
            this.investAmontText.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.investAmontText.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel25
            // 
            this.bunifuLabel25.AutoEllipsis = false;
            this.bunifuLabel25.CursorType = null;
            this.bunifuLabel25.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel25.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel25.Location = new System.Drawing.Point(9, 4);
            this.bunifuLabel25.Name = "bunifuLabel25";
            this.bunifuLabel25.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel25.Size = new System.Drawing.Size(171, 30);
            this.bunifuLabel25.TabIndex = 4;
            this.bunifuLabel25.Text = "Expenses Amount";
            this.bunifuLabel25.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel25.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuDatepicker3
            // 
            this.bunifuDatepicker3.BackColor = System.Drawing.Color.SeaGreen;
            this.bunifuDatepicker3.BorderRadius = 0;
            this.bunifuDatepicker3.ForeColor = System.Drawing.Color.White;
            this.bunifuDatepicker3.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.bunifuDatepicker3.FormatCustom = null;
            this.bunifuDatepicker3.Location = new System.Drawing.Point(-32768, 32620);
            this.bunifuDatepicker3.Margin = new System.Windows.Forms.Padding(185280);
            this.bunifuDatepicker3.Name = "bunifuDatepicker3";
            this.bunifuDatepicker3.Size = new System.Drawing.Size(0, 0);
            this.bunifuDatepicker3.TabIndex = 3;
            this.bunifuDatepicker3.Value = new System.DateTime(2021, 1, 8, 0, 24, 17, 96);
            // 
            // bunifuCards2
            // 
            this.bunifuCards2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards2.BorderRadius = 5;
            this.bunifuCards2.BottomSahddow = false;
            this.bunifuCards2.color = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards2.Controls.Add(this.pictureBox2);
            this.bunifuCards2.Controls.Add(this.saleAmountText);
            this.bunifuCards2.Controls.Add(this.bunifuLabel24);
            this.bunifuCards2.LeftSahddow = false;
            this.bunifuCards2.Location = new System.Drawing.Point(287, 105);
            this.bunifuCards2.Name = "bunifuCards2";
            this.bunifuCards2.RightSahddow = false;
            this.bunifuCards2.ShadowDepth = 20;
            this.bunifuCards2.Size = new System.Drawing.Size(217, 143);
            this.bunifuCards2.TabIndex = 3;
            this.bunifuCards2.Paint += new System.Windows.Forms.PaintEventHandler(this.bunifuCards2_Paint);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::GoldRaven.Properties.Resources.icons8_internal_50px;
            this.pictureBox2.Location = new System.Drawing.Point(148, 61);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(52, 50);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // saleAmountText
            // 
            this.saleAmountText.AutoEllipsis = false;
            this.saleAmountText.CursorType = null;
            this.saleAmountText.Font = new System.Drawing.Font("Segoe UI", 22F, System.Drawing.FontStyle.Bold);
            this.saleAmountText.ForeColor = System.Drawing.Color.Aqua;
            this.saleAmountText.Location = new System.Drawing.Point(9, 63);
            this.saleAmountText.Name = "saleAmountText";
            this.saleAmountText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.saleAmountText.Size = new System.Drawing.Size(129, 52);
            this.saleAmountText.TabIndex = 4;
            this.saleAmountText.Text = "000000";
            this.saleAmountText.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.saleAmountText.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.saleAmountText.Click += new System.EventHandler(this.saleAmountText_Click);
            // 
            // bunifuLabel24
            // 
            this.bunifuLabel24.AutoEllipsis = false;
            this.bunifuLabel24.CursorType = null;
            this.bunifuLabel24.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel24.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel24.Location = new System.Drawing.Point(9, 4);
            this.bunifuLabel24.Name = "bunifuLabel24";
            this.bunifuLabel24.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel24.Size = new System.Drawing.Size(124, 30);
            this.bunifuLabel24.TabIndex = 2;
            this.bunifuLabel24.Text = "Sale Amount";
            this.bunifuLabel24.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel24.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel24.Click += new System.EventHandler(this.bunifuLabel24_Click);
            // 
            // bunifuCards1
            // 
            this.bunifuCards1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards1.BorderRadius = 5;
            this.bunifuCards1.BottomSahddow = false;
            this.bunifuCards1.color = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(44)))), ((int)(((byte)(85)))));
            this.bunifuCards1.Controls.Add(this.pictureBox1);
            this.bunifuCards1.Controls.Add(this.totalReportText);
            this.bunifuCards1.Controls.Add(this.bunifuLabel22);
            this.bunifuCards1.LeftSahddow = false;
            this.bunifuCards1.Location = new System.Drawing.Point(26, 105);
            this.bunifuCards1.Name = "bunifuCards1";
            this.bunifuCards1.RightSahddow = false;
            this.bunifuCards1.ShadowDepth = 20;
            this.bunifuCards1.Size = new System.Drawing.Size(217, 143);
            this.bunifuCards1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GoldRaven.Properties.Resources.icons8_file_50px;
            this.pictureBox1.Location = new System.Drawing.Point(155, 61);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(52, 50);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // totalReportText
            // 
            this.totalReportText.AutoEllipsis = false;
            this.totalReportText.CursorType = null;
            this.totalReportText.Font = new System.Drawing.Font("Segoe UI", 22F, System.Drawing.FontStyle.Bold);
            this.totalReportText.ForeColor = System.Drawing.Color.Yellow;
            this.totalReportText.Location = new System.Drawing.Point(11, 63);
            this.totalReportText.Name = "totalReportText";
            this.totalReportText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.totalReportText.Size = new System.Drawing.Size(129, 52);
            this.totalReportText.TabIndex = 3;
            this.totalReportText.Text = "000000";
            this.totalReportText.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.totalReportText.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel22
            // 
            this.bunifuLabel22.AutoEllipsis = false;
            this.bunifuLabel22.CursorType = null;
            this.bunifuLabel22.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.bunifuLabel22.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel22.Location = new System.Drawing.Point(11, 4);
            this.bunifuLabel22.Name = "bunifuLabel22";
            this.bunifuLabel22.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel22.Size = new System.Drawing.Size(122, 30);
            this.bunifuLabel22.TabIndex = 1;
            this.bunifuLabel22.Text = "Total Report";
            this.bunifuLabel22.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel22.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel22.Click += new System.EventHandler(this.bunifuLabel22_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(21, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dashboard";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // customerMasterPage
            // 
            this.customerMasterPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.customerMasterPage.Controls.Add(this.custMastRatetextbox);
            this.customerMasterPage.Controls.Add(this.customerDataGridView);
            this.customerMasterPage.Controls.Add(this.bunifuLabel21);
            this.customerMasterPage.Controls.Add(this.custMastFirmNametextbox);
            this.customerMasterPage.Controls.Add(this.bunifuLabel15);
            this.customerMasterPage.Controls.Add(this.bunifuLabel16);
            this.customerMasterPage.Controls.Add(this.custMastMobiletextbox);
            this.customerMasterPage.Controls.Add(this.bunifuLabel17);
            this.customerMasterPage.Controls.Add(this.custMastAddresstextbox);
            this.customerMasterPage.Controls.Add(this.bunifuLabel20);
            this.customerMasterPage.Controls.Add(this.custMastNametextbox);
            this.customerMasterPage.Controls.Add(this.label2);
            this.customerMasterPage.Controls.Add(this.bunifuButton10);
            this.customerMasterPage.Controls.Add(this.bunifuButton9);
            this.customerMasterPage.Controls.Add(this.reportDetailSaveButton);
            this.customerMasterPage.Controls.Add(this.reportDetailPrintButton);
            this.customerMasterPage.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.customerMasterPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.customerMasterPage.Location = new System.Drawing.Point(4, 4);
            this.customerMasterPage.Name = "customerMasterPage";
            this.customerMasterPage.Padding = new System.Windows.Forms.Padding(3);
            this.customerMasterPage.Size = new System.Drawing.Size(1041, 728);
            this.customerMasterPage.TabIndex = 1;
            this.customerMasterPage.Text = "customerMaster";
            this.customerMasterPage.Click += new System.EventHandler(this.customerMasterPage_Click);
            // 
            // custMastRatetextbox
            // 
            this.custMastRatetextbox.AcceptsReturn = false;
            this.custMastRatetextbox.AcceptsTab = false;
            this.custMastRatetextbox.AnimationSpeed = 200;
            this.custMastRatetextbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.custMastRatetextbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.custMastRatetextbox.BackColor = System.Drawing.Color.Transparent;
            this.custMastRatetextbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("custMastRatetextbox.BackgroundImage")));
            this.custMastRatetextbox.BorderColorActive = System.Drawing.Color.Red;
            this.custMastRatetextbox.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.custMastRatetextbox.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.custMastRatetextbox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.custMastRatetextbox.BorderRadius = 1;
            this.custMastRatetextbox.BorderThickness = 1;
            this.custMastRatetextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.custMastRatetextbox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.custMastRatetextbox.DefaultFont = new System.Drawing.Font("Century Gothic", 44F, System.Drawing.FontStyle.Bold);
            this.custMastRatetextbox.DefaultText = "50";
            this.custMastRatetextbox.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.custMastRatetextbox.ForeColor = System.Drawing.Color.White;
            this.custMastRatetextbox.HideSelection = true;
            this.custMastRatetextbox.IconLeft = null;
            this.custMastRatetextbox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.custMastRatetextbox.IconPadding = 0;
            this.custMastRatetextbox.IconRight = null;
            this.custMastRatetextbox.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.custMastRatetextbox.Lines = new string[] {
        "50"};
            this.custMastRatetextbox.Location = new System.Drawing.Point(760, 104);
            this.custMastRatetextbox.Margin = new System.Windows.Forms.Padding(0);
            this.custMastRatetextbox.MaxLength = 32767;
            this.custMastRatetextbox.MinimumSize = new System.Drawing.Size(0, 33);
            this.custMastRatetextbox.Modified = false;
            this.custMastRatetextbox.Multiline = false;
            this.custMastRatetextbox.Name = "custMastRatetextbox";
            stateProperties21.BorderColor = System.Drawing.Color.Red;
            stateProperties21.FillColor = System.Drawing.Color.Empty;
            stateProperties21.ForeColor = System.Drawing.Color.Empty;
            stateProperties21.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.custMastRatetextbox.OnActiveState = stateProperties21;
            stateProperties22.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties22.FillColor = System.Drawing.Color.White;
            stateProperties22.ForeColor = System.Drawing.Color.Empty;
            stateProperties22.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.custMastRatetextbox.OnDisabledState = stateProperties22;
            stateProperties23.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties23.FillColor = System.Drawing.Color.Empty;
            stateProperties23.ForeColor = System.Drawing.Color.Empty;
            stateProperties23.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.custMastRatetextbox.OnHoverState = stateProperties23;
            stateProperties24.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties24.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties24.ForeColor = System.Drawing.Color.White;
            stateProperties24.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.custMastRatetextbox.OnIdleState = stateProperties24;
            this.custMastRatetextbox.PasswordChar = '\0';
            this.custMastRatetextbox.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.custMastRatetextbox.PlaceholderText = "₹";
            this.custMastRatetextbox.ReadOnly = false;
            this.custMastRatetextbox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.custMastRatetextbox.SelectedText = "";
            this.custMastRatetextbox.SelectionLength = 0;
            this.custMastRatetextbox.SelectionStart = 0;
            this.custMastRatetextbox.ShortcutsEnabled = true;
            this.custMastRatetextbox.Size = new System.Drawing.Size(250, 186);
            this.custMastRatetextbox.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.custMastRatetextbox.TabIndex = 4;
            this.custMastRatetextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.custMastRatetextbox.TextMarginBottom = 0;
            this.custMastRatetextbox.TextMarginLeft = 5;
            this.custMastRatetextbox.TextMarginTop = 0;
            this.custMastRatetextbox.TextPlaceholder = "₹";
            this.custMastRatetextbox.UseSystemPasswordChar = false;
            this.custMastRatetextbox.WordWrap = true;
            this.custMastRatetextbox.TextChanged += new System.EventHandler(this.bunifuTextBox1_TextChanged);
            this.custMastRatetextbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.custMastRatetextbox_KeyDown);
            this.custMastRatetextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.custMastRatetextbox_KeyPress);
            // 
            // customerDataGridView
            // 
            this.customerDataGridView.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.customerDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.customerDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.customerDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.customerDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.customerDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.customerDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.customerDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.customerDataGridView.ColumnHeadersHeight = 50;
            this.customerDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.Column1,
            this.Column2,
            this.Column4,
            this.Column3,
            this.Column5});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(252)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.customerDataGridView.DefaultCellStyle = dataGridViewCellStyle4;
            this.customerDataGridView.DoubleBuffered = true;
            this.customerDataGridView.EnableHeadersVisualStyles = false;
            this.customerDataGridView.GridColor = System.Drawing.Color.WhiteSmoke;
            this.customerDataGridView.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            this.customerDataGridView.HeaderForeColor = System.Drawing.Color.White;
            this.customerDataGridView.Location = new System.Drawing.Point(52, 363);
            this.customerDataGridView.Name = "customerDataGridView";
            this.customerDataGridView.ReadOnly = true;
            this.customerDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.customerDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.customerDataGridView.RowHeadersVisible = false;
            this.customerDataGridView.RowHeadersWidth = 51;
            this.customerDataGridView.RowTemplate.DividerHeight = 1;
            this.customerDataGridView.RowTemplate.Height = 40;
            this.customerDataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.customerDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.customerDataGridView.Size = new System.Drawing.Size(958, 327);
            this.customerDataGridView.TabIndex = 155;
            this.customerDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.bunifuCustomDataGrid1_CellContentClick);
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "id";
            this.Column6.HeaderText = "ID";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "customerName";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Teal;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.HeaderText = "Name";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "custMobile";
            this.Column2.HeaderText = "Mobile No.";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Rate";
            this.Column4.HeaderText = "Rate";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "custFirm";
            this.Column3.HeaderText = "Firm Name";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "customerAddress";
            this.Column5.HeaderText = "Address";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // bunifuLabel21
            // 
            this.bunifuLabel21.AutoEllipsis = false;
            this.bunifuLabel21.CursorType = null;
            this.bunifuLabel21.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel21.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel21.Location = new System.Drawing.Point(51, 227);
            this.bunifuLabel21.Name = "bunifuLabel21";
            this.bunifuLabel21.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel21.Size = new System.Drawing.Size(81, 23);
            this.bunifuLabel21.TabIndex = 0;
            this.bunifuLabel21.TabStop = false;
            this.bunifuLabel21.Text = "Firm Name";
            this.bunifuLabel21.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel21.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel21.Click += new System.EventHandler(this.bunifuLabel21_Click_1);
            // 
            // custMastFirmNametextbox
            // 
            this.custMastFirmNametextbox.BorderColorFocused = System.Drawing.Color.Red;
            this.custMastFirmNametextbox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.custMastFirmNametextbox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.custMastFirmNametextbox.BorderThickness = 1;
            this.custMastFirmNametextbox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.custMastFirmNametextbox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.custMastFirmNametextbox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.custMastFirmNametextbox.ForeColor = System.Drawing.Color.White;
            this.custMastFirmNametextbox.isPassword = false;
            this.custMastFirmNametextbox.Location = new System.Drawing.Point(51, 257);
            this.custMastFirmNametextbox.Margin = new System.Windows.Forms.Padding(4);
            this.custMastFirmNametextbox.MaxLength = 32767;
            this.custMastFirmNametextbox.Name = "custMastFirmNametextbox";
            this.custMastFirmNametextbox.Size = new System.Drawing.Size(695, 33);
            this.custMastFirmNametextbox.TabIndex = 5;
            this.custMastFirmNametextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.custMastFirmNametextbox.OnValueChanged += new System.EventHandler(this.bunifuMetroTextbox9_OnValueChanged_1);
            this.custMastFirmNametextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.custMastFirmNametextbox_KeyPress);
            // 
            // bunifuLabel15
            // 
            this.bunifuLabel15.AutoEllipsis = false;
            this.bunifuLabel15.CursorType = null;
            this.bunifuLabel15.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel15.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel15.Location = new System.Drawing.Point(763, 74);
            this.bunifuLabel15.Name = "bunifuLabel15";
            this.bunifuLabel15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel15.Size = new System.Drawing.Size(34, 23);
            this.bunifuLabel15.TabIndex = 0;
            this.bunifuLabel15.TabStop = false;
            this.bunifuLabel15.Text = "Rate";
            this.bunifuLabel15.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel15.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel16
            // 
            this.bunifuLabel16.AutoEllipsis = false;
            this.bunifuLabel16.CursorType = null;
            this.bunifuLabel16.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel16.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel16.Location = new System.Drawing.Point(471, 74);
            this.bunifuLabel16.Name = "bunifuLabel16";
            this.bunifuLabel16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel16.Size = new System.Drawing.Size(51, 23);
            this.bunifuLabel16.TabIndex = 0;
            this.bunifuLabel16.TabStop = false;
            this.bunifuLabel16.Text = "Mobile";
            this.bunifuLabel16.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel16.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // custMastMobiletextbox
            // 
            this.custMastMobiletextbox.BorderColorFocused = System.Drawing.Color.Red;
            this.custMastMobiletextbox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.custMastMobiletextbox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.custMastMobiletextbox.BorderThickness = 1;
            this.custMastMobiletextbox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.custMastMobiletextbox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.custMastMobiletextbox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.custMastMobiletextbox.ForeColor = System.Drawing.Color.White;
            this.custMastMobiletextbox.isPassword = false;
            this.custMastMobiletextbox.Location = new System.Drawing.Point(471, 104);
            this.custMastMobiletextbox.Margin = new System.Windows.Forms.Padding(4);
            this.custMastMobiletextbox.MaxLength = 10;
            this.custMastMobiletextbox.Name = "custMastMobiletextbox";
            this.custMastMobiletextbox.Size = new System.Drawing.Size(270, 33);
            this.custMastMobiletextbox.TabIndex = 2;
            this.custMastMobiletextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.custMastMobiletextbox.OnValueChanged += new System.EventHandler(this.custMastMobiletextbox_OnValueChanged);
            this.custMastMobiletextbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.custMastMobiletextbox_KeyDown);
            this.custMastMobiletextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.custMastMobiletextbox_KeyPress);
            this.custMastMobiletextbox.Leave += new System.EventHandler(this.custMastMobiletextbox_Leave);
            // 
            // bunifuLabel17
            // 
            this.bunifuLabel17.AutoEllipsis = false;
            this.bunifuLabel17.CursorType = null;
            this.bunifuLabel17.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel17.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel17.Location = new System.Drawing.Point(52, 147);
            this.bunifuLabel17.Name = "bunifuLabel17";
            this.bunifuLabel17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel17.Size = new System.Drawing.Size(59, 23);
            this.bunifuLabel17.TabIndex = 0;
            this.bunifuLabel17.TabStop = false;
            this.bunifuLabel17.Text = "Address";
            this.bunifuLabel17.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel17.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // custMastAddresstextbox
            // 
            this.custMastAddresstextbox.BorderColorFocused = System.Drawing.Color.Red;
            this.custMastAddresstextbox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.custMastAddresstextbox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.custMastAddresstextbox.BorderThickness = 1;
            this.custMastAddresstextbox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.custMastAddresstextbox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.custMastAddresstextbox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.custMastAddresstextbox.ForeColor = System.Drawing.Color.White;
            this.custMastAddresstextbox.isPassword = false;
            this.custMastAddresstextbox.Location = new System.Drawing.Point(51, 182);
            this.custMastAddresstextbox.Margin = new System.Windows.Forms.Padding(4);
            this.custMastAddresstextbox.MaxLength = 32767;
            this.custMastAddresstextbox.Name = "custMastAddresstextbox";
            this.custMastAddresstextbox.Size = new System.Drawing.Size(695, 33);
            this.custMastAddresstextbox.TabIndex = 3;
            this.custMastAddresstextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.custMastAddresstextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.custMastAddresstextbox_KeyPress);
            // 
            // bunifuLabel20
            // 
            this.bunifuLabel20.AutoEllipsis = false;
            this.bunifuLabel20.CursorType = null;
            this.bunifuLabel20.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel20.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel20.Location = new System.Drawing.Point(49, 74);
            this.bunifuLabel20.Name = "bunifuLabel20";
            this.bunifuLabel20.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel20.Size = new System.Drawing.Size(117, 23);
            this.bunifuLabel20.TabIndex = 0;
            this.bunifuLabel20.TabStop = false;
            this.bunifuLabel20.Text = "Customer Name";
            this.bunifuLabel20.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel20.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // custMastNametextbox
            // 
            this.custMastNametextbox.BorderColorFocused = System.Drawing.Color.Red;
            this.custMastNametextbox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.custMastNametextbox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.custMastNametextbox.BorderThickness = 1;
            this.custMastNametextbox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.custMastNametextbox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.custMastNametextbox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.custMastNametextbox.ForeColor = System.Drawing.Color.White;
            this.custMastNametextbox.isPassword = false;
            this.custMastNametextbox.Location = new System.Drawing.Point(51, 104);
            this.custMastNametextbox.Margin = new System.Windows.Forms.Padding(4);
            this.custMastNametextbox.MaxLength = 32767;
            this.custMastNametextbox.Name = "custMastNametextbox";
            this.custMastNametextbox.Size = new System.Drawing.Size(387, 33);
            this.custMastNametextbox.TabIndex = 1;
            this.custMastNametextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.custMastNametextbox.OnValueChanged += new System.EventHandler(this.bunifuMetroTextbox8_OnValueChanged_1);
            this.custMastNametextbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.custMastNametextbox_KeyDown);
            this.custMastNametextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.custMastNametextbox_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(43, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(211, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "Customer Master";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // bunifuButton10
            // 
            this.bunifuButton10.AllowToggling = false;
            this.bunifuButton10.AnimationSpeed = 200;
            this.bunifuButton10.AutoGenerateColors = false;
            this.bunifuButton10.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton10.BackColor1 = System.Drawing.Color.Goldenrod;
            this.bunifuButton10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton10.BackgroundImage")));
            this.bunifuButton10.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton10.ButtonText = " Modify";
            this.bunifuButton10.ButtonTextMarginLeft = 0;
            this.bunifuButton10.ColorContrastOnClick = 45;
            this.bunifuButton10.ColorContrastOnHover = 45;
            this.bunifuButton10.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges11.BottomLeft = true;
            borderEdges11.BottomRight = true;
            borderEdges11.TopLeft = true;
            borderEdges11.TopRight = true;
            this.bunifuButton10.CustomizableEdges = borderEdges11;
            this.bunifuButton10.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton10.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton10.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton10.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton10.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton10.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton10.ForeColor = System.Drawing.Color.White;
            this.bunifuButton10.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton10.IconMarginLeft = 11;
            this.bunifuButton10.IconPadding = 10;
            this.bunifuButton10.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton10.IdleBorderColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton10.IdleBorderRadius = 3;
            this.bunifuButton10.IdleBorderThickness = 1;
            this.bunifuButton10.IdleFillColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton10.IdleIconLeftImage = null;
            this.bunifuButton10.IdleIconRightImage = null;
            this.bunifuButton10.IndicateFocus = false;
            this.bunifuButton10.Location = new System.Drawing.Point(545, 310);
            this.bunifuButton10.Name = "bunifuButton10";
            stateProperties25.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties25.BorderRadius = 3;
            stateProperties25.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties25.BorderThickness = 1;
            stateProperties25.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties25.ForeColor = System.Drawing.Color.White;
            stateProperties25.IconLeftImage = null;
            stateProperties25.IconRightImage = null;
            this.bunifuButton10.onHoverState = stateProperties25;
            stateProperties26.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties26.BorderRadius = 3;
            stateProperties26.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties26.BorderThickness = 1;
            stateProperties26.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties26.ForeColor = System.Drawing.Color.White;
            stateProperties26.IconLeftImage = null;
            stateProperties26.IconRightImage = null;
            this.bunifuButton10.OnPressedState = stateProperties26;
            this.bunifuButton10.Size = new System.Drawing.Size(140, 33);
            this.bunifuButton10.TabIndex = 7;
            this.bunifuButton10.TabStop = false;
            this.bunifuButton10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton10.TextMarginLeft = 0;
            this.bunifuButton10.UseDefaultRadiusAndThickness = true;
            this.bunifuButton10.Click += new System.EventHandler(this.bunifuButton10_Click_1);
            // 
            // bunifuButton9
            // 
            this.bunifuButton9.AllowToggling = false;
            this.bunifuButton9.AnimationSpeed = 200;
            this.bunifuButton9.AutoGenerateColors = false;
            this.bunifuButton9.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton9.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton9.BackgroundImage")));
            this.bunifuButton9.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton9.ButtonText = "New";
            this.bunifuButton9.ButtonTextMarginLeft = 0;
            this.bunifuButton9.ColorContrastOnClick = 45;
            this.bunifuButton9.ColorContrastOnHover = 45;
            this.bunifuButton9.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges12.BottomLeft = true;
            borderEdges12.BottomRight = true;
            borderEdges12.TopLeft = true;
            borderEdges12.TopRight = true;
            this.bunifuButton9.CustomizableEdges = borderEdges12;
            this.bunifuButton9.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton9.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton9.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton9.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton9.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton9.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton9.ForeColor = System.Drawing.Color.White;
            this.bunifuButton9.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton9.IconMarginLeft = 11;
            this.bunifuButton9.IconPadding = 10;
            this.bunifuButton9.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton9.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton9.IdleBorderRadius = 3;
            this.bunifuButton9.IdleBorderThickness = 1;
            this.bunifuButton9.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton9.IdleIconLeftImage = null;
            this.bunifuButton9.IdleIconRightImage = null;
            this.bunifuButton9.IndicateFocus = false;
            this.bunifuButton9.Location = new System.Drawing.Point(383, 310);
            this.bunifuButton9.Name = "bunifuButton9";
            stateProperties27.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties27.BorderRadius = 3;
            stateProperties27.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties27.BorderThickness = 1;
            stateProperties27.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties27.ForeColor = System.Drawing.Color.White;
            stateProperties27.IconLeftImage = null;
            stateProperties27.IconRightImage = null;
            this.bunifuButton9.onHoverState = stateProperties27;
            stateProperties28.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties28.BorderRadius = 3;
            stateProperties28.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties28.BorderThickness = 1;
            stateProperties28.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties28.ForeColor = System.Drawing.Color.White;
            stateProperties28.IconLeftImage = null;
            stateProperties28.IconRightImage = null;
            this.bunifuButton9.OnPressedState = stateProperties28;
            this.bunifuButton9.Size = new System.Drawing.Size(140, 33);
            this.bunifuButton9.TabIndex = 8;
            this.bunifuButton9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton9.TextMarginLeft = 0;
            this.bunifuButton9.UseDefaultRadiusAndThickness = true;
            this.bunifuButton9.Click += new System.EventHandler(this.bunifuButton9_Click_1);
            // 
            // reportDetailSaveButton
            // 
            this.reportDetailSaveButton.AllowToggling = false;
            this.reportDetailSaveButton.AnimationSpeed = 200;
            this.reportDetailSaveButton.AutoGenerateColors = false;
            this.reportDetailSaveButton.BackColor = System.Drawing.Color.Transparent;
            this.reportDetailSaveButton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.reportDetailSaveButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("reportDetailSaveButton.BackgroundImage")));
            this.reportDetailSaveButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.reportDetailSaveButton.ButtonText = "Save";
            this.reportDetailSaveButton.ButtonTextMarginLeft = 0;
            this.reportDetailSaveButton.ColorContrastOnClick = 45;
            this.reportDetailSaveButton.ColorContrastOnHover = 45;
            this.reportDetailSaveButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges13.BottomLeft = true;
            borderEdges13.BottomRight = true;
            borderEdges13.TopLeft = true;
            borderEdges13.TopRight = true;
            this.reportDetailSaveButton.CustomizableEdges = borderEdges13;
            this.reportDetailSaveButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.reportDetailSaveButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.reportDetailSaveButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.reportDetailSaveButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.reportDetailSaveButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.reportDetailSaveButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.reportDetailSaveButton.ForeColor = System.Drawing.Color.White;
            this.reportDetailSaveButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.reportDetailSaveButton.IconMarginLeft = 11;
            this.reportDetailSaveButton.IconPadding = 10;
            this.reportDetailSaveButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.reportDetailSaveButton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.reportDetailSaveButton.IdleBorderRadius = 3;
            this.reportDetailSaveButton.IdleBorderThickness = 1;
            this.reportDetailSaveButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.reportDetailSaveButton.IdleIconLeftImage = null;
            this.reportDetailSaveButton.IdleIconRightImage = null;
            this.reportDetailSaveButton.IndicateFocus = false;
            this.reportDetailSaveButton.Location = new System.Drawing.Point(707, 310);
            this.reportDetailSaveButton.Name = "reportDetailSaveButton";
            stateProperties29.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties29.BorderRadius = 3;
            stateProperties29.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties29.BorderThickness = 1;
            stateProperties29.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties29.ForeColor = System.Drawing.Color.White;
            stateProperties29.IconLeftImage = null;
            stateProperties29.IconRightImage = null;
            this.reportDetailSaveButton.onHoverState = stateProperties29;
            stateProperties30.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties30.BorderRadius = 3;
            stateProperties30.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties30.BorderThickness = 1;
            stateProperties30.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties30.ForeColor = System.Drawing.Color.White;
            stateProperties30.IconLeftImage = null;
            stateProperties30.IconRightImage = null;
            this.reportDetailSaveButton.OnPressedState = stateProperties30;
            this.reportDetailSaveButton.Size = new System.Drawing.Size(140, 33);
            this.reportDetailSaveButton.TabIndex = 6;
            this.reportDetailSaveButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reportDetailSaveButton.TextMarginLeft = 0;
            this.reportDetailSaveButton.UseDefaultRadiusAndThickness = true;
            this.reportDetailSaveButton.Click += new System.EventHandler(this.reportDetailSaveButton_Click);
            this.reportDetailSaveButton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSaveButton_KeyPress);
            // 
            // reportDetailPrintButton
            // 
            this.reportDetailPrintButton.AllowToggling = false;
            this.reportDetailPrintButton.AnimationSpeed = 200;
            this.reportDetailPrintButton.AutoGenerateColors = false;
            this.reportDetailPrintButton.BackColor = System.Drawing.Color.Transparent;
            this.reportDetailPrintButton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.reportDetailPrintButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("reportDetailPrintButton.BackgroundImage")));
            this.reportDetailPrintButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.reportDetailPrintButton.ButtonText = "Delete";
            this.reportDetailPrintButton.ButtonTextMarginLeft = 0;
            this.reportDetailPrintButton.ColorContrastOnClick = 45;
            this.reportDetailPrintButton.ColorContrastOnHover = 45;
            this.reportDetailPrintButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges14.BottomLeft = true;
            borderEdges14.BottomRight = true;
            borderEdges14.TopLeft = true;
            borderEdges14.TopRight = true;
            this.reportDetailPrintButton.CustomizableEdges = borderEdges14;
            this.reportDetailPrintButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.reportDetailPrintButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.reportDetailPrintButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.reportDetailPrintButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.reportDetailPrintButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.reportDetailPrintButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.reportDetailPrintButton.ForeColor = System.Drawing.Color.White;
            this.reportDetailPrintButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.reportDetailPrintButton.IconMarginLeft = 11;
            this.reportDetailPrintButton.IconPadding = 10;
            this.reportDetailPrintButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.reportDetailPrintButton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.reportDetailPrintButton.IdleBorderRadius = 3;
            this.reportDetailPrintButton.IdleBorderThickness = 1;
            this.reportDetailPrintButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.reportDetailPrintButton.IdleIconLeftImage = null;
            this.reportDetailPrintButton.IdleIconRightImage = null;
            this.reportDetailPrintButton.IndicateFocus = false;
            this.reportDetailPrintButton.Location = new System.Drawing.Point(869, 310);
            this.reportDetailPrintButton.Name = "reportDetailPrintButton";
            stateProperties31.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties31.BorderRadius = 3;
            stateProperties31.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties31.BorderThickness = 1;
            stateProperties31.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties31.ForeColor = System.Drawing.Color.White;
            stateProperties31.IconLeftImage = null;
            stateProperties31.IconRightImage = null;
            this.reportDetailPrintButton.onHoverState = stateProperties31;
            stateProperties32.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties32.BorderRadius = 3;
            stateProperties32.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties32.BorderThickness = 1;
            stateProperties32.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties32.ForeColor = System.Drawing.Color.White;
            stateProperties32.IconLeftImage = null;
            stateProperties32.IconRightImage = null;
            this.reportDetailPrintButton.OnPressedState = stateProperties32;
            this.reportDetailPrintButton.Size = new System.Drawing.Size(141, 33);
            this.reportDetailPrintButton.TabIndex = 0;
            this.reportDetailPrintButton.TabStop = false;
            this.reportDetailPrintButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reportDetailPrintButton.TextMarginLeft = 0;
            this.reportDetailPrintButton.UseDefaultRadiusAndThickness = true;
            this.reportDetailPrintButton.Click += new System.EventHandler(this.reportDetailPrintButton_Click);
            // 
            // sellItemPage
            // 
            this.sellItemPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.sellItemPage.Controls.Add(this.bunifuLabel19);
            this.sellItemPage.Controls.Add(this.serialNoTextBox);
            this.sellItemPage.Controls.Add(this.panelAddDetail);
            this.sellItemPage.Controls.Add(this.reportDataTableGirdView);
            this.sellItemPage.Controls.Add(this.sampleTextBox);
            this.sellItemPage.Controls.Add(this.customerTextBox);
            this.sellItemPage.Controls.Add(this.currentTimebox);
            this.sellItemPage.Controls.Add(this.bunifuLabel14);
            this.sellItemPage.Controls.Add(this.bunifuLabel11);
            this.sellItemPage.Controls.Add(this.bunifuLabel13);
            this.sellItemPage.Controls.Add(this.customerAddressTextBox);
            this.sellItemPage.Controls.Add(this.bunifuLabel12);
            this.sellItemPage.Controls.Add(this.bunifuLabel10);
            this.sellItemPage.Controls.Add(this.bunifuLabel9);
            this.sellItemPage.Controls.Add(this.bunifuLabel8);
            this.sellItemPage.Controls.Add(this.bunifuLabel7);
            this.sellItemPage.Controls.Add(this.bunifuLabel6);
            this.sellItemPage.Controls.Add(this.bunifuLabel4);
            this.sellItemPage.Controls.Add(this.bunifuLabel3);
            this.sellItemPage.Controls.Add(this.bunifuLabel2);
            this.sellItemPage.Controls.Add(this.seriesNoTexBox);
            this.sellItemPage.Controls.Add(this.bunifuLabel1);
            this.sellItemPage.Controls.Add(this.label3);
            this.sellItemPage.Controls.Add(this.bunifuLabel18);
            this.sellItemPage.Controls.Add(this.customerMobileTextBox);
            this.sellItemPage.Controls.Add(this.bunifuMetroTextbox4);
            this.sellItemPage.Controls.Add(this.bunifuButton13);
            this.sellItemPage.Controls.Add(this.startButton);
            this.sellItemPage.Controls.Add(this.startStopTimerButton);
            this.sellItemPage.Controls.Add(this.goldFineTextBox);
            this.sellItemPage.Controls.Add(this.customerRate);
            this.sellItemPage.Controls.Add(this.tounchWeightTextBox);
            this.sellItemPage.Controls.Add(this.originalWeightTextBox);
            this.sellItemPage.Controls.Add(this.goldKaratTextBox);
            this.sellItemPage.Controls.Add(this.goldPurityTextBox);
            this.sellItemPage.Controls.Add(this.reportImageArea);
            this.sellItemPage.Controls.Add(this.reportDatePicker);
            this.sellItemPage.Controls.Add(this.reportSelectButton);
            this.sellItemPage.Controls.Add(this.reportScanImageButton);
            this.sellItemPage.Controls.Add(this.adddetailButton);
            this.sellItemPage.Controls.Add(this.reportPrintButton);
            this.sellItemPage.Controls.Add(this.reportSaveButton);
            this.sellItemPage.Controls.Add(this.reportModifyButton);
            this.sellItemPage.Controls.Add(this.reportNewButton);
            this.sellItemPage.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.sellItemPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.sellItemPage.Location = new System.Drawing.Point(4, 4);
            this.sellItemPage.Name = "sellItemPage";
            this.sellItemPage.Padding = new System.Windows.Forms.Padding(3);
            this.sellItemPage.Size = new System.Drawing.Size(1041, 728);
            this.sellItemPage.TabIndex = 2;
            this.sellItemPage.Text = "sellItem";
            this.sellItemPage.Click += new System.EventHandler(this.sellItemPage_Click);
            // 
            // bunifuLabel19
            // 
            this.bunifuLabel19.AutoEllipsis = false;
            this.bunifuLabel19.CursorType = null;
            this.bunifuLabel19.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel19.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel19.Location = new System.Drawing.Point(522, 195);
            this.bunifuLabel19.Name = "bunifuLabel19";
            this.bunifuLabel19.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel19.Size = new System.Drawing.Size(69, 23);
            this.bunifuLabel19.TabIndex = 166;
            this.bunifuLabel19.TabStop = false;
            this.bunifuLabel19.Text = "Gold Fine";
            this.bunifuLabel19.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel19.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel19.Click += new System.EventHandler(this.bunifuLabel19_Click);
            // 
            // serialNoTextBox
            // 
            this.serialNoTextBox.AutoEllipsis = false;
            this.serialNoTextBox.CursorType = null;
            this.serialNoTextBox.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialNoTextBox.ForeColor = System.Drawing.Color.White;
            this.serialNoTextBox.Location = new System.Drawing.Point(814, 34);
            this.serialNoTextBox.Name = "serialNoTextBox";
            this.serialNoTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.serialNoTextBox.Size = new System.Drawing.Size(182, 39);
            this.serialNoTextBox.TabIndex = 164;
            this.serialNoTextBox.TabStop = false;
            this.serialNoTextBox.Text = "Serial number";
            this.serialNoTextBox.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.serialNoTextBox.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.serialNoTextBox.Click += new System.EventHandler(this.serialNoTextBox_Click);
            // 
            // panelAddDetail
            // 
            this.panelAddDetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.panelAddDetail.Controls.Add(this.bunifuLabel88);
            this.panelAddDetail.Controls.Add(this.totalPercentageLabel);
            this.panelAddDetail.Controls.Add(this.bunifuLabel23);
            this.panelAddDetail.Controls.Add(this.bunifuLabel45);
            this.panelAddDetail.Controls.Add(this.bunifuLabel46);
            this.panelAddDetail.Controls.Add(this.reportDetailGalliumTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel47);
            this.panelAddDetail.Controls.Add(this.reportDetailRehniumTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel48);
            this.panelAddDetail.Controls.Add(this.reportDetailOsmiumTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel49);
            this.panelAddDetail.Controls.Add(this.reportDetailManganeseTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel50);
            this.panelAddDetail.Controls.Add(this.reportDetailTungustanTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel51);
            this.panelAddDetail.Controls.Add(this.reportDetailRutheniumTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel52);
            this.panelAddDetail.Controls.Add(this.reportDetailTitaniumTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel53);
            this.panelAddDetail.Controls.Add(this.reportDetailIndiumTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel54);
            this.panelAddDetail.Controls.Add(this.reportDetailNickelTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel55);
            this.panelAddDetail.Controls.Add(this.reportDetailBismuthTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel56);
            this.panelAddDetail.Controls.Add(this.reportDetailChromiumTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel57);
            this.panelAddDetail.Controls.Add(this.reportDetailLeadTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel58);
            this.panelAddDetail.Controls.Add(this.reportDetailTinTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel59);
            this.panelAddDetail.Controls.Add(this.reportDetailIronTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel60);
            this.panelAddDetail.Controls.Add(this.reportDetailAntimonyTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel61);
            this.panelAddDetail.Controls.Add(this.reportDetailPalladiumTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel62);
            this.panelAddDetail.Controls.Add(this.reportDetailPlatinumTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel63);
            this.panelAddDetail.Controls.Add(this.reportDetailCobaltTextBox);
            this.panelAddDetail.Controls.Add(this.reportDetailRhodiumTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel64);
            this.panelAddDetail.Controls.Add(this.reportDetailIridiumTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel65);
            this.panelAddDetail.Controls.Add(this.reportDetailCadmiumTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel66);
            this.panelAddDetail.Controls.Add(this.reportDetailZincTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel67);
            this.panelAddDetail.Controls.Add(this.reportDetailCopperTextBox);
            this.panelAddDetail.Controls.Add(this.bunifuLabel68);
            this.panelAddDetail.Controls.Add(this.reportDetailSilverTextBox);
            this.panelAddDetail.Location = new System.Drawing.Point(34, 391);
            this.panelAddDetail.MaximumSize = new System.Drawing.Size(992, 324);
            this.panelAddDetail.MinimumSize = new System.Drawing.Size(992, 12);
            this.panelAddDetail.Name = "panelAddDetail";
            this.panelAddDetail.Size = new System.Drawing.Size(992, 12);
            this.panelAddDetail.TabIndex = 0;
            this.panelAddDetail.Paint += new System.Windows.Forms.PaintEventHandler(this.panelAddDetail_Paint);
            // 
            // bunifuLabel88
            // 
            this.bunifuLabel88.AutoEllipsis = false;
            this.bunifuLabel88.CursorType = null;
            this.bunifuLabel88.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel88.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel88.Location = new System.Drawing.Point(194, 185);
            this.bunifuLabel88.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel88.Name = "bunifuLabel88";
            this.bunifuLabel88.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel88.Size = new System.Drawing.Size(73, 23);
            this.bunifuLabel88.TabIndex = 197;
            this.bunifuLabel88.TabStop = false;
            this.bunifuLabel88.Text = "Iridium(Ir)";
            this.bunifuLabel88.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel88.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // totalPercentageLabel
            // 
            this.totalPercentageLabel.AutoEllipsis = false;
            this.totalPercentageLabel.CursorType = null;
            this.totalPercentageLabel.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalPercentageLabel.ForeColor = System.Drawing.Color.Red;
            this.totalPercentageLabel.Location = new System.Drawing.Point(765, 10);
            this.totalPercentageLabel.Name = "totalPercentageLabel";
            this.totalPercentageLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.totalPercentageLabel.Size = new System.Drawing.Size(0, 0);
            this.totalPercentageLabel.TabIndex = 196;
            this.totalPercentageLabel.TabStop = false;
            this.totalPercentageLabel.Text = null;
            this.totalPercentageLabel.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.totalPercentageLabel.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.totalPercentageLabel.Click += new System.EventHandler(this.bunifuLabel22_Click);
            // 
            // bunifuLabel23
            // 
            this.bunifuLabel23.AutoEllipsis = false;
            this.bunifuLabel23.CursorType = null;
            this.bunifuLabel23.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel23.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel23.Location = new System.Drawing.Point(636, 20);
            this.bunifuLabel23.Name = "bunifuLabel23";
            this.bunifuLabel23.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel23.Size = new System.Drawing.Size(125, 23);
            this.bunifuLabel23.TabIndex = 195;
            this.bunifuLabel23.TabStop = false;
            this.bunifuLabel23.Text = "Total Percentage :";
            this.bunifuLabel23.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel23.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel23.Click += new System.EventHandler(this.bunifuLabel23_Click_1);
            // 
            // bunifuLabel45
            // 
            this.bunifuLabel45.AutoEllipsis = false;
            this.bunifuLabel45.CursorType = null;
            this.bunifuLabel45.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel45.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel45.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bunifuLabel45.Location = new System.Drawing.Point(34, 53);
            this.bunifuLabel45.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel45.Name = "bunifuLabel45";
            this.bunifuLabel45.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel45.Size = new System.Drawing.Size(71, 23);
            this.bunifuLabel45.TabIndex = 0;
            this.bunifuLabel45.TabStop = false;
            this.bunifuLabel45.Text = "Silver(Ag)";
            this.bunifuLabel45.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.bunifuLabel45.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel46
            // 
            this.bunifuLabel46.AutoEllipsis = false;
            this.bunifuLabel46.CursorType = null;
            this.bunifuLabel46.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel46.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel46.Location = new System.Drawing.Point(503, 251);
            this.bunifuLabel46.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel46.Name = "bunifuLabel46";
            this.bunifuLabel46.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel46.Size = new System.Drawing.Size(77, 23);
            this.bunifuLabel46.TabIndex = 0;
            this.bunifuLabel46.TabStop = false;
            this.bunifuLabel46.Text = "Cobalt(Co)";
            this.bunifuLabel46.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.bunifuLabel46.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailGalliumTextBox
            // 
            this.reportDetailGalliumTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailGalliumTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailGalliumTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailGalliumTextBox.BorderThickness = 1;
            this.reportDetailGalliumTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailGalliumTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailGalliumTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailGalliumTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailGalliumTextBox.isPassword = false;
            this.reportDetailGalliumTextBox.Location = new System.Drawing.Point(807, 279);
            this.reportDetailGalliumTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailGalliumTextBox.MaxLength = 32767;
            this.reportDetailGalliumTextBox.Name = "reportDetailGalliumTextBox";
            this.reportDetailGalliumTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailGalliumTextBox.TabIndex = 34;
            this.reportDetailGalliumTextBox.Text = "0.0";
            this.reportDetailGalliumTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailGalliumTextBox.OnValueChanged += new System.EventHandler(this.reportDetailGalliumTextBox_OnValueChanged);
            this.reportDetailGalliumTextBox.Enter += new System.EventHandler(this.reportDetailGalliumTextBox_Enter);
            this.reportDetailGalliumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailGalliumTextBox.Leave += new System.EventHandler(this.reportDetailGalliumTextBox_Leave);
            // 
            // bunifuLabel47
            // 
            this.bunifuLabel47.AutoEllipsis = false;
            this.bunifuLabel47.CursorType = null;
            this.bunifuLabel47.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel47.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel47.Location = new System.Drawing.Point(807, 251);
            this.bunifuLabel47.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel47.Name = "bunifuLabel47";
            this.bunifuLabel47.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel47.Size = new System.Drawing.Size(86, 23);
            this.bunifuLabel47.TabIndex = 0;
            this.bunifuLabel47.TabStop = false;
            this.bunifuLabel47.Text = "Gallium(Ga)";
            this.bunifuLabel47.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel47.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailRehniumTextBox
            // 
            this.reportDetailRehniumTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailRehniumTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailRehniumTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailRehniumTextBox.BorderThickness = 1;
            this.reportDetailRehniumTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailRehniumTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailRehniumTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailRehniumTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailRehniumTextBox.isPassword = false;
            this.reportDetailRehniumTextBox.Location = new System.Drawing.Point(503, 81);
            this.reportDetailRehniumTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailRehniumTextBox.MaxLength = 32767;
            this.reportDetailRehniumTextBox.Name = "reportDetailRehniumTextBox";
            this.reportDetailRehniumTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailRehniumTextBox.TabIndex = 23;
            this.reportDetailRehniumTextBox.Text = "0.0";
            this.reportDetailRehniumTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailRehniumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailRehniumTextBox.Leave += new System.EventHandler(this.reportDetailRehniumTextBox_Leave);
            // 
            // bunifuLabel48
            // 
            this.bunifuLabel48.AutoEllipsis = false;
            this.bunifuLabel48.CursorType = null;
            this.bunifuLabel48.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel48.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel48.Location = new System.Drawing.Point(503, 53);
            this.bunifuLabel48.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel48.Name = "bunifuLabel48";
            this.bunifuLabel48.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel48.Size = new System.Drawing.Size(94, 23);
            this.bunifuLabel48.TabIndex = 0;
            this.bunifuLabel48.TabStop = false;
            this.bunifuLabel48.Text = "Rehnium(Re)";
            this.bunifuLabel48.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel48.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel48.Click += new System.EventHandler(this.bunifuLabel48_Click);
            // 
            // reportDetailOsmiumTextBox
            // 
            this.reportDetailOsmiumTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailOsmiumTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailOsmiumTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailOsmiumTextBox.BorderThickness = 1;
            this.reportDetailOsmiumTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailOsmiumTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailOsmiumTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailOsmiumTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailOsmiumTextBox.isPassword = false;
            this.reportDetailOsmiumTextBox.Location = new System.Drawing.Point(348, 279);
            this.reportDetailOsmiumTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailOsmiumTextBox.MaxLength = 32767;
            this.reportDetailOsmiumTextBox.Name = "reportDetailOsmiumTextBox";
            this.reportDetailOsmiumTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailOsmiumTextBox.TabIndex = 22;
            this.reportDetailOsmiumTextBox.Text = "0.0";
            this.reportDetailOsmiumTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailOsmiumTextBox.OnValueChanged += new System.EventHandler(this.reportDetailOsmiumTextBox_OnValueChanged);
            this.reportDetailOsmiumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailOsmiumTextBox.Leave += new System.EventHandler(this.reportDetailOsmiumTextBox_Leave);
            // 
            // bunifuLabel49
            // 
            this.bunifuLabel49.AutoEllipsis = false;
            this.bunifuLabel49.CursorType = null;
            this.bunifuLabel49.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel49.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel49.Location = new System.Drawing.Point(348, 251);
            this.bunifuLabel49.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel49.Name = "bunifuLabel49";
            this.bunifuLabel49.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel49.Size = new System.Drawing.Size(92, 23);
            this.bunifuLabel49.TabIndex = 0;
            this.bunifuLabel49.TabStop = false;
            this.bunifuLabel49.Text = "Osmium(Os)";
            this.bunifuLabel49.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel49.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel49.Click += new System.EventHandler(this.bunifuLabel49_Click);
            // 
            // reportDetailManganeseTextBox
            // 
            this.reportDetailManganeseTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailManganeseTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailManganeseTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailManganeseTextBox.BorderThickness = 1;
            this.reportDetailManganeseTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailManganeseTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailManganeseTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailManganeseTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailManganeseTextBox.isPassword = false;
            this.reportDetailManganeseTextBox.Location = new System.Drawing.Point(653, 213);
            this.reportDetailManganeseTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailManganeseTextBox.MaxLength = 32767;
            this.reportDetailManganeseTextBox.Name = "reportDetailManganeseTextBox";
            this.reportDetailManganeseTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailManganeseTextBox.TabIndex = 29;
            this.reportDetailManganeseTextBox.Text = "0.0";
            this.reportDetailManganeseTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailManganeseTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailManganeseTextBox.Leave += new System.EventHandler(this.reportDetailManganeseTextBox_Leave);
            // 
            // bunifuLabel50
            // 
            this.bunifuLabel50.AutoEllipsis = false;
            this.bunifuLabel50.CursorType = null;
            this.bunifuLabel50.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel50.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel50.Location = new System.Drawing.Point(653, 185);
            this.bunifuLabel50.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel50.Name = "bunifuLabel50";
            this.bunifuLabel50.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel50.Size = new System.Drawing.Size(116, 23);
            this.bunifuLabel50.TabIndex = 0;
            this.bunifuLabel50.TabStop = false;
            this.bunifuLabel50.Text = "Manganese(Mn)";
            this.bunifuLabel50.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel50.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailTungustanTextBox
            // 
            this.reportDetailTungustanTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailTungustanTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailTungustanTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailTungustanTextBox.BorderThickness = 1;
            this.reportDetailTungustanTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailTungustanTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailTungustanTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailTungustanTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailTungustanTextBox.isPassword = false;
            this.reportDetailTungustanTextBox.Location = new System.Drawing.Point(653, 147);
            this.reportDetailTungustanTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailTungustanTextBox.MaxLength = 32767;
            this.reportDetailTungustanTextBox.Name = "reportDetailTungustanTextBox";
            this.reportDetailTungustanTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailTungustanTextBox.TabIndex = 28;
            this.reportDetailTungustanTextBox.Text = "0.0";
            this.reportDetailTungustanTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailTungustanTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailTungustanTextBox.Leave += new System.EventHandler(this.reportDetailTungustanTextBox_Leave);
            // 
            // bunifuLabel51
            // 
            this.bunifuLabel51.AutoEllipsis = false;
            this.bunifuLabel51.CursorType = null;
            this.bunifuLabel51.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel51.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel51.Location = new System.Drawing.Point(653, 119);
            this.bunifuLabel51.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel51.Name = "bunifuLabel51";
            this.bunifuLabel51.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel51.Size = new System.Drawing.Size(101, 23);
            this.bunifuLabel51.TabIndex = 0;
            this.bunifuLabel51.TabStop = false;
            this.bunifuLabel51.Text = "Tungustan(W)";
            this.bunifuLabel51.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel51.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel51.Click += new System.EventHandler(this.bunifuLabel51_Click);
            // 
            // reportDetailRutheniumTextBox
            // 
            this.reportDetailRutheniumTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailRutheniumTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailRutheniumTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailRutheniumTextBox.BorderThickness = 1;
            this.reportDetailRutheniumTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailRutheniumTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailRutheniumTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailRutheniumTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailRutheniumTextBox.isPassword = false;
            this.reportDetailRutheniumTextBox.Location = new System.Drawing.Point(194, 279);
            this.reportDetailRutheniumTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailRutheniumTextBox.MaxLength = 32767;
            this.reportDetailRutheniumTextBox.Name = "reportDetailRutheniumTextBox";
            this.reportDetailRutheniumTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailRutheniumTextBox.TabIndex = 18;
            this.reportDetailRutheniumTextBox.Text = "0.0";
            this.reportDetailRutheniumTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailRutheniumTextBox.OnValueChanged += new System.EventHandler(this.reportDetailRutheniumTextBox_OnValueChanged);
            this.reportDetailRutheniumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailRutheniumTextBox.Leave += new System.EventHandler(this.reportDetailRutheniumTextBox_Leave);
            // 
            // bunifuLabel52
            // 
            this.bunifuLabel52.AutoEllipsis = false;
            this.bunifuLabel52.CursorType = null;
            this.bunifuLabel52.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel52.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel52.Location = new System.Drawing.Point(194, 251);
            this.bunifuLabel52.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel52.Name = "bunifuLabel52";
            this.bunifuLabel52.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel52.Size = new System.Drawing.Size(109, 23);
            this.bunifuLabel52.TabIndex = 0;
            this.bunifuLabel52.TabStop = false;
            this.bunifuLabel52.Text = "Ruthenium(Ru)";
            this.bunifuLabel52.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel52.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailTitaniumTextBox
            // 
            this.reportDetailTitaniumTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailTitaniumTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailTitaniumTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailTitaniumTextBox.BorderThickness = 1;
            this.reportDetailTitaniumTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailTitaniumTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailTitaniumTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailTitaniumTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailTitaniumTextBox.isPassword = false;
            this.reportDetailTitaniumTextBox.Location = new System.Drawing.Point(807, 213);
            this.reportDetailTitaniumTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailTitaniumTextBox.MaxLength = 32767;
            this.reportDetailTitaniumTextBox.Name = "reportDetailTitaniumTextBox";
            this.reportDetailTitaniumTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailTitaniumTextBox.TabIndex = 33;
            this.reportDetailTitaniumTextBox.Text = "0.0";
            this.reportDetailTitaniumTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailTitaniumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailTitaniumTextBox.Leave += new System.EventHandler(this.reportDetailTitaniumTextBox_Leave);
            // 
            // bunifuLabel53
            // 
            this.bunifuLabel53.AutoEllipsis = false;
            this.bunifuLabel53.CursorType = null;
            this.bunifuLabel53.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel53.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel53.Location = new System.Drawing.Point(807, 185);
            this.bunifuLabel53.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel53.Name = "bunifuLabel53";
            this.bunifuLabel53.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel53.Size = new System.Drawing.Size(86, 23);
            this.bunifuLabel53.TabIndex = 0;
            this.bunifuLabel53.TabStop = false;
            this.bunifuLabel53.Text = "Titanium(Ti)";
            this.bunifuLabel53.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel53.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailIndiumTextBox
            // 
            this.reportDetailIndiumTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailIndiumTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailIndiumTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailIndiumTextBox.BorderThickness = 1;
            this.reportDetailIndiumTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailIndiumTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailIndiumTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailIndiumTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailIndiumTextBox.isPassword = false;
            this.reportDetailIndiumTextBox.Location = new System.Drawing.Point(348, 213);
            this.reportDetailIndiumTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailIndiumTextBox.MaxLength = 32767;
            this.reportDetailIndiumTextBox.Name = "reportDetailIndiumTextBox";
            this.reportDetailIndiumTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailIndiumTextBox.TabIndex = 21;
            this.reportDetailIndiumTextBox.Text = "0.0";
            this.reportDetailIndiumTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailIndiumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailIndiumTextBox.Leave += new System.EventHandler(this.reportDetailIndiumTextBox_Leave);
            // 
            // bunifuLabel54
            // 
            this.bunifuLabel54.AutoEllipsis = false;
            this.bunifuLabel54.CursorType = null;
            this.bunifuLabel54.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel54.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel54.Location = new System.Drawing.Point(348, 185);
            this.bunifuLabel54.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel54.Name = "bunifuLabel54";
            this.bunifuLabel54.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel54.Size = new System.Drawing.Size(75, 23);
            this.bunifuLabel54.TabIndex = 0;
            this.bunifuLabel54.TabStop = false;
            this.bunifuLabel54.Text = "Indium(In)";
            this.bunifuLabel54.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel54.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailNickelTextBox
            // 
            this.reportDetailNickelTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailNickelTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailNickelTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailNickelTextBox.BorderThickness = 1;
            this.reportDetailNickelTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailNickelTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailNickelTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailNickelTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailNickelTextBox.isPassword = false;
            this.reportDetailNickelTextBox.Location = new System.Drawing.Point(194, 81);
            this.reportDetailNickelTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailNickelTextBox.MaxLength = 32767;
            this.reportDetailNickelTextBox.Name = "reportDetailNickelTextBox";
            this.reportDetailNickelTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailNickelTextBox.TabIndex = 15;
            this.reportDetailNickelTextBox.Text = "0.0";
            this.reportDetailNickelTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailNickelTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailNickelTextBox.Leave += new System.EventHandler(this.reportDetailNickelTextBox_Leave);
            // 
            // bunifuLabel55
            // 
            this.bunifuLabel55.AutoEllipsis = false;
            this.bunifuLabel55.CursorType = null;
            this.bunifuLabel55.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel55.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel55.Location = new System.Drawing.Point(194, 53);
            this.bunifuLabel55.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel55.Name = "bunifuLabel55";
            this.bunifuLabel55.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel55.Size = new System.Drawing.Size(72, 23);
            this.bunifuLabel55.TabIndex = 0;
            this.bunifuLabel55.TabStop = false;
            this.bunifuLabel55.Text = "Nickel(Ni)";
            this.bunifuLabel55.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel55.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailBismuthTextBox
            // 
            this.reportDetailBismuthTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailBismuthTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailBismuthTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailBismuthTextBox.BorderThickness = 1;
            this.reportDetailBismuthTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailBismuthTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailBismuthTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailBismuthTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailBismuthTextBox.isPassword = false;
            this.reportDetailBismuthTextBox.Location = new System.Drawing.Point(653, 279);
            this.reportDetailBismuthTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailBismuthTextBox.MaxLength = 32767;
            this.reportDetailBismuthTextBox.Name = "reportDetailBismuthTextBox";
            this.reportDetailBismuthTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailBismuthTextBox.TabIndex = 30;
            this.reportDetailBismuthTextBox.Text = "0.0";
            this.reportDetailBismuthTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailBismuthTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailBismuthTextBox.Leave += new System.EventHandler(this.reportDetailBismuthTextBox_Leave);
            // 
            // bunifuLabel56
            // 
            this.bunifuLabel56.AutoEllipsis = false;
            this.bunifuLabel56.CursorType = null;
            this.bunifuLabel56.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel56.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel56.Location = new System.Drawing.Point(653, 251);
            this.bunifuLabel56.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel56.Name = "bunifuLabel56";
            this.bunifuLabel56.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel56.Size = new System.Drawing.Size(83, 23);
            this.bunifuLabel56.TabIndex = 0;
            this.bunifuLabel56.TabStop = false;
            this.bunifuLabel56.Text = "Bismuth(Bi)";
            this.bunifuLabel56.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel56.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailChromiumTextBox
            // 
            this.reportDetailChromiumTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailChromiumTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailChromiumTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailChromiumTextBox.BorderThickness = 1;
            this.reportDetailChromiumTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailChromiumTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailChromiumTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailChromiumTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailChromiumTextBox.isPassword = false;
            this.reportDetailChromiumTextBox.Location = new System.Drawing.Point(807, 147);
            this.reportDetailChromiumTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailChromiumTextBox.MaxLength = 32767;
            this.reportDetailChromiumTextBox.Name = "reportDetailChromiumTextBox";
            this.reportDetailChromiumTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailChromiumTextBox.TabIndex = 32;
            this.reportDetailChromiumTextBox.Text = "0.0";
            this.reportDetailChromiumTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailChromiumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailChromiumTextBox.Leave += new System.EventHandler(this.reportDetailChromiumTextBox_Leave);
            // 
            // bunifuLabel57
            // 
            this.bunifuLabel57.AutoEllipsis = false;
            this.bunifuLabel57.CursorType = null;
            this.bunifuLabel57.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel57.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel57.Location = new System.Drawing.Point(807, 119);
            this.bunifuLabel57.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel57.Name = "bunifuLabel57";
            this.bunifuLabel57.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel57.Size = new System.Drawing.Size(104, 23);
            this.bunifuLabel57.TabIndex = 0;
            this.bunifuLabel57.TabStop = false;
            this.bunifuLabel57.Text = "Chromium(Cr)";
            this.bunifuLabel57.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel57.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailLeadTextBox
            // 
            this.reportDetailLeadTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailLeadTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailLeadTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailLeadTextBox.BorderThickness = 1;
            this.reportDetailLeadTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailLeadTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailLeadTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailLeadTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailLeadTextBox.isPassword = false;
            this.reportDetailLeadTextBox.Location = new System.Drawing.Point(348, 147);
            this.reportDetailLeadTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailLeadTextBox.MaxLength = 32767;
            this.reportDetailLeadTextBox.Name = "reportDetailLeadTextBox";
            this.reportDetailLeadTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailLeadTextBox.TabIndex = 20;
            this.reportDetailLeadTextBox.Text = "0.0";
            this.reportDetailLeadTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailLeadTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailLeadTextBox.Leave += new System.EventHandler(this.reportDetailLeadTextBox_Leave);
            // 
            // bunifuLabel58
            // 
            this.bunifuLabel58.AutoEllipsis = false;
            this.bunifuLabel58.CursorType = null;
            this.bunifuLabel58.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel58.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel58.Location = new System.Drawing.Point(348, 119);
            this.bunifuLabel58.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel58.Name = "bunifuLabel58";
            this.bunifuLabel58.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel58.Size = new System.Drawing.Size(64, 23);
            this.bunifuLabel58.TabIndex = 0;
            this.bunifuLabel58.TabStop = false;
            this.bunifuLabel58.Text = "Lead(Pb)";
            this.bunifuLabel58.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel58.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailTinTextBox
            // 
            this.reportDetailTinTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailTinTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailTinTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailTinTextBox.BorderThickness = 1;
            this.reportDetailTinTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailTinTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailTinTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailTinTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailTinTextBox.isPassword = false;
            this.reportDetailTinTextBox.Location = new System.Drawing.Point(348, 81);
            this.reportDetailTinTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailTinTextBox.MaxLength = 32767;
            this.reportDetailTinTextBox.Name = "reportDetailTinTextBox";
            this.reportDetailTinTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailTinTextBox.TabIndex = 19;
            this.reportDetailTinTextBox.Text = "0.0";
            this.reportDetailTinTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailTinTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailTinTextBox.Leave += new System.EventHandler(this.reportDetailTinTextBox_Leave);
            // 
            // bunifuLabel59
            // 
            this.bunifuLabel59.AutoEllipsis = false;
            this.bunifuLabel59.CursorType = null;
            this.bunifuLabel59.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel59.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel59.Location = new System.Drawing.Point(348, 53);
            this.bunifuLabel59.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel59.Name = "bunifuLabel59";
            this.bunifuLabel59.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel59.Size = new System.Drawing.Size(52, 23);
            this.bunifuLabel59.TabIndex = 0;
            this.bunifuLabel59.TabStop = false;
            this.bunifuLabel59.Text = "Tin(Sn)";
            this.bunifuLabel59.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel59.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailIronTextBox
            // 
            this.reportDetailIronTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailIronTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailIronTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailIronTextBox.BorderThickness = 1;
            this.reportDetailIronTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailIronTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailIronTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailIronTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailIronTextBox.isPassword = false;
            this.reportDetailIronTextBox.Location = new System.Drawing.Point(503, 147);
            this.reportDetailIronTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailIronTextBox.MaxLength = 32767;
            this.reportDetailIronTextBox.Name = "reportDetailIronTextBox";
            this.reportDetailIronTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailIronTextBox.TabIndex = 24;
            this.reportDetailIronTextBox.Text = "0.0";
            this.reportDetailIronTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailIronTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailIronTextBox.Leave += new System.EventHandler(this.reportDetailIronTextBox_Leave);
            // 
            // bunifuLabel60
            // 
            this.bunifuLabel60.AutoEllipsis = false;
            this.bunifuLabel60.CursorType = null;
            this.bunifuLabel60.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel60.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel60.Location = new System.Drawing.Point(503, 119);
            this.bunifuLabel60.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel60.Name = "bunifuLabel60";
            this.bunifuLabel60.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel60.Size = new System.Drawing.Size(57, 23);
            this.bunifuLabel60.TabIndex = 0;
            this.bunifuLabel60.TabStop = false;
            this.bunifuLabel60.Text = "Iron(Fe)";
            this.bunifuLabel60.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel60.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailAntimonyTextBox
            // 
            this.reportDetailAntimonyTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailAntimonyTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailAntimonyTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailAntimonyTextBox.BorderThickness = 1;
            this.reportDetailAntimonyTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailAntimonyTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailAntimonyTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailAntimonyTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailAntimonyTextBox.isPassword = false;
            this.reportDetailAntimonyTextBox.Location = new System.Drawing.Point(807, 81);
            this.reportDetailAntimonyTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailAntimonyTextBox.MaxLength = 32767;
            this.reportDetailAntimonyTextBox.Name = "reportDetailAntimonyTextBox";
            this.reportDetailAntimonyTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailAntimonyTextBox.TabIndex = 31;
            this.reportDetailAntimonyTextBox.Text = "0.0";
            this.reportDetailAntimonyTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailAntimonyTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailAntimonyTextBox.Leave += new System.EventHandler(this.reportDetailAntimonyTextBox_Leave);
            // 
            // bunifuLabel61
            // 
            this.bunifuLabel61.AutoEllipsis = false;
            this.bunifuLabel61.CursorType = null;
            this.bunifuLabel61.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel61.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel61.Location = new System.Drawing.Point(807, 53);
            this.bunifuLabel61.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel61.Name = "bunifuLabel61";
            this.bunifuLabel61.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel61.Size = new System.Drawing.Size(99, 23);
            this.bunifuLabel61.TabIndex = 0;
            this.bunifuLabel61.TabStop = false;
            this.bunifuLabel61.Text = "Antimony(Sb)";
            this.bunifuLabel61.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel61.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailPalladiumTextBox
            // 
            this.reportDetailPalladiumTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailPalladiumTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailPalladiumTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailPalladiumTextBox.BorderThickness = 1;
            this.reportDetailPalladiumTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailPalladiumTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailPalladiumTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailPalladiumTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailPalladiumTextBox.isPassword = false;
            this.reportDetailPalladiumTextBox.Location = new System.Drawing.Point(194, 147);
            this.reportDetailPalladiumTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailPalladiumTextBox.MaxLength = 32767;
            this.reportDetailPalladiumTextBox.Name = "reportDetailPalladiumTextBox";
            this.reportDetailPalladiumTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailPalladiumTextBox.TabIndex = 16;
            this.reportDetailPalladiumTextBox.Text = "0.0";
            this.reportDetailPalladiumTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailPalladiumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailPalladiumTextBox.Leave += new System.EventHandler(this.reportDetailPalladiumTextBox_Leave);
            // 
            // bunifuLabel62
            // 
            this.bunifuLabel62.AutoEllipsis = false;
            this.bunifuLabel62.CursorType = null;
            this.bunifuLabel62.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel62.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel62.Location = new System.Drawing.Point(194, 119);
            this.bunifuLabel62.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel62.Name = "bunifuLabel62";
            this.bunifuLabel62.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel62.Size = new System.Drawing.Size(100, 23);
            this.bunifuLabel62.TabIndex = 0;
            this.bunifuLabel62.TabStop = false;
            this.bunifuLabel62.Text = "Palladium(Pd)";
            this.bunifuLabel62.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel62.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailPlatinumTextBox
            // 
            this.reportDetailPlatinumTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailPlatinumTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailPlatinumTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailPlatinumTextBox.BorderThickness = 1;
            this.reportDetailPlatinumTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailPlatinumTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailPlatinumTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailPlatinumTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailPlatinumTextBox.isPassword = false;
            this.reportDetailPlatinumTextBox.Location = new System.Drawing.Point(653, 81);
            this.reportDetailPlatinumTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailPlatinumTextBox.MaxLength = 32767;
            this.reportDetailPlatinumTextBox.Name = "reportDetailPlatinumTextBox";
            this.reportDetailPlatinumTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailPlatinumTextBox.TabIndex = 27;
            this.reportDetailPlatinumTextBox.Text = "0.0";
            this.reportDetailPlatinumTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailPlatinumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailPlatinumTextBox.Leave += new System.EventHandler(this.reportDetailPlatinumTextBox_Leave);
            // 
            // bunifuLabel63
            // 
            this.bunifuLabel63.AutoEllipsis = false;
            this.bunifuLabel63.CursorType = null;
            this.bunifuLabel63.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel63.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel63.Location = new System.Drawing.Point(653, 53);
            this.bunifuLabel63.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel63.Name = "bunifuLabel63";
            this.bunifuLabel63.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel63.Size = new System.Drawing.Size(89, 23);
            this.bunifuLabel63.TabIndex = 0;
            this.bunifuLabel63.TabStop = false;
            this.bunifuLabel63.Text = "Platinum(Pt)";
            this.bunifuLabel63.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel63.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel63.Click += new System.EventHandler(this.bunifuLabel63_Click);
            // 
            // reportDetailCobaltTextBox
            // 
            this.reportDetailCobaltTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailCobaltTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailCobaltTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailCobaltTextBox.BorderThickness = 1;
            this.reportDetailCobaltTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailCobaltTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailCobaltTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailCobaltTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailCobaltTextBox.isPassword = false;
            this.reportDetailCobaltTextBox.Location = new System.Drawing.Point(503, 279);
            this.reportDetailCobaltTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailCobaltTextBox.MaxLength = 32767;
            this.reportDetailCobaltTextBox.Name = "reportDetailCobaltTextBox";
            this.reportDetailCobaltTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailCobaltTextBox.TabIndex = 26;
            this.reportDetailCobaltTextBox.Text = "0.0";
            this.reportDetailCobaltTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailCobaltTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailCobaltTextBox.Leave += new System.EventHandler(this.reportDetailCobaltTextBox_Leave);
            // 
            // reportDetailRhodiumTextBox
            // 
            this.reportDetailRhodiumTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailRhodiumTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailRhodiumTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailRhodiumTextBox.BorderThickness = 1;
            this.reportDetailRhodiumTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailRhodiumTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailRhodiumTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailRhodiumTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailRhodiumTextBox.isPassword = false;
            this.reportDetailRhodiumTextBox.Location = new System.Drawing.Point(503, 213);
            this.reportDetailRhodiumTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailRhodiumTextBox.MaxLength = 32767;
            this.reportDetailRhodiumTextBox.Name = "reportDetailRhodiumTextBox";
            this.reportDetailRhodiumTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailRhodiumTextBox.TabIndex = 25;
            this.reportDetailRhodiumTextBox.Text = "0.0";
            this.reportDetailRhodiumTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailRhodiumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailRhodiumTextBox.Leave += new System.EventHandler(this.reportDetailRhodiumTextBox_Leave);
            // 
            // bunifuLabel64
            // 
            this.bunifuLabel64.AutoEllipsis = false;
            this.bunifuLabel64.CursorType = null;
            this.bunifuLabel64.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel64.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel64.Location = new System.Drawing.Point(503, 185);
            this.bunifuLabel64.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel64.Name = "bunifuLabel64";
            this.bunifuLabel64.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel64.Size = new System.Drawing.Size(96, 23);
            this.bunifuLabel64.TabIndex = 0;
            this.bunifuLabel64.TabStop = false;
            this.bunifuLabel64.Text = "Rhodium(Rh)";
            this.bunifuLabel64.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel64.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailIridiumTextBox
            // 
            this.reportDetailIridiumTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailIridiumTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailIridiumTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailIridiumTextBox.BorderThickness = 1;
            this.reportDetailIridiumTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailIridiumTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailIridiumTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailIridiumTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailIridiumTextBox.isPassword = false;
            this.reportDetailIridiumTextBox.Location = new System.Drawing.Point(194, 213);
            this.reportDetailIridiumTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailIridiumTextBox.MaxLength = 32767;
            this.reportDetailIridiumTextBox.Name = "reportDetailIridiumTextBox";
            this.reportDetailIridiumTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailIridiumTextBox.TabIndex = 17;
            this.reportDetailIridiumTextBox.Text = "0.0";
            this.reportDetailIridiumTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailIridiumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailIridiumTextBox.Leave += new System.EventHandler(this.reportDetailIridiumTextBox_Leave);
            // 
            // bunifuLabel65
            // 
            this.bunifuLabel65.AutoEllipsis = false;
            this.bunifuLabel65.CursorType = null;
            this.bunifuLabel65.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel65.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel65.Location = new System.Drawing.Point(1253, 91);
            this.bunifuLabel65.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel65.Name = "bunifuLabel65";
            this.bunifuLabel65.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel65.Size = new System.Drawing.Size(73, 23);
            this.bunifuLabel65.TabIndex = 0;
            this.bunifuLabel65.TabStop = false;
            this.bunifuLabel65.Text = "Iridium(Ir)";
            this.bunifuLabel65.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel65.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailCadmiumTextBox
            // 
            this.reportDetailCadmiumTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailCadmiumTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailCadmiumTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailCadmiumTextBox.BorderThickness = 1;
            this.reportDetailCadmiumTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailCadmiumTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailCadmiumTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailCadmiumTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailCadmiumTextBox.isPassword = false;
            this.reportDetailCadmiumTextBox.Location = new System.Drawing.Point(34, 279);
            this.reportDetailCadmiumTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailCadmiumTextBox.MaxLength = 32767;
            this.reportDetailCadmiumTextBox.Name = "reportDetailCadmiumTextBox";
            this.reportDetailCadmiumTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailCadmiumTextBox.TabIndex = 14;
            this.reportDetailCadmiumTextBox.Text = "0.0";
            this.reportDetailCadmiumTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailCadmiumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailCadmiumTextBox.Leave += new System.EventHandler(this.reportDetailCadmiumTextBox_Leave);
            // 
            // bunifuLabel66
            // 
            this.bunifuLabel66.AutoEllipsis = false;
            this.bunifuLabel66.CursorType = null;
            this.bunifuLabel66.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel66.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel66.Location = new System.Drawing.Point(34, 251);
            this.bunifuLabel66.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel66.Name = "bunifuLabel66";
            this.bunifuLabel66.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel66.Size = new System.Drawing.Size(100, 23);
            this.bunifuLabel66.TabIndex = 0;
            this.bunifuLabel66.TabStop = false;
            this.bunifuLabel66.Text = "Cadmium(Cd)";
            this.bunifuLabel66.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel66.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailZincTextBox
            // 
            this.reportDetailZincTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailZincTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailZincTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailZincTextBox.BorderThickness = 1;
            this.reportDetailZincTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailZincTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailZincTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailZincTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailZincTextBox.isPassword = false;
            this.reportDetailZincTextBox.Location = new System.Drawing.Point(34, 213);
            this.reportDetailZincTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailZincTextBox.MaxLength = 32767;
            this.reportDetailZincTextBox.Name = "reportDetailZincTextBox";
            this.reportDetailZincTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailZincTextBox.TabIndex = 13;
            this.reportDetailZincTextBox.Text = "0.0";
            this.reportDetailZincTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailZincTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailZincTextBox.Leave += new System.EventHandler(this.reportDetailZincTextBox_Leave);
            // 
            // bunifuLabel67
            // 
            this.bunifuLabel67.AutoEllipsis = false;
            this.bunifuLabel67.CursorType = null;
            this.bunifuLabel67.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel67.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel67.Location = new System.Drawing.Point(34, 185);
            this.bunifuLabel67.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel67.Name = "bunifuLabel67";
            this.bunifuLabel67.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel67.Size = new System.Drawing.Size(60, 23);
            this.bunifuLabel67.TabIndex = 0;
            this.bunifuLabel67.TabStop = false;
            this.bunifuLabel67.Text = "Zinc(Zn)";
            this.bunifuLabel67.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel67.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailCopperTextBox
            // 
            this.reportDetailCopperTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailCopperTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailCopperTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailCopperTextBox.BorderThickness = 1;
            this.reportDetailCopperTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailCopperTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailCopperTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailCopperTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailCopperTextBox.isPassword = false;
            this.reportDetailCopperTextBox.Location = new System.Drawing.Point(34, 147);
            this.reportDetailCopperTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailCopperTextBox.MaxLength = 32767;
            this.reportDetailCopperTextBox.Name = "reportDetailCopperTextBox";
            this.reportDetailCopperTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailCopperTextBox.TabIndex = 12;
            this.reportDetailCopperTextBox.Text = "0.0";
            this.reportDetailCopperTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailCopperTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailCopperTextBox.Leave += new System.EventHandler(this.reportDetailCopperTextBox_Leave);
            // 
            // bunifuLabel68
            // 
            this.bunifuLabel68.AutoEllipsis = false;
            this.bunifuLabel68.CursorType = null;
            this.bunifuLabel68.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel68.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel68.Location = new System.Drawing.Point(34, 119);
            this.bunifuLabel68.Margin = new System.Windows.Forms.Padding(1);
            this.bunifuLabel68.Name = "bunifuLabel68";
            this.bunifuLabel68.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel68.Size = new System.Drawing.Size(83, 23);
            this.bunifuLabel68.TabIndex = 0;
            this.bunifuLabel68.TabStop = false;
            this.bunifuLabel68.Text = "Copper(Cu)";
            this.bunifuLabel68.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel68.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // reportDetailSilverTextBox
            // 
            this.reportDetailSilverTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.reportDetailSilverTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(220)))), ((int)(((byte)(243)))));
            this.reportDetailSilverTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.reportDetailSilverTextBox.BorderThickness = 1;
            this.reportDetailSilverTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.reportDetailSilverTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.reportDetailSilverTextBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDetailSilverTextBox.ForeColor = System.Drawing.Color.White;
            this.reportDetailSilverTextBox.isPassword = false;
            this.reportDetailSilverTextBox.Location = new System.Drawing.Point(34, 81);
            this.reportDetailSilverTextBox.Margin = new System.Windows.Forms.Padding(1);
            this.reportDetailSilverTextBox.MaxLength = 32767;
            this.reportDetailSilverTextBox.Name = "reportDetailSilverTextBox";
            this.reportDetailSilverTextBox.Size = new System.Drawing.Size(141, 33);
            this.reportDetailSilverTextBox.TabIndex = 11;
            this.reportDetailSilverTextBox.Text = "0.0";
            this.reportDetailSilverTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.reportDetailSilverTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportDetailSilverTextBox_KeyPress_1);
            this.reportDetailSilverTextBox.Leave += new System.EventHandler(this.reportDetailSilverTextBox_Leave);
            // 
            // reportDataTableGirdView
            // 
            this.reportDataTableGirdView.AllowUserToAddRows = false;
            this.reportDataTableGirdView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.reportDataTableGirdView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.reportDataTableGirdView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.reportDataTableGirdView.BackgroundColor = System.Drawing.Color.White;
            this.reportDataTableGirdView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reportDataTableGirdView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.reportDataTableGirdView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.reportDataTableGirdView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.reportDataTableGirdView.ColumnHeadersHeight = 50;
            this.reportDataTableGirdView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column21,
            this.Column77,
            this.Column9,
            this.Column74,
            this.Column75,
            this.Column48,
            this.Column49,
            this.Column50,
            this.Column51,
            this.Column52,
            this.Column53,
            this.Column54,
            this.Column55,
            this.Column56,
            this.Column57,
            this.Column58,
            this.Column59,
            this.Column60,
            this.Column61,
            this.Column62,
            this.Column63,
            this.Column64,
            this.Column65,
            this.Column66,
            this.Column67,
            this.Column68,
            this.Column69,
            this.Column70,
            this.Column71,
            this.Column72,
            this.Column73,
            this.Column7,
            this.Column8,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column20,
            this.Column18,
            this.Column19});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(252)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.reportDataTableGirdView.DefaultCellStyle = dataGridViewCellStyle10;
            this.reportDataTableGirdView.DoubleBuffered = true;
            this.reportDataTableGirdView.EnableHeadersVisualStyles = false;
            this.reportDataTableGirdView.GridColor = System.Drawing.Color.WhiteSmoke;
            this.reportDataTableGirdView.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            this.reportDataTableGirdView.HeaderForeColor = System.Drawing.Color.White;
            this.reportDataTableGirdView.Location = new System.Drawing.Point(34, 409);
            this.reportDataTableGirdView.Name = "reportDataTableGirdView";
            this.reportDataTableGirdView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.reportDataTableGirdView.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.reportDataTableGirdView.RowHeadersVisible = false;
            this.reportDataTableGirdView.RowHeadersWidth = 51;
            this.reportDataTableGirdView.RowTemplate.DividerHeight = 1;
            this.reportDataTableGirdView.RowTemplate.Height = 40;
            this.reportDataTableGirdView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.reportDataTableGirdView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.reportDataTableGirdView.Size = new System.Drawing.Size(991, 323);
            this.reportDataTableGirdView.TabIndex = 156;
            this.reportDataTableGirdView.TabStop = false;
            this.reportDataTableGirdView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.reportDataTableGirdView_CellClick);
            this.reportDataTableGirdView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.reportDataTableGirdView_CellContentClick);
            this.reportDataTableGirdView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.reportDataTableGirdView_CellValueChanged);
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "payment";
            this.Column21.HeaderText = "Paid/Unpaid";
            this.Column21.MinimumWidth = 6;
            this.Column21.Name = "Column21";
            this.Column21.Width = 109;
            // 
            // Column77
            // 
            this.Column77.DataPropertyName = "amount";
            this.Column77.HeaderText = "Amount";
            this.Column77.MinimumWidth = 6;
            this.Column77.Name = "Column77";
            this.Column77.ReadOnly = true;
            this.Column77.Width = 99;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "currentDate";
            this.Column9.HeaderText = "Date";
            this.Column9.MinimumWidth = 6;
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 73;
            // 
            // Column74
            // 
            this.Column74.DataPropertyName = "iconFirst";
            this.Column74.HeaderText = "FirstIcon";
            this.Column74.MinimumWidth = 6;
            this.Column74.Name = "Column74";
            this.Column74.ReadOnly = true;
            this.Column74.Visible = false;
            this.Column74.Width = 102;
            // 
            // Column75
            // 
            this.Column75.DataPropertyName = "iconLast";
            this.Column75.HeaderText = "LastIcon";
            this.Column75.MinimumWidth = 6;
            this.Column75.Name = "Column75";
            this.Column75.ReadOnly = true;
            this.Column75.Visible = false;
            // 
            // Column48
            // 
            this.Column48.DataPropertyName = "relatedImage";
            this.Column48.HeaderText = "Image";
            this.Column48.MinimumWidth = 6;
            this.Column48.Name = "Column48";
            this.Column48.ReadOnly = true;
            this.Column48.Visible = false;
            this.Column48.Width = 85;
            // 
            // Column49
            // 
            this.Column49.DataPropertyName = "iconImage";
            this.Column49.HeaderText = "IconImage";
            this.Column49.MinimumWidth = 6;
            this.Column49.Name = "Column49";
            this.Column49.ReadOnly = true;
            this.Column49.Visible = false;
            this.Column49.Width = 118;
            // 
            // Column50
            // 
            this.Column50.DataPropertyName = "Silver";
            this.Column50.HeaderText = "Silver";
            this.Column50.MinimumWidth = 6;
            this.Column50.Name = "Column50";
            this.Column50.ReadOnly = true;
            this.Column50.Visible = false;
            this.Column50.Width = 78;
            // 
            // Column51
            // 
            this.Column51.DataPropertyName = "Cobalt";
            this.Column51.HeaderText = "Cobalt";
            this.Column51.MinimumWidth = 6;
            this.Column51.Name = "Column51";
            this.Column51.ReadOnly = true;
            this.Column51.Visible = false;
            this.Column51.Width = 87;
            // 
            // Column52
            // 
            this.Column52.DataPropertyName = "Lead";
            this.Column52.HeaderText = "Lead";
            this.Column52.MinimumWidth = 6;
            this.Column52.Name = "Column52";
            this.Column52.ReadOnly = true;
            this.Column52.Visible = false;
            this.Column52.Width = 73;
            // 
            // Column53
            // 
            this.Column53.DataPropertyName = "Ruthenium";
            this.Column53.HeaderText = "Ruthenium";
            this.Column53.MinimumWidth = 6;
            this.Column53.Name = "Column53";
            this.Column53.ReadOnly = true;
            this.Column53.Visible = false;
            this.Column53.Width = 122;
            // 
            // Column54
            // 
            this.Column54.DataPropertyName = "Copper";
            this.Column54.HeaderText = "Copper";
            this.Column54.MinimumWidth = 6;
            this.Column54.Name = "Column54";
            this.Column54.ReadOnly = true;
            this.Column54.Visible = false;
            this.Column54.Width = 93;
            // 
            // Column55
            // 
            this.Column55.DataPropertyName = "Platinum";
            this.Column55.HeaderText = "Platinum";
            this.Column55.MinimumWidth = 6;
            this.Column55.Name = "Column55";
            this.Column55.ReadOnly = true;
            this.Column55.Visible = false;
            this.Column55.Width = 105;
            // 
            // Column56
            // 
            this.Column56.DataPropertyName = "Chromium";
            this.Column56.HeaderText = "Chromium";
            this.Column56.MinimumWidth = 6;
            this.Column56.Name = "Column56";
            this.Column56.ReadOnly = true;
            this.Column56.Visible = false;
            this.Column56.Width = 118;
            // 
            // Column57
            // 
            this.Column57.DataPropertyName = "Tungustan";
            this.Column57.HeaderText = "Tungustan";
            this.Column57.MinimumWidth = 6;
            this.Column57.Name = "Column57";
            this.Column57.ReadOnly = true;
            this.Column57.Visible = false;
            this.Column57.Width = 117;
            // 
            // Column58
            // 
            this.Column58.DataPropertyName = "Zinc";
            this.Column58.HeaderText = "Zinc";
            this.Column58.MinimumWidth = 6;
            this.Column58.Name = "Column58";
            this.Column58.ReadOnly = true;
            this.Column58.Visible = false;
            this.Column58.Width = 69;
            // 
            // Column59
            // 
            this.Column59.DataPropertyName = "Palladium";
            this.Column59.HeaderText = "Palladium";
            this.Column59.MinimumWidth = 6;
            this.Column59.Name = "Column59";
            this.Column59.ReadOnly = true;
            this.Column59.Visible = false;
            this.Column59.Width = 112;
            // 
            // Column60
            // 
            this.Column60.DataPropertyName = "Bismuth";
            this.Column60.HeaderText = "Bismuth";
            this.Column60.MinimumWidth = 6;
            this.Column60.Name = "Column60";
            this.Column60.ReadOnly = true;
            this.Column60.Visible = false;
            this.Column60.Width = 99;
            // 
            // Column61
            // 
            this.Column61.DataPropertyName = "Manganese";
            this.Column61.HeaderText = "Manganese";
            this.Column61.MinimumWidth = 6;
            this.Column61.Name = "Column61";
            this.Column61.ReadOnly = true;
            this.Column61.Visible = false;
            this.Column61.Width = 126;
            // 
            // Column62
            // 
            this.Column62.DataPropertyName = "Cadmium";
            this.Column62.HeaderText = "Cadmium";
            this.Column62.MinimumWidth = 6;
            this.Column62.Name = "Column62";
            this.Column62.ReadOnly = true;
            this.Column62.Visible = false;
            this.Column62.Width = 111;
            // 
            // Column63
            // 
            this.Column63.DataPropertyName = "Antimony";
            this.Column63.HeaderText = "Antimony";
            this.Column63.MinimumWidth = 6;
            this.Column63.Name = "Column63";
            this.Column63.ReadOnly = true;
            this.Column63.Visible = false;
            this.Column63.Width = 112;
            // 
            // Column64
            // 
            this.Column64.DataPropertyName = "Nickel";
            this.Column64.HeaderText = "Nickel";
            this.Column64.MinimumWidth = 6;
            this.Column64.Name = "Column64";
            this.Column64.ReadOnly = true;
            this.Column64.Visible = false;
            this.Column64.Width = 84;
            // 
            // Column65
            // 
            this.Column65.DataPropertyName = "Osmium";
            this.Column65.HeaderText = "Osmium";
            this.Column65.MinimumWidth = 6;
            this.Column65.Name = "Column65";
            this.Column65.ReadOnly = true;
            this.Column65.Visible = false;
            this.Column65.Width = 101;
            // 
            // Column66
            // 
            this.Column66.DataPropertyName = "Iridium";
            this.Column66.HeaderText = "Iridium";
            this.Column66.MinimumWidth = 6;
            this.Column66.Name = "Column66";
            this.Column66.ReadOnly = true;
            this.Column66.Visible = false;
            this.Column66.Width = 91;
            // 
            // Column67
            // 
            this.Column67.DataPropertyName = "Iron";
            this.Column67.HeaderText = "Iron";
            this.Column67.MinimumWidth = 6;
            this.Column67.Name = "Column67";
            this.Column67.ReadOnly = true;
            this.Column67.Visible = false;
            this.Column67.Width = 68;
            // 
            // Column68
            // 
            this.Column68.DataPropertyName = "Indium";
            this.Column68.HeaderText = "Indium";
            this.Column68.MinimumWidth = 6;
            this.Column68.Name = "Column68";
            this.Column68.ReadOnly = true;
            this.Column68.Visible = false;
            this.Column68.Width = 91;
            // 
            // Column69
            // 
            this.Column69.DataPropertyName = "Rehnium";
            this.Column69.HeaderText = "Rehnium";
            this.Column69.MinimumWidth = 6;
            this.Column69.Name = "Column69";
            this.Column69.ReadOnly = true;
            this.Column69.Visible = false;
            this.Column69.Width = 106;
            // 
            // Column70
            // 
            this.Column70.DataPropertyName = "Rhodium";
            this.Column70.HeaderText = "Rhodium";
            this.Column70.MinimumWidth = 6;
            this.Column70.Name = "Column70";
            this.Column70.ReadOnly = true;
            this.Column70.Visible = false;
            this.Column70.Width = 107;
            // 
            // Column71
            // 
            this.Column71.DataPropertyName = "Tin";
            this.Column71.HeaderText = "Tin";
            this.Column71.MinimumWidth = 6;
            this.Column71.Name = "Column71";
            this.Column71.ReadOnly = true;
            this.Column71.Visible = false;
            this.Column71.Width = 60;
            // 
            // Column72
            // 
            this.Column72.DataPropertyName = "Titanium";
            this.Column72.HeaderText = "Titanium";
            this.Column72.MinimumWidth = 6;
            this.Column72.Name = "Column72";
            this.Column72.ReadOnly = true;
            this.Column72.Visible = false;
            this.Column72.Width = 104;
            // 
            // Column73
            // 
            this.Column73.DataPropertyName = "Gallium";
            this.Column73.HeaderText = "Gallium";
            this.Column73.MinimumWidth = 6;
            this.Column73.Name = "Column73";
            this.Column73.ReadOnly = true;
            this.Column73.Visible = false;
            this.Column73.Width = 95;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "id";
            this.Column7.HeaderText = "Serial No";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 106;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "seriesNo";
            this.Column8.HeaderText = "Series No";
            this.Column8.MinimumWidth = 6;
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 109;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "currentTime";
            this.Column10.HeaderText = "Time";
            this.Column10.MinimumWidth = 6;
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 74;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "customerName";
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Teal;
            this.Column11.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column11.HeaderText = "Name";
            this.Column11.MinimumWidth = 6;
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 83;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "custMobile";
            this.Column12.HeaderText = "Mobile No";
            this.Column12.MinimumWidth = 6;
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 118;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "sample";
            this.Column13.HeaderText = "Sample";
            this.Column13.MinimumWidth = 6;
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 93;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "goldPurity";
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Column14.DefaultCellStyle = dataGridViewCellStyle9;
            this.Column14.HeaderText = "Purity";
            this.Column14.MinimumWidth = 6;
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 82;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "goldKarat";
            this.Column15.HeaderText = "Karat";
            this.Column15.MinimumWidth = 6;
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 77;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "orgWeight";
            this.Column16.HeaderText = "Weight";
            this.Column16.MinimumWidth = 6;
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 92;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "tounchweight";
            this.Column17.HeaderText = "Tounch Weight";
            this.Column17.MinimumWidth = 6;
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 152;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "goldFine";
            this.Column20.HeaderText = "Gold Fine";
            this.Column20.MinimumWidth = 6;
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Width = 110;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "Rate";
            this.Column18.HeaderText = "Rate";
            this.Column18.MinimumWidth = 6;
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Width = 72;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "customerAddress";
            this.Column19.HeaderText = "Address";
            this.Column19.MinimumWidth = 6;
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.Width = 97;
            // 
            // sampleTextBox
            // 
            this.sampleTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.sampleTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.sampleTextBox.Font = new System.Drawing.Font("Century Gothic", 12.5F, System.Drawing.FontStyle.Bold);
            this.sampleTextBox.ForeColor = System.Drawing.Color.Black;
            this.sampleTextBox.FormattingEnabled = true;
            this.sampleTextBox.ItemHeight = 25;
            this.sampleTextBox.Location = new System.Drawing.Point(34, 225);
            this.sampleTextBox.Name = "sampleTextBox";
            this.sampleTextBox.Size = new System.Drawing.Size(272, 33);
            this.sampleTextBox.TabIndex = 3;
            this.sampleTextBox.SelectedIndexChanged += new System.EventHandler(this.sampleTextBox_SelectedIndexChanged);
            this.sampleTextBox.Leave += new System.EventHandler(this.sampleTextBox_Leave);
            // 
            // customerTextBox
            // 
            this.customerTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.customerTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.customerTextBox.Font = new System.Drawing.Font("Century Gothic", 12.5F, System.Drawing.FontStyle.Bold);
            this.customerTextBox.ForeColor = System.Drawing.Color.Black;
            this.customerTextBox.FormattingEnabled = true;
            this.customerTextBox.ItemHeight = 25;
            this.customerTextBox.Location = new System.Drawing.Point(34, 159);
            this.customerTextBox.Name = "customerTextBox";
            this.customerTextBox.Size = new System.Drawing.Size(272, 33);
            this.customerTextBox.TabIndex = 2;
            this.customerTextBox.SelectedIndexChanged += new System.EventHandler(this.customerTextBox_SelectedIndexChanged_1);
            this.customerTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.customerTextBox_KeyDown);
            this.customerTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.customerTextBox_KeyPress);
            this.customerTextBox.Leave += new System.EventHandler(this.customerTextBox_Leave);
            // 
            // currentTimebox
            // 
            this.currentTimebox.CalendarFont = new System.Drawing.Font("Century Gothic", 7.75F, System.Drawing.FontStyle.Bold);
            this.currentTimebox.CalendarMonthBackground = System.Drawing.Color.SeaGreen;
            this.currentTimebox.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.currentTimebox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.currentTimebox.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.currentTimebox.Location = new System.Drawing.Point(493, 90);
            this.currentTimebox.Margin = new System.Windows.Forms.Padding(4);
            this.currentTimebox.MaximumSize = new System.Drawing.Size(4, 23);
            this.currentTimebox.MinimumSize = new System.Drawing.Size(180, 33);
            this.currentTimebox.Name = "currentTimebox";
            this.currentTimebox.ShowUpDown = true;
            this.currentTimebox.Size = new System.Drawing.Size(180, 33);
            this.currentTimebox.TabIndex = 0;
            this.currentTimebox.TabStop = false;
            this.currentTimebox.ValueChanged += new System.EventHandler(this.currentTimebox_ValueChanged);
            this.currentTimebox.DropDown += new System.EventHandler(this.currentTimebox_DropDown);
            this.currentTimebox.TabStopChanged += new System.EventHandler(this.currentTimebox_TabStopChanged);
            this.currentTimebox.DragDrop += new System.Windows.Forms.DragEventHandler(this.currentTimebox_DragDrop);
            this.currentTimebox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.currentTimebox_KeyPress);
            this.currentTimebox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.currentTimebox_KeyUp);
            this.currentTimebox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.currentTimebox_MouseDown);
            this.currentTimebox.MouseEnter += new System.EventHandler(this.currentTimebox_MouseEnter);
            this.currentTimebox.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.currentTimebox_PreviewKeyDown);
            // 
            // bunifuLabel14
            // 
            this.bunifuLabel14.AutoEllipsis = false;
            this.bunifuLabel14.CursorType = null;
            this.bunifuLabel14.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel14.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel14.Location = new System.Drawing.Point(567, 260);
            this.bunifuLabel14.Name = "bunifuLabel14";
            this.bunifuLabel14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel14.Size = new System.Drawing.Size(34, 23);
            this.bunifuLabel14.TabIndex = 0;
            this.bunifuLabel14.TabStop = false;
            this.bunifuLabel14.Text = "Rate";
            this.bunifuLabel14.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel14.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel14.Click += new System.EventHandler(this.bunifuLabel14_Click);
            // 
            // bunifuLabel11
            // 
            this.bunifuLabel11.AutoEllipsis = false;
            this.bunifuLabel11.CursorType = null;
            this.bunifuLabel11.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel11.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel11.Location = new System.Drawing.Point(415, 325);
            this.bunifuLabel11.Name = "bunifuLabel11";
            this.bunifuLabel11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel11.Size = new System.Drawing.Size(51, 23);
            this.bunifuLabel11.TabIndex = 0;
            this.bunifuLabel11.TabStop = false;
            this.bunifuLabel11.Text = "Mobile";
            this.bunifuLabel11.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel11.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel11.Click += new System.EventHandler(this.bunifuLabel11_Click_1);
            // 
            // bunifuLabel13
            // 
            this.bunifuLabel13.AutoEllipsis = false;
            this.bunifuLabel13.CursorType = null;
            this.bunifuLabel13.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel13.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel13.Location = new System.Drawing.Point(34, 325);
            this.bunifuLabel13.Name = "bunifuLabel13";
            this.bunifuLabel13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel13.Size = new System.Drawing.Size(59, 23);
            this.bunifuLabel13.TabIndex = 0;
            this.bunifuLabel13.TabStop = false;
            this.bunifuLabel13.Text = "Address";
            this.bunifuLabel13.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel13.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel13.Click += new System.EventHandler(this.bunifuLabel13_Click);
            // 
            // customerAddressTextBox
            // 
            this.customerAddressTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.customerAddressTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.customerAddressTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.customerAddressTextBox.BorderThickness = 1;
            this.customerAddressTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.customerAddressTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.customerAddressTextBox.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerAddressTextBox.ForeColor = System.Drawing.Color.White;
            this.customerAddressTextBox.isPassword = false;
            this.customerAddressTextBox.Location = new System.Drawing.Point(34, 355);
            this.customerAddressTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.customerAddressTextBox.MaxLength = 32767;
            this.customerAddressTextBox.Name = "customerAddressTextBox";
            this.customerAddressTextBox.Size = new System.Drawing.Size(360, 33);
            this.customerAddressTextBox.TabIndex = 0;
            this.customerAddressTextBox.TabStop = false;
            this.customerAddressTextBox.Text = "Enter Address";
            this.customerAddressTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.customerAddressTextBox.OnValueChanged += new System.EventHandler(this.bunifuMetroTextbox14_OnValueChanged);
            this.customerAddressTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.customerAddressTextBox_KeyPress);
            // 
            // bunifuLabel12
            // 
            this.bunifuLabel12.AutoEllipsis = false;
            this.bunifuLabel12.CursorType = null;
            this.bunifuLabel12.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel12.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel12.Location = new System.Drawing.Point(34, 260);
            this.bunifuLabel12.Name = "bunifuLabel12";
            this.bunifuLabel12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel12.Size = new System.Drawing.Size(108, 23);
            this.bunifuLabel12.TabIndex = 0;
            this.bunifuLabel12.TabStop = false;
            this.bunifuLabel12.Text = "Tounch Weight";
            this.bunifuLabel12.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel12.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel12.Click += new System.EventHandler(this.bunifuLabel12_Click);
            // 
            // bunifuLabel10
            // 
            this.bunifuLabel10.AutoEllipsis = false;
            this.bunifuLabel10.CursorType = null;
            this.bunifuLabel10.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel10.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel10.Location = new System.Drawing.Point(519, 128);
            this.bunifuLabel10.Name = "bunifuLabel10";
            this.bunifuLabel10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel10.Size = new System.Drawing.Size(39, 23);
            this.bunifuLabel10.TabIndex = 0;
            this.bunifuLabel10.TabStop = false;
            this.bunifuLabel10.Text = "Karat";
            this.bunifuLabel10.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel10.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel10.Click += new System.EventHandler(this.bunifuLabel10_Click);
            // 
            // bunifuLabel9
            // 
            this.bunifuLabel9.AutoEllipsis = false;
            this.bunifuLabel9.CursorType = null;
            this.bunifuLabel9.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel9.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel9.Location = new System.Drawing.Point(320, 128);
            this.bunifuLabel9.Name = "bunifuLabel9";
            this.bunifuLabel9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel9.Size = new System.Drawing.Size(44, 23);
            this.bunifuLabel9.TabIndex = 0;
            this.bunifuLabel9.TabStop = false;
            this.bunifuLabel9.Text = "Purity";
            this.bunifuLabel9.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel9.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel9.Click += new System.EventHandler(this.bunifuLabel9_Click);
            // 
            // bunifuLabel8
            // 
            this.bunifuLabel8.AutoEllipsis = false;
            this.bunifuLabel8.CursorType = null;
            this.bunifuLabel8.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel8.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel8.Location = new System.Drawing.Point(302, 259);
            this.bunifuLabel8.Name = "bunifuLabel8";
            this.bunifuLabel8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel8.Size = new System.Drawing.Size(53, 23);
            this.bunifuLabel8.TabIndex = 0;
            this.bunifuLabel8.TabStop = false;
            this.bunifuLabel8.Text = "Weight";
            this.bunifuLabel8.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel8.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel8.Click += new System.EventHandler(this.bunifuLabel8_Click);
            // 
            // bunifuLabel7
            // 
            this.bunifuLabel7.AutoEllipsis = false;
            this.bunifuLabel7.CursorType = null;
            this.bunifuLabel7.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel7.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel7.Location = new System.Drawing.Point(263, 58);
            this.bunifuLabel7.Name = "bunifuLabel7";
            this.bunifuLabel7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel7.Size = new System.Drawing.Size(35, 23);
            this.bunifuLabel7.TabIndex = 0;
            this.bunifuLabel7.TabStop = false;
            this.bunifuLabel7.Text = "Date";
            this.bunifuLabel7.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel7.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel7.Click += new System.EventHandler(this.bunifuLabel7_Click);
            // 
            // bunifuLabel6
            // 
            this.bunifuLabel6.AutoEllipsis = false;
            this.bunifuLabel6.CursorType = null;
            this.bunifuLabel6.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel6.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel6.Location = new System.Drawing.Point(493, 57);
            this.bunifuLabel6.Name = "bunifuLabel6";
            this.bunifuLabel6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel6.Size = new System.Drawing.Size(37, 23);
            this.bunifuLabel6.TabIndex = 0;
            this.bunifuLabel6.TabStop = false;
            this.bunifuLabel6.Text = "Time";
            this.bunifuLabel6.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel6.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel6.Click += new System.EventHandler(this.bunifuLabel6_Click);
            // 
            // bunifuLabel4
            // 
            this.bunifuLabel4.AutoEllipsis = false;
            this.bunifuLabel4.CursorType = null;
            this.bunifuLabel4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel4.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel4.Location = new System.Drawing.Point(34, 195);
            this.bunifuLabel4.Name = "bunifuLabel4";
            this.bunifuLabel4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel4.Size = new System.Drawing.Size(55, 23);
            this.bunifuLabel4.TabIndex = 0;
            this.bunifuLabel4.TabStop = false;
            this.bunifuLabel4.Text = "Sample";
            this.bunifuLabel4.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel4.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel4.Click += new System.EventHandler(this.bunifuLabel4_Click);
            // 
            // bunifuLabel3
            // 
            this.bunifuLabel3.AutoEllipsis = false;
            this.bunifuLabel3.CursorType = null;
            this.bunifuLabel3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel3.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel3.Location = new System.Drawing.Point(34, 128);
            this.bunifuLabel3.Name = "bunifuLabel3";
            this.bunifuLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel3.Size = new System.Drawing.Size(117, 23);
            this.bunifuLabel3.TabIndex = 0;
            this.bunifuLabel3.TabStop = false;
            this.bunifuLabel3.Text = "Customer Name";
            this.bunifuLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel3.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel3.Click += new System.EventHandler(this.bunifuLabel3_Click);
            // 
            // bunifuLabel2
            // 
            this.bunifuLabel2.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bunifuLabel2.AutoEllipsis = false;
            this.bunifuLabel2.CursorType = null;
            this.bunifuLabel2.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel2.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel2.Location = new System.Drawing.Point(34, 58);
            this.bunifuLabel2.Name = "bunifuLabel2";
            this.bunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel2.Size = new System.Drawing.Size(70, 23);
            this.bunifuLabel2.TabIndex = 0;
            this.bunifuLabel2.TabStop = false;
            this.bunifuLabel2.Text = "Series No";
            this.bunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel2.Click += new System.EventHandler(this.bunifuLabel2_Click);
            // 
            // seriesNoTexBox
            // 
            this.seriesNoTexBox.BorderColorFocused = System.Drawing.Color.Red;
            this.seriesNoTexBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.seriesNoTexBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.seriesNoTexBox.BorderThickness = 1;
            this.seriesNoTexBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.seriesNoTexBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.seriesNoTexBox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.seriesNoTexBox.ForeColor = System.Drawing.Color.White;
            this.seriesNoTexBox.isPassword = false;
            this.seriesNoTexBox.Location = new System.Drawing.Point(34, 90);
            this.seriesNoTexBox.Margin = new System.Windows.Forms.Padding(4);
            this.seriesNoTexBox.MaxLength = 32767;
            this.seriesNoTexBox.Name = "seriesNoTexBox";
            this.seriesNoTexBox.Size = new System.Drawing.Size(215, 33);
            this.seriesNoTexBox.TabIndex = 1;
            this.seriesNoTexBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.seriesNoTexBox.OnValueChanged += new System.EventHandler(this.bunifuMetroTextbox2_OnValueChanged);
            this.seriesNoTexBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.seriesNoTexBox_KeyDown);
            this.seriesNoTexBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.seriesNoTexBox_KeyPress);
            // 
            // bunifuLabel1
            // 
            this.bunifuLabel1.AutoEllipsis = false;
            this.bunifuLabel1.CursorType = null;
            this.bunifuLabel1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel1.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel1.Location = new System.Drawing.Point(738, 45);
            this.bunifuLabel1.Name = "bunifuLabel1";
            this.bunifuLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel1.Size = new System.Drawing.Size(70, 23);
            this.bunifuLabel1.TabIndex = 0;
            this.bunifuLabel1.TabStop = false;
            this.bunifuLabel1.Text = "Serial No.";
            this.bunifuLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel1.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel1.Click += new System.EventHandler(this.bunifuLabel1_Click_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(28, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(180, 35);
            this.label3.TabIndex = 0;
            this.label3.Text = "Create Report";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // bunifuLabel18
            // 
            this.bunifuLabel18.BorderColorFocused = System.Drawing.Color.Goldenrod;
            this.bunifuLabel18.BorderColorIdle = System.Drawing.Color.Teal;
            this.bunifuLabel18.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.bunifuLabel18.BorderThickness = 1;
            this.bunifuLabel18.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.bunifuLabel18.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuLabel18.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.bunifuLabel18.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel18.isPassword = false;
            this.bunifuLabel18.Location = new System.Drawing.Point(103, 364);
            this.bunifuLabel18.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuLabel18.MaxLength = 32767;
            this.bunifuLabel18.Name = "bunifuLabel18";
            this.bunifuLabel18.Size = new System.Drawing.Size(273, 23);
            this.bunifuLabel18.TabIndex = 62;
            this.bunifuLabel18.TabStop = false;
            this.bunifuLabel18.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.bunifuLabel18.Visible = false;
            // 
            // customerMobileTextBox
            // 
            this.customerMobileTextBox.BorderColorFocused = System.Drawing.Color.Red;
            this.customerMobileTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.customerMobileTextBox.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.customerMobileTextBox.BorderThickness = 1;
            this.customerMobileTextBox.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.customerMobileTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.customerMobileTextBox.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerMobileTextBox.ForeColor = System.Drawing.Color.White;
            this.customerMobileTextBox.isPassword = false;
            this.customerMobileTextBox.Location = new System.Drawing.Point(413, 355);
            this.customerMobileTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.customerMobileTextBox.MaxLength = 10;
            this.customerMobileTextBox.Name = "customerMobileTextBox";
            this.customerMobileTextBox.Size = new System.Drawing.Size(297, 33);
            this.customerMobileTextBox.TabIndex = 0;
            this.customerMobileTextBox.TabStop = false;
            this.customerMobileTextBox.Text = "Enter Mobile";
            this.customerMobileTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.customerMobileTextBox.OnValueChanged += new System.EventHandler(this.customerMobileTextBox_OnValueChanged);
            this.customerMobileTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.customerMobileTextBox_KeyDown);
            this.customerMobileTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.customerMobileTextBox_KeyPress);
            this.customerMobileTextBox.Leave += new System.EventHandler(this.customerMobileTextBox_Leave);
            // 
            // bunifuMetroTextbox4
            // 
            this.bunifuMetroTextbox4.BorderColorFocused = System.Drawing.Color.Goldenrod;
            this.bunifuMetroTextbox4.BorderColorIdle = System.Drawing.Color.Teal;
            this.bunifuMetroTextbox4.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.bunifuMetroTextbox4.BorderThickness = 1;
            this.bunifuMetroTextbox4.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.bunifuMetroTextbox4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuMetroTextbox4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.bunifuMetroTextbox4.ForeColor = System.Drawing.Color.White;
            this.bunifuMetroTextbox4.isPassword = false;
            this.bunifuMetroTextbox4.Location = new System.Drawing.Point(419, 366);
            this.bunifuMetroTextbox4.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuMetroTextbox4.MaxLength = 32767;
            this.bunifuMetroTextbox4.Name = "bunifuMetroTextbox4";
            this.bunifuMetroTextbox4.Size = new System.Drawing.Size(273, 23);
            this.bunifuMetroTextbox4.TabIndex = 62;
            this.bunifuMetroTextbox4.TabStop = false;
            this.bunifuMetroTextbox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.bunifuMetroTextbox4.Visible = false;
            // 
            // bunifuButton13
            // 
            this.bunifuButton13.AllowToggling = false;
            this.bunifuButton13.AnimationSpeed = 200;
            this.bunifuButton13.AutoGenerateColors = false;
            this.bunifuButton13.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton13.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton13.BackgroundImage")));
            this.bunifuButton13.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton13.ButtonText = "Clear ";
            this.bunifuButton13.ButtonTextMarginLeft = 0;
            this.bunifuButton13.ColorContrastOnClick = 45;
            this.bunifuButton13.ColorContrastOnHover = 45;
            this.bunifuButton13.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges15.BottomLeft = true;
            borderEdges15.BottomRight = true;
            borderEdges15.TopLeft = true;
            borderEdges15.TopRight = true;
            this.bunifuButton13.CustomizableEdges = borderEdges15;
            this.bunifuButton13.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton13.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton13.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton13.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton13.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton13.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton13.ForeColor = System.Drawing.Color.White;
            this.bunifuButton13.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton13.IconMarginLeft = 11;
            this.bunifuButton13.IconPadding = 10;
            this.bunifuButton13.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton13.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton13.IdleBorderRadius = 3;
            this.bunifuButton13.IdleBorderThickness = 1;
            this.bunifuButton13.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton13.IdleIconLeftImage = null;
            this.bunifuButton13.IdleIconRightImage = null;
            this.bunifuButton13.IndicateFocus = false;
            this.bunifuButton13.Location = new System.Drawing.Point(937, 168);
            this.bunifuButton13.Name = "bunifuButton13";
            stateProperties33.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties33.BorderRadius = 3;
            stateProperties33.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties33.BorderThickness = 1;
            stateProperties33.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties33.ForeColor = System.Drawing.Color.White;
            stateProperties33.IconLeftImage = null;
            stateProperties33.IconRightImage = null;
            this.bunifuButton13.onHoverState = stateProperties33;
            stateProperties34.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties34.BorderRadius = 3;
            stateProperties34.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties34.BorderThickness = 1;
            stateProperties34.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties34.ForeColor = System.Drawing.Color.White;
            stateProperties34.IconLeftImage = null;
            stateProperties34.IconRightImage = null;
            this.bunifuButton13.OnPressedState = stateProperties34;
            this.bunifuButton13.Size = new System.Drawing.Size(84, 33);
            this.bunifuButton13.TabIndex = 168;
            this.bunifuButton13.TabStop = false;
            this.bunifuButton13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton13.TextMarginLeft = 0;
            this.bunifuButton13.UseDefaultRadiusAndThickness = true;
            this.bunifuButton13.Click += new System.EventHandler(this.bunifuButton13_Click_1);
            // 
            // startButton
            // 
            this.startButton.AllowToggling = false;
            this.startButton.AnimationSpeed = 200;
            this.startButton.AutoGenerateColors = false;
            this.startButton.BackColor = System.Drawing.Color.Transparent;
            this.startButton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.startButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("startButton.BackgroundImage")));
            this.startButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.startButton.ButtonText = "";
            this.startButton.ButtonTextMarginLeft = 0;
            this.startButton.ColorContrastOnClick = 45;
            this.startButton.ColorContrastOnHover = 45;
            this.startButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges16.BottomLeft = true;
            borderEdges16.BottomRight = true;
            borderEdges16.TopLeft = true;
            borderEdges16.TopRight = true;
            this.startButton.CustomizableEdges = borderEdges16;
            this.startButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.startButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.startButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.startButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.startButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.startButton.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.startButton.ForeColor = System.Drawing.Color.White;
            this.startButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.startButton.IconMarginLeft = 1;
            this.startButton.IconPadding = 2;
            this.startButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.startButton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.startButton.IdleBorderRadius = 3;
            this.startButton.IdleBorderThickness = 1;
            this.startButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.startButton.IdleIconLeftImage = null;
            this.startButton.IdleIconRightImage = global::GoldRaven.Properties.Resources.icons8_play_45px1;
            this.startButton.IndicateFocus = false;
            this.startButton.Location = new System.Drawing.Point(675, 91);
            this.startButton.Name = "startButton";
            stateProperties35.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties35.BorderRadius = 3;
            stateProperties35.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties35.BorderThickness = 1;
            stateProperties35.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties35.ForeColor = System.Drawing.Color.White;
            stateProperties35.IconLeftImage = null;
            stateProperties35.IconRightImage = null;
            this.startButton.onHoverState = stateProperties35;
            stateProperties36.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties36.BorderRadius = 3;
            stateProperties36.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties36.BorderThickness = 1;
            stateProperties36.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties36.ForeColor = System.Drawing.Color.White;
            stateProperties36.IconLeftImage = null;
            stateProperties36.IconRightImage = null;
            this.startButton.OnPressedState = stateProperties36;
            this.startButton.Size = new System.Drawing.Size(33, 33);
            this.startButton.TabIndex = 0;
            this.startButton.TabStop = false;
            this.startButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.startButton.TextMarginLeft = 0;
            this.startButton.UseDefaultRadiusAndThickness = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // startStopTimerButton
            // 
            this.startStopTimerButton.AllowToggling = false;
            this.startStopTimerButton.AnimationSpeed = 200;
            this.startStopTimerButton.AutoGenerateColors = false;
            this.startStopTimerButton.BackColor = System.Drawing.Color.Transparent;
            this.startStopTimerButton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.startStopTimerButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("startStopTimerButton.BackgroundImage")));
            this.startStopTimerButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.startStopTimerButton.ButtonText = "";
            this.startStopTimerButton.ButtonTextMarginLeft = 0;
            this.startStopTimerButton.ColorContrastOnClick = 45;
            this.startStopTimerButton.ColorContrastOnHover = 45;
            this.startStopTimerButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges17.BottomLeft = true;
            borderEdges17.BottomRight = true;
            borderEdges17.TopLeft = true;
            borderEdges17.TopRight = true;
            this.startStopTimerButton.CustomizableEdges = borderEdges17;
            this.startStopTimerButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.startStopTimerButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.startStopTimerButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.startStopTimerButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.startStopTimerButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.startStopTimerButton.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.startStopTimerButton.ForeColor = System.Drawing.Color.White;
            this.startStopTimerButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.startStopTimerButton.IconMarginLeft = 1;
            this.startStopTimerButton.IconPadding = 2;
            this.startStopTimerButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.startStopTimerButton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.startStopTimerButton.IdleBorderRadius = 3;
            this.startStopTimerButton.IdleBorderThickness = 1;
            this.startStopTimerButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.startStopTimerButton.IdleIconLeftImage = null;
            this.startStopTimerButton.IdleIconRightImage = global::GoldRaven.Properties.Resources.icons8_stop_45px;
            this.startStopTimerButton.IndicateFocus = false;
            this.startStopTimerButton.Location = new System.Drawing.Point(675, 90);
            this.startStopTimerButton.Name = "startStopTimerButton";
            stateProperties37.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties37.BorderRadius = 3;
            stateProperties37.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties37.BorderThickness = 1;
            stateProperties37.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties37.ForeColor = System.Drawing.Color.White;
            stateProperties37.IconLeftImage = null;
            stateProperties37.IconRightImage = null;
            this.startStopTimerButton.onHoverState = stateProperties37;
            stateProperties38.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties38.BorderRadius = 3;
            stateProperties38.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties38.BorderThickness = 1;
            stateProperties38.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties38.ForeColor = System.Drawing.Color.White;
            stateProperties38.IconLeftImage = null;
            stateProperties38.IconRightImage = null;
            this.startStopTimerButton.OnPressedState = stateProperties38;
            this.startStopTimerButton.Size = new System.Drawing.Size(33, 33);
            this.startStopTimerButton.TabIndex = 167;
            this.startStopTimerButton.TabStop = false;
            this.startStopTimerButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.startStopTimerButton.TextMarginLeft = 0;
            this.startStopTimerButton.UseDefaultRadiusAndThickness = true;
            this.startStopTimerButton.Click += new System.EventHandler(this.bunifuButton7_Click_2);
            // 
            // goldFineTextBox
            // 
            this.goldFineTextBox.AcceptsReturn = false;
            this.goldFineTextBox.AcceptsTab = false;
            this.goldFineTextBox.AnimationSpeed = 200;
            this.goldFineTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.goldFineTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.goldFineTextBox.BackColor = System.Drawing.Color.Transparent;
            this.goldFineTextBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("goldFineTextBox.BackgroundImage")));
            this.goldFineTextBox.BorderColorActive = System.Drawing.Color.Goldenrod;
            this.goldFineTextBox.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.goldFineTextBox.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.goldFineTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.goldFineTextBox.BorderRadius = 1;
            this.goldFineTextBox.BorderThickness = 1;
            this.goldFineTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.goldFineTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.goldFineTextBox.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold);
            this.goldFineTextBox.DefaultText = "";
            this.goldFineTextBox.Enabled = false;
            this.goldFineTextBox.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.goldFineTextBox.ForeColor = System.Drawing.Color.White;
            this.goldFineTextBox.HideSelection = true;
            this.goldFineTextBox.IconLeft = null;
            this.goldFineTextBox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.goldFineTextBox.IconPadding = 3;
            this.goldFineTextBox.IconRight = global::GoldRaven.Properties.Resources.Layer_1;
            this.goldFineTextBox.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.goldFineTextBox.Lines = new string[0];
            this.goldFineTextBox.Location = new System.Drawing.Point(522, 224);
            this.goldFineTextBox.MaxLength = 32767;
            this.goldFineTextBox.MinimumSize = new System.Drawing.Size(0, 33);
            this.goldFineTextBox.Modified = false;
            this.goldFineTextBox.Multiline = false;
            this.goldFineTextBox.Name = "goldFineTextBox";
            stateProperties39.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties39.FillColor = System.Drawing.Color.Empty;
            stateProperties39.ForeColor = System.Drawing.Color.Empty;
            stateProperties39.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.goldFineTextBox.OnActiveState = stateProperties39;
            stateProperties40.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties40.FillColor = System.Drawing.Color.White;
            stateProperties40.ForeColor = System.Drawing.Color.Empty;
            stateProperties40.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.goldFineTextBox.OnDisabledState = stateProperties40;
            stateProperties41.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties41.FillColor = System.Drawing.Color.Empty;
            stateProperties41.ForeColor = System.Drawing.Color.Empty;
            stateProperties41.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.goldFineTextBox.OnHoverState = stateProperties41;
            stateProperties42.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties42.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties42.ForeColor = System.Drawing.Color.White;
            stateProperties42.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.goldFineTextBox.OnIdleState = stateProperties42;
            this.goldFineTextBox.PasswordChar = '\0';
            this.goldFineTextBox.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.goldFineTextBox.PlaceholderText = "Fine";
            this.goldFineTextBox.ReadOnly = true;
            this.goldFineTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.goldFineTextBox.SelectedText = "";
            this.goldFineTextBox.SelectionLength = 0;
            this.goldFineTextBox.SelectionStart = 0;
            this.goldFineTextBox.ShortcutsEnabled = true;
            this.goldFineTextBox.Size = new System.Drawing.Size(188, 33);
            this.goldFineTextBox.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.goldFineTextBox.TabIndex = 0;
            this.goldFineTextBox.TabStop = false;
            this.goldFineTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.goldFineTextBox.TextMarginBottom = 0;
            this.goldFineTextBox.TextMarginLeft = 5;
            this.goldFineTextBox.TextMarginTop = 0;
            this.goldFineTextBox.TextPlaceholder = "Fine";
            this.goldFineTextBox.UseSystemPasswordChar = false;
            this.goldFineTextBox.WordWrap = true;
            // 
            // customerRate
            // 
            this.customerRate.AcceptsReturn = false;
            this.customerRate.AcceptsTab = false;
            this.customerRate.AnimationSpeed = 200;
            this.customerRate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.customerRate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.customerRate.BackColor = System.Drawing.Color.Transparent;
            this.customerRate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("customerRate.BackgroundImage")));
            this.customerRate.BorderColorActive = System.Drawing.Color.Goldenrod;
            this.customerRate.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.customerRate.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.customerRate.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.customerRate.BorderRadius = 1;
            this.customerRate.BorderThickness = 1;
            this.customerRate.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.customerRate.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.customerRate.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerRate.DefaultText = "50";
            this.customerRate.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.customerRate.ForeColor = System.Drawing.Color.White;
            this.customerRate.HideSelection = true;
            this.customerRate.IconLeft = null;
            this.customerRate.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.customerRate.IconPadding = 3;
            this.customerRate.IconRight = global::GoldRaven.Properties.Resources.icons8_rupee_45px;
            this.customerRate.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.customerRate.Lines = new string[] {
        "50"};
            this.customerRate.Location = new System.Drawing.Point(564, 290);
            this.customerRate.MaxLength = 32767;
            this.customerRate.MinimumSize = new System.Drawing.Size(0, 33);
            this.customerRate.Modified = false;
            this.customerRate.Multiline = false;
            this.customerRate.Name = "customerRate";
            stateProperties43.BorderColor = System.Drawing.Color.Red;
            stateProperties43.FillColor = System.Drawing.Color.Empty;
            stateProperties43.ForeColor = System.Drawing.Color.Empty;
            stateProperties43.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.customerRate.OnActiveState = stateProperties43;
            stateProperties44.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties44.FillColor = System.Drawing.Color.White;
            stateProperties44.ForeColor = System.Drawing.Color.Empty;
            stateProperties44.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.customerRate.OnDisabledState = stateProperties44;
            stateProperties45.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties45.FillColor = System.Drawing.Color.Empty;
            stateProperties45.ForeColor = System.Drawing.Color.Empty;
            stateProperties45.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.customerRate.OnHoverState = stateProperties45;
            stateProperties46.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties46.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties46.ForeColor = System.Drawing.Color.White;
            stateProperties46.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.customerRate.OnIdleState = stateProperties46;
            this.customerRate.PasswordChar = '\0';
            this.customerRate.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.customerRate.PlaceholderText = "Rate";
            this.customerRate.ReadOnly = false;
            this.customerRate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.customerRate.SelectedText = "";
            this.customerRate.SelectionLength = 0;
            this.customerRate.SelectionStart = 2;
            this.customerRate.ShortcutsEnabled = true;
            this.customerRate.Size = new System.Drawing.Size(146, 33);
            this.customerRate.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.customerRate.TabIndex = 0;
            this.customerRate.TabStop = false;
            this.customerRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.customerRate.TextMarginBottom = 0;
            this.customerRate.TextMarginLeft = 5;
            this.customerRate.TextMarginTop = 0;
            this.customerRate.TextPlaceholder = "Rate";
            this.customerRate.UseSystemPasswordChar = false;
            this.customerRate.WordWrap = true;
            this.customerRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.customerRate_KeyDown);
            this.customerRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.customerRate_KeyPress);
            // 
            // tounchWeightTextBox
            // 
            this.tounchWeightTextBox.AcceptsReturn = false;
            this.tounchWeightTextBox.AcceptsTab = false;
            this.tounchWeightTextBox.AnimationSpeed = 200;
            this.tounchWeightTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tounchWeightTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tounchWeightTextBox.BackColor = System.Drawing.Color.Transparent;
            this.tounchWeightTextBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tounchWeightTextBox.BackgroundImage")));
            this.tounchWeightTextBox.BorderColorActive = System.Drawing.Color.Goldenrod;
            this.tounchWeightTextBox.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tounchWeightTextBox.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.tounchWeightTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tounchWeightTextBox.BorderRadius = 1;
            this.tounchWeightTextBox.BorderThickness = 1;
            this.tounchWeightTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tounchWeightTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tounchWeightTextBox.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tounchWeightTextBox.DefaultText = "";
            this.tounchWeightTextBox.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.tounchWeightTextBox.ForeColor = System.Drawing.Color.White;
            this.tounchWeightTextBox.HideSelection = true;
            this.tounchWeightTextBox.IconLeft = null;
            this.tounchWeightTextBox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.tounchWeightTextBox.IconPadding = 3;
            this.tounchWeightTextBox.IconRight = global::GoldRaven.Properties.Resources.Layer_11;
            this.tounchWeightTextBox.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.tounchWeightTextBox.Lines = new string[0];
            this.tounchWeightTextBox.Location = new System.Drawing.Point(34, 289);
            this.tounchWeightTextBox.MaxLength = 32767;
            this.tounchWeightTextBox.MinimumSize = new System.Drawing.Size(0, 33);
            this.tounchWeightTextBox.Modified = false;
            this.tounchWeightTextBox.Multiline = false;
            this.tounchWeightTextBox.Name = "tounchWeightTextBox";
            stateProperties47.BorderColor = System.Drawing.Color.Red;
            stateProperties47.FillColor = System.Drawing.Color.Empty;
            stateProperties47.ForeColor = System.Drawing.Color.Empty;
            stateProperties47.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.tounchWeightTextBox.OnActiveState = stateProperties47;
            stateProperties48.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties48.FillColor = System.Drawing.Color.White;
            stateProperties48.ForeColor = System.Drawing.Color.Empty;
            stateProperties48.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.tounchWeightTextBox.OnDisabledState = stateProperties48;
            stateProperties49.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties49.FillColor = System.Drawing.Color.Empty;
            stateProperties49.ForeColor = System.Drawing.Color.Empty;
            stateProperties49.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.tounchWeightTextBox.OnHoverState = stateProperties49;
            stateProperties50.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties50.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties50.ForeColor = System.Drawing.Color.White;
            stateProperties50.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.tounchWeightTextBox.OnIdleState = stateProperties50;
            this.tounchWeightTextBox.PasswordChar = '\0';
            this.tounchWeightTextBox.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.tounchWeightTextBox.PlaceholderText = "Enter Weight";
            this.tounchWeightTextBox.ReadOnly = false;
            this.tounchWeightTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tounchWeightTextBox.SelectedText = "";
            this.tounchWeightTextBox.SelectionLength = 0;
            this.tounchWeightTextBox.SelectionStart = 0;
            this.tounchWeightTextBox.ShortcutsEnabled = true;
            this.tounchWeightTextBox.Size = new System.Drawing.Size(246, 33);
            this.tounchWeightTextBox.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.tounchWeightTextBox.TabIndex = 4;
            this.tounchWeightTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tounchWeightTextBox.TextMarginBottom = 0;
            this.tounchWeightTextBox.TextMarginLeft = 5;
            this.tounchWeightTextBox.TextMarginTop = 0;
            this.tounchWeightTextBox.TextPlaceholder = "Enter Weight";
            this.tounchWeightTextBox.UseSystemPasswordChar = false;
            this.tounchWeightTextBox.WordWrap = true;
            this.tounchWeightTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tounchWeightTextBox_KeyDown);
            this.tounchWeightTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tounchWeightTextBox_KeyPress);
            this.tounchWeightTextBox.Leave += new System.EventHandler(this.tounchWeightTextBox_Leave);
            // 
            // originalWeightTextBox
            // 
            this.originalWeightTextBox.AcceptsReturn = false;
            this.originalWeightTextBox.AcceptsTab = false;
            this.originalWeightTextBox.AnimationSpeed = 200;
            this.originalWeightTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.originalWeightTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.originalWeightTextBox.BackColor = System.Drawing.Color.Transparent;
            this.originalWeightTextBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("originalWeightTextBox.BackgroundImage")));
            this.originalWeightTextBox.BorderColorActive = System.Drawing.Color.Goldenrod;
            this.originalWeightTextBox.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.originalWeightTextBox.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.originalWeightTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.originalWeightTextBox.BorderRadius = 1;
            this.originalWeightTextBox.BorderThickness = 1;
            this.originalWeightTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.originalWeightTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.originalWeightTextBox.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.originalWeightTextBox.DefaultText = "";
            this.originalWeightTextBox.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.originalWeightTextBox.ForeColor = System.Drawing.Color.White;
            this.originalWeightTextBox.HideSelection = true;
            this.originalWeightTextBox.IconLeft = null;
            this.originalWeightTextBox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.originalWeightTextBox.IconPadding = 3;
            this.originalWeightTextBox.IconRight = global::GoldRaven.Properties.Resources.Layer_11;
            this.originalWeightTextBox.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.originalWeightTextBox.Lines = new string[0];
            this.originalWeightTextBox.Location = new System.Drawing.Point(302, 289);
            this.originalWeightTextBox.MaxLength = 32767;
            this.originalWeightTextBox.MinimumSize = new System.Drawing.Size(0, 33);
            this.originalWeightTextBox.Modified = false;
            this.originalWeightTextBox.Multiline = false;
            this.originalWeightTextBox.Name = "originalWeightTextBox";
            stateProperties51.BorderColor = System.Drawing.Color.Red;
            stateProperties51.FillColor = System.Drawing.Color.Empty;
            stateProperties51.ForeColor = System.Drawing.Color.Empty;
            stateProperties51.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.originalWeightTextBox.OnActiveState = stateProperties51;
            stateProperties52.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties52.FillColor = System.Drawing.Color.White;
            stateProperties52.ForeColor = System.Drawing.Color.Empty;
            stateProperties52.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.originalWeightTextBox.OnDisabledState = stateProperties52;
            stateProperties53.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties53.FillColor = System.Drawing.Color.Empty;
            stateProperties53.ForeColor = System.Drawing.Color.Empty;
            stateProperties53.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.originalWeightTextBox.OnHoverState = stateProperties53;
            stateProperties54.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties54.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties54.ForeColor = System.Drawing.Color.White;
            stateProperties54.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.originalWeightTextBox.OnIdleState = stateProperties54;
            this.originalWeightTextBox.PasswordChar = '\0';
            this.originalWeightTextBox.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.originalWeightTextBox.PlaceholderText = "Enter Weight";
            this.originalWeightTextBox.ReadOnly = false;
            this.originalWeightTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.originalWeightTextBox.SelectedText = "";
            this.originalWeightTextBox.SelectionLength = 0;
            this.originalWeightTextBox.SelectionStart = 0;
            this.originalWeightTextBox.ShortcutsEnabled = true;
            this.originalWeightTextBox.Size = new System.Drawing.Size(246, 33);
            this.originalWeightTextBox.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.originalWeightTextBox.TabIndex = 5;
            this.originalWeightTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.originalWeightTextBox.TextMarginBottom = 0;
            this.originalWeightTextBox.TextMarginLeft = 5;
            this.originalWeightTextBox.TextMarginTop = 0;
            this.originalWeightTextBox.TextPlaceholder = "Enter Weight";
            this.originalWeightTextBox.UseSystemPasswordChar = false;
            this.originalWeightTextBox.WordWrap = true;
            this.originalWeightTextBox.TextChanged += new System.EventHandler(this.originalWeightTextBox_TextChanged);
            this.originalWeightTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.originalWeightTextBox_KeyDown);
            this.originalWeightTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.originalWeightTextBox_KeyPress);
            this.originalWeightTextBox.Leave += new System.EventHandler(this.originalWeightTextBox_Leave);
            // 
            // goldKaratTextBox
            // 
            this.goldKaratTextBox.AcceptsReturn = false;
            this.goldKaratTextBox.AcceptsTab = false;
            this.goldKaratTextBox.AnimationSpeed = 200;
            this.goldKaratTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.goldKaratTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.goldKaratTextBox.BackColor = System.Drawing.Color.Transparent;
            this.goldKaratTextBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("goldKaratTextBox.BackgroundImage")));
            this.goldKaratTextBox.BorderColorActive = System.Drawing.Color.Goldenrod;
            this.goldKaratTextBox.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.goldKaratTextBox.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.goldKaratTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.goldKaratTextBox.BorderRadius = 1;
            this.goldKaratTextBox.BorderThickness = 1;
            this.goldKaratTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.goldKaratTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.goldKaratTextBox.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold);
            this.goldKaratTextBox.DefaultText = "";
            this.goldKaratTextBox.Enabled = false;
            this.goldKaratTextBox.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.goldKaratTextBox.ForeColor = System.Drawing.Color.White;
            this.goldKaratTextBox.HideSelection = true;
            this.goldKaratTextBox.IconLeft = null;
            this.goldKaratTextBox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.goldKaratTextBox.IconPadding = 10;
            this.goldKaratTextBox.IconRight = global::GoldRaven.Properties.Resources.icons8_k_45px_1;
            this.goldKaratTextBox.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.goldKaratTextBox.Lines = new string[0];
            this.goldKaratTextBox.Location = new System.Drawing.Point(523, 159);
            this.goldKaratTextBox.MaxLength = 32767;
            this.goldKaratTextBox.MinimumSize = new System.Drawing.Size(0, 33);
            this.goldKaratTextBox.Modified = false;
            this.goldKaratTextBox.Multiline = false;
            this.goldKaratTextBox.Name = "goldKaratTextBox";
            stateProperties55.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties55.FillColor = System.Drawing.Color.Empty;
            stateProperties55.ForeColor = System.Drawing.Color.Empty;
            stateProperties55.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.goldKaratTextBox.OnActiveState = stateProperties55;
            stateProperties56.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties56.FillColor = System.Drawing.Color.White;
            stateProperties56.ForeColor = System.Drawing.Color.Empty;
            stateProperties56.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.goldKaratTextBox.OnDisabledState = stateProperties56;
            stateProperties57.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties57.FillColor = System.Drawing.Color.Empty;
            stateProperties57.ForeColor = System.Drawing.Color.Empty;
            stateProperties57.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.goldKaratTextBox.OnHoverState = stateProperties57;
            stateProperties58.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties58.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties58.ForeColor = System.Drawing.Color.White;
            stateProperties58.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.goldKaratTextBox.OnIdleState = stateProperties58;
            this.goldKaratTextBox.PasswordChar = '\0';
            this.goldKaratTextBox.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.goldKaratTextBox.PlaceholderText = "Karat";
            this.goldKaratTextBox.ReadOnly = true;
            this.goldKaratTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.goldKaratTextBox.SelectedText = "";
            this.goldKaratTextBox.SelectionLength = 0;
            this.goldKaratTextBox.SelectionStart = 0;
            this.goldKaratTextBox.ShortcutsEnabled = true;
            this.goldKaratTextBox.Size = new System.Drawing.Size(187, 33);
            this.goldKaratTextBox.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.goldKaratTextBox.TabIndex = 0;
            this.goldKaratTextBox.TabStop = false;
            this.goldKaratTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.goldKaratTextBox.TextMarginBottom = 0;
            this.goldKaratTextBox.TextMarginLeft = 5;
            this.goldKaratTextBox.TextMarginTop = 0;
            this.goldKaratTextBox.TextPlaceholder = "Karat";
            this.goldKaratTextBox.UseSystemPasswordChar = false;
            this.goldKaratTextBox.WordWrap = true;
            // 
            // goldPurityTextBox
            // 
            this.goldPurityTextBox.AcceptsReturn = false;
            this.goldPurityTextBox.AcceptsTab = false;
            this.goldPurityTextBox.AnimationSpeed = 200;
            this.goldPurityTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.goldPurityTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.goldPurityTextBox.BackColor = System.Drawing.Color.Transparent;
            this.goldPurityTextBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("goldPurityTextBox.BackgroundImage")));
            this.goldPurityTextBox.BorderColorActive = System.Drawing.Color.Red;
            this.goldPurityTextBox.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.goldPurityTextBox.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.goldPurityTextBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.goldPurityTextBox.BorderRadius = 1;
            this.goldPurityTextBox.BorderThickness = 1;
            this.goldPurityTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.goldPurityTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.goldPurityTextBox.DefaultFont = new System.Drawing.Font("Century Gothic", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goldPurityTextBox.DefaultText = "";
            this.goldPurityTextBox.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.goldPurityTextBox.ForeColor = System.Drawing.Color.Red;
            this.goldPurityTextBox.HideSelection = true;
            this.goldPurityTextBox.IconLeft = null;
            this.goldPurityTextBox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.goldPurityTextBox.IconPadding = 20;
            this.goldPurityTextBox.IconRight = global::GoldRaven.Properties.Resources.PERCENTAGE;
            this.goldPurityTextBox.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.goldPurityTextBox.Lines = new string[0];
            this.goldPurityTextBox.Location = new System.Drawing.Point(319, 159);
            this.goldPurityTextBox.MaxLength = 32767;
            this.goldPurityTextBox.MinimumSize = new System.Drawing.Size(0, 33);
            this.goldPurityTextBox.Modified = false;
            this.goldPurityTextBox.Multiline = false;
            this.goldPurityTextBox.Name = "goldPurityTextBox";
            stateProperties59.BorderColor = System.Drawing.Color.Red;
            stateProperties59.FillColor = System.Drawing.Color.Empty;
            stateProperties59.ForeColor = System.Drawing.Color.Empty;
            stateProperties59.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.goldPurityTextBox.OnActiveState = stateProperties59;
            stateProperties60.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties60.FillColor = System.Drawing.Color.White;
            stateProperties60.ForeColor = System.Drawing.Color.Empty;
            stateProperties60.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.goldPurityTextBox.OnDisabledState = stateProperties60;
            stateProperties61.BorderColor = System.Drawing.Color.Red;
            stateProperties61.FillColor = System.Drawing.Color.Empty;
            stateProperties61.ForeColor = System.Drawing.Color.Empty;
            stateProperties61.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.goldPurityTextBox.OnHoverState = stateProperties61;
            stateProperties62.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties62.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties62.ForeColor = System.Drawing.Color.Red;
            stateProperties62.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.goldPurityTextBox.OnIdleState = stateProperties62;
            this.goldPurityTextBox.PasswordChar = '\0';
            this.goldPurityTextBox.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.goldPurityTextBox.PlaceholderText = "Enter Percentage";
            this.goldPurityTextBox.ReadOnly = false;
            this.goldPurityTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.goldPurityTextBox.SelectedText = "";
            this.goldPurityTextBox.SelectionLength = 0;
            this.goldPurityTextBox.SelectionStart = 0;
            this.goldPurityTextBox.ShortcutsEnabled = true;
            this.goldPurityTextBox.Size = new System.Drawing.Size(187, 99);
            this.goldPurityTextBox.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.goldPurityTextBox.TabIndex = 6;
            this.goldPurityTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.goldPurityTextBox.TextMarginBottom = 0;
            this.goldPurityTextBox.TextMarginLeft = 5;
            this.goldPurityTextBox.TextMarginTop = 0;
            this.goldPurityTextBox.TextPlaceholder = "Enter Percentage";
            this.goldPurityTextBox.UseSystemPasswordChar = false;
            this.goldPurityTextBox.WordWrap = true;
            this.goldPurityTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.goldPurityTextBox_KeyDown);
            this.goldPurityTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.goldPurityTextBox_KeyPress);
            this.goldPurityTextBox.Leave += new System.EventHandler(this.goldPurityTextBox_Leave);
            // 
            // reportImageArea
            // 
            this.reportImageArea.ErrorImage = null;
            this.reportImageArea.Image = global::GoldRaven.Properties.Resources.add_image;
            this.reportImageArea.InitialImage = null;
            this.reportImageArea.Location = new System.Drawing.Point(738, 92);
            this.reportImageArea.Name = "reportImageArea";
            this.reportImageArea.Size = new System.Drawing.Size(193, 189);
            this.reportImageArea.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.reportImageArea.TabIndex = 161;
            this.reportImageArea.TabStop = false;
            this.reportImageArea.Click += new System.EventHandler(this.reportImageArea_Click);
            // 
            // reportDatePicker
            // 
            this.reportDatePicker.BackColor = System.Drawing.Color.Transparent;
            this.reportDatePicker.BorderRadius = 1;
            this.reportDatePicker.Color = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.reportDatePicker.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.reportDatePicker.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Right;
            this.reportDatePicker.DisabledColor = System.Drawing.Color.Gray;
            this.reportDatePicker.DisplayWeekNumbers = false;
            this.reportDatePicker.DPHeight = 0;
            this.reportDatePicker.FillDatePicker = false;
            this.reportDatePicker.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.reportDatePicker.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.reportDatePicker.Icon = ((System.Drawing.Image)(resources.GetObject("reportDatePicker.Icon")));
            this.reportDatePicker.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.reportDatePicker.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Left;
            this.reportDatePicker.Location = new System.Drawing.Point(263, 90);
            this.reportDatePicker.MinimumSize = new System.Drawing.Size(217, 33);
            this.reportDatePicker.Name = "reportDatePicker";
            this.reportDatePicker.Size = new System.Drawing.Size(217, 33);
            this.reportDatePicker.TabIndex = 0;
            this.reportDatePicker.TabStop = false;
            this.reportDatePicker.ValueChanged += new System.EventHandler(this.reportDatePicker_ValueChanged_1);
            this.reportDatePicker.VisibleChanged += new System.EventHandler(this.reportDatePicker_VisibleChanged);
            this.reportDatePicker.Enter += new System.EventHandler(this.reportDatePicker_ValueChanged);
            // 
            // reportSelectButton
            // 
            this.reportSelectButton.AllowToggling = false;
            this.reportSelectButton.AnimationSpeed = 200;
            this.reportSelectButton.AutoGenerateColors = false;
            this.reportSelectButton.BackColor = System.Drawing.Color.Transparent;
            this.reportSelectButton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(117)))), ((int)(((byte)(125)))));
            this.reportSelectButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("reportSelectButton.BackgroundImage")));
            this.reportSelectButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.reportSelectButton.ButtonText = "Select";
            this.reportSelectButton.ButtonTextMarginLeft = 0;
            this.reportSelectButton.ColorContrastOnClick = 45;
            this.reportSelectButton.ColorContrastOnHover = 45;
            this.reportSelectButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges18.BottomLeft = true;
            borderEdges18.BottomRight = true;
            borderEdges18.TopLeft = true;
            borderEdges18.TopRight = true;
            this.reportSelectButton.CustomizableEdges = borderEdges18;
            this.reportSelectButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.reportSelectButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.reportSelectButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.reportSelectButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.reportSelectButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.reportSelectButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.reportSelectButton.ForeColor = System.Drawing.Color.White;
            this.reportSelectButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.reportSelectButton.IconMarginLeft = 11;
            this.reportSelectButton.IconPadding = 10;
            this.reportSelectButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.reportSelectButton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(117)))), ((int)(((byte)(125)))));
            this.reportSelectButton.IdleBorderRadius = 3;
            this.reportSelectButton.IdleBorderThickness = 1;
            this.reportSelectButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(117)))), ((int)(((byte)(125)))));
            this.reportSelectButton.IdleIconLeftImage = null;
            this.reportSelectButton.IdleIconRightImage = null;
            this.reportSelectButton.IndicateFocus = false;
            this.reportSelectButton.Location = new System.Drawing.Point(937, 92);
            this.reportSelectButton.Name = "reportSelectButton";
            stateProperties63.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties63.BorderRadius = 3;
            stateProperties63.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties63.BorderThickness = 1;
            stateProperties63.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties63.ForeColor = System.Drawing.Color.White;
            stateProperties63.IconLeftImage = null;
            stateProperties63.IconRightImage = null;
            this.reportSelectButton.onHoverState = stateProperties63;
            stateProperties64.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties64.BorderRadius = 3;
            stateProperties64.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties64.BorderThickness = 1;
            stateProperties64.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties64.ForeColor = System.Drawing.Color.White;
            stateProperties64.IconLeftImage = null;
            stateProperties64.IconRightImage = null;
            this.reportSelectButton.OnPressedState = stateProperties64;
            this.reportSelectButton.Size = new System.Drawing.Size(84, 33);
            this.reportSelectButton.TabIndex = 0;
            this.reportSelectButton.TabStop = false;
            this.reportSelectButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reportSelectButton.TextMarginLeft = 0;
            this.reportSelectButton.UseDefaultRadiusAndThickness = true;
            this.reportSelectButton.Click += new System.EventHandler(this.reportSelectButton_Click);
            // 
            // reportScanImageButton
            // 
            this.reportScanImageButton.AllowToggling = false;
            this.reportScanImageButton.AnimationSpeed = 200;
            this.reportScanImageButton.AutoGenerateColors = false;
            this.reportScanImageButton.BackColor = System.Drawing.Color.Transparent;
            this.reportScanImageButton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(117)))), ((int)(((byte)(125)))));
            this.reportScanImageButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("reportScanImageButton.BackgroundImage")));
            this.reportScanImageButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.reportScanImageButton.ButtonText = "Scan";
            this.reportScanImageButton.ButtonTextMarginLeft = 0;
            this.reportScanImageButton.ColorContrastOnClick = 45;
            this.reportScanImageButton.ColorContrastOnHover = 45;
            this.reportScanImageButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges19.BottomLeft = true;
            borderEdges19.BottomRight = true;
            borderEdges19.TopLeft = true;
            borderEdges19.TopRight = true;
            this.reportScanImageButton.CustomizableEdges = borderEdges19;
            this.reportScanImageButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.reportScanImageButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.reportScanImageButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.reportScanImageButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.reportScanImageButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.reportScanImageButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.reportScanImageButton.ForeColor = System.Drawing.Color.White;
            this.reportScanImageButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.reportScanImageButton.IconMarginLeft = 11;
            this.reportScanImageButton.IconPadding = 10;
            this.reportScanImageButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.reportScanImageButton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(117)))), ((int)(((byte)(125)))));
            this.reportScanImageButton.IdleBorderRadius = 3;
            this.reportScanImageButton.IdleBorderThickness = 1;
            this.reportScanImageButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(117)))), ((int)(((byte)(125)))));
            this.reportScanImageButton.IdleIconLeftImage = null;
            this.reportScanImageButton.IdleIconRightImage = null;
            this.reportScanImageButton.IndicateFocus = false;
            this.reportScanImageButton.Location = new System.Drawing.Point(937, 132);
            this.reportScanImageButton.Name = "reportScanImageButton";
            stateProperties65.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties65.BorderRadius = 3;
            stateProperties65.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties65.BorderThickness = 1;
            stateProperties65.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties65.ForeColor = System.Drawing.Color.White;
            stateProperties65.IconLeftImage = null;
            stateProperties65.IconRightImage = null;
            this.reportScanImageButton.onHoverState = stateProperties65;
            stateProperties66.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties66.BorderRadius = 3;
            stateProperties66.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties66.BorderThickness = 1;
            stateProperties66.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties66.ForeColor = System.Drawing.Color.White;
            stateProperties66.IconLeftImage = null;
            stateProperties66.IconRightImage = null;
            this.reportScanImageButton.OnPressedState = stateProperties66;
            this.reportScanImageButton.Size = new System.Drawing.Size(84, 33);
            this.reportScanImageButton.TabIndex = 0;
            this.reportScanImageButton.TabStop = false;
            this.reportScanImageButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reportScanImageButton.TextMarginLeft = 0;
            this.reportScanImageButton.UseDefaultRadiusAndThickness = true;
            this.reportScanImageButton.Click += new System.EventHandler(this.reportScanImageButton_Click);
            // 
            // adddetailButton
            // 
            this.adddetailButton.AllowToggling = false;
            this.adddetailButton.AnimationSpeed = 200;
            this.adddetailButton.AutoGenerateColors = false;
            this.adddetailButton.BackColor = System.Drawing.Color.Transparent;
            this.adddetailButton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(162)))), ((int)(((byte)(184)))));
            this.adddetailButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("adddetailButton.BackgroundImage")));
            this.adddetailButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.adddetailButton.ButtonText = "Add Detail";
            this.adddetailButton.ButtonTextMarginLeft = 0;
            this.adddetailButton.ColorContrastOnClick = 45;
            this.adddetailButton.ColorContrastOnHover = 45;
            this.adddetailButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges20.BottomLeft = true;
            borderEdges20.BottomRight = true;
            borderEdges20.TopLeft = true;
            borderEdges20.TopRight = true;
            this.adddetailButton.CustomizableEdges = borderEdges20;
            this.adddetailButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.adddetailButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.adddetailButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.adddetailButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.adddetailButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.adddetailButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.adddetailButton.ForeColor = System.Drawing.Color.White;
            this.adddetailButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.adddetailButton.IconMarginLeft = 11;
            this.adddetailButton.IconPadding = 10;
            this.adddetailButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.adddetailButton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(162)))), ((int)(((byte)(184)))));
            this.adddetailButton.IdleBorderRadius = 3;
            this.adddetailButton.IdleBorderThickness = 1;
            this.adddetailButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(162)))), ((int)(((byte)(184)))));
            this.adddetailButton.IdleIconLeftImage = null;
            this.adddetailButton.IdleIconRightImage = global::GoldRaven.Properties.Resources.icons8_expand_arrow_45px;
            this.adddetailButton.IndicateFocus = false;
            this.adddetailButton.Location = new System.Drawing.Point(739, 290);
            this.adddetailButton.Name = "adddetailButton";
            stateProperties67.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties67.BorderRadius = 3;
            stateProperties67.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties67.BorderThickness = 1;
            stateProperties67.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties67.ForeColor = System.Drawing.Color.White;
            stateProperties67.IconLeftImage = null;
            stateProperties67.IconRightImage = null;
            this.adddetailButton.onHoverState = stateProperties67;
            stateProperties68.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties68.BorderRadius = 3;
            stateProperties68.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties68.BorderThickness = 1;
            stateProperties68.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties68.ForeColor = System.Drawing.Color.White;
            stateProperties68.IconLeftImage = null;
            stateProperties68.IconRightImage = null;
            this.adddetailButton.OnPressedState = stateProperties68;
            this.adddetailButton.Size = new System.Drawing.Size(286, 33);
            this.adddetailButton.TabIndex = 7;
            this.adddetailButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.adddetailButton.TextMarginLeft = 0;
            this.adddetailButton.UseDefaultRadiusAndThickness = true;
            this.adddetailButton.Click += new System.EventHandler(this.bunifuButton7_Click_1);
            this.adddetailButton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.adddetailButton_KeyPress);
            // 
            // reportPrintButton
            // 
            this.reportPrintButton.AllowToggling = false;
            this.reportPrintButton.AnimationSpeed = 200;
            this.reportPrintButton.AutoGenerateColors = false;
            this.reportPrintButton.BackColor = System.Drawing.Color.Transparent;
            this.reportPrintButton.BackColor1 = System.Drawing.Color.Goldenrod;
            this.reportPrintButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("reportPrintButton.BackgroundImage")));
            this.reportPrintButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.reportPrintButton.ButtonText = "Print";
            this.reportPrintButton.ButtonTextMarginLeft = 0;
            this.reportPrintButton.ColorContrastOnClick = 45;
            this.reportPrintButton.ColorContrastOnHover = 45;
            this.reportPrintButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges21.BottomLeft = true;
            borderEdges21.BottomRight = true;
            borderEdges21.TopLeft = true;
            borderEdges21.TopRight = true;
            this.reportPrintButton.CustomizableEdges = borderEdges21;
            this.reportPrintButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.reportPrintButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.reportPrintButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.reportPrintButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.reportPrintButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.reportPrintButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.reportPrintButton.ForeColor = System.Drawing.Color.White;
            this.reportPrintButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.reportPrintButton.IconMarginLeft = 11;
            this.reportPrintButton.IconPadding = 10;
            this.reportPrintButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.reportPrintButton.IdleBorderColor = System.Drawing.Color.Goldenrod;
            this.reportPrintButton.IdleBorderRadius = 3;
            this.reportPrintButton.IdleBorderThickness = 1;
            this.reportPrintButton.IdleFillColor = System.Drawing.Color.Goldenrod;
            this.reportPrintButton.IdleIconLeftImage = null;
            this.reportPrintButton.IdleIconRightImage = null;
            this.reportPrintButton.IndicateFocus = false;
            this.reportPrintButton.Location = new System.Drawing.Point(937, 207);
            this.reportPrintButton.Name = "reportPrintButton";
            stateProperties69.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties69.BorderRadius = 3;
            stateProperties69.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties69.BorderThickness = 1;
            stateProperties69.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties69.ForeColor = System.Drawing.Color.White;
            stateProperties69.IconLeftImage = null;
            stateProperties69.IconRightImage = null;
            this.reportPrintButton.onHoverState = stateProperties69;
            stateProperties70.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties70.BorderRadius = 3;
            stateProperties70.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties70.BorderThickness = 1;
            stateProperties70.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties70.ForeColor = System.Drawing.Color.White;
            stateProperties70.IconLeftImage = null;
            stateProperties70.IconRightImage = null;
            this.reportPrintButton.OnPressedState = stateProperties70;
            this.reportPrintButton.Size = new System.Drawing.Size(84, 33);
            this.reportPrintButton.TabIndex = 0;
            this.reportPrintButton.TabStop = false;
            this.reportPrintButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reportPrintButton.TextMarginLeft = 0;
            this.reportPrintButton.UseDefaultRadiusAndThickness = true;
            this.reportPrintButton.Click += new System.EventHandler(this.reportPrintButton_Click);
            // 
            // reportSaveButton
            // 
            this.reportSaveButton.AllowToggling = false;
            this.reportSaveButton.AnimationSpeed = 200;
            this.reportSaveButton.AutoGenerateColors = false;
            this.reportSaveButton.BackColor = System.Drawing.Color.Transparent;
            this.reportSaveButton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.reportSaveButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("reportSaveButton.BackgroundImage")));
            this.reportSaveButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.reportSaveButton.ButtonText = "Save";
            this.reportSaveButton.ButtonTextMarginLeft = 0;
            this.reportSaveButton.ColorContrastOnClick = 45;
            this.reportSaveButton.ColorContrastOnHover = 45;
            this.reportSaveButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges22.BottomLeft = true;
            borderEdges22.BottomRight = true;
            borderEdges22.TopLeft = true;
            borderEdges22.TopRight = true;
            this.reportSaveButton.CustomizableEdges = borderEdges22;
            this.reportSaveButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.reportSaveButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.reportSaveButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.reportSaveButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.reportSaveButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.reportSaveButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.reportSaveButton.ForeColor = System.Drawing.Color.White;
            this.reportSaveButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.reportSaveButton.IconMarginLeft = 11;
            this.reportSaveButton.IconPadding = 10;
            this.reportSaveButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.reportSaveButton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.reportSaveButton.IdleBorderRadius = 3;
            this.reportSaveButton.IdleBorderThickness = 1;
            this.reportSaveButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.reportSaveButton.IdleIconLeftImage = null;
            this.reportSaveButton.IdleIconRightImage = null;
            this.reportSaveButton.IndicateFocus = false;
            this.reportSaveButton.Location = new System.Drawing.Point(739, 354);
            this.reportSaveButton.Name = "reportSaveButton";
            stateProperties71.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties71.BorderRadius = 3;
            stateProperties71.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties71.BorderThickness = 1;
            stateProperties71.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties71.ForeColor = System.Drawing.Color.White;
            stateProperties71.IconLeftImage = null;
            stateProperties71.IconRightImage = null;
            this.reportSaveButton.onHoverState = stateProperties71;
            stateProperties72.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties72.BorderRadius = 3;
            stateProperties72.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties72.BorderThickness = 1;
            stateProperties72.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties72.ForeColor = System.Drawing.Color.White;
            stateProperties72.IconLeftImage = null;
            stateProperties72.IconRightImage = null;
            this.reportSaveButton.OnPressedState = stateProperties72;
            this.reportSaveButton.Size = new System.Drawing.Size(126, 33);
            this.reportSaveButton.TabIndex = 35;
            this.reportSaveButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reportSaveButton.TextMarginLeft = 0;
            this.reportSaveButton.UseDefaultRadiusAndThickness = true;
            this.reportSaveButton.Click += new System.EventHandler(this.reportSaveButton_Click);
            this.reportSaveButton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reportSaveButton_KeyPress);
            // 
            // reportModifyButton
            // 
            this.reportModifyButton.AllowToggling = false;
            this.reportModifyButton.AnimationSpeed = 200;
            this.reportModifyButton.AutoGenerateColors = false;
            this.reportModifyButton.BackColor = System.Drawing.Color.Transparent;
            this.reportModifyButton.BackColor1 = System.Drawing.Color.Goldenrod;
            this.reportModifyButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("reportModifyButton.BackgroundImage")));
            this.reportModifyButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.reportModifyButton.ButtonText = "Modify";
            this.reportModifyButton.ButtonTextMarginLeft = 0;
            this.reportModifyButton.ColorContrastOnClick = 45;
            this.reportModifyButton.ColorContrastOnHover = 45;
            this.reportModifyButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges23.BottomLeft = true;
            borderEdges23.BottomRight = true;
            borderEdges23.TopLeft = true;
            borderEdges23.TopRight = true;
            this.reportModifyButton.CustomizableEdges = borderEdges23;
            this.reportModifyButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.reportModifyButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.reportModifyButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.reportModifyButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.reportModifyButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.reportModifyButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.reportModifyButton.ForeColor = System.Drawing.Color.White;
            this.reportModifyButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.reportModifyButton.IconMarginLeft = 11;
            this.reportModifyButton.IconPadding = 10;
            this.reportModifyButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.reportModifyButton.IdleBorderColor = System.Drawing.Color.Goldenrod;
            this.reportModifyButton.IdleBorderRadius = 3;
            this.reportModifyButton.IdleBorderThickness = 1;
            this.reportModifyButton.IdleFillColor = System.Drawing.Color.Goldenrod;
            this.reportModifyButton.IdleIconLeftImage = null;
            this.reportModifyButton.IdleIconRightImage = null;
            this.reportModifyButton.IndicateFocus = false;
            this.reportModifyButton.Location = new System.Drawing.Point(937, 248);
            this.reportModifyButton.Name = "reportModifyButton";
            stateProperties73.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties73.BorderRadius = 3;
            stateProperties73.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties73.BorderThickness = 1;
            stateProperties73.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties73.ForeColor = System.Drawing.Color.White;
            stateProperties73.IconLeftImage = null;
            stateProperties73.IconRightImage = null;
            this.reportModifyButton.onHoverState = stateProperties73;
            stateProperties74.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties74.BorderRadius = 3;
            stateProperties74.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties74.BorderThickness = 1;
            stateProperties74.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties74.ForeColor = System.Drawing.Color.White;
            stateProperties74.IconLeftImage = null;
            stateProperties74.IconRightImage = null;
            this.reportModifyButton.OnPressedState = stateProperties74;
            this.reportModifyButton.Size = new System.Drawing.Size(87, 33);
            this.reportModifyButton.TabIndex = 0;
            this.reportModifyButton.TabStop = false;
            this.reportModifyButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reportModifyButton.TextMarginLeft = 0;
            this.reportModifyButton.UseDefaultRadiusAndThickness = true;
            this.reportModifyButton.Visible = false;
            this.reportModifyButton.Click += new System.EventHandler(this.reportModifyButton_Click);
            // 
            // reportNewButton
            // 
            this.reportNewButton.AllowToggling = false;
            this.reportNewButton.AnimationSpeed = 200;
            this.reportNewButton.AutoGenerateColors = false;
            this.reportNewButton.BackColor = System.Drawing.Color.Transparent;
            this.reportNewButton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.reportNewButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("reportNewButton.BackgroundImage")));
            this.reportNewButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.reportNewButton.ButtonText = "New";
            this.reportNewButton.ButtonTextMarginLeft = 0;
            this.reportNewButton.ColorContrastOnClick = 45;
            this.reportNewButton.ColorContrastOnHover = 45;
            this.reportNewButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges24.BottomLeft = true;
            borderEdges24.BottomRight = true;
            borderEdges24.TopLeft = true;
            borderEdges24.TopRight = true;
            this.reportNewButton.CustomizableEdges = borderEdges24;
            this.reportNewButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.reportNewButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.reportNewButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.reportNewButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.reportNewButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.reportNewButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.reportNewButton.ForeColor = System.Drawing.Color.White;
            this.reportNewButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.reportNewButton.IconMarginLeft = 11;
            this.reportNewButton.IconPadding = 10;
            this.reportNewButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.reportNewButton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.reportNewButton.IdleBorderRadius = 3;
            this.reportNewButton.IdleBorderThickness = 1;
            this.reportNewButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.reportNewButton.IdleIconLeftImage = null;
            this.reportNewButton.IdleIconRightImage = null;
            this.reportNewButton.IndicateFocus = false;
            this.reportNewButton.Location = new System.Drawing.Point(898, 354);
            this.reportNewButton.Name = "reportNewButton";
            stateProperties75.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties75.BorderRadius = 3;
            stateProperties75.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties75.BorderThickness = 1;
            stateProperties75.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties75.ForeColor = System.Drawing.Color.White;
            stateProperties75.IconLeftImage = null;
            stateProperties75.IconRightImage = null;
            this.reportNewButton.onHoverState = stateProperties75;
            stateProperties76.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties76.BorderRadius = 3;
            stateProperties76.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties76.BorderThickness = 1;
            stateProperties76.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties76.ForeColor = System.Drawing.Color.White;
            stateProperties76.IconLeftImage = null;
            stateProperties76.IconRightImage = null;
            this.reportNewButton.OnPressedState = stateProperties76;
            this.reportNewButton.Size = new System.Drawing.Size(126, 33);
            this.reportNewButton.TabIndex = 0;
            this.reportNewButton.TabStop = false;
            this.reportNewButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reportNewButton.TextMarginLeft = 0;
            this.reportNewButton.UseDefaultRadiusAndThickness = true;
            this.reportNewButton.Click += new System.EventHandler(this.reportNewButton_Click);
            // 
            // reportPage
            // 
            this.reportPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.reportPage.Controls.Add(this.rowCountTotal);
            this.reportPage.Controls.Add(this.totalrow);
            this.reportPage.Controls.Add(this.allReportDataGrid);
            this.reportPage.Controls.Add(this.bunifuLabel30);
            this.reportPage.Controls.Add(this.fullfilterMobileNo);
            this.reportPage.Controls.Add(this.bunifuLabel29);
            this.reportPage.Controls.Add(this.bunifuLabel28);
            this.reportPage.Controls.Add(this.fullcustomerReportBox);
            this.reportPage.Controls.Add(this.bunifuLabel27);
            this.reportPage.Controls.Add(this.label4);
            this.reportPage.Controls.Add(this.bunifuButton29);
            this.reportPage.Controls.Add(this.bunifuButton30);
            this.reportPage.Controls.Add(this.bunifuButton7);
            this.reportPage.Controls.Add(this.fullReportEndDatePicker);
            this.reportPage.Controls.Add(this.fullReportStartDatepicker);
            this.reportPage.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.reportPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.reportPage.Location = new System.Drawing.Point(4, 4);
            this.reportPage.Name = "reportPage";
            this.reportPage.Padding = new System.Windows.Forms.Padding(3);
            this.reportPage.Size = new System.Drawing.Size(1041, 728);
            this.reportPage.TabIndex = 3;
            this.reportPage.Text = "Report";
            this.reportPage.Click += new System.EventHandler(this.reportPage_Click);
            // 
            // rowCountTotal
            // 
            this.rowCountTotal.AutoSize = true;
            this.rowCountTotal.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold);
            this.rowCountTotal.ForeColor = System.Drawing.Color.Yellow;
            this.rowCountTotal.Location = new System.Drawing.Point(489, 137);
            this.rowCountTotal.Name = "rowCountTotal";
            this.rowCountTotal.Size = new System.Drawing.Size(127, 32);
            this.rowCountTotal.TabIndex = 172;
            this.rowCountTotal.Text = "Total Row";
            // 
            // totalrow
            // 
            this.totalrow.AutoSize = true;
            this.totalrow.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold);
            this.totalrow.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.totalrow.Location = new System.Drawing.Point(317, 137);
            this.totalrow.Name = "totalrow";
            this.totalrow.Size = new System.Drawing.Size(188, 32);
            this.totalrow.TabIndex = 171;
            this.totalrow.Text = "Total Reports : ";
            // 
            // allReportDataGrid
            // 
            this.allReportDataGrid.AllowUserToAddRows = false;
            this.allReportDataGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.allReportDataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle12;
            this.allReportDataGrid.AutoGenerateContextFilters = true;
            this.allReportDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.allReportDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.allReportDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.allReportDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.allReportDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.allReportDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.allReportDataGrid.ColumnHeadersHeight = 50;
            this.allReportDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column27,
            this.Column28,
            this.Column29,
            this.Column30,
            this.Column35,
            this.Column78,
            this.Column31,
            this.Column32,
            this.Column33,
            this.Column34,
            this.Column26,
            this.Column36});
            this.allReportDataGrid.DateWithTime = false;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(252)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.allReportDataGrid.DefaultCellStyle = dataGridViewCellStyle14;
            this.allReportDataGrid.EnableHeadersVisualStyles = false;
            this.allReportDataGrid.GridColor = System.Drawing.Color.WhiteSmoke;
            this.allReportDataGrid.Location = new System.Drawing.Point(25, 188);
            this.allReportDataGrid.Name = "allReportDataGrid";
            this.allReportDataGrid.ReadOnly = true;
            this.allReportDataGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.allReportDataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.allReportDataGrid.RowHeadersVisible = false;
            this.allReportDataGrid.RowHeadersWidth = 51;
            this.allReportDataGrid.RowTemplate.DividerHeight = 1;
            this.allReportDataGrid.RowTemplate.Height = 40;
            this.allReportDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.allReportDataGrid.Size = new System.Drawing.Size(985, 559);
            this.allReportDataGrid.TabIndex = 168;
            this.allReportDataGrid.TabStop = false;
            this.allReportDataGrid.TimeFilter = false;
            this.allReportDataGrid.SortStringChanged += new System.EventHandler(this.allReportDataGrid_SortStringChanged);
            this.allReportDataGrid.FilterStringChanged += new System.EventHandler(this.allReportDataGrid_FilterStringChanged);
            this.allReportDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.allReportDataGrid_CellContentClick);
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "payment";
            this.Column22.HeaderText = "Paid/Unpaid";
            this.Column22.MinimumWidth = 22;
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column22.Width = 132;
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "id";
            this.Column23.HeaderText = "Serial No";
            this.Column23.MinimumWidth = 22;
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            this.Column23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column23.Width = 106;
            // 
            // Column24
            // 
            this.Column24.DataPropertyName = "seriesNo";
            this.Column24.HeaderText = "Series No";
            this.Column24.MinimumWidth = 22;
            this.Column24.Name = "Column24";
            this.Column24.ReadOnly = true;
            this.Column24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column24.Width = 109;
            // 
            // Column25
            // 
            this.Column25.DataPropertyName = "currentDate";
            this.Column25.HeaderText = "Date";
            this.Column25.MinimumWidth = 22;
            this.Column25.Name = "Column25";
            this.Column25.ReadOnly = true;
            this.Column25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column25.Width = 73;
            // 
            // Column27
            // 
            this.Column27.DataPropertyName = "customerName";
            this.Column27.HeaderText = "Name";
            this.Column27.MinimumWidth = 22;
            this.Column27.Name = "Column27";
            this.Column27.ReadOnly = true;
            this.Column27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column27.Width = 83;
            // 
            // Column28
            // 
            this.Column28.DataPropertyName = "custMobile";
            this.Column28.HeaderText = "Mobile No.";
            this.Column28.MinimumWidth = 22;
            this.Column28.Name = "Column28";
            this.Column28.ReadOnly = true;
            this.Column28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column28.Width = 122;
            // 
            // Column29
            // 
            this.Column29.DataPropertyName = "sample";
            this.Column29.HeaderText = "Sample";
            this.Column29.MinimumWidth = 22;
            this.Column29.Name = "Column29";
            this.Column29.ReadOnly = true;
            this.Column29.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column29.Width = 93;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "goldPurity";
            this.Column30.HeaderText = "Purity";
            this.Column30.MinimumWidth = 22;
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            this.Column30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column30.Width = 82;
            // 
            // Column35
            // 
            this.Column35.DataPropertyName = "Rate";
            this.Column35.HeaderText = "Rate";
            this.Column35.MinimumWidth = 22;
            this.Column35.Name = "Column35";
            this.Column35.ReadOnly = true;
            this.Column35.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column35.Width = 72;
            // 
            // Column78
            // 
            this.Column78.DataPropertyName = "amount";
            this.Column78.HeaderText = "Amount";
            this.Column78.MinimumWidth = 22;
            this.Column78.Name = "Column78";
            this.Column78.ReadOnly = true;
            this.Column78.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column78.Width = 99;
            // 
            // Column31
            // 
            this.Column31.DataPropertyName = "goldKarat";
            this.Column31.HeaderText = "Karat";
            this.Column31.MinimumWidth = 22;
            this.Column31.Name = "Column31";
            this.Column31.ReadOnly = true;
            this.Column31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column31.Width = 77;
            // 
            // Column32
            // 
            this.Column32.DataPropertyName = "orgWeight";
            this.Column32.HeaderText = "Weight";
            this.Column32.MinimumWidth = 22;
            this.Column32.Name = "Column32";
            this.Column32.ReadOnly = true;
            this.Column32.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column32.Width = 92;
            // 
            // Column33
            // 
            this.Column33.DataPropertyName = "tounchweight";
            this.Column33.HeaderText = "Tounch Weight";
            this.Column33.MinimumWidth = 22;
            this.Column33.Name = "Column33";
            this.Column33.ReadOnly = true;
            this.Column33.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column33.Width = 152;
            // 
            // Column34
            // 
            this.Column34.DataPropertyName = "goldFine";
            this.Column34.HeaderText = "Gold Fine";
            this.Column34.MinimumWidth = 22;
            this.Column34.Name = "Column34";
            this.Column34.ReadOnly = true;
            this.Column34.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column34.Width = 110;
            // 
            // Column26
            // 
            this.Column26.DataPropertyName = "currentTime";
            this.Column26.HeaderText = "Time";
            this.Column26.MinimumWidth = 22;
            this.Column26.Name = "Column26";
            this.Column26.ReadOnly = true;
            this.Column26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column26.Width = 74;
            // 
            // Column36
            // 
            this.Column36.DataPropertyName = "customerAddress";
            this.Column36.HeaderText = "Address";
            this.Column36.MinimumWidth = 22;
            this.Column36.Name = "Column36";
            this.Column36.ReadOnly = true;
            this.Column36.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column36.Width = 97;
            // 
            // bunifuLabel30
            // 
            this.bunifuLabel30.AutoEllipsis = false;
            this.bunifuLabel30.CursorType = null;
            this.bunifuLabel30.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel30.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel30.Location = new System.Drawing.Point(661, 55);
            this.bunifuLabel30.Name = "bunifuLabel30";
            this.bunifuLabel30.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel30.Size = new System.Drawing.Size(51, 23);
            this.bunifuLabel30.TabIndex = 164;
            this.bunifuLabel30.TabStop = false;
            this.bunifuLabel30.Text = "Mobile";
            this.bunifuLabel30.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel30.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // fullfilterMobileNo
            // 
            this.fullfilterMobileNo.BorderColorFocused = System.Drawing.Color.Red;
            this.fullfilterMobileNo.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.fullfilterMobileNo.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.fullfilterMobileNo.BorderThickness = 1;
            this.fullfilterMobileNo.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.fullfilterMobileNo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fullfilterMobileNo.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.fullfilterMobileNo.ForeColor = System.Drawing.Color.White;
            this.fullfilterMobileNo.isPassword = false;
            this.fullfilterMobileNo.Location = new System.Drawing.Point(658, 84);
            this.fullfilterMobileNo.Margin = new System.Windows.Forms.Padding(4);
            this.fullfilterMobileNo.MaxLength = 10;
            this.fullfilterMobileNo.Name = "fullfilterMobileNo";
            this.fullfilterMobileNo.Size = new System.Drawing.Size(184, 33);
            this.fullfilterMobileNo.TabIndex = 4;
            this.fullfilterMobileNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.fullfilterMobileNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fullfilterMobileNo_KeyPress);
            // 
            // bunifuLabel29
            // 
            this.bunifuLabel29.AutoEllipsis = false;
            this.bunifuLabel29.CursorType = null;
            this.bunifuLabel29.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel29.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel29.Location = new System.Drawing.Point(489, 55);
            this.bunifuLabel29.Name = "bunifuLabel29";
            this.bunifuLabel29.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel29.Size = new System.Drawing.Size(65, 23);
            this.bunifuLabel29.TabIndex = 162;
            this.bunifuLabel29.TabStop = false;
            this.bunifuLabel29.Text = "End Date";
            this.bunifuLabel29.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel29.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel28
            // 
            this.bunifuLabel28.AutoEllipsis = false;
            this.bunifuLabel28.CursorType = null;
            this.bunifuLabel28.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel28.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel28.Location = new System.Drawing.Point(315, 55);
            this.bunifuLabel28.Name = "bunifuLabel28";
            this.bunifuLabel28.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel28.Size = new System.Drawing.Size(72, 23);
            this.bunifuLabel28.TabIndex = 160;
            this.bunifuLabel28.TabStop = false;
            this.bunifuLabel28.Text = "Start Date";
            this.bunifuLabel28.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel28.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel28.Click += new System.EventHandler(this.bunifuLabel28_Click);
            // 
            // fullcustomerReportBox
            // 
            this.fullcustomerReportBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fullcustomerReportBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fullcustomerReportBox.Font = new System.Drawing.Font("Century Gothic", 12.5F, System.Drawing.FontStyle.Bold);
            this.fullcustomerReportBox.ForeColor = System.Drawing.Color.Black;
            this.fullcustomerReportBox.FormattingEnabled = true;
            this.fullcustomerReportBox.ItemHeight = 25;
            this.fullcustomerReportBox.Location = new System.Drawing.Point(25, 84);
            this.fullcustomerReportBox.Name = "fullcustomerReportBox";
            this.fullcustomerReportBox.Size = new System.Drawing.Size(272, 33);
            this.fullcustomerReportBox.TabIndex = 1;
            // 
            // bunifuLabel27
            // 
            this.bunifuLabel27.AutoEllipsis = false;
            this.bunifuLabel27.CursorType = null;
            this.bunifuLabel27.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel27.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel27.Location = new System.Drawing.Point(25, 55);
            this.bunifuLabel27.Name = "bunifuLabel27";
            this.bunifuLabel27.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel27.Size = new System.Drawing.Size(117, 23);
            this.bunifuLabel27.TabIndex = 158;
            this.bunifuLabel27.TabStop = false;
            this.bunifuLabel27.Text = "Customer Name";
            this.bunifuLabel27.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel27.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(20, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 32);
            this.label4.TabIndex = 1;
            this.label4.Text = "Reports";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // bunifuButton29
            // 
            this.bunifuButton29.AllowToggling = false;
            this.bunifuButton29.AnimationSpeed = 200;
            this.bunifuButton29.AutoGenerateColors = false;
            this.bunifuButton29.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton29.BackColor1 = System.Drawing.Color.Goldenrod;
            this.bunifuButton29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton29.BackgroundImage")));
            this.bunifuButton29.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton29.ButtonText = "Print";
            this.bunifuButton29.ButtonTextMarginLeft = 0;
            this.bunifuButton29.ColorContrastOnClick = 45;
            this.bunifuButton29.ColorContrastOnHover = 45;
            this.bunifuButton29.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges25.BottomLeft = true;
            borderEdges25.BottomRight = true;
            borderEdges25.TopLeft = true;
            borderEdges25.TopRight = true;
            this.bunifuButton29.CustomizableEdges = borderEdges25;
            this.bunifuButton29.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton29.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton29.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton29.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton29.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton29.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton29.ForeColor = System.Drawing.Color.White;
            this.bunifuButton29.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton29.IconMarginLeft = 11;
            this.bunifuButton29.IconPadding = 10;
            this.bunifuButton29.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton29.IdleBorderColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton29.IdleBorderRadius = 3;
            this.bunifuButton29.IdleBorderThickness = 1;
            this.bunifuButton29.IdleFillColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton29.IdleIconLeftImage = null;
            this.bunifuButton29.IdleIconRightImage = null;
            this.bunifuButton29.IndicateFocus = false;
            this.bunifuButton29.Location = new System.Drawing.Point(858, 133);
            this.bunifuButton29.Name = "bunifuButton29";
            stateProperties77.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties77.BorderRadius = 3;
            stateProperties77.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties77.BorderThickness = 1;
            stateProperties77.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties77.ForeColor = System.Drawing.Color.White;
            stateProperties77.IconLeftImage = null;
            stateProperties77.IconRightImage = null;
            this.bunifuButton29.onHoverState = stateProperties77;
            stateProperties78.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties78.BorderRadius = 3;
            stateProperties78.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties78.BorderThickness = 1;
            stateProperties78.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties78.ForeColor = System.Drawing.Color.White;
            stateProperties78.IconLeftImage = null;
            stateProperties78.IconRightImage = null;
            this.bunifuButton29.OnPressedState = stateProperties78;
            this.bunifuButton29.Size = new System.Drawing.Size(149, 33);
            this.bunifuButton29.TabIndex = 6;
            this.bunifuButton29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton29.TextMarginLeft = 0;
            this.bunifuButton29.UseDefaultRadiusAndThickness = true;
            this.bunifuButton29.Click += new System.EventHandler(this.bunifuButton29_Click);
            // 
            // bunifuButton30
            // 
            this.bunifuButton30.AllowToggling = false;
            this.bunifuButton30.AnimationSpeed = 200;
            this.bunifuButton30.AutoGenerateColors = false;
            this.bunifuButton30.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton30.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton30.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton30.BackgroundImage")));
            this.bunifuButton30.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton30.ButtonText = "New";
            this.bunifuButton30.ButtonTextMarginLeft = 0;
            this.bunifuButton30.ColorContrastOnClick = 45;
            this.bunifuButton30.ColorContrastOnHover = 45;
            this.bunifuButton30.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges26.BottomLeft = true;
            borderEdges26.BottomRight = true;
            borderEdges26.TopLeft = true;
            borderEdges26.TopRight = true;
            this.bunifuButton30.CustomizableEdges = borderEdges26;
            this.bunifuButton30.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton30.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton30.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton30.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton30.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton30.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton30.ForeColor = System.Drawing.Color.White;
            this.bunifuButton30.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton30.IconMarginLeft = 11;
            this.bunifuButton30.IconPadding = 10;
            this.bunifuButton30.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton30.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton30.IdleBorderRadius = 3;
            this.bunifuButton30.IdleBorderThickness = 1;
            this.bunifuButton30.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton30.IdleIconLeftImage = null;
            this.bunifuButton30.IdleIconRightImage = null;
            this.bunifuButton30.IndicateFocus = false;
            this.bunifuButton30.Location = new System.Drawing.Point(661, 133);
            this.bunifuButton30.Name = "bunifuButton30";
            stateProperties79.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties79.BorderRadius = 3;
            stateProperties79.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties79.BorderThickness = 1;
            stateProperties79.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties79.ForeColor = System.Drawing.Color.White;
            stateProperties79.IconLeftImage = null;
            stateProperties79.IconRightImage = null;
            this.bunifuButton30.onHoverState = stateProperties79;
            stateProperties80.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties80.BorderRadius = 3;
            stateProperties80.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties80.BorderThickness = 1;
            stateProperties80.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties80.ForeColor = System.Drawing.Color.White;
            stateProperties80.IconLeftImage = null;
            stateProperties80.IconRightImage = null;
            this.bunifuButton30.OnPressedState = stateProperties80;
            this.bunifuButton30.Size = new System.Drawing.Size(181, 33);
            this.bunifuButton30.TabIndex = 0;
            this.bunifuButton30.TabStop = false;
            this.bunifuButton30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton30.TextMarginLeft = 0;
            this.bunifuButton30.UseDefaultRadiusAndThickness = true;
            this.bunifuButton30.Click += new System.EventHandler(this.bunifuButton30_Click);
            // 
            // bunifuButton7
            // 
            this.bunifuButton7.AllowToggling = false;
            this.bunifuButton7.AnimationSpeed = 200;
            this.bunifuButton7.AutoGenerateColors = false;
            this.bunifuButton7.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton7.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton7.BackgroundImage")));
            this.bunifuButton7.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton7.ButtonText = "Filter";
            this.bunifuButton7.ButtonTextMarginLeft = 0;
            this.bunifuButton7.ColorContrastOnClick = 45;
            this.bunifuButton7.ColorContrastOnHover = 45;
            this.bunifuButton7.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges27.BottomLeft = true;
            borderEdges27.BottomRight = true;
            borderEdges27.TopLeft = true;
            borderEdges27.TopRight = true;
            this.bunifuButton7.CustomizableEdges = borderEdges27;
            this.bunifuButton7.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton7.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton7.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton7.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton7.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton7.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton7.ForeColor = System.Drawing.Color.White;
            this.bunifuButton7.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton7.IconMarginLeft = 11;
            this.bunifuButton7.IconPadding = 10;
            this.bunifuButton7.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton7.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton7.IdleBorderRadius = 3;
            this.bunifuButton7.IdleBorderThickness = 1;
            this.bunifuButton7.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton7.IdleIconLeftImage = null;
            this.bunifuButton7.IdleIconRightImage = null;
            this.bunifuButton7.IndicateFocus = false;
            this.bunifuButton7.Location = new System.Drawing.Point(858, 84);
            this.bunifuButton7.Name = "bunifuButton7";
            stateProperties81.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties81.BorderRadius = 3;
            stateProperties81.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties81.BorderThickness = 1;
            stateProperties81.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties81.ForeColor = System.Drawing.Color.White;
            stateProperties81.IconLeftImage = null;
            stateProperties81.IconRightImage = null;
            this.bunifuButton7.onHoverState = stateProperties81;
            stateProperties82.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties82.BorderRadius = 3;
            stateProperties82.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties82.BorderThickness = 1;
            stateProperties82.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties82.ForeColor = System.Drawing.Color.White;
            stateProperties82.IconLeftImage = null;
            stateProperties82.IconRightImage = null;
            this.bunifuButton7.OnPressedState = stateProperties82;
            this.bunifuButton7.Size = new System.Drawing.Size(152, 33);
            this.bunifuButton7.TabIndex = 5;
            this.bunifuButton7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton7.TextMarginLeft = 0;
            this.bunifuButton7.UseDefaultRadiusAndThickness = true;
            this.bunifuButton7.Click += new System.EventHandler(this.bunifuButton7_Click_3);
            // 
            // fullReportEndDatePicker
            // 
            this.fullReportEndDatePicker.BackColor = System.Drawing.Color.Transparent;
            this.fullReportEndDatePicker.BorderRadius = 1;
            this.fullReportEndDatePicker.Color = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.fullReportEndDatePicker.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.fullReportEndDatePicker.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Right;
            this.fullReportEndDatePicker.DisabledColor = System.Drawing.Color.Gray;
            this.fullReportEndDatePicker.DisplayWeekNumbers = false;
            this.fullReportEndDatePicker.DPHeight = 0;
            this.fullReportEndDatePicker.FillDatePicker = false;
            this.fullReportEndDatePicker.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.fullReportEndDatePicker.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.fullReportEndDatePicker.Icon = ((System.Drawing.Image)(resources.GetObject("fullReportEndDatePicker.Icon")));
            this.fullReportEndDatePicker.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.fullReportEndDatePicker.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Left;
            this.fullReportEndDatePicker.Location = new System.Drawing.Point(485, 84);
            this.fullReportEndDatePicker.MinimumSize = new System.Drawing.Size(158, 33);
            this.fullReportEndDatePicker.Name = "fullReportEndDatePicker";
            this.fullReportEndDatePicker.Size = new System.Drawing.Size(158, 33);
            this.fullReportEndDatePicker.TabIndex = 3;
            this.fullReportEndDatePicker.ValueChanged += new System.EventHandler(this.bunifuDatePicker5_ValueChanged);
            // 
            // fullReportStartDatepicker
            // 
            this.fullReportStartDatepicker.BackColor = System.Drawing.Color.Transparent;
            this.fullReportStartDatepicker.BorderRadius = 1;
            this.fullReportStartDatepicker.Color = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.fullReportStartDatepicker.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.fullReportStartDatepicker.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Right;
            this.fullReportStartDatepicker.DisabledColor = System.Drawing.Color.Gray;
            this.fullReportStartDatepicker.DisplayWeekNumbers = false;
            this.fullReportStartDatepicker.DPHeight = 0;
            this.fullReportStartDatepicker.FillDatePicker = false;
            this.fullReportStartDatepicker.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.fullReportStartDatepicker.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.fullReportStartDatepicker.Icon = ((System.Drawing.Image)(resources.GetObject("fullReportStartDatepicker.Icon")));
            this.fullReportStartDatepicker.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.fullReportStartDatepicker.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Left;
            this.fullReportStartDatepicker.Location = new System.Drawing.Point(312, 84);
            this.fullReportStartDatepicker.MinimumSize = new System.Drawing.Size(158, 33);
            this.fullReportStartDatepicker.Name = "fullReportStartDatepicker";
            this.fullReportStartDatepicker.Size = new System.Drawing.Size(158, 33);
            this.fullReportStartDatepicker.TabIndex = 2;
            this.fullReportStartDatepicker.ValueChanged += new System.EventHandler(this.bunifuDatePicker4_ValueChanged);
            // 
            // tokenPage
            // 
            this.tokenPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.tokenPage.Controls.Add(this.tokecustcomboBox2);
            this.tokenPage.Controls.Add(this.tokenSeries);
            this.tokenPage.Controls.Add(this.bunifuLabel44);
            this.tokenPage.Controls.Add(this.tokenListDataGrid);
            this.tokenPage.Controls.Add(this.bunifuLabel40);
            this.tokenPage.Controls.Add(this.tokenSampleDataGrid);
            this.tokenPage.Controls.Add(this.label10);
            this.tokenPage.Controls.Add(this.tokenSample);
            this.tokenPage.Controls.Add(this.tokenDateTimePicker);
            this.tokenPage.Controls.Add(this.bunifuLabel31);
            this.tokenPage.Controls.Add(this.bunifuLabel32);
            this.tokenPage.Controls.Add(this.bunifuLabel33);
            this.tokenPage.Controls.Add(this.tokenAddress);
            this.tokenPage.Controls.Add(this.bunifuLabel34);
            this.tokenPage.Controls.Add(this.bunifuLabel35);
            this.tokenPage.Controls.Add(this.bunifuLabel36);
            this.tokenPage.Controls.Add(this.bunifuLabel37);
            this.tokenPage.Controls.Add(this.bunifuLabel38);
            this.tokenPage.Controls.Add(this.bunifuLabel39);
            this.tokenPage.Controls.Add(this.tokenSeriesno);
            this.tokenPage.Controls.Add(this.tokenMobile);
            this.tokenPage.Controls.Add(this.label6);
            this.tokenPage.Controls.Add(this.tokenTotal);
            this.tokenPage.Controls.Add(this.tokenAddButton);
            this.tokenPage.Controls.Add(this.bunifuButton16);
            this.tokenPage.Controls.Add(this.bunifuButton18);
            this.tokenPage.Controls.Add(this.tokenSampleDeletebutton);
            this.tokenPage.Controls.Add(this.tokenRate);
            this.tokenPage.Controls.Add(this.tokenWeightText);
            this.tokenPage.Controls.Add(this.tokenDatePicker);
            this.tokenPage.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tokenPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tokenPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tokenPage.Location = new System.Drawing.Point(4, 4);
            this.tokenPage.Name = "tokenPage";
            this.tokenPage.Padding = new System.Windows.Forms.Padding(3);
            this.tokenPage.Size = new System.Drawing.Size(1041, 728);
            this.tokenPage.TabIndex = 4;
            this.tokenPage.Text = "Token";
            this.tokenPage.Click += new System.EventHandler(this.tokenPage_Click);
            // 
            // tokecustcomboBox2
            // 
            this.tokecustcomboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tokecustcomboBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.tokecustcomboBox2.Font = new System.Drawing.Font("Century Gothic", 12.5F, System.Drawing.FontStyle.Bold);
            this.tokecustcomboBox2.ForeColor = System.Drawing.Color.Black;
            this.tokecustcomboBox2.FormattingEnabled = true;
            this.tokecustcomboBox2.ItemHeight = 25;
            this.tokecustcomboBox2.Location = new System.Drawing.Point(32, 161);
            this.tokecustcomboBox2.Name = "tokecustcomboBox2";
            this.tokecustcomboBox2.Size = new System.Drawing.Size(334, 33);
            this.tokecustcomboBox2.TabIndex = 2;
            this.tokecustcomboBox2.SelectedIndexChanged += new System.EventHandler(this.tokecustcomboBox2_SelectedIndexChanged);
            this.tokecustcomboBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tokecustcomboBox2_KeyDown);
            this.tokecustcomboBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tokecustcomboBox2_KeyPress);
            this.tokecustcomboBox2.Leave += new System.EventHandler(this.tokecustcomboBox2_Leave);
            // 
            // tokenSeries
            // 
            this.tokenSeries.AutoEllipsis = false;
            this.tokenSeries.CursorType = null;
            this.tokenSeries.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tokenSeries.ForeColor = System.Drawing.Color.White;
            this.tokenSeries.Location = new System.Drawing.Point(777, 16);
            this.tokenSeries.Name = "tokenSeries";
            this.tokenSeries.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tokenSeries.Size = new System.Drawing.Size(182, 39);
            this.tokenSeries.TabIndex = 168;
            this.tokenSeries.TabStop = false;
            this.tokenSeries.Text = "Serial number";
            this.tokenSeries.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.tokenSeries.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel44
            // 
            this.bunifuLabel44.AutoEllipsis = false;
            this.bunifuLabel44.CursorType = null;
            this.bunifuLabel44.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel44.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel44.Location = new System.Drawing.Point(701, 27);
            this.bunifuLabel44.Name = "bunifuLabel44";
            this.bunifuLabel44.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel44.Size = new System.Drawing.Size(70, 23);
            this.bunifuLabel44.TabIndex = 167;
            this.bunifuLabel44.TabStop = false;
            this.bunifuLabel44.Text = "Serial No.";
            this.bunifuLabel44.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel44.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // tokenListDataGrid
            // 
            this.tokenListDataGrid.AllowUserToAddRows = false;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.tokenListDataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.tokenListDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.tokenListDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.tokenListDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tokenListDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.tokenListDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.tokenListDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.tokenListDataGrid.ColumnHeadersHeight = 50;
            this.tokenListDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.Column76,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.Column41,
            this.Column42,
            this.Column46,
            this.Column47,
            this.Column45,
            this.Column44,
            this.Column43});
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(252)))));
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.tokenListDataGrid.DefaultCellStyle = dataGridViewCellStyle20;
            this.tokenListDataGrid.DoubleBuffered = true;
            this.tokenListDataGrid.EnableHeadersVisualStyles = false;
            this.tokenListDataGrid.GridColor = System.Drawing.Color.WhiteSmoke;
            this.tokenListDataGrid.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            this.tokenListDataGrid.HeaderForeColor = System.Drawing.Color.White;
            this.tokenListDataGrid.Location = new System.Drawing.Point(380, 381);
            this.tokenListDataGrid.Name = "tokenListDataGrid";
            this.tokenListDataGrid.ReadOnly = true;
            this.tokenListDataGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.tokenListDataGrid.RowHeadersVisible = false;
            this.tokenListDataGrid.RowHeadersWidth = 51;
            this.tokenListDataGrid.RowTemplate.DividerHeight = 1;
            this.tokenListDataGrid.RowTemplate.Height = 40;
            this.tokenListDataGrid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.tokenListDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tokenListDataGrid.Size = new System.Drawing.Size(630, 327);
            this.tokenListDataGrid.TabIndex = 166;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "id";
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewTextBoxColumn2.HeaderText = "Id";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 51;
            // 
            // Column76
            // 
            this.Column76.DataPropertyName = "series";
            this.Column76.HeaderText = "Series";
            this.Column76.MinimumWidth = 6;
            this.Column76.Name = "Column76";
            this.Column76.ReadOnly = true;
            this.Column76.Width = 90;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "customerName";
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn3.HeaderText = "Name";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 84;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "custMobile";
            this.dataGridViewTextBoxColumn4.HeaderText = "Mobile";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 91;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Quantity";
            this.dataGridViewTextBoxColumn7.HeaderText = "Sample ";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 104;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "totalAmount";
            this.dataGridViewTextBoxColumn8.HeaderText = "Total Amount";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 147;
            // 
            // Column41
            // 
            this.Column41.DataPropertyName = "date";
            this.Column41.HeaderText = "Date";
            this.Column41.MinimumWidth = 6;
            this.Column41.Name = "Column41";
            this.Column41.ReadOnly = true;
            this.Column41.Width = 76;
            // 
            // Column42
            // 
            this.Column42.DataPropertyName = "time";
            this.Column42.HeaderText = "Time";
            this.Column42.MinimumWidth = 6;
            this.Column42.Name = "Column42";
            this.Column42.ReadOnly = true;
            this.Column42.Width = 77;
            // 
            // Column46
            // 
            this.Column46.DataPropertyName = "Rate";
            this.Column46.HeaderText = "Rate";
            this.Column46.MinimumWidth = 6;
            this.Column46.Name = "Column46";
            this.Column46.ReadOnly = true;
            this.Column46.Visible = false;
            this.Column46.Width = 75;
            // 
            // Column47
            // 
            this.Column47.DataPropertyName = "custFirm";
            this.Column47.HeaderText = "FIRM";
            this.Column47.MinimumWidth = 6;
            this.Column47.Name = "Column47";
            this.Column47.ReadOnly = true;
            this.Column47.Visible = false;
            this.Column47.Width = 80;
            // 
            // Column45
            // 
            this.Column45.DataPropertyName = "customerAddress";
            this.Column45.HeaderText = "ADDRESS";
            this.Column45.MinimumWidth = 6;
            this.Column45.Name = "Column45";
            this.Column45.ReadOnly = true;
            this.Column45.Visible = false;
            this.Column45.Width = 125;
            // 
            // Column44
            // 
            this.Column44.DataPropertyName = "id1";
            this.Column44.HeaderText = "ID2";
            this.Column44.MinimumWidth = 6;
            this.Column44.Name = "Column44";
            this.Column44.ReadOnly = true;
            this.Column44.Visible = false;
            this.Column44.Width = 65;
            // 
            // Column43
            // 
            this.Column43.DataPropertyName = "customerId";
            this.Column43.HeaderText = "CUTOMERID";
            this.Column43.MinimumWidth = 6;
            this.Column43.Name = "Column43";
            this.Column43.ReadOnly = true;
            this.Column43.Visible = false;
            this.Column43.Width = 146;
            // 
            // bunifuLabel40
            // 
            this.bunifuLabel40.AutoEllipsis = false;
            this.bunifuLabel40.CursorType = null;
            this.bunifuLabel40.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel40.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel40.Location = new System.Drawing.Point(712, 173);
            this.bunifuLabel40.Name = "bunifuLabel40";
            this.bunifuLabel40.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel40.Size = new System.Drawing.Size(97, 23);
            this.bunifuLabel40.TabIndex = 164;
            this.bunifuLabel40.TabStop = false;
            this.bunifuLabel40.Text = "Total Amount";
            this.bunifuLabel40.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel40.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel40.Click += new System.EventHandler(this.bunifuLabel40_Click);
            // 
            // tokenSampleDataGrid
            // 
            this.tokenSampleDataGrid.AllowUserToAddRows = false;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.tokenSampleDataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            this.tokenSampleDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tokenSampleDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.tokenSampleDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tokenSampleDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.tokenSampleDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.tokenSampleDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.tokenSampleDataGrid.ColumnHeadersHeight = 50;
            this.tokenSampleDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Column37,
            this.Column38,
            this.Column40,
            this.Column39});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(252)))));
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.tokenSampleDataGrid.DefaultCellStyle = dataGridViewCellStyle23;
            this.tokenSampleDataGrid.DoubleBuffered = true;
            this.tokenSampleDataGrid.EnableHeadersVisualStyles = false;
            this.tokenSampleDataGrid.GridColor = System.Drawing.Color.WhiteSmoke;
            this.tokenSampleDataGrid.HeaderBgColor = System.Drawing.Color.Maroon;
            this.tokenSampleDataGrid.HeaderForeColor = System.Drawing.Color.White;
            this.tokenSampleDataGrid.Location = new System.Drawing.Point(32, 381);
            this.tokenSampleDataGrid.Name = "tokenSampleDataGrid";
            this.tokenSampleDataGrid.ReadOnly = true;
            this.tokenSampleDataGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.tokenSampleDataGrid.RowHeadersVisible = false;
            this.tokenSampleDataGrid.RowHeadersWidth = 51;
            this.tokenSampleDataGrid.RowTemplate.DividerHeight = 1;
            this.tokenSampleDataGrid.RowTemplate.Height = 40;
            this.tokenSampleDataGrid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.tokenSampleDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tokenSampleDataGrid.Size = new System.Drawing.Size(334, 327);
            this.tokenSampleDataGrid.TabIndex = 162;
            this.tokenSampleDataGrid.TabStop = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // Column37
            // 
            this.Column37.DataPropertyName = "tokenId";
            this.Column37.HeaderText = "TokenId";
            this.Column37.MinimumWidth = 6;
            this.Column37.Name = "Column37";
            this.Column37.ReadOnly = true;
            this.Column37.Visible = false;
            // 
            // Column38
            // 
            this.Column38.DataPropertyName = "itemNo";
            this.Column38.HeaderText = "Items";
            this.Column38.MinimumWidth = 6;
            this.Column38.Name = "Column38";
            this.Column38.ReadOnly = true;
            // 
            // Column40
            // 
            this.Column40.DataPropertyName = "weight";
            this.Column40.HeaderText = "Weight";
            this.Column40.MinimumWidth = 6;
            this.Column40.Name = "Column40";
            this.Column40.ReadOnly = true;
            // 
            // Column39
            // 
            this.Column39.DataPropertyName = "sSample";
            this.Column39.HeaderText = "Sample";
            this.Column39.MinimumWidth = 6;
            this.Column39.Name = "Column39";
            this.Column39.ReadOnly = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label10.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label10.Location = new System.Drawing.Point(35, 274);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(127, 28);
            this.label10.TabIndex = 30;
            this.label10.Text = "Add Samples";
            this.label10.Click += new System.EventHandler(this.label10_Click_1);
            // 
            // tokenSample
            // 
            this.tokenSample.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tokenSample.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.tokenSample.Font = new System.Drawing.Font("Century Gothic", 12.5F, System.Drawing.FontStyle.Bold);
            this.tokenSample.ForeColor = System.Drawing.Color.Black;
            this.tokenSample.FormattingEnabled = true;
            this.tokenSample.ItemHeight = 25;
            this.tokenSample.Location = new System.Drawing.Point(34, 332);
            this.tokenSample.Name = "tokenSample";
            this.tokenSample.Size = new System.Drawing.Size(332, 33);
            this.tokenSample.TabIndex = 25;
            // 
            // tokenDateTimePicker
            // 
            this.tokenDateTimePicker.CalendarFont = new System.Drawing.Font("Century Gothic", 7.75F, System.Drawing.FontStyle.Bold);
            this.tokenDateTimePicker.CalendarMonthBackground = System.Drawing.Color.SeaGreen;
            this.tokenDateTimePicker.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tokenDateTimePicker.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tokenDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.tokenDateTimePicker.Location = new System.Drawing.Point(491, 86);
            this.tokenDateTimePicker.Margin = new System.Windows.Forms.Padding(4);
            this.tokenDateTimePicker.MaximumSize = new System.Drawing.Size(4, 23);
            this.tokenDateTimePicker.MinimumSize = new System.Drawing.Size(180, 33);
            this.tokenDateTimePicker.Name = "tokenDateTimePicker";
            this.tokenDateTimePicker.ShowUpDown = true;
            this.tokenDateTimePicker.Size = new System.Drawing.Size(180, 33);
            this.tokenDateTimePicker.TabIndex = 0;
            this.tokenDateTimePicker.TabStop = false;
            // 
            // bunifuLabel31
            // 
            this.bunifuLabel31.AutoEllipsis = false;
            this.bunifuLabel31.CursorType = null;
            this.bunifuLabel31.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel31.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel31.Location = new System.Drawing.Point(712, 59);
            this.bunifuLabel31.Name = "bunifuLabel31";
            this.bunifuLabel31.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel31.Size = new System.Drawing.Size(34, 23);
            this.bunifuLabel31.TabIndex = 21;
            this.bunifuLabel31.TabStop = false;
            this.bunifuLabel31.Text = "Rate";
            this.bunifuLabel31.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel31.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel32
            // 
            this.bunifuLabel32.AutoEllipsis = false;
            this.bunifuLabel32.CursorType = null;
            this.bunifuLabel32.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel32.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel32.Location = new System.Drawing.Point(380, 133);
            this.bunifuLabel32.Name = "bunifuLabel32";
            this.bunifuLabel32.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel32.Size = new System.Drawing.Size(51, 23);
            this.bunifuLabel32.TabIndex = 20;
            this.bunifuLabel32.TabStop = false;
            this.bunifuLabel32.Text = "Mobile";
            this.bunifuLabel32.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel32.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel32.Click += new System.EventHandler(this.bunifuLabel32_Click_1);
            // 
            // bunifuLabel33
            // 
            this.bunifuLabel33.AutoEllipsis = false;
            this.bunifuLabel33.CursorType = null;
            this.bunifuLabel33.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel33.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel33.Location = new System.Drawing.Point(35, 203);
            this.bunifuLabel33.Name = "bunifuLabel33";
            this.bunifuLabel33.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel33.Size = new System.Drawing.Size(59, 23);
            this.bunifuLabel33.TabIndex = 19;
            this.bunifuLabel33.TabStop = false;
            this.bunifuLabel33.Text = "Address";
            this.bunifuLabel33.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel33.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // tokenAddress
            // 
            this.tokenAddress.BorderColorFocused = System.Drawing.Color.Red;
            this.tokenAddress.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tokenAddress.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.tokenAddress.BorderThickness = 1;
            this.tokenAddress.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tokenAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tokenAddress.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.tokenAddress.ForeColor = System.Drawing.Color.White;
            this.tokenAddress.isPassword = false;
            this.tokenAddress.Location = new System.Drawing.Point(32, 230);
            this.tokenAddress.Margin = new System.Windows.Forms.Padding(4);
            this.tokenAddress.MaxLength = 32767;
            this.tokenAddress.Name = "tokenAddress";
            this.tokenAddress.Size = new System.Drawing.Size(639, 33);
            this.tokenAddress.TabIndex = 5;
            this.tokenAddress.TabStop = false;
            this.tokenAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tokenAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tokenAddress_KeyDown);
            this.tokenAddress.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tokenAddress_KeyPress);
            // 
            // bunifuLabel34
            // 
            this.bunifuLabel34.AutoEllipsis = false;
            this.bunifuLabel34.CursorType = null;
            this.bunifuLabel34.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel34.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel34.Location = new System.Drawing.Point(379, 305);
            this.bunifuLabel34.Name = "bunifuLabel34";
            this.bunifuLabel34.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel34.Size = new System.Drawing.Size(53, 23);
            this.bunifuLabel34.TabIndex = 17;
            this.bunifuLabel34.TabStop = false;
            this.bunifuLabel34.Text = "Weight";
            this.bunifuLabel34.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel34.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel35
            // 
            this.bunifuLabel35.AutoEllipsis = false;
            this.bunifuLabel35.CursorType = null;
            this.bunifuLabel35.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel35.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel35.Location = new System.Drawing.Point(264, 57);
            this.bunifuLabel35.Name = "bunifuLabel35";
            this.bunifuLabel35.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel35.Size = new System.Drawing.Size(35, 23);
            this.bunifuLabel35.TabIndex = 18;
            this.bunifuLabel35.TabStop = false;
            this.bunifuLabel35.Text = "Date";
            this.bunifuLabel35.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel35.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel36
            // 
            this.bunifuLabel36.AutoEllipsis = false;
            this.bunifuLabel36.CursorType = null;
            this.bunifuLabel36.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel36.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel36.Location = new System.Drawing.Point(494, 56);
            this.bunifuLabel36.Name = "bunifuLabel36";
            this.bunifuLabel36.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel36.Size = new System.Drawing.Size(37, 23);
            this.bunifuLabel36.TabIndex = 15;
            this.bunifuLabel36.TabStop = false;
            this.bunifuLabel36.Text = "Time";
            this.bunifuLabel36.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel36.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel37
            // 
            this.bunifuLabel37.AutoEllipsis = false;
            this.bunifuLabel37.CursorType = null;
            this.bunifuLabel37.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel37.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel37.Location = new System.Drawing.Point(37, 305);
            this.bunifuLabel37.Name = "bunifuLabel37";
            this.bunifuLabel37.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel37.Size = new System.Drawing.Size(55, 23);
            this.bunifuLabel37.TabIndex = 14;
            this.bunifuLabel37.TabStop = false;
            this.bunifuLabel37.Text = "Sample";
            this.bunifuLabel37.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel37.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel38
            // 
            this.bunifuLabel38.AutoEllipsis = false;
            this.bunifuLabel38.CursorType = null;
            this.bunifuLabel38.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel38.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel38.Location = new System.Drawing.Point(35, 133);
            this.bunifuLabel38.Name = "bunifuLabel38";
            this.bunifuLabel38.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel38.Size = new System.Drawing.Size(117, 23);
            this.bunifuLabel38.TabIndex = 13;
            this.bunifuLabel38.TabStop = false;
            this.bunifuLabel38.Text = "Customer Name";
            this.bunifuLabel38.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel38.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel39
            // 
            this.bunifuLabel39.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bunifuLabel39.AutoEllipsis = false;
            this.bunifuLabel39.CursorType = null;
            this.bunifuLabel39.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel39.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel39.Location = new System.Drawing.Point(35, 57);
            this.bunifuLabel39.Name = "bunifuLabel39";
            this.bunifuLabel39.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel39.Size = new System.Drawing.Size(70, 23);
            this.bunifuLabel39.TabIndex = 12;
            this.bunifuLabel39.TabStop = false;
            this.bunifuLabel39.Text = "Series No";
            this.bunifuLabel39.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel39.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // tokenSeriesno
            // 
            this.tokenSeriesno.BorderColorFocused = System.Drawing.Color.Red;
            this.tokenSeriesno.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tokenSeriesno.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.tokenSeriesno.BorderThickness = 1;
            this.tokenSeriesno.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tokenSeriesno.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tokenSeriesno.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.tokenSeriesno.ForeColor = System.Drawing.Color.White;
            this.tokenSeriesno.isPassword = false;
            this.tokenSeriesno.Location = new System.Drawing.Point(32, 84);
            this.tokenSeriesno.Margin = new System.Windows.Forms.Padding(4);
            this.tokenSeriesno.MaxLength = 32767;
            this.tokenSeriesno.Name = "tokenSeriesno";
            this.tokenSeriesno.Size = new System.Drawing.Size(215, 35);
            this.tokenSeriesno.TabIndex = 1;
            this.tokenSeriesno.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tokenSeriesno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tokenSeriesno_KeyPress);
            // 
            // tokenMobile
            // 
            this.tokenMobile.BorderColorFocused = System.Drawing.Color.Red;
            this.tokenMobile.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tokenMobile.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.tokenMobile.BorderThickness = 1;
            this.tokenMobile.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tokenMobile.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tokenMobile.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.tokenMobile.ForeColor = System.Drawing.Color.White;
            this.tokenMobile.isPassword = false;
            this.tokenMobile.Location = new System.Drawing.Point(373, 161);
            this.tokenMobile.Margin = new System.Windows.Forms.Padding(4);
            this.tokenMobile.MaxLength = 10;
            this.tokenMobile.Name = "tokenMobile";
            this.tokenMobile.Size = new System.Drawing.Size(298, 33);
            this.tokenMobile.TabIndex = 3;
            this.tokenMobile.TabStop = false;
            this.tokenMobile.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tokenMobile.OnValueChanged += new System.EventHandler(this.bunifuMetroTextbox3_OnValueChanged_1);
            this.tokenMobile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tokenMobile_KeyDown);
            this.tokenMobile.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tokenMobile_KeyPress);
            this.tokenMobile.Leave += new System.EventHandler(this.tokenMobile_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Location = new System.Drawing.Point(29, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(193, 32);
            this.label6.TabIndex = 2;
            this.label6.Text = "Generate Token";
            this.label6.Click += new System.EventHandler(this.label6_Click_2);
            // 
            // tokenTotal
            // 
            this.tokenTotal.AcceptsReturn = false;
            this.tokenTotal.AcceptsTab = false;
            this.tokenTotal.AnimationSpeed = 200;
            this.tokenTotal.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tokenTotal.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tokenTotal.BackColor = System.Drawing.Color.Transparent;
            this.tokenTotal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tokenTotal.BackgroundImage")));
            this.tokenTotal.BorderColorActive = System.Drawing.Color.Goldenrod;
            this.tokenTotal.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tokenTotal.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.tokenTotal.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tokenTotal.BorderRadius = 1;
            this.tokenTotal.BorderThickness = 1;
            this.tokenTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tokenTotal.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tokenTotal.DefaultFont = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tokenTotal.DefaultText = "";
            this.tokenTotal.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.tokenTotal.ForeColor = System.Drawing.Color.White;
            this.tokenTotal.HideSelection = true;
            this.tokenTotal.IconLeft = null;
            this.tokenTotal.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.tokenTotal.IconPadding = 3;
            this.tokenTotal.IconRight = null;
            this.tokenTotal.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.tokenTotal.Lines = new string[0];
            this.tokenTotal.Location = new System.Drawing.Point(706, 202);
            this.tokenTotal.MaxLength = 32767;
            this.tokenTotal.MinimumSize = new System.Drawing.Size(0, 33);
            this.tokenTotal.Modified = false;
            this.tokenTotal.Multiline = false;
            this.tokenTotal.Name = "tokenTotal";
            stateProperties83.BorderColor = System.Drawing.Color.Red;
            stateProperties83.FillColor = System.Drawing.Color.Empty;
            stateProperties83.ForeColor = System.Drawing.Color.Empty;
            stateProperties83.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.tokenTotal.OnActiveState = stateProperties83;
            stateProperties84.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties84.FillColor = System.Drawing.Color.White;
            stateProperties84.ForeColor = System.Drawing.Color.Empty;
            stateProperties84.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.tokenTotal.OnDisabledState = stateProperties84;
            stateProperties85.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties85.FillColor = System.Drawing.Color.Empty;
            stateProperties85.ForeColor = System.Drawing.Color.Empty;
            stateProperties85.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.tokenTotal.OnHoverState = stateProperties85;
            stateProperties86.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties86.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties86.ForeColor = System.Drawing.Color.White;
            stateProperties86.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.tokenTotal.OnIdleState = stateProperties86;
            this.tokenTotal.PasswordChar = '\0';
            this.tokenTotal.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.tokenTotal.PlaceholderText = "₹";
            this.tokenTotal.ReadOnly = false;
            this.tokenTotal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tokenTotal.SelectedText = "";
            this.tokenTotal.SelectionLength = 0;
            this.tokenTotal.SelectionStart = 0;
            this.tokenTotal.ShortcutsEnabled = true;
            this.tokenTotal.Size = new System.Drawing.Size(304, 52);
            this.tokenTotal.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.tokenTotal.TabIndex = 0;
            this.tokenTotal.TabStop = false;
            this.tokenTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tokenTotal.TextMarginBottom = 0;
            this.tokenTotal.TextMarginLeft = 5;
            this.tokenTotal.TextMarginTop = 0;
            this.tokenTotal.TextPlaceholder = "₹";
            this.tokenTotal.UseSystemPasswordChar = false;
            this.tokenTotal.WordWrap = true;
            this.tokenTotal.TextChanged += new System.EventHandler(this.bunifuTextBox2_TextChanged);
            // 
            // tokenAddButton
            // 
            this.tokenAddButton.AllowToggling = false;
            this.tokenAddButton.AnimationSpeed = 200;
            this.tokenAddButton.AutoGenerateColors = false;
            this.tokenAddButton.BackColor = System.Drawing.Color.Transparent;
            this.tokenAddButton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.tokenAddButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tokenAddButton.BackgroundImage")));
            this.tokenAddButton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.tokenAddButton.ButtonText = "Add";
            this.tokenAddButton.ButtonTextMarginLeft = 0;
            this.tokenAddButton.ColorContrastOnClick = 45;
            this.tokenAddButton.ColorContrastOnHover = 45;
            this.tokenAddButton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges28.BottomLeft = true;
            borderEdges28.BottomRight = true;
            borderEdges28.TopLeft = true;
            borderEdges28.TopRight = true;
            this.tokenAddButton.CustomizableEdges = borderEdges28;
            this.tokenAddButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.tokenAddButton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.tokenAddButton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tokenAddButton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.tokenAddButton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.tokenAddButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.tokenAddButton.ForeColor = System.Drawing.Color.White;
            this.tokenAddButton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.tokenAddButton.IconMarginLeft = 11;
            this.tokenAddButton.IconPadding = 10;
            this.tokenAddButton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.tokenAddButton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.tokenAddButton.IdleBorderRadius = 3;
            this.tokenAddButton.IdleBorderThickness = 1;
            this.tokenAddButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.tokenAddButton.IdleIconLeftImage = null;
            this.tokenAddButton.IdleIconRightImage = null;
            this.tokenAddButton.IndicateFocus = false;
            this.tokenAddButton.Location = new System.Drawing.Point(706, 332);
            this.tokenAddButton.Name = "tokenAddButton";
            stateProperties87.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties87.BorderRadius = 3;
            stateProperties87.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties87.BorderThickness = 1;
            stateProperties87.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties87.ForeColor = System.Drawing.Color.White;
            stateProperties87.IconLeftImage = null;
            stateProperties87.IconRightImage = null;
            this.tokenAddButton.onHoverState = stateProperties87;
            stateProperties88.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties88.BorderRadius = 3;
            stateProperties88.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties88.BorderThickness = 1;
            stateProperties88.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties88.ForeColor = System.Drawing.Color.White;
            stateProperties88.IconLeftImage = null;
            stateProperties88.IconRightImage = null;
            this.tokenAddButton.OnPressedState = stateProperties88;
            this.tokenAddButton.Size = new System.Drawing.Size(140, 33);
            this.tokenAddButton.TabIndex = 161;
            this.tokenAddButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tokenAddButton.TextMarginLeft = 0;
            this.tokenAddButton.UseDefaultRadiusAndThickness = true;
            this.tokenAddButton.Click += new System.EventHandler(this.tokenAddButton_Click);
            // 
            // bunifuButton16
            // 
            this.bunifuButton16.AllowToggling = false;
            this.bunifuButton16.AnimationSpeed = 200;
            this.bunifuButton16.AutoGenerateColors = false;
            this.bunifuButton16.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton16.BackColor1 = System.Drawing.Color.Goldenrod;
            this.bunifuButton16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton16.BackgroundImage")));
            this.bunifuButton16.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton16.ButtonText = "Modify";
            this.bunifuButton16.ButtonTextMarginLeft = 0;
            this.bunifuButton16.ColorContrastOnClick = 45;
            this.bunifuButton16.ColorContrastOnHover = 45;
            this.bunifuButton16.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges29.BottomLeft = true;
            borderEdges29.BottomRight = true;
            borderEdges29.TopLeft = true;
            borderEdges29.TopRight = true;
            this.bunifuButton16.CustomizableEdges = borderEdges29;
            this.bunifuButton16.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton16.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton16.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton16.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton16.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton16.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton16.ForeColor = System.Drawing.Color.White;
            this.bunifuButton16.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton16.IconMarginLeft = 11;
            this.bunifuButton16.IconPadding = 10;
            this.bunifuButton16.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton16.IdleBorderColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton16.IdleBorderRadius = 3;
            this.bunifuButton16.IdleBorderThickness = 1;
            this.bunifuButton16.IdleFillColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton16.IdleIconLeftImage = null;
            this.bunifuButton16.IdleIconRightImage = null;
            this.bunifuButton16.IndicateFocus = false;
            this.bunifuButton16.Location = new System.Drawing.Point(870, 281);
            this.bunifuButton16.Name = "bunifuButton16";
            stateProperties89.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties89.BorderRadius = 3;
            stateProperties89.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties89.BorderThickness = 1;
            stateProperties89.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties89.ForeColor = System.Drawing.Color.White;
            stateProperties89.IconLeftImage = null;
            stateProperties89.IconRightImage = null;
            this.bunifuButton16.onHoverState = stateProperties89;
            stateProperties90.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties90.BorderRadius = 3;
            stateProperties90.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties90.BorderThickness = 1;
            stateProperties90.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties90.ForeColor = System.Drawing.Color.White;
            stateProperties90.IconLeftImage = null;
            stateProperties90.IconRightImage = null;
            this.bunifuButton16.OnPressedState = stateProperties90;
            this.bunifuButton16.Size = new System.Drawing.Size(140, 33);
            this.bunifuButton16.TabIndex = 160;
            this.bunifuButton16.TabStop = false;
            this.bunifuButton16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton16.TextMarginLeft = 0;
            this.bunifuButton16.UseDefaultRadiusAndThickness = true;
            this.bunifuButton16.Click += new System.EventHandler(this.bunifuButton16_Click);
            // 
            // bunifuButton18
            // 
            this.bunifuButton18.AllowToggling = false;
            this.bunifuButton18.AnimationSpeed = 200;
            this.bunifuButton18.AutoGenerateColors = false;
            this.bunifuButton18.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton18.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton18.BackgroundImage")));
            this.bunifuButton18.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton18.ButtonText = "Save";
            this.bunifuButton18.ButtonTextMarginLeft = 0;
            this.bunifuButton18.ColorContrastOnClick = 45;
            this.bunifuButton18.ColorContrastOnHover = 45;
            this.bunifuButton18.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges30.BottomLeft = true;
            borderEdges30.BottomRight = true;
            borderEdges30.TopLeft = true;
            borderEdges30.TopRight = true;
            this.bunifuButton18.CustomizableEdges = borderEdges30;
            this.bunifuButton18.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton18.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton18.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton18.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton18.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton18.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton18.ForeColor = System.Drawing.Color.White;
            this.bunifuButton18.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton18.IconMarginLeft = 11;
            this.bunifuButton18.IconPadding = 10;
            this.bunifuButton18.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton18.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton18.IdleBorderRadius = 3;
            this.bunifuButton18.IdleBorderThickness = 1;
            this.bunifuButton18.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton18.IdleIconLeftImage = null;
            this.bunifuButton18.IdleIconRightImage = null;
            this.bunifuButton18.IndicateFocus = false;
            this.bunifuButton18.Location = new System.Drawing.Point(706, 281);
            this.bunifuButton18.Name = "bunifuButton18";
            stateProperties91.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties91.BorderRadius = 3;
            stateProperties91.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties91.BorderThickness = 1;
            stateProperties91.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties91.ForeColor = System.Drawing.Color.White;
            stateProperties91.IconLeftImage = null;
            stateProperties91.IconRightImage = null;
            this.bunifuButton18.onHoverState = stateProperties91;
            stateProperties92.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties92.BorderRadius = 3;
            stateProperties92.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties92.BorderThickness = 1;
            stateProperties92.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties92.ForeColor = System.Drawing.Color.White;
            stateProperties92.IconLeftImage = null;
            stateProperties92.IconRightImage = null;
            this.bunifuButton18.OnPressedState = stateProperties92;
            this.bunifuButton18.Size = new System.Drawing.Size(140, 33);
            this.bunifuButton18.TabIndex = 6;
            this.bunifuButton18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton18.TextMarginLeft = 0;
            this.bunifuButton18.UseDefaultRadiusAndThickness = true;
            this.bunifuButton18.Click += new System.EventHandler(this.bunifuButton18_Click);
            // 
            // tokenSampleDeletebutton
            // 
            this.tokenSampleDeletebutton.AllowToggling = false;
            this.tokenSampleDeletebutton.AnimationSpeed = 200;
            this.tokenSampleDeletebutton.AutoGenerateColors = false;
            this.tokenSampleDeletebutton.BackColor = System.Drawing.Color.Transparent;
            this.tokenSampleDeletebutton.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.tokenSampleDeletebutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tokenSampleDeletebutton.BackgroundImage")));
            this.tokenSampleDeletebutton.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.tokenSampleDeletebutton.ButtonText = "Delete";
            this.tokenSampleDeletebutton.ButtonTextMarginLeft = 0;
            this.tokenSampleDeletebutton.ColorContrastOnClick = 45;
            this.tokenSampleDeletebutton.ColorContrastOnHover = 45;
            this.tokenSampleDeletebutton.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges31.BottomLeft = true;
            borderEdges31.BottomRight = true;
            borderEdges31.TopLeft = true;
            borderEdges31.TopRight = true;
            this.tokenSampleDeletebutton.CustomizableEdges = borderEdges31;
            this.tokenSampleDeletebutton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.tokenSampleDeletebutton.DisabledBorderColor = System.Drawing.Color.Empty;
            this.tokenSampleDeletebutton.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.tokenSampleDeletebutton.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.tokenSampleDeletebutton.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.tokenSampleDeletebutton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.tokenSampleDeletebutton.ForeColor = System.Drawing.Color.White;
            this.tokenSampleDeletebutton.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.tokenSampleDeletebutton.IconMarginLeft = 11;
            this.tokenSampleDeletebutton.IconPadding = 10;
            this.tokenSampleDeletebutton.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.tokenSampleDeletebutton.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.tokenSampleDeletebutton.IdleBorderRadius = 3;
            this.tokenSampleDeletebutton.IdleBorderThickness = 1;
            this.tokenSampleDeletebutton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.tokenSampleDeletebutton.IdleIconLeftImage = null;
            this.tokenSampleDeletebutton.IdleIconRightImage = null;
            this.tokenSampleDeletebutton.IndicateFocus = false;
            this.tokenSampleDeletebutton.Location = new System.Drawing.Point(869, 332);
            this.tokenSampleDeletebutton.Name = "tokenSampleDeletebutton";
            stateProperties93.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties93.BorderRadius = 3;
            stateProperties93.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties93.BorderThickness = 1;
            stateProperties93.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties93.ForeColor = System.Drawing.Color.White;
            stateProperties93.IconLeftImage = null;
            stateProperties93.IconRightImage = null;
            this.tokenSampleDeletebutton.onHoverState = stateProperties93;
            stateProperties94.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties94.BorderRadius = 3;
            stateProperties94.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties94.BorderThickness = 1;
            stateProperties94.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties94.ForeColor = System.Drawing.Color.White;
            stateProperties94.IconLeftImage = null;
            stateProperties94.IconRightImage = null;
            this.tokenSampleDeletebutton.OnPressedState = stateProperties94;
            this.tokenSampleDeletebutton.Size = new System.Drawing.Size(141, 33);
            this.tokenSampleDeletebutton.TabIndex = 157;
            this.tokenSampleDeletebutton.TabStop = false;
            this.tokenSampleDeletebutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tokenSampleDeletebutton.TextMarginLeft = 0;
            this.tokenSampleDeletebutton.UseDefaultRadiusAndThickness = true;
            this.tokenSampleDeletebutton.Click += new System.EventHandler(this.bunifuButton19_Click);
            // 
            // tokenRate
            // 
            this.tokenRate.AcceptsReturn = false;
            this.tokenRate.AcceptsTab = false;
            this.tokenRate.AnimationSpeed = 200;
            this.tokenRate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tokenRate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tokenRate.BackColor = System.Drawing.Color.Transparent;
            this.tokenRate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tokenRate.BackgroundImage")));
            this.tokenRate.BorderColorActive = System.Drawing.Color.Goldenrod;
            this.tokenRate.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tokenRate.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.tokenRate.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tokenRate.BorderRadius = 1;
            this.tokenRate.BorderThickness = 1;
            this.tokenRate.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tokenRate.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tokenRate.DefaultFont = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tokenRate.DefaultText = "50";
            this.tokenRate.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.tokenRate.ForeColor = System.Drawing.Color.White;
            this.tokenRate.HideSelection = true;
            this.tokenRate.IconLeft = null;
            this.tokenRate.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.tokenRate.IconPadding = 3;
            this.tokenRate.IconRight = null;
            this.tokenRate.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.tokenRate.Lines = new string[] {
        "50"};
            this.tokenRate.Location = new System.Drawing.Point(706, 86);
            this.tokenRate.MaxLength = 32767;
            this.tokenRate.MinimumSize = new System.Drawing.Size(0, 33);
            this.tokenRate.Modified = false;
            this.tokenRate.Multiline = false;
            this.tokenRate.Name = "tokenRate";
            stateProperties95.BorderColor = System.Drawing.Color.Red;
            stateProperties95.FillColor = System.Drawing.Color.Empty;
            stateProperties95.ForeColor = System.Drawing.Color.Empty;
            stateProperties95.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.tokenRate.OnActiveState = stateProperties95;
            stateProperties96.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties96.FillColor = System.Drawing.Color.White;
            stateProperties96.ForeColor = System.Drawing.Color.Empty;
            stateProperties96.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.tokenRate.OnDisabledState = stateProperties96;
            stateProperties97.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties97.FillColor = System.Drawing.Color.Empty;
            stateProperties97.ForeColor = System.Drawing.Color.Empty;
            stateProperties97.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.tokenRate.OnHoverState = stateProperties97;
            stateProperties98.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties98.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties98.ForeColor = System.Drawing.Color.White;
            stateProperties98.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.tokenRate.OnIdleState = stateProperties98;
            this.tokenRate.PasswordChar = '\0';
            this.tokenRate.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.tokenRate.PlaceholderText = "₹";
            this.tokenRate.ReadOnly = false;
            this.tokenRate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tokenRate.SelectedText = "";
            this.tokenRate.SelectionLength = 0;
            this.tokenRate.SelectionStart = 2;
            this.tokenRate.ShortcutsEnabled = true;
            this.tokenRate.Size = new System.Drawing.Size(304, 52);
            this.tokenRate.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.tokenRate.TabIndex = 4;
            this.tokenRate.TabStop = false;
            this.tokenRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tokenRate.TextMarginBottom = 0;
            this.tokenRate.TextMarginLeft = 5;
            this.tokenRate.TextMarginTop = 0;
            this.tokenRate.TextPlaceholder = "₹";
            this.tokenRate.UseSystemPasswordChar = false;
            this.tokenRate.WordWrap = true;
            this.tokenRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tokenRate_KeyDown);
            this.tokenRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tokenRate_KeyPress);
            // 
            // tokenWeightText
            // 
            this.tokenWeightText.AcceptsReturn = false;
            this.tokenWeightText.AcceptsTab = false;
            this.tokenWeightText.AnimationSpeed = 200;
            this.tokenWeightText.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tokenWeightText.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tokenWeightText.BackColor = System.Drawing.Color.Transparent;
            this.tokenWeightText.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tokenWeightText.BackgroundImage")));
            this.tokenWeightText.BorderColorActive = System.Drawing.Color.Goldenrod;
            this.tokenWeightText.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tokenWeightText.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.tokenWeightText.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tokenWeightText.BorderRadius = 1;
            this.tokenWeightText.BorderThickness = 1;
            this.tokenWeightText.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tokenWeightText.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tokenWeightText.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tokenWeightText.DefaultText = "";
            this.tokenWeightText.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.tokenWeightText.ForeColor = System.Drawing.Color.White;
            this.tokenWeightText.HideSelection = true;
            this.tokenWeightText.IconLeft = null;
            this.tokenWeightText.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.tokenWeightText.IconPadding = 3;
            this.tokenWeightText.IconRight = global::GoldRaven.Properties.Resources.Layer_11;
            this.tokenWeightText.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.tokenWeightText.Lines = new string[0];
            this.tokenWeightText.Location = new System.Drawing.Point(373, 332);
            this.tokenWeightText.MaxLength = 32767;
            this.tokenWeightText.MinimumSize = new System.Drawing.Size(0, 33);
            this.tokenWeightText.Modified = false;
            this.tokenWeightText.Multiline = false;
            this.tokenWeightText.Name = "tokenWeightText";
            stateProperties99.BorderColor = System.Drawing.Color.Red;
            stateProperties99.FillColor = System.Drawing.Color.Empty;
            stateProperties99.ForeColor = System.Drawing.Color.Empty;
            stateProperties99.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.tokenWeightText.OnActiveState = stateProperties99;
            stateProperties100.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties100.FillColor = System.Drawing.Color.White;
            stateProperties100.ForeColor = System.Drawing.Color.Empty;
            stateProperties100.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.tokenWeightText.OnDisabledState = stateProperties100;
            stateProperties101.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties101.FillColor = System.Drawing.Color.Empty;
            stateProperties101.ForeColor = System.Drawing.Color.Empty;
            stateProperties101.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.tokenWeightText.OnHoverState = stateProperties101;
            stateProperties102.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties102.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties102.ForeColor = System.Drawing.Color.White;
            stateProperties102.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.tokenWeightText.OnIdleState = stateProperties102;
            this.tokenWeightText.PasswordChar = '\0';
            this.tokenWeightText.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.tokenWeightText.PlaceholderText = "Enter Weight";
            this.tokenWeightText.ReadOnly = false;
            this.tokenWeightText.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tokenWeightText.SelectedText = "";
            this.tokenWeightText.SelectionLength = 0;
            this.tokenWeightText.SelectionStart = 0;
            this.tokenWeightText.ShortcutsEnabled = true;
            this.tokenWeightText.Size = new System.Drawing.Size(298, 33);
            this.tokenWeightText.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.tokenWeightText.TabIndex = 26;
            this.tokenWeightText.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tokenWeightText.TextMarginBottom = 0;
            this.tokenWeightText.TextMarginLeft = 5;
            this.tokenWeightText.TextMarginTop = 0;
            this.tokenWeightText.TextPlaceholder = "Enter Weight";
            this.tokenWeightText.UseSystemPasswordChar = false;
            this.tokenWeightText.WordWrap = true;
            this.tokenWeightText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tokenWeightText_KeyDown);
            this.tokenWeightText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tokenWeightText_KeyPress);
            // 
            // tokenDatePicker
            // 
            this.tokenDatePicker.BackColor = System.Drawing.Color.Transparent;
            this.tokenDatePicker.BorderRadius = 1;
            this.tokenDatePicker.Color = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tokenDatePicker.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.tokenDatePicker.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Right;
            this.tokenDatePicker.DisabledColor = System.Drawing.Color.Gray;
            this.tokenDatePicker.DisplayWeekNumbers = false;
            this.tokenDatePicker.DPHeight = 0;
            this.tokenDatePicker.FillDatePicker = false;
            this.tokenDatePicker.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.tokenDatePicker.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tokenDatePicker.Icon = ((System.Drawing.Image)(resources.GetObject("tokenDatePicker.Icon")));
            this.tokenDatePicker.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.tokenDatePicker.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Left;
            this.tokenDatePicker.Location = new System.Drawing.Point(261, 86);
            this.tokenDatePicker.MinimumSize = new System.Drawing.Size(217, 33);
            this.tokenDatePicker.Name = "tokenDatePicker";
            this.tokenDatePicker.Size = new System.Drawing.Size(217, 33);
            this.tokenDatePicker.TabIndex = 0;
            this.tokenDatePicker.TabStop = false;
            this.tokenDatePicker.ValueChanged += new System.EventHandler(this.tokenDatePicker_ValueChanged);
            this.tokenDatePicker.VisibleChanged += new System.EventHandler(this.tokenDatePicker_VisibleChanged);
            // 
            // testPage
            // 
            this.testPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.testPage.Controls.Add(this.testingDropdown);
            this.testPage.Controls.Add(this.bunifuLabel89);
            this.testPage.Controls.Add(this.testDatagridView);
            this.testPage.Controls.Add(this.bunifuLabel41);
            this.testPage.Controls.Add(this.testId);
            this.testPage.Controls.Add(this.testSmaplecomboBox);
            this.testPage.Controls.Add(this.testCustcombo);
            this.testPage.Controls.Add(this.testTime);
            this.testPage.Controls.Add(this.bunifuLabel43);
            this.testPage.Controls.Add(this.bunifuLabel70);
            this.testPage.Controls.Add(this.bunifuLabel71);
            this.testPage.Controls.Add(this.bunifuLabel72);
            this.testPage.Controls.Add(this.bunifuLabel73);
            this.testPage.Controls.Add(this.bunifuLabel74);
            this.testPage.Controls.Add(this.bunifuLabel75);
            this.testPage.Controls.Add(this.bunifuLabel77);
            this.testPage.Controls.Add(this.bunifuLabel78);
            this.testPage.Controls.Add(this.bunifuLabel79);
            this.testPage.Controls.Add(this.testSeries);
            this.testPage.Controls.Add(this.bunifuLabel80);
            this.testPage.Controls.Add(this.label11);
            this.testPage.Controls.Add(this.testgoldFine);
            this.testPage.Controls.Add(this.testAmount);
            this.testPage.Controls.Add(this.testtounchweight);
            this.testPage.Controls.Add(this.testorgweight);
            this.testPage.Controls.Add(this.testKarat);
            this.testPage.Controls.Add(this.testPurity);
            this.testPage.Controls.Add(this.testDate);
            this.testPage.Controls.Add(this.bunifuButton21);
            this.testPage.Controls.Add(this.bunifuButton22);
            this.testPage.Controls.Add(this.bunifuButton23);
            this.testPage.Controls.Add(this.bunifuButton24);
            this.testPage.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.testPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.testPage.Location = new System.Drawing.Point(4, 4);
            this.testPage.Name = "testPage";
            this.testPage.Padding = new System.Windows.Forms.Padding(3);
            this.testPage.Size = new System.Drawing.Size(1041, 728);
            this.testPage.TabIndex = 5;
            this.testPage.Text = "Testing";
            this.testPage.Click += new System.EventHandler(this.testPage_Click);
            // 
            // testingDropdown
            // 
            this.testingDropdown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.testingDropdown.BorderRadius = 1;
            this.testingDropdown.Color = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testingDropdown.Direction = Bunifu.UI.WinForms.BunifuDropdown.Directions.Down;
            this.testingDropdown.DisabledColor = System.Drawing.Color.Gray;
            this.testingDropdown.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.testingDropdown.DropdownBorderThickness = Bunifu.UI.WinForms.BunifuDropdown.BorderThickness.Thick;
            this.testingDropdown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.testingDropdown.DropDownTextAlign = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left;
            this.testingDropdown.FillDropDown = false;
            this.testingDropdown.FillIndicator = false;
            this.testingDropdown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.testingDropdown.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testingDropdown.ForeColor = System.Drawing.Color.White;
            this.testingDropdown.FormattingEnabled = true;
            this.testingDropdown.Icon = null;
            this.testingDropdown.IndicatorColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testingDropdown.IndicatorLocation = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right;
            this.testingDropdown.ItemBackColor = System.Drawing.Color.White;
            this.testingDropdown.ItemBorderColor = System.Drawing.Color.White;
            this.testingDropdown.ItemForeColor = System.Drawing.Color.Black;
            this.testingDropdown.ItemHeight = 26;
            this.testingDropdown.ItemHighLightColor = System.Drawing.Color.Aquamarine;
            this.testingDropdown.Items.AddRange(new object[] {
            "Silver",
            "Gold"});
            this.testingDropdown.Location = new System.Drawing.Point(306, 238);
            this.testingDropdown.Name = "testingDropdown";
            this.testingDropdown.Size = new System.Drawing.Size(186, 32);
            this.testingDropdown.TabIndex = 7;
            this.testingDropdown.Text = null;
            this.testingDropdown.SelectedValueChanged += new System.EventHandler(this.testingDropdown_SelectedValueChanged);
            // 
            // bunifuLabel89
            // 
            this.bunifuLabel89.AutoEllipsis = false;
            this.bunifuLabel89.CursorType = null;
            this.bunifuLabel89.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel89.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel89.Location = new System.Drawing.Point(305, 212);
            this.bunifuLabel89.Name = "bunifuLabel89";
            this.bunifuLabel89.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel89.Size = new System.Drawing.Size(79, 23);
            this.bunifuLabel89.TabIndex = 216;
            this.bunifuLabel89.TabStop = false;
            this.bunifuLabel89.Text = "Metal Type";
            this.bunifuLabel89.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel89.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel89.Click += new System.EventHandler(this.bunifuLabel89_Click);
            // 
            // testDatagridView
            // 
            this.testDatagridView.AllowUserToAddRows = false;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.testDatagridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle24;
            this.testDatagridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.testDatagridView.BackgroundColor = System.Drawing.Color.White;
            this.testDatagridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.testDatagridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.testDatagridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.testDatagridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.testDatagridView.ColumnHeadersHeight = 50;
            this.testDatagridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn41,
            this.Column79,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45,
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47});
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(252)))));
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.testDatagridView.DefaultCellStyle = dataGridViewCellStyle29;
            this.testDatagridView.DoubleBuffered = true;
            this.testDatagridView.EnableHeadersVisualStyles = false;
            this.testDatagridView.GridColor = System.Drawing.Color.WhiteSmoke;
            this.testDatagridView.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            this.testDatagridView.HeaderForeColor = System.Drawing.Color.White;
            this.testDatagridView.Location = new System.Drawing.Point(20, 354);
            this.testDatagridView.Name = "testDatagridView";
            this.testDatagridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.testDatagridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.testDatagridView.RowHeadersVisible = false;
            this.testDatagridView.RowHeadersWidth = 51;
            this.testDatagridView.RowTemplate.DividerHeight = 1;
            this.testDatagridView.RowTemplate.Height = 40;
            this.testDatagridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.testDatagridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.testDatagridView.Size = new System.Drawing.Size(991, 393);
            this.testDatagridView.TabIndex = 215;
            this.testDatagridView.TabStop = false;
            this.testDatagridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.testDatagridView_CellClick);
            this.testDatagridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.testDatagridView_CellContentClick);
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "payment";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Paid/Unpaid";
            this.dataGridViewCheckBoxColumn1.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 109;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn35.HeaderText = "Serial No";
            this.dataGridViewTextBoxColumn35.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.ReadOnly = true;
            this.dataGridViewTextBoxColumn35.Width = 106;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.DataPropertyName = "series";
            this.dataGridViewTextBoxColumn36.HeaderText = "Series No";
            this.dataGridViewTextBoxColumn36.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.ReadOnly = true;
            this.dataGridViewTextBoxColumn36.Width = 109;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "date";
            dataGridViewCellStyle26.Format = "D";
            dataGridViewCellStyle26.NullValue = null;
            this.dataGridViewTextBoxColumn37.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn37.HeaderText = "Date";
            this.dataGridViewTextBoxColumn37.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            this.dataGridViewTextBoxColumn37.Width = 73;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "time";
            this.dataGridViewTextBoxColumn38.HeaderText = "Time";
            this.dataGridViewTextBoxColumn38.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.Width = 74;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.DataPropertyName = "customerName";
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.Teal;
            this.dataGridViewTextBoxColumn39.DefaultCellStyle = dataGridViewCellStyle27;
            this.dataGridViewTextBoxColumn39.HeaderText = "Name";
            this.dataGridViewTextBoxColumn39.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            this.dataGridViewTextBoxColumn39.Width = 83;
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.DataPropertyName = "sample";
            this.dataGridViewTextBoxColumn41.HeaderText = "Sample";
            this.dataGridViewTextBoxColumn41.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.ReadOnly = true;
            this.dataGridViewTextBoxColumn41.Width = 93;
            // 
            // Column79
            // 
            this.Column79.DataPropertyName = "type";
            this.Column79.HeaderText = "Type";
            this.Column79.MinimumWidth = 6;
            this.Column79.Name = "Column79";
            this.Column79.ReadOnly = true;
            this.Column79.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column79.Width = 73;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.DataPropertyName = "purity";
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dataGridViewTextBoxColumn42.DefaultCellStyle = dataGridViewCellStyle28;
            this.dataGridViewTextBoxColumn42.HeaderText = "Purity";
            this.dataGridViewTextBoxColumn42.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            this.dataGridViewTextBoxColumn42.Width = 82;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.DataPropertyName = "karat";
            this.dataGridViewTextBoxColumn43.HeaderText = "Karat";
            this.dataGridViewTextBoxColumn43.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            this.dataGridViewTextBoxColumn43.Width = 77;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.DataPropertyName = "orgweight";
            this.dataGridViewTextBoxColumn44.HeaderText = "Weight";
            this.dataGridViewTextBoxColumn44.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.ReadOnly = true;
            this.dataGridViewTextBoxColumn44.Width = 92;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.DataPropertyName = "tounchweight";
            this.dataGridViewTextBoxColumn45.HeaderText = "Tounch Weight";
            this.dataGridViewTextBoxColumn45.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.ReadOnly = true;
            this.dataGridViewTextBoxColumn45.Width = 152;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.DataPropertyName = "goldFine";
            this.dataGridViewTextBoxColumn46.HeaderText = "Gold Fine";
            this.dataGridViewTextBoxColumn46.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.ReadOnly = true;
            this.dataGridViewTextBoxColumn46.Width = 110;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.DataPropertyName = "Amount";
            this.dataGridViewTextBoxColumn47.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn47.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.ReadOnly = true;
            this.dataGridViewTextBoxColumn47.Width = 72;
            // 
            // bunifuLabel41
            // 
            this.bunifuLabel41.AutoEllipsis = false;
            this.bunifuLabel41.CursorType = null;
            this.bunifuLabel41.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel41.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel41.Location = new System.Drawing.Point(505, 212);
            this.bunifuLabel41.Name = "bunifuLabel41";
            this.bunifuLabel41.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel41.Size = new System.Drawing.Size(69, 23);
            this.bunifuLabel41.TabIndex = 212;
            this.bunifuLabel41.TabStop = false;
            this.bunifuLabel41.Text = "Gold Fine";
            this.bunifuLabel41.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel41.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // testId
            // 
            this.testId.AutoEllipsis = false;
            this.testId.CursorType = null;
            this.testId.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testId.ForeColor = System.Drawing.Color.White;
            this.testId.Location = new System.Drawing.Point(829, 32);
            this.testId.Name = "testId";
            this.testId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.testId.Size = new System.Drawing.Size(182, 39);
            this.testId.TabIndex = 211;
            this.testId.TabStop = false;
            this.testId.Text = "Serial number";
            this.testId.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.testId.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // testSmaplecomboBox
            // 
            this.testSmaplecomboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.testSmaplecomboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.testSmaplecomboBox.Font = new System.Drawing.Font("Century Gothic", 12.5F, System.Drawing.FontStyle.Bold);
            this.testSmaplecomboBox.ForeColor = System.Drawing.Color.Black;
            this.testSmaplecomboBox.FormattingEnabled = true;
            this.testSmaplecomboBox.ItemHeight = 25;
            this.testSmaplecomboBox.Location = new System.Drawing.Point(20, 239);
            this.testSmaplecomboBox.Name = "testSmaplecomboBox";
            this.testSmaplecomboBox.Size = new System.Drawing.Size(272, 33);
            this.testSmaplecomboBox.TabIndex = 3;
            // 
            // testCustcombo
            // 
            this.testCustcombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.testCustcombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.testCustcombo.Font = new System.Drawing.Font("Century Gothic", 12.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testCustcombo.ForeColor = System.Drawing.Color.Black;
            this.testCustcombo.FormattingEnabled = true;
            this.testCustcombo.ItemHeight = 25;
            this.testCustcombo.Location = new System.Drawing.Point(20, 173);
            this.testCustcombo.Name = "testCustcombo";
            this.testCustcombo.Size = new System.Drawing.Size(272, 33);
            this.testCustcombo.TabIndex = 2;
            this.testCustcombo.SelectedIndexChanged += new System.EventHandler(this.testCustcombo_SelectedIndexChanged);
            this.testCustcombo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.testCustcombo_KeyDown);
            this.testCustcombo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.testCustcombo_KeyPress);
            this.testCustcombo.Leave += new System.EventHandler(this.testCustcombo_Leave);
            // 
            // testTime
            // 
            this.testTime.CalendarFont = new System.Drawing.Font("Century Gothic", 7.75F, System.Drawing.FontStyle.Bold);
            this.testTime.CalendarMonthBackground = System.Drawing.Color.SeaGreen;
            this.testTime.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.testTime.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.testTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.testTime.Location = new System.Drawing.Point(514, 104);
            this.testTime.Margin = new System.Windows.Forms.Padding(4);
            this.testTime.MaximumSize = new System.Drawing.Size(4, 23);
            this.testTime.MinimumSize = new System.Drawing.Size(180, 33);
            this.testTime.Name = "testTime";
            this.testTime.ShowUpDown = true;
            this.testTime.Size = new System.Drawing.Size(180, 33);
            this.testTime.TabIndex = 190;
            this.testTime.TabStop = false;
            // 
            // bunifuLabel43
            // 
            this.bunifuLabel43.AutoEllipsis = false;
            this.bunifuLabel43.CursorType = null;
            this.bunifuLabel43.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel43.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel43.Location = new System.Drawing.Point(728, 74);
            this.bunifuLabel43.Name = "bunifuLabel43";
            this.bunifuLabel43.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel43.Size = new System.Drawing.Size(34, 23);
            this.bunifuLabel43.TabIndex = 189;
            this.bunifuLabel43.TabStop = false;
            this.bunifuLabel43.Text = "Rate";
            this.bunifuLabel43.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel43.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel70
            // 
            this.bunifuLabel70.AutoEllipsis = false;
            this.bunifuLabel70.CursorType = null;
            this.bunifuLabel70.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel70.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel70.Location = new System.Drawing.Point(20, 278);
            this.bunifuLabel70.Name = "bunifuLabel70";
            this.bunifuLabel70.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel70.Size = new System.Drawing.Size(108, 23);
            this.bunifuLabel70.TabIndex = 186;
            this.bunifuLabel70.TabStop = false;
            this.bunifuLabel70.Text = "Tounch Weight";
            this.bunifuLabel70.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel70.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel71
            // 
            this.bunifuLabel71.AutoEllipsis = false;
            this.bunifuLabel71.CursorType = null;
            this.bunifuLabel71.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel71.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel71.Location = new System.Drawing.Point(505, 142);
            this.bunifuLabel71.Name = "bunifuLabel71";
            this.bunifuLabel71.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel71.Size = new System.Drawing.Size(39, 23);
            this.bunifuLabel71.TabIndex = 193;
            this.bunifuLabel71.TabStop = false;
            this.bunifuLabel71.Text = "Karat";
            this.bunifuLabel71.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel71.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel72
            // 
            this.bunifuLabel72.AutoEllipsis = false;
            this.bunifuLabel72.CursorType = null;
            this.bunifuLabel72.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel72.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel72.Location = new System.Drawing.Point(306, 142);
            this.bunifuLabel72.Name = "bunifuLabel72";
            this.bunifuLabel72.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel72.Size = new System.Drawing.Size(44, 23);
            this.bunifuLabel72.TabIndex = 184;
            this.bunifuLabel72.TabStop = false;
            this.bunifuLabel72.Text = "Purity";
            this.bunifuLabel72.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel72.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel73
            // 
            this.bunifuLabel73.AutoEllipsis = false;
            this.bunifuLabel73.CursorType = null;
            this.bunifuLabel73.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel73.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel73.Location = new System.Drawing.Point(336, 278);
            this.bunifuLabel73.Name = "bunifuLabel73";
            this.bunifuLabel73.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel73.Size = new System.Drawing.Size(53, 23);
            this.bunifuLabel73.TabIndex = 183;
            this.bunifuLabel73.TabStop = false;
            this.bunifuLabel73.Text = "Weight";
            this.bunifuLabel73.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel73.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel74
            // 
            this.bunifuLabel74.AutoEllipsis = false;
            this.bunifuLabel74.CursorType = null;
            this.bunifuLabel74.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel74.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel74.Location = new System.Drawing.Point(269, 73);
            this.bunifuLabel74.Name = "bunifuLabel74";
            this.bunifuLabel74.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel74.Size = new System.Drawing.Size(35, 23);
            this.bunifuLabel74.TabIndex = 182;
            this.bunifuLabel74.TabStop = false;
            this.bunifuLabel74.Text = "Date";
            this.bunifuLabel74.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel74.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel75
            // 
            this.bunifuLabel75.AutoEllipsis = false;
            this.bunifuLabel75.CursorType = null;
            this.bunifuLabel75.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel75.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel75.Location = new System.Drawing.Point(516, 72);
            this.bunifuLabel75.Name = "bunifuLabel75";
            this.bunifuLabel75.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel75.Size = new System.Drawing.Size(37, 23);
            this.bunifuLabel75.TabIndex = 181;
            this.bunifuLabel75.TabStop = false;
            this.bunifuLabel75.Text = "Time";
            this.bunifuLabel75.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel75.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel77
            // 
            this.bunifuLabel77.AutoEllipsis = false;
            this.bunifuLabel77.CursorType = null;
            this.bunifuLabel77.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel77.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel77.Location = new System.Drawing.Point(20, 209);
            this.bunifuLabel77.Name = "bunifuLabel77";
            this.bunifuLabel77.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel77.Size = new System.Drawing.Size(55, 23);
            this.bunifuLabel77.TabIndex = 169;
            this.bunifuLabel77.TabStop = false;
            this.bunifuLabel77.Text = "Sample";
            this.bunifuLabel77.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel77.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel78
            // 
            this.bunifuLabel78.AutoEllipsis = false;
            this.bunifuLabel78.CursorType = null;
            this.bunifuLabel78.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel78.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel78.Location = new System.Drawing.Point(20, 142);
            this.bunifuLabel78.Name = "bunifuLabel78";
            this.bunifuLabel78.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel78.Size = new System.Drawing.Size(117, 23);
            this.bunifuLabel78.TabIndex = 178;
            this.bunifuLabel78.TabStop = false;
            this.bunifuLabel78.Text = "Customer Name";
            this.bunifuLabel78.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel78.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel79
            // 
            this.bunifuLabel79.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bunifuLabel79.AutoEllipsis = false;
            this.bunifuLabel79.CursorType = null;
            this.bunifuLabel79.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel79.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel79.Location = new System.Drawing.Point(21, 73);
            this.bunifuLabel79.Name = "bunifuLabel79";
            this.bunifuLabel79.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel79.Size = new System.Drawing.Size(70, 23);
            this.bunifuLabel79.TabIndex = 177;
            this.bunifuLabel79.TabStop = false;
            this.bunifuLabel79.Text = "Series No";
            this.bunifuLabel79.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel79.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // testSeries
            // 
            this.testSeries.BorderColorFocused = System.Drawing.Color.Red;
            this.testSeries.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testSeries.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.testSeries.BorderThickness = 1;
            this.testSeries.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.testSeries.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.testSeries.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.testSeries.ForeColor = System.Drawing.Color.White;
            this.testSeries.isPassword = false;
            this.testSeries.Location = new System.Drawing.Point(20, 104);
            this.testSeries.Margin = new System.Windows.Forms.Padding(4);
            this.testSeries.MaxLength = 32767;
            this.testSeries.Name = "testSeries";
            this.testSeries.Size = new System.Drawing.Size(215, 33);
            this.testSeries.TabIndex = 1;
            this.testSeries.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.testSeries.KeyDown += new System.Windows.Forms.KeyEventHandler(this.testSeries_KeyDown);
            this.testSeries.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.testSeries_KeyPress);
            // 
            // bunifuLabel80
            // 
            this.bunifuLabel80.AutoEllipsis = false;
            this.bunifuLabel80.CursorType = null;
            this.bunifuLabel80.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel80.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel80.Location = new System.Drawing.Point(753, 43);
            this.bunifuLabel80.Name = "bunifuLabel80";
            this.bunifuLabel80.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel80.Size = new System.Drawing.Size(70, 23);
            this.bunifuLabel80.TabIndex = 176;
            this.bunifuLabel80.TabStop = false;
            this.bunifuLabel80.Text = "Serial No.";
            this.bunifuLabel80.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel80.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label11.Location = new System.Drawing.Point(14, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(188, 35);
            this.label11.TabIndex = 175;
            this.label11.Text = "Testing Report";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // testgoldFine
            // 
            this.testgoldFine.AcceptsReturn = false;
            this.testgoldFine.AcceptsTab = false;
            this.testgoldFine.AnimationSpeed = 200;
            this.testgoldFine.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.testgoldFine.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.testgoldFine.BackColor = System.Drawing.Color.Transparent;
            this.testgoldFine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("testgoldFine.BackgroundImage")));
            this.testgoldFine.BorderColorActive = System.Drawing.Color.Goldenrod;
            this.testgoldFine.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testgoldFine.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.testgoldFine.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testgoldFine.BorderRadius = 1;
            this.testgoldFine.BorderThickness = 1;
            this.testgoldFine.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.testgoldFine.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.testgoldFine.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold);
            this.testgoldFine.DefaultText = "";
            this.testgoldFine.Enabled = false;
            this.testgoldFine.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.testgoldFine.ForeColor = System.Drawing.Color.White;
            this.testgoldFine.HideSelection = true;
            this.testgoldFine.IconLeft = null;
            this.testgoldFine.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.testgoldFine.IconPadding = 3;
            this.testgoldFine.IconRight = global::GoldRaven.Properties.Resources.Layer_1;
            this.testgoldFine.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.testgoldFine.Lines = new string[0];
            this.testgoldFine.Location = new System.Drawing.Point(509, 238);
            this.testgoldFine.MaxLength = 32767;
            this.testgoldFine.MinimumSize = new System.Drawing.Size(0, 33);
            this.testgoldFine.Modified = false;
            this.testgoldFine.Multiline = false;
            this.testgoldFine.Name = "testgoldFine";
            stateProperties103.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties103.FillColor = System.Drawing.Color.Empty;
            stateProperties103.ForeColor = System.Drawing.Color.Empty;
            stateProperties103.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testgoldFine.OnActiveState = stateProperties103;
            stateProperties104.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties104.FillColor = System.Drawing.Color.White;
            stateProperties104.ForeColor = System.Drawing.Color.Empty;
            stateProperties104.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.testgoldFine.OnDisabledState = stateProperties104;
            stateProperties105.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties105.FillColor = System.Drawing.Color.Empty;
            stateProperties105.ForeColor = System.Drawing.Color.Empty;
            stateProperties105.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testgoldFine.OnHoverState = stateProperties105;
            stateProperties106.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties106.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties106.ForeColor = System.Drawing.Color.White;
            stateProperties106.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testgoldFine.OnIdleState = stateProperties106;
            this.testgoldFine.PasswordChar = '\0';
            this.testgoldFine.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.testgoldFine.PlaceholderText = "Fine";
            this.testgoldFine.ReadOnly = true;
            this.testgoldFine.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.testgoldFine.SelectedText = "";
            this.testgoldFine.SelectionLength = 0;
            this.testgoldFine.SelectionStart = 0;
            this.testgoldFine.ShortcutsEnabled = true;
            this.testgoldFine.Size = new System.Drawing.Size(187, 33);
            this.testgoldFine.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.testgoldFine.TabIndex = 0;
            this.testgoldFine.TabStop = false;
            this.testgoldFine.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.testgoldFine.TextMarginBottom = 0;
            this.testgoldFine.TextMarginLeft = 5;
            this.testgoldFine.TextMarginTop = 0;
            this.testgoldFine.TextPlaceholder = "Fine";
            this.testgoldFine.UseSystemPasswordChar = false;
            this.testgoldFine.WordWrap = true;
            this.testgoldFine.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.testgoldFine_KeyPress);
            // 
            // testAmount
            // 
            this.testAmount.AcceptsReturn = false;
            this.testAmount.AcceptsTab = false;
            this.testAmount.AnimationSpeed = 200;
            this.testAmount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.testAmount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.testAmount.BackColor = System.Drawing.Color.Transparent;
            this.testAmount.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("testAmount.BackgroundImage")));
            this.testAmount.BorderColorActive = System.Drawing.Color.Red;
            this.testAmount.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testAmount.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.testAmount.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testAmount.BorderRadius = 1;
            this.testAmount.BorderThickness = 1;
            this.testAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.testAmount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.testAmount.DefaultFont = new System.Drawing.Font("Century Gothic", 44F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testAmount.DefaultText = "30";
            this.testAmount.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.testAmount.ForeColor = System.Drawing.Color.White;
            this.testAmount.HideSelection = true;
            this.testAmount.IconLeft = null;
            this.testAmount.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.testAmount.IconPadding = 3;
            this.testAmount.IconRight = null;
            this.testAmount.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.testAmount.Lines = new string[] {
        "30"};
            this.testAmount.Location = new System.Drawing.Point(725, 104);
            this.testAmount.MaxLength = 32767;
            this.testAmount.MinimumSize = new System.Drawing.Size(0, 33);
            this.testAmount.Modified = false;
            this.testAmount.Multiline = false;
            this.testAmount.Name = "testAmount";
            stateProperties107.BorderColor = System.Drawing.Color.Red;
            stateProperties107.FillColor = System.Drawing.Color.Empty;
            stateProperties107.ForeColor = System.Drawing.Color.Empty;
            stateProperties107.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testAmount.OnActiveState = stateProperties107;
            stateProperties108.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties108.FillColor = System.Drawing.Color.White;
            stateProperties108.ForeColor = System.Drawing.Color.Empty;
            stateProperties108.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.testAmount.OnDisabledState = stateProperties108;
            stateProperties109.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties109.FillColor = System.Drawing.Color.Empty;
            stateProperties109.ForeColor = System.Drawing.Color.Empty;
            stateProperties109.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testAmount.OnHoverState = stateProperties109;
            stateProperties110.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties110.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties110.ForeColor = System.Drawing.Color.White;
            stateProperties110.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testAmount.OnIdleState = stateProperties110;
            this.testAmount.PasswordChar = '\0';
            this.testAmount.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.testAmount.PlaceholderText = "₹";
            this.testAmount.ReadOnly = false;
            this.testAmount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.testAmount.SelectedText = "";
            this.testAmount.SelectionLength = 0;
            this.testAmount.SelectionStart = 0;
            this.testAmount.ShortcutsEnabled = true;
            this.testAmount.Size = new System.Drawing.Size(285, 101);
            this.testAmount.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.testAmount.TabIndex = 8;
            this.testAmount.TabStop = false;
            this.testAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.testAmount.TextMarginBottom = 0;
            this.testAmount.TextMarginLeft = 5;
            this.testAmount.TextMarginTop = 0;
            this.testAmount.TextPlaceholder = "₹";
            this.testAmount.UseSystemPasswordChar = false;
            this.testAmount.WordWrap = true;
            this.testAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.testAmount_KeyDown);
            this.testAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.testAmount_KeyPress);
            // 
            // testtounchweight
            // 
            this.testtounchweight.AcceptsReturn = false;
            this.testtounchweight.AcceptsTab = false;
            this.testtounchweight.AnimationSpeed = 200;
            this.testtounchweight.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.testtounchweight.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.testtounchweight.BackColor = System.Drawing.Color.Transparent;
            this.testtounchweight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("testtounchweight.BackgroundImage")));
            this.testtounchweight.BorderColorActive = System.Drawing.Color.Red;
            this.testtounchweight.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testtounchweight.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.testtounchweight.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testtounchweight.BorderRadius = 1;
            this.testtounchweight.BorderThickness = 1;
            this.testtounchweight.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.testtounchweight.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.testtounchweight.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testtounchweight.DefaultText = "";
            this.testtounchweight.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.testtounchweight.ForeColor = System.Drawing.Color.White;
            this.testtounchweight.HideSelection = true;
            this.testtounchweight.IconLeft = null;
            this.testtounchweight.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.testtounchweight.IconPadding = 3;
            this.testtounchweight.IconRight = global::GoldRaven.Properties.Resources.Layer_11;
            this.testtounchweight.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.testtounchweight.Lines = new string[0];
            this.testtounchweight.Location = new System.Drawing.Point(19, 308);
            this.testtounchweight.MaxLength = 32767;
            this.testtounchweight.MinimumSize = new System.Drawing.Size(0, 33);
            this.testtounchweight.Modified = false;
            this.testtounchweight.Multiline = false;
            this.testtounchweight.Name = "testtounchweight";
            stateProperties111.BorderColor = System.Drawing.Color.Red;
            stateProperties111.FillColor = System.Drawing.Color.Empty;
            stateProperties111.ForeColor = System.Drawing.Color.Empty;
            stateProperties111.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testtounchweight.OnActiveState = stateProperties111;
            stateProperties112.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties112.FillColor = System.Drawing.Color.White;
            stateProperties112.ForeColor = System.Drawing.Color.Empty;
            stateProperties112.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.testtounchweight.OnDisabledState = stateProperties112;
            stateProperties113.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties113.FillColor = System.Drawing.Color.Empty;
            stateProperties113.ForeColor = System.Drawing.Color.Empty;
            stateProperties113.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testtounchweight.OnHoverState = stateProperties113;
            stateProperties114.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties114.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties114.ForeColor = System.Drawing.Color.White;
            stateProperties114.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testtounchweight.OnIdleState = stateProperties114;
            this.testtounchweight.PasswordChar = '\0';
            this.testtounchweight.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.testtounchweight.PlaceholderText = "Enter Weight";
            this.testtounchweight.ReadOnly = false;
            this.testtounchweight.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.testtounchweight.SelectedText = "";
            this.testtounchweight.SelectionLength = 0;
            this.testtounchweight.SelectionStart = 0;
            this.testtounchweight.ShortcutsEnabled = true;
            this.testtounchweight.Size = new System.Drawing.Size(295, 33);
            this.testtounchweight.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.testtounchweight.TabIndex = 4;
            this.testtounchweight.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.testtounchweight.TextMarginBottom = 0;
            this.testtounchweight.TextMarginLeft = 5;
            this.testtounchweight.TextMarginTop = 0;
            this.testtounchweight.TextPlaceholder = "Enter Weight";
            this.testtounchweight.UseSystemPasswordChar = false;
            this.testtounchweight.WordWrap = true;
            this.testtounchweight.TextChanged += new System.EventHandler(this.bunifuTextBox3_TextChanged);
            this.testtounchweight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.testtounchweight_KeyDown);
            this.testtounchweight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.testtounchweight_KeyPress);
            // 
            // testorgweight
            // 
            this.testorgweight.AcceptsReturn = false;
            this.testorgweight.AcceptsTab = false;
            this.testorgweight.AnimationSpeed = 200;
            this.testorgweight.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.testorgweight.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.testorgweight.BackColor = System.Drawing.Color.Transparent;
            this.testorgweight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("testorgweight.BackgroundImage")));
            this.testorgweight.BorderColorActive = System.Drawing.Color.Red;
            this.testorgweight.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testorgweight.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.testorgweight.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testorgweight.BorderRadius = 1;
            this.testorgweight.BorderThickness = 1;
            this.testorgweight.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.testorgweight.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.testorgweight.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testorgweight.DefaultText = "";
            this.testorgweight.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.testorgweight.ForeColor = System.Drawing.Color.White;
            this.testorgweight.HideSelection = true;
            this.testorgweight.IconLeft = null;
            this.testorgweight.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.testorgweight.IconPadding = 3;
            this.testorgweight.IconRight = global::GoldRaven.Properties.Resources.Layer_11;
            this.testorgweight.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.testorgweight.Lines = new string[0];
            this.testorgweight.Location = new System.Drawing.Point(336, 308);
            this.testorgweight.MaxLength = 32767;
            this.testorgweight.MinimumSize = new System.Drawing.Size(0, 33);
            this.testorgweight.Modified = false;
            this.testorgweight.Multiline = false;
            this.testorgweight.Name = "testorgweight";
            stateProperties115.BorderColor = System.Drawing.Color.Red;
            stateProperties115.FillColor = System.Drawing.Color.Empty;
            stateProperties115.ForeColor = System.Drawing.Color.Empty;
            stateProperties115.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testorgweight.OnActiveState = stateProperties115;
            stateProperties116.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties116.FillColor = System.Drawing.Color.White;
            stateProperties116.ForeColor = System.Drawing.Color.Empty;
            stateProperties116.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.testorgweight.OnDisabledState = stateProperties116;
            stateProperties117.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties117.FillColor = System.Drawing.Color.Empty;
            stateProperties117.ForeColor = System.Drawing.Color.Empty;
            stateProperties117.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testorgweight.OnHoverState = stateProperties117;
            stateProperties118.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties118.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties118.ForeColor = System.Drawing.Color.White;
            stateProperties118.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testorgweight.OnIdleState = stateProperties118;
            this.testorgweight.PasswordChar = '\0';
            this.testorgweight.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.testorgweight.PlaceholderText = "Enter Weight";
            this.testorgweight.ReadOnly = false;
            this.testorgweight.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.testorgweight.SelectedText = "";
            this.testorgweight.SelectionLength = 0;
            this.testorgweight.SelectionStart = 0;
            this.testorgweight.ShortcutsEnabled = true;
            this.testorgweight.Size = new System.Drawing.Size(358, 33);
            this.testorgweight.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.testorgweight.TabIndex = 5;
            this.testorgweight.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.testorgweight.TextMarginBottom = 0;
            this.testorgweight.TextMarginLeft = 5;
            this.testorgweight.TextMarginTop = 0;
            this.testorgweight.TextPlaceholder = "Enter Weight";
            this.testorgweight.UseSystemPasswordChar = false;
            this.testorgweight.WordWrap = true;
            this.testorgweight.TextChanged += new System.EventHandler(this.bunifuTextBox4_TextChanged);
            this.testorgweight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.testorgweight_KeyDown);
            this.testorgweight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.testorgweight_KeyPress);
            this.testorgweight.Leave += new System.EventHandler(this.testorgweight_Leave);
            // 
            // testKarat
            // 
            this.testKarat.AcceptsReturn = false;
            this.testKarat.AcceptsTab = false;
            this.testKarat.AnimationSpeed = 200;
            this.testKarat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.testKarat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.testKarat.BackColor = System.Drawing.Color.Transparent;
            this.testKarat.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("testKarat.BackgroundImage")));
            this.testKarat.BorderColorActive = System.Drawing.Color.Goldenrod;
            this.testKarat.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testKarat.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.testKarat.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testKarat.BorderRadius = 1;
            this.testKarat.BorderThickness = 1;
            this.testKarat.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.testKarat.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.testKarat.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold);
            this.testKarat.DefaultText = "";
            this.testKarat.Enabled = false;
            this.testKarat.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.testKarat.ForeColor = System.Drawing.Color.White;
            this.testKarat.HideSelection = true;
            this.testKarat.IconLeft = null;
            this.testKarat.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.testKarat.IconPadding = 10;
            this.testKarat.IconRight = global::GoldRaven.Properties.Resources.icons8_k_45px_1;
            this.testKarat.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.testKarat.Lines = new string[0];
            this.testKarat.Location = new System.Drawing.Point(509, 173);
            this.testKarat.MaxLength = 32767;
            this.testKarat.MinimumSize = new System.Drawing.Size(0, 33);
            this.testKarat.Modified = false;
            this.testKarat.Multiline = false;
            this.testKarat.Name = "testKarat";
            stateProperties119.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties119.FillColor = System.Drawing.Color.Empty;
            stateProperties119.ForeColor = System.Drawing.Color.Empty;
            stateProperties119.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testKarat.OnActiveState = stateProperties119;
            stateProperties120.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties120.FillColor = System.Drawing.Color.White;
            stateProperties120.ForeColor = System.Drawing.Color.Empty;
            stateProperties120.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.testKarat.OnDisabledState = stateProperties120;
            stateProperties121.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties121.FillColor = System.Drawing.Color.Empty;
            stateProperties121.ForeColor = System.Drawing.Color.Empty;
            stateProperties121.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testKarat.OnHoverState = stateProperties121;
            stateProperties122.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties122.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties122.ForeColor = System.Drawing.Color.White;
            stateProperties122.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testKarat.OnIdleState = stateProperties122;
            this.testKarat.PasswordChar = '\0';
            this.testKarat.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.testKarat.PlaceholderText = "Karat";
            this.testKarat.ReadOnly = true;
            this.testKarat.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.testKarat.SelectedText = "";
            this.testKarat.SelectionLength = 0;
            this.testKarat.SelectionStart = 0;
            this.testKarat.ShortcutsEnabled = true;
            this.testKarat.Size = new System.Drawing.Size(187, 33);
            this.testKarat.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.testKarat.TabIndex = 174;
            this.testKarat.TabStop = false;
            this.testKarat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.testKarat.TextMarginBottom = 0;
            this.testKarat.TextMarginLeft = 5;
            this.testKarat.TextMarginTop = 0;
            this.testKarat.TextPlaceholder = "Karat";
            this.testKarat.UseSystemPasswordChar = false;
            this.testKarat.WordWrap = true;
            // 
            // testPurity
            // 
            this.testPurity.AcceptsReturn = false;
            this.testPurity.AcceptsTab = false;
            this.testPurity.AnimationSpeed = 200;
            this.testPurity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.testPurity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.testPurity.BackColor = System.Drawing.Color.Transparent;
            this.testPurity.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("testPurity.BackgroundImage")));
            this.testPurity.BorderColorActive = System.Drawing.Color.Red;
            this.testPurity.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testPurity.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.testPurity.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testPurity.BorderRadius = 1;
            this.testPurity.BorderThickness = 1;
            this.testPurity.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.testPurity.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.testPurity.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testPurity.DefaultText = "";
            this.testPurity.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.testPurity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testPurity.HideSelection = true;
            this.testPurity.IconLeft = null;
            this.testPurity.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.testPurity.IconPadding = 3;
            this.testPurity.IconRight = global::GoldRaven.Properties.Resources.PERCENTAGE;
            this.testPurity.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.testPurity.Lines = new string[0];
            this.testPurity.Location = new System.Drawing.Point(305, 173);
            this.testPurity.MaxLength = 32767;
            this.testPurity.MinimumSize = new System.Drawing.Size(0, 33);
            this.testPurity.Modified = false;
            this.testPurity.Multiline = false;
            this.testPurity.Name = "testPurity";
            stateProperties123.BorderColor = System.Drawing.Color.Red;
            stateProperties123.FillColor = System.Drawing.Color.Empty;
            stateProperties123.ForeColor = System.Drawing.Color.Empty;
            stateProperties123.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testPurity.OnActiveState = stateProperties123;
            stateProperties124.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties124.FillColor = System.Drawing.Color.White;
            stateProperties124.ForeColor = System.Drawing.Color.Empty;
            stateProperties124.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.testPurity.OnDisabledState = stateProperties124;
            stateProperties125.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties125.FillColor = System.Drawing.Color.Empty;
            stateProperties125.ForeColor = System.Drawing.Color.Empty;
            stateProperties125.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testPurity.OnHoverState = stateProperties125;
            stateProperties126.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties126.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties126.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties126.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.testPurity.OnIdleState = stateProperties126;
            this.testPurity.PasswordChar = '\0';
            this.testPurity.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.testPurity.PlaceholderText = "Enter Percentage";
            this.testPurity.ReadOnly = false;
            this.testPurity.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.testPurity.SelectedText = "";
            this.testPurity.SelectionLength = 0;
            this.testPurity.SelectionStart = 0;
            this.testPurity.ShortcutsEnabled = true;
            this.testPurity.Size = new System.Drawing.Size(187, 33);
            this.testPurity.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.testPurity.TabIndex = 6;
            this.testPurity.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.testPurity.TextMarginBottom = 0;
            this.testPurity.TextMarginLeft = 5;
            this.testPurity.TextMarginTop = 0;
            this.testPurity.TextPlaceholder = "Enter Percentage";
            this.testPurity.UseSystemPasswordChar = false;
            this.testPurity.WordWrap = true;
            this.testPurity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.testPurity_KeyDown);
            this.testPurity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.testPurity_KeyPress);
            this.testPurity.Enter += new System.EventHandler(this.testPurity_Enter);
            this.testPurity.Leave += new System.EventHandler(this.testPurity_Leave);
            // 
            // testDate
            // 
            this.testDate.BackColor = System.Drawing.Color.Transparent;
            this.testDate.BorderRadius = 1;
            this.testDate.Color = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testDate.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.testDate.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Right;
            this.testDate.DisabledColor = System.Drawing.Color.Gray;
            this.testDate.DisplayWeekNumbers = false;
            this.testDate.DPHeight = 0;
            this.testDate.FillDatePicker = false;
            this.testDate.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.testDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testDate.Icon = ((System.Drawing.Image)(resources.GetObject("testDate.Icon")));
            this.testDate.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.testDate.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Left;
            this.testDate.Location = new System.Drawing.Point(267, 104);
            this.testDate.MinimumSize = new System.Drawing.Size(217, 33);
            this.testDate.Name = "testDate";
            this.testDate.Size = new System.Drawing.Size(217, 33);
            this.testDate.TabIndex = 173;
            this.testDate.TabStop = false;
            this.testDate.ValueChanged += new System.EventHandler(this.testDate_ValueChanged);
            this.testDate.VisibleChanged += new System.EventHandler(this.testDate_VisibleChanged);
            // 
            // bunifuButton21
            // 
            this.bunifuButton21.AllowToggling = false;
            this.bunifuButton21.AnimationSpeed = 200;
            this.bunifuButton21.AutoGenerateColors = false;
            this.bunifuButton21.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton21.BackColor1 = System.Drawing.Color.Goldenrod;
            this.bunifuButton21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton21.BackgroundImage")));
            this.bunifuButton21.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton21.ButtonText = "Print";
            this.bunifuButton21.ButtonTextMarginLeft = 0;
            this.bunifuButton21.ColorContrastOnClick = 45;
            this.bunifuButton21.ColorContrastOnHover = 45;
            this.bunifuButton21.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges32.BottomLeft = true;
            borderEdges32.BottomRight = true;
            borderEdges32.TopLeft = true;
            borderEdges32.TopRight = true;
            this.bunifuButton21.CustomizableEdges = borderEdges32;
            this.bunifuButton21.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton21.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton21.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton21.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton21.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton21.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton21.ForeColor = System.Drawing.Color.White;
            this.bunifuButton21.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton21.IconMarginLeft = 11;
            this.bunifuButton21.IconPadding = 10;
            this.bunifuButton21.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton21.IdleBorderColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton21.IdleBorderRadius = 3;
            this.bunifuButton21.IdleBorderThickness = 1;
            this.bunifuButton21.IdleFillColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton21.IdleIconLeftImage = null;
            this.bunifuButton21.IdleIconRightImage = null;
            this.bunifuButton21.IndicateFocus = false;
            this.bunifuButton21.Location = new System.Drawing.Point(725, 238);
            this.bunifuButton21.Name = "bunifuButton21";
            stateProperties127.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties127.BorderRadius = 3;
            stateProperties127.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties127.BorderThickness = 1;
            stateProperties127.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties127.ForeColor = System.Drawing.Color.White;
            stateProperties127.IconLeftImage = null;
            stateProperties127.IconRightImage = null;
            this.bunifuButton21.onHoverState = stateProperties127;
            stateProperties128.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties128.BorderRadius = 3;
            stateProperties128.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties128.BorderThickness = 1;
            stateProperties128.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties128.ForeColor = System.Drawing.Color.White;
            stateProperties128.IconLeftImage = null;
            stateProperties128.IconRightImage = null;
            this.bunifuButton21.OnPressedState = stateProperties128;
            this.bunifuButton21.Size = new System.Drawing.Size(126, 33);
            this.bunifuButton21.TabIndex = 0;
            this.bunifuButton21.TabStop = false;
            this.bunifuButton21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton21.TextMarginLeft = 0;
            this.bunifuButton21.UseDefaultRadiusAndThickness = true;
            this.bunifuButton21.Click += new System.EventHandler(this.bunifuButton21_Click);
            // 
            // bunifuButton22
            // 
            this.bunifuButton22.AllowToggling = false;
            this.bunifuButton22.AnimationSpeed = 200;
            this.bunifuButton22.AutoGenerateColors = false;
            this.bunifuButton22.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton22.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton22.BackgroundImage")));
            this.bunifuButton22.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton22.ButtonText = "Save";
            this.bunifuButton22.ButtonTextMarginLeft = 0;
            this.bunifuButton22.ColorContrastOnClick = 45;
            this.bunifuButton22.ColorContrastOnHover = 45;
            this.bunifuButton22.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges33.BottomLeft = true;
            borderEdges33.BottomRight = true;
            borderEdges33.TopLeft = true;
            borderEdges33.TopRight = true;
            this.bunifuButton22.CustomizableEdges = borderEdges33;
            this.bunifuButton22.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton22.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton22.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton22.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton22.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton22.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton22.ForeColor = System.Drawing.Color.White;
            this.bunifuButton22.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton22.IconMarginLeft = 11;
            this.bunifuButton22.IconPadding = 10;
            this.bunifuButton22.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton22.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton22.IdleBorderRadius = 3;
            this.bunifuButton22.IdleBorderThickness = 1;
            this.bunifuButton22.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton22.IdleIconLeftImage = null;
            this.bunifuButton22.IdleIconRightImage = null;
            this.bunifuButton22.IndicateFocus = false;
            this.bunifuButton22.Location = new System.Drawing.Point(725, 304);
            this.bunifuButton22.Name = "bunifuButton22";
            stateProperties129.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties129.BorderRadius = 3;
            stateProperties129.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties129.BorderThickness = 1;
            stateProperties129.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties129.ForeColor = System.Drawing.Color.White;
            stateProperties129.IconLeftImage = null;
            stateProperties129.IconRightImage = null;
            this.bunifuButton22.onHoverState = stateProperties129;
            stateProperties130.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties130.BorderRadius = 3;
            stateProperties130.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties130.BorderThickness = 1;
            stateProperties130.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties130.ForeColor = System.Drawing.Color.White;
            stateProperties130.IconLeftImage = null;
            stateProperties130.IconRightImage = null;
            this.bunifuButton22.OnPressedState = stateProperties130;
            this.bunifuButton22.Size = new System.Drawing.Size(126, 33);
            this.bunifuButton22.TabIndex = 9;
            this.bunifuButton22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton22.TextMarginLeft = 0;
            this.bunifuButton22.UseDefaultRadiusAndThickness = true;
            this.bunifuButton22.Click += new System.EventHandler(this.bunifuButton22_Click);
            this.bunifuButton22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.bunifuButton22_KeyPress);
            // 
            // bunifuButton23
            // 
            this.bunifuButton23.AllowToggling = false;
            this.bunifuButton23.AnimationSpeed = 200;
            this.bunifuButton23.AutoGenerateColors = false;
            this.bunifuButton23.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton23.BackColor1 = System.Drawing.Color.Goldenrod;
            this.bunifuButton23.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton23.BackgroundImage")));
            this.bunifuButton23.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton23.ButtonText = "Modify";
            this.bunifuButton23.ButtonTextMarginLeft = 0;
            this.bunifuButton23.ColorContrastOnClick = 45;
            this.bunifuButton23.ColorContrastOnHover = 45;
            this.bunifuButton23.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges34.BottomLeft = true;
            borderEdges34.BottomRight = true;
            borderEdges34.TopLeft = true;
            borderEdges34.TopRight = true;
            this.bunifuButton23.CustomizableEdges = borderEdges34;
            this.bunifuButton23.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton23.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton23.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton23.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton23.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton23.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton23.ForeColor = System.Drawing.Color.White;
            this.bunifuButton23.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton23.IconMarginLeft = 11;
            this.bunifuButton23.IconPadding = 10;
            this.bunifuButton23.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton23.IdleBorderColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton23.IdleBorderRadius = 3;
            this.bunifuButton23.IdleBorderThickness = 1;
            this.bunifuButton23.IdleFillColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton23.IdleIconLeftImage = null;
            this.bunifuButton23.IdleIconRightImage = null;
            this.bunifuButton23.IndicateFocus = false;
            this.bunifuButton23.Location = new System.Drawing.Point(885, 239);
            this.bunifuButton23.Name = "bunifuButton23";
            stateProperties131.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties131.BorderRadius = 3;
            stateProperties131.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties131.BorderThickness = 1;
            stateProperties131.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties131.ForeColor = System.Drawing.Color.White;
            stateProperties131.IconLeftImage = null;
            stateProperties131.IconRightImage = null;
            this.bunifuButton23.onHoverState = stateProperties131;
            stateProperties132.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties132.BorderRadius = 3;
            stateProperties132.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties132.BorderThickness = 1;
            stateProperties132.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties132.ForeColor = System.Drawing.Color.White;
            stateProperties132.IconLeftImage = null;
            stateProperties132.IconRightImage = null;
            this.bunifuButton23.OnPressedState = stateProperties132;
            this.bunifuButton23.Size = new System.Drawing.Size(126, 33);
            this.bunifuButton23.TabIndex = 0;
            this.bunifuButton23.TabStop = false;
            this.bunifuButton23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton23.TextMarginLeft = 0;
            this.bunifuButton23.UseDefaultRadiusAndThickness = true;
            this.bunifuButton23.Click += new System.EventHandler(this.bunifuButton23_Click);
            // 
            // bunifuButton24
            // 
            this.bunifuButton24.AllowToggling = false;
            this.bunifuButton24.AnimationSpeed = 200;
            this.bunifuButton24.AutoGenerateColors = false;
            this.bunifuButton24.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton24.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton24.BackgroundImage")));
            this.bunifuButton24.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton24.ButtonText = "New";
            this.bunifuButton24.ButtonTextMarginLeft = 0;
            this.bunifuButton24.ColorContrastOnClick = 45;
            this.bunifuButton24.ColorContrastOnHover = 45;
            this.bunifuButton24.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges35.BottomLeft = true;
            borderEdges35.BottomRight = true;
            borderEdges35.TopLeft = true;
            borderEdges35.TopRight = true;
            this.bunifuButton24.CustomizableEdges = borderEdges35;
            this.bunifuButton24.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton24.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton24.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton24.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton24.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton24.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton24.ForeColor = System.Drawing.Color.White;
            this.bunifuButton24.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton24.IconMarginLeft = 11;
            this.bunifuButton24.IconPadding = 10;
            this.bunifuButton24.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton24.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton24.IdleBorderRadius = 3;
            this.bunifuButton24.IdleBorderThickness = 1;
            this.bunifuButton24.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton24.IdleIconLeftImage = null;
            this.bunifuButton24.IdleIconRightImage = null;
            this.bunifuButton24.IndicateFocus = false;
            this.bunifuButton24.Location = new System.Drawing.Point(884, 304);
            this.bunifuButton24.Name = "bunifuButton24";
            stateProperties133.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties133.BorderRadius = 3;
            stateProperties133.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties133.BorderThickness = 1;
            stateProperties133.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties133.ForeColor = System.Drawing.Color.White;
            stateProperties133.IconLeftImage = null;
            stateProperties133.IconRightImage = null;
            this.bunifuButton24.onHoverState = stateProperties133;
            stateProperties134.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties134.BorderRadius = 3;
            stateProperties134.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties134.BorderThickness = 1;
            stateProperties134.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties134.ForeColor = System.Drawing.Color.White;
            stateProperties134.IconLeftImage = null;
            stateProperties134.IconRightImage = null;
            this.bunifuButton24.OnPressedState = stateProperties134;
            this.bunifuButton24.Size = new System.Drawing.Size(126, 33);
            this.bunifuButton24.TabIndex = 0;
            this.bunifuButton24.TabStop = false;
            this.bunifuButton24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton24.TextMarginLeft = 0;
            this.bunifuButton24.UseDefaultRadiusAndThickness = true;
            this.bunifuButton24.Click += new System.EventHandler(this.bunifuButton24_Click);
            // 
            // expenses
            // 
            this.expenses.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.expenses.Controls.Add(this.bunifuLabel86);
            this.expenses.Controls.Add(this.expenseId);
            this.expenses.Controls.Add(this.expenseTime);
            this.expenses.Controls.Add(this.bunifuLabel81);
            this.expenses.Controls.Add(this.bunifuLabel84);
            this.expenses.Controls.Add(this.label7);
            this.expenses.Controls.Add(this.expensesDataGridView);
            this.expenses.Controls.Add(this.bunifuLabel82);
            this.expenses.Controls.Add(this.bunifuLabel85);
            this.expenses.Controls.Add(this.expenseRemark);
            this.expenses.Controls.Add(this.expenseDate);
            this.expenses.Controls.Add(this.expensesAmount);
            this.expenses.Controls.Add(this.bunifuButton20);
            this.expenses.Controls.Add(this.bunifuButton25);
            this.expenses.Controls.Add(this.bunifuButton26);
            this.expenses.Controls.Add(this.bunifuButton27);
            this.expenses.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.expenses.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.expenses.Location = new System.Drawing.Point(4, 4);
            this.expenses.Name = "expenses";
            this.expenses.Padding = new System.Windows.Forms.Padding(3);
            this.expenses.Size = new System.Drawing.Size(1041, 728);
            this.expenses.TabIndex = 6;
            this.expenses.Text = "Expenses";
            this.expenses.Click += new System.EventHandler(this.expenses_Click);
            // 
            // bunifuLabel86
            // 
            this.bunifuLabel86.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bunifuLabel86.AutoEllipsis = false;
            this.bunifuLabel86.CursorType = null;
            this.bunifuLabel86.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel86.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel86.Location = new System.Drawing.Point(37, 84);
            this.bunifuLabel86.Name = "bunifuLabel86";
            this.bunifuLabel86.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel86.Size = new System.Drawing.Size(67, 23);
            this.bunifuLabel86.TabIndex = 176;
            this.bunifuLabel86.TabStop = false;
            this.bunifuLabel86.Text = "Serial No";
            this.bunifuLabel86.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel86.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // expenseId
            // 
            this.expenseId.BorderColorFocused = System.Drawing.Color.Red;
            this.expenseId.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.expenseId.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.expenseId.BorderThickness = 1;
            this.expenseId.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.expenseId.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.expenseId.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.expenseId.ForeColor = System.Drawing.Color.White;
            this.expenseId.isPassword = false;
            this.expenseId.Location = new System.Drawing.Point(34, 111);
            this.expenseId.Margin = new System.Windows.Forms.Padding(4);
            this.expenseId.MaxLength = 32767;
            this.expenseId.Name = "expenseId";
            this.expenseId.Size = new System.Drawing.Size(215, 35);
            this.expenseId.TabIndex = 1;
            this.expenseId.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.expenseId.OnValueChanged += new System.EventHandler(this.bunifuMetroTextbox9_OnValueChanged_2);
            this.expenseId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.expenseId_KeyPress);
            // 
            // expenseTime
            // 
            this.expenseTime.CalendarFont = new System.Drawing.Font("Century Gothic", 7.75F, System.Drawing.FontStyle.Bold);
            this.expenseTime.CalendarMonthBackground = System.Drawing.Color.SeaGreen;
            this.expenseTime.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.expenseTime.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.expenseTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.expenseTime.Location = new System.Drawing.Point(544, 112);
            this.expenseTime.Margin = new System.Windows.Forms.Padding(4);
            this.expenseTime.MaximumSize = new System.Drawing.Size(4, 23);
            this.expenseTime.MinimumSize = new System.Drawing.Size(180, 33);
            this.expenseTime.Name = "expenseTime";
            this.expenseTime.ShowUpDown = true;
            this.expenseTime.Size = new System.Drawing.Size(180, 33);
            this.expenseTime.TabIndex = 174;
            this.expenseTime.TabStop = false;
            // 
            // bunifuLabel81
            // 
            this.bunifuLabel81.AutoEllipsis = false;
            this.bunifuLabel81.CursorType = null;
            this.bunifuLabel81.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel81.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel81.Location = new System.Drawing.Point(289, 84);
            this.bunifuLabel81.Name = "bunifuLabel81";
            this.bunifuLabel81.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel81.Size = new System.Drawing.Size(35, 23);
            this.bunifuLabel81.TabIndex = 173;
            this.bunifuLabel81.TabStop = false;
            this.bunifuLabel81.Text = "Date";
            this.bunifuLabel81.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel81.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel84
            // 
            this.bunifuLabel84.AutoEllipsis = false;
            this.bunifuLabel84.CursorType = null;
            this.bunifuLabel84.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel84.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel84.Location = new System.Drawing.Point(547, 84);
            this.bunifuLabel84.Name = "bunifuLabel84";
            this.bunifuLabel84.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel84.Size = new System.Drawing.Size(37, 23);
            this.bunifuLabel84.TabIndex = 171;
            this.bunifuLabel84.TabStop = false;
            this.bunifuLabel84.Text = "Time";
            this.bunifuLabel84.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel84.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(28, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 32);
            this.label7.TabIndex = 170;
            this.label7.Text = "Expenses";
            this.label7.Click += new System.EventHandler(this.label7_Click_1);
            // 
            // expensesDataGridView
            // 
            this.expensesDataGridView.AllowUserToAddRows = false;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.White;
            this.expensesDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle31;
            this.expensesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.expensesDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.expensesDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.expensesDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.expensesDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.expensesDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle32;
            this.expensesDataGridView.ColumnHeadersHeight = 50;
            this.expensesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn53});
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(252)))));
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.expensesDataGridView.DefaultCellStyle = dataGridViewCellStyle34;
            this.expensesDataGridView.DoubleBuffered = true;
            this.expensesDataGridView.EnableHeadersVisualStyles = false;
            this.expensesDataGridView.GridColor = System.Drawing.Color.WhiteSmoke;
            this.expensesDataGridView.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            this.expensesDataGridView.HeaderForeColor = System.Drawing.Color.White;
            this.expensesDataGridView.Location = new System.Drawing.Point(34, 313);
            this.expensesDataGridView.Name = "expensesDataGridView";
            this.expensesDataGridView.ReadOnly = true;
            this.expensesDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.expensesDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle35;
            this.expensesDataGridView.RowHeadersVisible = false;
            this.expensesDataGridView.RowHeadersWidth = 51;
            this.expensesDataGridView.RowTemplate.DividerHeight = 1;
            this.expensesDataGridView.RowTemplate.Height = 40;
            this.expensesDataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.expensesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.expensesDataGridView.Size = new System.Drawing.Size(956, 448);
            this.expensesDataGridView.TabIndex = 169;
            this.expensesDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.expensesDataGridView_CellContentClick);
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn49.HeaderText = "ID";
            this.dataGridViewTextBoxColumn49.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.DataPropertyName = "date";
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.Color.Teal;
            this.dataGridViewTextBoxColumn50.DefaultCellStyle = dataGridViewCellStyle33;
            this.dataGridViewTextBoxColumn50.HeaderText = "DATE";
            this.dataGridViewTextBoxColumn50.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.DataPropertyName = "time";
            this.dataGridViewTextBoxColumn52.HeaderText = "TIME";
            this.dataGridViewTextBoxColumn52.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.DataPropertyName = "remarks";
            this.dataGridViewTextBoxColumn51.HeaderText = "REMARKS";
            this.dataGridViewTextBoxColumn51.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.DataPropertyName = "expenses";
            this.dataGridViewTextBoxColumn53.HeaderText = "EXPENSES";
            this.dataGridViewTextBoxColumn53.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.ReadOnly = true;
            // 
            // bunifuLabel82
            // 
            this.bunifuLabel82.AutoEllipsis = false;
            this.bunifuLabel82.CursorType = null;
            this.bunifuLabel82.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel82.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel82.Location = new System.Drawing.Point(740, 81);
            this.bunifuLabel82.Name = "bunifuLabel82";
            this.bunifuLabel82.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel82.Size = new System.Drawing.Size(126, 23);
            this.bunifuLabel82.TabIndex = 156;
            this.bunifuLabel82.TabStop = false;
            this.bunifuLabel82.Text = "Expenses Amount";
            this.bunifuLabel82.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel82.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel82.Click += new System.EventHandler(this.bunifuLabel82_Click);
            // 
            // bunifuLabel85
            // 
            this.bunifuLabel85.AutoEllipsis = false;
            this.bunifuLabel85.CursorType = null;
            this.bunifuLabel85.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel85.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel85.Location = new System.Drawing.Point(35, 159);
            this.bunifuLabel85.Name = "bunifuLabel85";
            this.bunifuLabel85.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel85.Size = new System.Drawing.Size(124, 23);
            this.bunifuLabel85.TabIndex = 159;
            this.bunifuLabel85.TabStop = false;
            this.bunifuLabel85.Text = "Expenses Remark";
            this.bunifuLabel85.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel85.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel85.Click += new System.EventHandler(this.bunifuLabel85_Click);
            // 
            // expenseRemark
            // 
            this.expenseRemark.BorderColorFocused = System.Drawing.Color.Red;
            this.expenseRemark.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.expenseRemark.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.expenseRemark.BorderThickness = 1;
            this.expenseRemark.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.expenseRemark.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.expenseRemark.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expenseRemark.ForeColor = System.Drawing.Color.White;
            this.expenseRemark.isPassword = false;
            this.expenseRemark.Location = new System.Drawing.Point(35, 189);
            this.expenseRemark.Margin = new System.Windows.Forms.Padding(4);
            this.expenseRemark.MaxLength = 32767;
            this.expenseRemark.Name = "expenseRemark";
            this.expenseRemark.Size = new System.Drawing.Size(689, 33);
            this.expenseRemark.TabIndex = 2;
            this.expenseRemark.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.expenseRemark.OnValueChanged += new System.EventHandler(this.bunifuMetroTextbox12_OnValueChanged_1);
            this.expenseRemark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.expenseRemark_KeyDown);
            this.expenseRemark.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.expenseRemark_KeyPress);
            // 
            // expenseDate
            // 
            this.expenseDate.BackColor = System.Drawing.Color.Transparent;
            this.expenseDate.BorderRadius = 1;
            this.expenseDate.Color = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.expenseDate.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.expenseDate.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Right;
            this.expenseDate.DisabledColor = System.Drawing.Color.Gray;
            this.expenseDate.DisplayWeekNumbers = false;
            this.expenseDate.DPHeight = 0;
            this.expenseDate.FillDatePicker = false;
            this.expenseDate.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.expenseDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.expenseDate.Icon = ((System.Drawing.Image)(resources.GetObject("expenseDate.Icon")));
            this.expenseDate.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.expenseDate.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Left;
            this.expenseDate.Location = new System.Drawing.Point(286, 112);
            this.expenseDate.MinimumSize = new System.Drawing.Size(217, 33);
            this.expenseDate.Name = "expenseDate";
            this.expenseDate.Size = new System.Drawing.Size(217, 33);
            this.expenseDate.TabIndex = 172;
            this.expenseDate.TabStop = false;
            this.expenseDate.ValueChanged += new System.EventHandler(this.expenseDate_ValueChanged);
            this.expenseDate.VisibleChanged += new System.EventHandler(this.expenseDate_VisibleChanged);
            // 
            // expensesAmount
            // 
            this.expensesAmount.AcceptsReturn = false;
            this.expensesAmount.AcceptsTab = false;
            this.expensesAmount.AnimationSpeed = 200;
            this.expensesAmount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.expensesAmount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.expensesAmount.BackColor = System.Drawing.Color.Transparent;
            this.expensesAmount.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("expensesAmount.BackgroundImage")));
            this.expensesAmount.BorderColorActive = System.Drawing.Color.Red;
            this.expensesAmount.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.expensesAmount.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.expensesAmount.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.expensesAmount.BorderRadius = 1;
            this.expensesAmount.BorderThickness = 1;
            this.expensesAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.expensesAmount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.expensesAmount.DefaultFont = new System.Drawing.Font("Century Gothic", 44F, System.Drawing.FontStyle.Bold);
            this.expensesAmount.DefaultText = "";
            this.expensesAmount.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.expensesAmount.ForeColor = System.Drawing.Color.White;
            this.expensesAmount.HideSelection = true;
            this.expensesAmount.IconLeft = null;
            this.expensesAmount.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.expensesAmount.IconPadding = 0;
            this.expensesAmount.IconRight = null;
            this.expensesAmount.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.expensesAmount.Lines = new string[0];
            this.expensesAmount.Location = new System.Drawing.Point(740, 111);
            this.expensesAmount.Margin = new System.Windows.Forms.Padding(0);
            this.expensesAmount.MaxLength = 32767;
            this.expensesAmount.MinimumSize = new System.Drawing.Size(0, 33);
            this.expensesAmount.Modified = false;
            this.expensesAmount.Multiline = false;
            this.expensesAmount.Name = "expensesAmount";
            stateProperties135.BorderColor = System.Drawing.Color.Red;
            stateProperties135.FillColor = System.Drawing.Color.Empty;
            stateProperties135.ForeColor = System.Drawing.Color.Empty;
            stateProperties135.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.expensesAmount.OnActiveState = stateProperties135;
            stateProperties136.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties136.FillColor = System.Drawing.Color.White;
            stateProperties136.ForeColor = System.Drawing.Color.Empty;
            stateProperties136.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.expensesAmount.OnDisabledState = stateProperties136;
            stateProperties137.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties137.FillColor = System.Drawing.Color.Empty;
            stateProperties137.ForeColor = System.Drawing.Color.Empty;
            stateProperties137.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.expensesAmount.OnHoverState = stateProperties137;
            stateProperties138.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties138.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties138.ForeColor = System.Drawing.Color.White;
            stateProperties138.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.expensesAmount.OnIdleState = stateProperties138;
            this.expensesAmount.PasswordChar = '\0';
            this.expensesAmount.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.expensesAmount.PlaceholderText = "₹";
            this.expensesAmount.ReadOnly = false;
            this.expensesAmount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.expensesAmount.SelectedText = "";
            this.expensesAmount.SelectionLength = 0;
            this.expensesAmount.SelectionStart = 0;
            this.expensesAmount.ShortcutsEnabled = true;
            this.expensesAmount.Size = new System.Drawing.Size(250, 170);
            this.expensesAmount.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.expensesAmount.TabIndex = 3;
            this.expensesAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.expensesAmount.TextMarginBottom = 0;
            this.expensesAmount.TextMarginLeft = 5;
            this.expensesAmount.TextMarginTop = 0;
            this.expensesAmount.TextPlaceholder = "₹";
            this.expensesAmount.UseSystemPasswordChar = false;
            this.expensesAmount.WordWrap = true;
            this.expensesAmount.TextChanged += new System.EventHandler(this.bunifuTextBox7_TextChanged);
            this.expensesAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.expensesAmount_KeyDown);
            this.expensesAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.expensesAmount_KeyPress);
            // 
            // bunifuButton20
            // 
            this.bunifuButton20.AllowToggling = false;
            this.bunifuButton20.AnimationSpeed = 200;
            this.bunifuButton20.AutoGenerateColors = false;
            this.bunifuButton20.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton20.BackColor1 = System.Drawing.Color.Goldenrod;
            this.bunifuButton20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton20.BackgroundImage")));
            this.bunifuButton20.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton20.ButtonText = "Modify";
            this.bunifuButton20.ButtonTextMarginLeft = 0;
            this.bunifuButton20.ColorContrastOnClick = 45;
            this.bunifuButton20.ColorContrastOnHover = 45;
            this.bunifuButton20.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges36.BottomLeft = true;
            borderEdges36.BottomRight = true;
            borderEdges36.TopLeft = true;
            borderEdges36.TopRight = true;
            this.bunifuButton20.CustomizableEdges = borderEdges36;
            this.bunifuButton20.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton20.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton20.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton20.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton20.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton20.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton20.ForeColor = System.Drawing.Color.White;
            this.bunifuButton20.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton20.IconMarginLeft = 11;
            this.bunifuButton20.IconPadding = 10;
            this.bunifuButton20.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton20.IdleBorderColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton20.IdleBorderRadius = 3;
            this.bunifuButton20.IdleBorderThickness = 1;
            this.bunifuButton20.IdleFillColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton20.IdleIconLeftImage = null;
            this.bunifuButton20.IdleIconRightImage = null;
            this.bunifuButton20.IndicateFocus = false;
            this.bunifuButton20.Location = new System.Drawing.Point(219, 248);
            this.bunifuButton20.Name = "bunifuButton20";
            stateProperties139.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties139.BorderRadius = 3;
            stateProperties139.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties139.BorderThickness = 1;
            stateProperties139.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties139.ForeColor = System.Drawing.Color.White;
            stateProperties139.IconLeftImage = null;
            stateProperties139.IconRightImage = null;
            this.bunifuButton20.onHoverState = stateProperties139;
            stateProperties140.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties140.BorderRadius = 3;
            stateProperties140.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties140.BorderThickness = 1;
            stateProperties140.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties140.ForeColor = System.Drawing.Color.White;
            stateProperties140.IconLeftImage = null;
            stateProperties140.IconRightImage = null;
            this.bunifuButton20.OnPressedState = stateProperties140;
            this.bunifuButton20.Size = new System.Drawing.Size(140, 33);
            this.bunifuButton20.TabIndex = 0;
            this.bunifuButton20.TabStop = false;
            this.bunifuButton20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton20.TextMarginLeft = 0;
            this.bunifuButton20.UseDefaultRadiusAndThickness = true;
            this.bunifuButton20.Click += new System.EventHandler(this.bunifuButton20_Click);
            // 
            // bunifuButton25
            // 
            this.bunifuButton25.AllowToggling = false;
            this.bunifuButton25.AnimationSpeed = 200;
            this.bunifuButton25.AutoGenerateColors = false;
            this.bunifuButton25.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton25.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton25.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton25.BackgroundImage")));
            this.bunifuButton25.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton25.ButtonText = "New";
            this.bunifuButton25.ButtonTextMarginLeft = 0;
            this.bunifuButton25.ColorContrastOnClick = 45;
            this.bunifuButton25.ColorContrastOnHover = 45;
            this.bunifuButton25.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges37.BottomLeft = true;
            borderEdges37.BottomRight = true;
            borderEdges37.TopLeft = true;
            borderEdges37.TopRight = true;
            this.bunifuButton25.CustomizableEdges = borderEdges37;
            this.bunifuButton25.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton25.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton25.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton25.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton25.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton25.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton25.ForeColor = System.Drawing.Color.White;
            this.bunifuButton25.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton25.IconMarginLeft = 11;
            this.bunifuButton25.IconPadding = 10;
            this.bunifuButton25.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton25.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton25.IdleBorderRadius = 3;
            this.bunifuButton25.IdleBorderThickness = 1;
            this.bunifuButton25.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton25.IdleIconLeftImage = null;
            this.bunifuButton25.IdleIconRightImage = null;
            this.bunifuButton25.IndicateFocus = false;
            this.bunifuButton25.Location = new System.Drawing.Point(37, 248);
            this.bunifuButton25.Name = "bunifuButton25";
            stateProperties141.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties141.BorderRadius = 3;
            stateProperties141.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties141.BorderThickness = 1;
            stateProperties141.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties141.ForeColor = System.Drawing.Color.White;
            stateProperties141.IconLeftImage = null;
            stateProperties141.IconRightImage = null;
            this.bunifuButton25.onHoverState = stateProperties141;
            stateProperties142.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties142.BorderRadius = 3;
            stateProperties142.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties142.BorderThickness = 1;
            stateProperties142.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties142.ForeColor = System.Drawing.Color.White;
            stateProperties142.IconLeftImage = null;
            stateProperties142.IconRightImage = null;
            this.bunifuButton25.OnPressedState = stateProperties142;
            this.bunifuButton25.Size = new System.Drawing.Size(140, 33);
            this.bunifuButton25.TabIndex = 0;
            this.bunifuButton25.TabStop = false;
            this.bunifuButton25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton25.TextMarginLeft = 0;
            this.bunifuButton25.UseDefaultRadiusAndThickness = true;
            this.bunifuButton25.Click += new System.EventHandler(this.bunifuButton25_Click);
            // 
            // bunifuButton26
            // 
            this.bunifuButton26.AllowToggling = false;
            this.bunifuButton26.AnimationSpeed = 200;
            this.bunifuButton26.AutoGenerateColors = false;
            this.bunifuButton26.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton26.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton26.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton26.BackgroundImage")));
            this.bunifuButton26.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton26.ButtonText = "Save";
            this.bunifuButton26.ButtonTextMarginLeft = 0;
            this.bunifuButton26.ColorContrastOnClick = 45;
            this.bunifuButton26.ColorContrastOnHover = 45;
            this.bunifuButton26.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges38.BottomLeft = true;
            borderEdges38.BottomRight = true;
            borderEdges38.TopLeft = true;
            borderEdges38.TopRight = true;
            this.bunifuButton26.CustomizableEdges = borderEdges38;
            this.bunifuButton26.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton26.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton26.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton26.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton26.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton26.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton26.ForeColor = System.Drawing.Color.White;
            this.bunifuButton26.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton26.IconMarginLeft = 11;
            this.bunifuButton26.IconPadding = 10;
            this.bunifuButton26.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton26.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton26.IdleBorderRadius = 3;
            this.bunifuButton26.IdleBorderThickness = 1;
            this.bunifuButton26.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton26.IdleIconLeftImage = null;
            this.bunifuButton26.IdleIconRightImage = null;
            this.bunifuButton26.IndicateFocus = false;
            this.bunifuButton26.Location = new System.Drawing.Point(401, 248);
            this.bunifuButton26.Name = "bunifuButton26";
            stateProperties143.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties143.BorderRadius = 3;
            stateProperties143.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties143.BorderThickness = 1;
            stateProperties143.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties143.ForeColor = System.Drawing.Color.White;
            stateProperties143.IconLeftImage = null;
            stateProperties143.IconRightImage = null;
            this.bunifuButton26.onHoverState = stateProperties143;
            stateProperties144.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties144.BorderRadius = 3;
            stateProperties144.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties144.BorderThickness = 1;
            stateProperties144.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties144.ForeColor = System.Drawing.Color.White;
            stateProperties144.IconLeftImage = null;
            stateProperties144.IconRightImage = null;
            this.bunifuButton26.OnPressedState = stateProperties144;
            this.bunifuButton26.Size = new System.Drawing.Size(140, 33);
            this.bunifuButton26.TabIndex = 4;
            this.bunifuButton26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton26.TextMarginLeft = 0;
            this.bunifuButton26.UseDefaultRadiusAndThickness = true;
            this.bunifuButton26.Click += new System.EventHandler(this.bunifuButton26_Click);
            this.bunifuButton26.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.bunifuButton26_KeyPress);
            // 
            // bunifuButton27
            // 
            this.bunifuButton27.AllowToggling = false;
            this.bunifuButton27.AnimationSpeed = 200;
            this.bunifuButton27.AutoGenerateColors = false;
            this.bunifuButton27.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton27.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.bunifuButton27.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton27.BackgroundImage")));
            this.bunifuButton27.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton27.ButtonText = "Delete";
            this.bunifuButton27.ButtonTextMarginLeft = 0;
            this.bunifuButton27.ColorContrastOnClick = 45;
            this.bunifuButton27.ColorContrastOnHover = 45;
            this.bunifuButton27.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges39.BottomLeft = true;
            borderEdges39.BottomRight = true;
            borderEdges39.TopLeft = true;
            borderEdges39.TopRight = true;
            this.bunifuButton27.CustomizableEdges = borderEdges39;
            this.bunifuButton27.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton27.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton27.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton27.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton27.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton27.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton27.ForeColor = System.Drawing.Color.White;
            this.bunifuButton27.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton27.IconMarginLeft = 11;
            this.bunifuButton27.IconPadding = 10;
            this.bunifuButton27.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton27.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.bunifuButton27.IdleBorderRadius = 3;
            this.bunifuButton27.IdleBorderThickness = 1;
            this.bunifuButton27.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.bunifuButton27.IdleIconLeftImage = null;
            this.bunifuButton27.IdleIconRightImage = null;
            this.bunifuButton27.IndicateFocus = false;
            this.bunifuButton27.Location = new System.Drawing.Point(583, 248);
            this.bunifuButton27.Name = "bunifuButton27";
            stateProperties145.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties145.BorderRadius = 3;
            stateProperties145.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties145.BorderThickness = 1;
            stateProperties145.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties145.ForeColor = System.Drawing.Color.White;
            stateProperties145.IconLeftImage = null;
            stateProperties145.IconRightImage = null;
            this.bunifuButton27.onHoverState = stateProperties145;
            stateProperties146.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties146.BorderRadius = 3;
            stateProperties146.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties146.BorderThickness = 1;
            stateProperties146.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties146.ForeColor = System.Drawing.Color.White;
            stateProperties146.IconLeftImage = null;
            stateProperties146.IconRightImage = null;
            this.bunifuButton27.OnPressedState = stateProperties146;
            this.bunifuButton27.Size = new System.Drawing.Size(141, 33);
            this.bunifuButton27.TabIndex = 0;
            this.bunifuButton27.TabStop = false;
            this.bunifuButton27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton27.TextMarginLeft = 0;
            this.bunifuButton27.UseDefaultRadiusAndThickness = true;
            this.bunifuButton27.Click += new System.EventHandler(this.bunifuButton27_Click);
            // 
            // backup
            // 
            this.backup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.backup.Controls.Add(this.label14);
            this.backup.Controls.Add(this.label15);
            this.backup.Controls.Add(this.label13);
            this.backup.Controls.Add(this.bunifuPictureBox1);
            this.backup.Controls.Add(this.label9);
            this.backup.Controls.Add(this.label8);
            this.backup.Controls.Add(this.label5);
            this.backup.Controls.Add(this.bunifuButton28);
            this.backup.Controls.Add(this.bunifuButton19);
            this.backup.Controls.Add(this.backUppathtext);
            this.backup.Controls.Add(this.label12);
            this.backup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.backup.Location = new System.Drawing.Point(4, 4);
            this.backup.Name = "backup";
            this.backup.Padding = new System.Windows.Forms.Padding(3);
            this.backup.Size = new System.Drawing.Size(1041, 728);
            this.backup.TabIndex = 7;
            this.backup.Text = "BackUp";
            this.backup.Click += new System.EventHandler(this.backup_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label14.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label14.Location = new System.Drawing.Point(32, 583);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(197, 32);
            this.label14.TabIndex = 174;
            this.label14.Text = "+91-8299155755";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label15.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label15.Location = new System.Drawing.Point(35, 619);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(334, 32);
            this.label15.TabIndex = 173;
            this.label15.Text = "abhisheksngh037@gmail.com";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label13.Location = new System.Drawing.Point(26, 532);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 32);
            this.label13.TabIndex = 172;
            this.label13.Text = "Contact";
            // 
            // bunifuPictureBox1
            // 
            this.bunifuPictureBox1.AllowFocused = false;
            this.bunifuPictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuPictureBox1.BorderRadius = 12;
            this.bunifuPictureBox1.ErrorImage = null;
            this.bunifuPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuPictureBox1.Image")));
            this.bunifuPictureBox1.InitialImage = null;
            this.bunifuPictureBox1.IsCircle = false;
            this.bunifuPictureBox1.Location = new System.Drawing.Point(32, 384);
            this.bunifuPictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.bunifuPictureBox1.Name = "bunifuPictureBox1";
            this.bunifuPictureBox1.Size = new System.Drawing.Size(103, 103);
            this.bunifuPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuPictureBox1.TabIndex = 171;
            this.bunifuPictureBox1.TabStop = false;
            this.bunifuPictureBox1.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Square;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label9.Location = new System.Drawing.Point(149, 401);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(260, 32);
            this.label9.TabIndex = 170;
            this.label9.Text = "Gold Raven Version 1.1";
            this.label9.Click += new System.EventHandler(this.label9_Click_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label8.Location = new System.Drawing.Point(149, 437);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(525, 32);
            this.label8.TabIndex = 169;
            this.label8.Text = "© 2021- 2022 Chetana Tech All Rights Reserved";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(26, 334);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 32);
            this.label5.TabIndex = 168;
            this.label5.Text = "About";
            this.label5.Click += new System.EventHandler(this.label5_Click_1);
            // 
            // bunifuButton28
            // 
            this.bunifuButton28.AllowToggling = false;
            this.bunifuButton28.AnimationSpeed = 200;
            this.bunifuButton28.AutoGenerateColors = false;
            this.bunifuButton28.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton28.BackColor1 = System.Drawing.Color.Goldenrod;
            this.bunifuButton28.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton28.BackgroundImage")));
            this.bunifuButton28.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton28.ButtonText = "BackUp Now";
            this.bunifuButton28.ButtonTextMarginLeft = 0;
            this.bunifuButton28.ColorContrastOnClick = 45;
            this.bunifuButton28.ColorContrastOnHover = 45;
            this.bunifuButton28.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges40.BottomLeft = true;
            borderEdges40.BottomRight = true;
            borderEdges40.TopLeft = true;
            borderEdges40.TopRight = true;
            this.bunifuButton28.CustomizableEdges = borderEdges40;
            this.bunifuButton28.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton28.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton28.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton28.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton28.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton28.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton28.ForeColor = System.Drawing.Color.White;
            this.bunifuButton28.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton28.IconMarginLeft = 11;
            this.bunifuButton28.IconPadding = 10;
            this.bunifuButton28.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton28.IdleBorderColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton28.IdleBorderRadius = 3;
            this.bunifuButton28.IdleBorderThickness = 1;
            this.bunifuButton28.IdleFillColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton28.IdleIconLeftImage = null;
            this.bunifuButton28.IdleIconRightImage = null;
            this.bunifuButton28.IndicateFocus = false;
            this.bunifuButton28.Location = new System.Drawing.Point(32, 191);
            this.bunifuButton28.Name = "bunifuButton28";
            stateProperties147.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties147.BorderRadius = 3;
            stateProperties147.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties147.BorderThickness = 1;
            stateProperties147.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties147.ForeColor = System.Drawing.Color.White;
            stateProperties147.IconLeftImage = null;
            stateProperties147.IconRightImage = null;
            this.bunifuButton28.onHoverState = stateProperties147;
            stateProperties148.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties148.BorderRadius = 3;
            stateProperties148.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties148.BorderThickness = 1;
            stateProperties148.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties148.ForeColor = System.Drawing.Color.White;
            stateProperties148.IconLeftImage = null;
            stateProperties148.IconRightImage = null;
            this.bunifuButton28.OnPressedState = stateProperties148;
            this.bunifuButton28.Size = new System.Drawing.Size(964, 73);
            this.bunifuButton28.TabIndex = 167;
            this.bunifuButton28.TabStop = false;
            this.bunifuButton28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton28.TextMarginLeft = 0;
            this.bunifuButton28.UseDefaultRadiusAndThickness = true;
            this.bunifuButton28.Click += new System.EventHandler(this.bunifuButton28_Click);
            // 
            // bunifuButton19
            // 
            this.bunifuButton19.AllowToggling = false;
            this.bunifuButton19.AnimationSpeed = 200;
            this.bunifuButton19.AutoGenerateColors = false;
            this.bunifuButton19.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton19.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton19.BackgroundImage")));
            this.bunifuButton19.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton19.ButtonText = "Select Folder";
            this.bunifuButton19.ButtonTextMarginLeft = 0;
            this.bunifuButton19.ColorContrastOnClick = 45;
            this.bunifuButton19.ColorContrastOnHover = 45;
            this.bunifuButton19.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges41.BottomLeft = true;
            borderEdges41.BottomRight = true;
            borderEdges41.TopLeft = true;
            borderEdges41.TopRight = true;
            this.bunifuButton19.CustomizableEdges = borderEdges41;
            this.bunifuButton19.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton19.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton19.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton19.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton19.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton19.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton19.ForeColor = System.Drawing.Color.White;
            this.bunifuButton19.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton19.IconMarginLeft = 11;
            this.bunifuButton19.IconPadding = 10;
            this.bunifuButton19.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton19.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton19.IdleBorderRadius = 3;
            this.bunifuButton19.IdleBorderThickness = 1;
            this.bunifuButton19.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton19.IdleIconLeftImage = null;
            this.bunifuButton19.IdleIconRightImage = null;
            this.bunifuButton19.IndicateFocus = false;
            this.bunifuButton19.Location = new System.Drawing.Point(752, 113);
            this.bunifuButton19.Name = "bunifuButton19";
            stateProperties149.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties149.BorderRadius = 3;
            stateProperties149.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties149.BorderThickness = 1;
            stateProperties149.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties149.ForeColor = System.Drawing.Color.White;
            stateProperties149.IconLeftImage = null;
            stateProperties149.IconRightImage = null;
            this.bunifuButton19.onHoverState = stateProperties149;
            stateProperties150.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties150.BorderRadius = 3;
            stateProperties150.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties150.BorderThickness = 1;
            stateProperties150.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties150.ForeColor = System.Drawing.Color.White;
            stateProperties150.IconLeftImage = null;
            stateProperties150.IconRightImage = null;
            this.bunifuButton19.OnPressedState = stateProperties150;
            this.bunifuButton19.Size = new System.Drawing.Size(244, 33);
            this.bunifuButton19.TabIndex = 0;
            this.bunifuButton19.TabStop = false;
            this.bunifuButton19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton19.TextMarginLeft = 0;
            this.bunifuButton19.UseDefaultRadiusAndThickness = true;
            this.bunifuButton19.Click += new System.EventHandler(this.bunifuButton19_Click_1);
            // 
            // backUppathtext
            // 
            this.backUppathtext.BorderColorFocused = System.Drawing.Color.Red;
            this.backUppathtext.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.backUppathtext.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.backUppathtext.BorderThickness = 1;
            this.backUppathtext.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.backUppathtext.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.backUppathtext.Enabled = false;
            this.backUppathtext.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backUppathtext.ForeColor = System.Drawing.Color.White;
            this.backUppathtext.isPassword = false;
            this.backUppathtext.Location = new System.Drawing.Point(32, 113);
            this.backUppathtext.Margin = new System.Windows.Forms.Padding(4);
            this.backUppathtext.MaxLength = 32767;
            this.backUppathtext.Name = "backUppathtext";
            this.backUppathtext.Size = new System.Drawing.Size(695, 33);
            this.backUppathtext.TabIndex = 0;
            this.backUppathtext.TabStop = false;
            this.backUppathtext.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label12.Location = new System.Drawing.Point(26, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(247, 32);
            this.label12.TabIndex = 162;
            this.label12.Text = "BackUp And Restore";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // sendReport
            // 
            this.sendReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.sendReport.Controls.Add(this.sendMobile);
            this.sendReport.Controls.Add(this.bunifuLabel91);
            this.sendReport.Controls.Add(this.label17);
            this.sendReport.Controls.Add(this.bunifuLabel100);
            this.sendReport.Controls.Add(this.selectDate);
            this.sendReport.Controls.Add(this.sendGoldPurity);
            this.sendReport.Controls.Add(this.sendreportDataTable);
            this.sendReport.Controls.Add(this.sendSample);
            this.sendReport.Controls.Add(this.bunifuLabel104);
            this.sendReport.Controls.Add(this.sendCustomer);
            this.sendReport.Controls.Add(this.bunifuLabel90);
            this.sendReport.Controls.Add(this.sendSerial);
            this.sendReport.Controls.Add(this.bunifuLabel92);
            this.sendReport.Controls.Add(this.bunifuLabel94);
            this.sendReport.Controls.Add(this.bunifuLabel96);
            this.sendReport.Controls.Add(this.bunifuLabel97);
            this.sendReport.Controls.Add(this.bunifuLabel98);
            this.sendReport.Controls.Add(this.bunifuLabel99);
            this.sendReport.Controls.Add(this.bunifuLabel101);
            this.sendReport.Controls.Add(this.bunifuLabel102);
            this.sendReport.Controls.Add(this.sendseries);
            this.sendReport.Controls.Add(this.bunifuLabel103);
            this.sendReport.Controls.Add(this.label16);
            this.sendReport.Controls.Add(this.sendFine);
            this.sendReport.Controls.Add(this.sendAmount);
            this.sendReport.Controls.Add(this.sendTweight);
            this.sendReport.Controls.Add(this.sendOweight);
            this.sendReport.Controls.Add(this.sendKarat);
            this.sendReport.Controls.Add(this.sendDate);
            this.sendReport.Controls.Add(this.bunifuButton17);
            this.sendReport.Controls.Add(this.bunifuButton31);
            this.sendReport.Controls.Add(this.bunifuButton33);
            this.sendReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.sendReport.Location = new System.Drawing.Point(4, 4);
            this.sendReport.Name = "sendReport";
            this.sendReport.Padding = new System.Windows.Forms.Padding(3);
            this.sendReport.Size = new System.Drawing.Size(1041, 728);
            this.sendReport.TabIndex = 8;
            this.sendReport.Text = "sendReport";
            // 
            // sendMobile
            // 
            this.sendMobile.AutoEllipsis = false;
            this.sendMobile.CursorType = null;
            this.sendMobile.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendMobile.ForeColor = System.Drawing.Color.White;
            this.sendMobile.Location = new System.Drawing.Point(493, 264);
            this.sendMobile.Name = "sendMobile";
            this.sendMobile.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.sendMobile.Size = new System.Drawing.Size(146, 39);
            this.sendMobile.TabIndex = 259;
            this.sendMobile.TabStop = false;
            this.sendMobile.Text = "Mobile No.";
            this.sendMobile.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.sendMobile.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel91
            // 
            this.bunifuLabel91.AutoEllipsis = false;
            this.bunifuLabel91.CursorType = null;
            this.bunifuLabel91.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel91.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel91.Location = new System.Drawing.Point(390, 276);
            this.bunifuLabel91.Name = "bunifuLabel91";
            this.bunifuLabel91.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel91.Size = new System.Drawing.Size(79, 23);
            this.bunifuLabel91.TabIndex = 258;
            this.bunifuLabel91.TabStop = false;
            this.bunifuLabel91.Text = "Mobile No.";
            this.bunifuLabel91.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel91.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label17.Location = new System.Drawing.Point(29, 264);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(145, 35);
            this.label17.TabIndex = 257;
            this.label17.Text = "Get Report";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // bunifuLabel100
            // 
            this.bunifuLabel100.AutoEllipsis = false;
            this.bunifuLabel100.CursorType = null;
            this.bunifuLabel100.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel100.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel100.Location = new System.Drawing.Point(42, 305);
            this.bunifuLabel100.Name = "bunifuLabel100";
            this.bunifuLabel100.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel100.Size = new System.Drawing.Size(80, 23);
            this.bunifuLabel100.TabIndex = 255;
            this.bunifuLabel100.TabStop = false;
            this.bunifuLabel100.Text = "Select Date";
            this.bunifuLabel100.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel100.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // selectDate
            // 
            this.selectDate.BackColor = System.Drawing.Color.Transparent;
            this.selectDate.BorderRadius = 1;
            this.selectDate.Color = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.selectDate.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.selectDate.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Right;
            this.selectDate.DisabledColor = System.Drawing.Color.Gray;
            this.selectDate.DisplayWeekNumbers = false;
            this.selectDate.DPHeight = 0;
            this.selectDate.FillDatePicker = false;
            this.selectDate.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.selectDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.selectDate.Icon = ((System.Drawing.Image)(resources.GetObject("selectDate.Icon")));
            this.selectDate.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.selectDate.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Left;
            this.selectDate.Location = new System.Drawing.Point(41, 334);
            this.selectDate.MinimumSize = new System.Drawing.Size(277, 33);
            this.selectDate.Name = "selectDate";
            this.selectDate.Size = new System.Drawing.Size(277, 33);
            this.selectDate.TabIndex = 253;
            this.selectDate.ValueChanged += new System.EventHandler(this.bunifuDatePicker4_ValueChanged_1);
            // 
            // sendGoldPurity
            // 
            this.sendGoldPurity.AcceptsReturn = false;
            this.sendGoldPurity.AcceptsTab = false;
            this.sendGoldPurity.AnimationSpeed = 200;
            this.sendGoldPurity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.sendGoldPurity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.sendGoldPurity.BackColor = System.Drawing.Color.Transparent;
            this.sendGoldPurity.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sendGoldPurity.BackgroundImage")));
            this.sendGoldPurity.BorderColorActive = System.Drawing.Color.Red;
            this.sendGoldPurity.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendGoldPurity.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.sendGoldPurity.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendGoldPurity.BorderRadius = 1;
            this.sendGoldPurity.BorderThickness = 1;
            this.sendGoldPurity.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.sendGoldPurity.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.sendGoldPurity.DefaultFont = new System.Drawing.Font("Century Gothic", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendGoldPurity.DefaultText = "";
            this.sendGoldPurity.Enabled = false;
            this.sendGoldPurity.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.sendGoldPurity.ForeColor = System.Drawing.Color.Red;
            this.sendGoldPurity.HideSelection = true;
            this.sendGoldPurity.IconLeft = null;
            this.sendGoldPurity.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.sendGoldPurity.IconPadding = 20;
            this.sendGoldPurity.IconRight = global::GoldRaven.Properties.Resources.PERCENTAGE;
            this.sendGoldPurity.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.sendGoldPurity.Lines = new string[0];
            this.sendGoldPurity.Location = new System.Drawing.Point(317, 151);
            this.sendGoldPurity.MaxLength = 32767;
            this.sendGoldPurity.MinimumSize = new System.Drawing.Size(0, 33);
            this.sendGoldPurity.Modified = false;
            this.sendGoldPurity.Multiline = false;
            this.sendGoldPurity.Name = "sendGoldPurity";
            stateProperties151.BorderColor = System.Drawing.Color.Red;
            stateProperties151.FillColor = System.Drawing.Color.Empty;
            stateProperties151.ForeColor = System.Drawing.Color.Empty;
            stateProperties151.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendGoldPurity.OnActiveState = stateProperties151;
            stateProperties152.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties152.FillColor = System.Drawing.Color.White;
            stateProperties152.ForeColor = System.Drawing.Color.Empty;
            stateProperties152.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.sendGoldPurity.OnDisabledState = stateProperties152;
            stateProperties153.BorderColor = System.Drawing.Color.Red;
            stateProperties153.FillColor = System.Drawing.Color.Empty;
            stateProperties153.ForeColor = System.Drawing.Color.Empty;
            stateProperties153.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendGoldPurity.OnHoverState = stateProperties153;
            stateProperties154.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties154.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties154.ForeColor = System.Drawing.Color.Red;
            stateProperties154.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendGoldPurity.OnIdleState = stateProperties154;
            this.sendGoldPurity.PasswordChar = '\0';
            this.sendGoldPurity.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.sendGoldPurity.PlaceholderText = "Enter Percentage";
            this.sendGoldPurity.ReadOnly = false;
            this.sendGoldPurity.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.sendGoldPurity.SelectedText = "";
            this.sendGoldPurity.SelectionLength = 0;
            this.sendGoldPurity.SelectionStart = 0;
            this.sendGoldPurity.ShortcutsEnabled = true;
            this.sendGoldPurity.Size = new System.Drawing.Size(187, 99);
            this.sendGoldPurity.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.sendGoldPurity.TabIndex = 252;
            this.sendGoldPurity.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sendGoldPurity.TextMarginBottom = 0;
            this.sendGoldPurity.TextMarginLeft = 5;
            this.sendGoldPurity.TextMarginTop = 0;
            this.sendGoldPurity.TextPlaceholder = "Enter Percentage";
            this.sendGoldPurity.UseSystemPasswordChar = false;
            this.sendGoldPurity.WordWrap = true;
            // 
            // sendreportDataTable
            // 
            this.sendreportDataTable.AllowUserToAddRows = false;
            this.sendreportDataTable.AllowUserToDeleteRows = false;
            dataGridViewCellStyle36.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.sendreportDataTable.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle36;
            this.sendreportDataTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.sendreportDataTable.BackgroundColor = System.Drawing.Color.White;
            this.sendreportDataTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.sendreportDataTable.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.sendreportDataTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.sendreportDataTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle37;
            this.sendreportDataTable.ColumnHeadersHeight = 50;
            this.sendreportDataTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn2,
            this.Column202,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewTextBoxColumn55,
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60,
            this.dataGridViewTextBoxColumn61,
            this.dataGridViewTextBoxColumn62,
            this.dataGridViewTextBoxColumn63,
            this.dataGridViewTextBoxColumn64,
            this.dataGridViewTextBoxColumn65,
            this.dataGridViewTextBoxColumn66});
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle40.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(252)))));
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.sendreportDataTable.DefaultCellStyle = dataGridViewCellStyle40;
            this.sendreportDataTable.DoubleBuffered = true;
            this.sendreportDataTable.EnableHeadersVisualStyles = false;
            this.sendreportDataTable.GridColor = System.Drawing.Color.WhiteSmoke;
            this.sendreportDataTable.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(77)))), ((int)(((byte)(107)))));
            this.sendreportDataTable.HeaderForeColor = System.Drawing.Color.White;
            this.sendreportDataTable.Location = new System.Drawing.Point(27, 389);
            this.sendreportDataTable.Name = "sendreportDataTable";
            this.sendreportDataTable.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.sendreportDataTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle41;
            this.sendreportDataTable.RowHeadersVisible = false;
            this.sendreportDataTable.RowHeadersWidth = 51;
            this.sendreportDataTable.RowTemplate.DividerHeight = 1;
            this.sendreportDataTable.RowTemplate.Height = 40;
            this.sendreportDataTable.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sendreportDataTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sendreportDataTable.Size = new System.Drawing.Size(991, 333);
            this.sendreportDataTable.TabIndex = 251;
            this.sendreportDataTable.TabStop = false;
            this.sendreportDataTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.sendreportDataTable_CellContentClick);
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "payment";
            this.dataGridViewCheckBoxColumn2.HeaderText = "Paid/Unpaid";
            this.dataGridViewCheckBoxColumn2.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 109;
            // 
            // Column202
            // 
            this.Column202.DataPropertyName = "amount";
            this.Column202.HeaderText = "Amount";
            this.Column202.MinimumWidth = 6;
            this.Column202.Name = "Column202";
            this.Column202.ReadOnly = true;
            this.Column202.Width = 99;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "currentDate";
            this.dataGridViewTextBoxColumn6.HeaderText = "Date";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 73;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "iconFirst";
            this.dataGridViewTextBoxColumn9.HeaderText = "FirstIcon";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            this.dataGridViewTextBoxColumn9.Width = 102;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "iconLast";
            this.dataGridViewTextBoxColumn10.HeaderText = "LastIcon";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "relatedImage";
            this.dataGridViewTextBoxColumn11.HeaderText = "Image";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Visible = false;
            this.dataGridViewTextBoxColumn11.Width = 85;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "iconImage";
            this.dataGridViewTextBoxColumn12.HeaderText = "IconImage";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Visible = false;
            this.dataGridViewTextBoxColumn12.Width = 118;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Silver";
            this.dataGridViewTextBoxColumn13.HeaderText = "Silver";
            this.dataGridViewTextBoxColumn13.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Visible = false;
            this.dataGridViewTextBoxColumn13.Width = 78;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "Cobalt";
            this.dataGridViewTextBoxColumn14.HeaderText = "Cobalt";
            this.dataGridViewTextBoxColumn14.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Visible = false;
            this.dataGridViewTextBoxColumn14.Width = 87;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Lead";
            this.dataGridViewTextBoxColumn15.HeaderText = "Lead";
            this.dataGridViewTextBoxColumn15.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Visible = false;
            this.dataGridViewTextBoxColumn15.Width = 73;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "Ruthenium";
            this.dataGridViewTextBoxColumn16.HeaderText = "Ruthenium";
            this.dataGridViewTextBoxColumn16.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Visible = false;
            this.dataGridViewTextBoxColumn16.Width = 122;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "Copper";
            this.dataGridViewTextBoxColumn17.HeaderText = "Copper";
            this.dataGridViewTextBoxColumn17.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Visible = false;
            this.dataGridViewTextBoxColumn17.Width = 93;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "Platinum";
            this.dataGridViewTextBoxColumn18.HeaderText = "Platinum";
            this.dataGridViewTextBoxColumn18.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Visible = false;
            this.dataGridViewTextBoxColumn18.Width = 105;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "Chromium";
            this.dataGridViewTextBoxColumn19.HeaderText = "Chromium";
            this.dataGridViewTextBoxColumn19.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Visible = false;
            this.dataGridViewTextBoxColumn19.Width = 118;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "Tungustan";
            this.dataGridViewTextBoxColumn20.HeaderText = "Tungustan";
            this.dataGridViewTextBoxColumn20.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Visible = false;
            this.dataGridViewTextBoxColumn20.Width = 117;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "Zinc";
            this.dataGridViewTextBoxColumn21.HeaderText = "Zinc";
            this.dataGridViewTextBoxColumn21.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Visible = false;
            this.dataGridViewTextBoxColumn21.Width = 69;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "Palladium";
            this.dataGridViewTextBoxColumn22.HeaderText = "Palladium";
            this.dataGridViewTextBoxColumn22.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Visible = false;
            this.dataGridViewTextBoxColumn22.Width = 112;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "Bismuth";
            this.dataGridViewTextBoxColumn23.HeaderText = "Bismuth";
            this.dataGridViewTextBoxColumn23.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Visible = false;
            this.dataGridViewTextBoxColumn23.Width = 99;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "Manganese";
            this.dataGridViewTextBoxColumn24.HeaderText = "Manganese";
            this.dataGridViewTextBoxColumn24.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Visible = false;
            this.dataGridViewTextBoxColumn24.Width = 126;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "Cadmium";
            this.dataGridViewTextBoxColumn25.HeaderText = "Cadmium";
            this.dataGridViewTextBoxColumn25.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Visible = false;
            this.dataGridViewTextBoxColumn25.Width = 111;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "Antimony";
            this.dataGridViewTextBoxColumn26.HeaderText = "Antimony";
            this.dataGridViewTextBoxColumn26.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Visible = false;
            this.dataGridViewTextBoxColumn26.Width = 112;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "Nickel";
            this.dataGridViewTextBoxColumn27.HeaderText = "Nickel";
            this.dataGridViewTextBoxColumn27.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Visible = false;
            this.dataGridViewTextBoxColumn27.Width = 84;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "Osmium";
            this.dataGridViewTextBoxColumn28.HeaderText = "Osmium";
            this.dataGridViewTextBoxColumn28.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Visible = false;
            this.dataGridViewTextBoxColumn28.Width = 101;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "Iridium";
            this.dataGridViewTextBoxColumn29.HeaderText = "Iridium";
            this.dataGridViewTextBoxColumn29.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.Visible = false;
            this.dataGridViewTextBoxColumn29.Width = 91;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "Iron";
            this.dataGridViewTextBoxColumn30.HeaderText = "Iron";
            this.dataGridViewTextBoxColumn30.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.Visible = false;
            this.dataGridViewTextBoxColumn30.Width = 68;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "Indium";
            this.dataGridViewTextBoxColumn31.HeaderText = "Indium";
            this.dataGridViewTextBoxColumn31.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.Visible = false;
            this.dataGridViewTextBoxColumn31.Width = 91;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "Rehnium";
            this.dataGridViewTextBoxColumn32.HeaderText = "Rehnium";
            this.dataGridViewTextBoxColumn32.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            this.dataGridViewTextBoxColumn32.Visible = false;
            this.dataGridViewTextBoxColumn32.Width = 106;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "Rhodium";
            this.dataGridViewTextBoxColumn33.HeaderText = "Rhodium";
            this.dataGridViewTextBoxColumn33.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            this.dataGridViewTextBoxColumn33.Visible = false;
            this.dataGridViewTextBoxColumn33.Width = 107;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "Tin";
            this.dataGridViewTextBoxColumn34.HeaderText = "Tin";
            this.dataGridViewTextBoxColumn34.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            this.dataGridViewTextBoxColumn34.Visible = false;
            this.dataGridViewTextBoxColumn34.Width = 60;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.DataPropertyName = "Titanium";
            this.dataGridViewTextBoxColumn40.HeaderText = "Titanium";
            this.dataGridViewTextBoxColumn40.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.ReadOnly = true;
            this.dataGridViewTextBoxColumn40.Visible = false;
            this.dataGridViewTextBoxColumn40.Width = 104;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.DataPropertyName = "Gallium";
            this.dataGridViewTextBoxColumn48.HeaderText = "Gallium";
            this.dataGridViewTextBoxColumn48.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.ReadOnly = true;
            this.dataGridViewTextBoxColumn48.Visible = false;
            this.dataGridViewTextBoxColumn48.Width = 95;
            // 
            // dataGridViewTextBoxColumn54
            // 
            this.dataGridViewTextBoxColumn54.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn54.HeaderText = "Serial No";
            this.dataGridViewTextBoxColumn54.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.ReadOnly = true;
            this.dataGridViewTextBoxColumn54.Width = 106;
            // 
            // dataGridViewTextBoxColumn55
            // 
            this.dataGridViewTextBoxColumn55.DataPropertyName = "seriesNo";
            this.dataGridViewTextBoxColumn55.HeaderText = "Series No";
            this.dataGridViewTextBoxColumn55.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.ReadOnly = true;
            this.dataGridViewTextBoxColumn55.Width = 109;
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.DataPropertyName = "currentTime";
            this.dataGridViewTextBoxColumn56.HeaderText = "Time";
            this.dataGridViewTextBoxColumn56.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.ReadOnly = true;
            this.dataGridViewTextBoxColumn56.Width = 74;
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.DataPropertyName = "customerName";
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.Teal;
            this.dataGridViewTextBoxColumn57.DefaultCellStyle = dataGridViewCellStyle38;
            this.dataGridViewTextBoxColumn57.HeaderText = "Name";
            this.dataGridViewTextBoxColumn57.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.ReadOnly = true;
            this.dataGridViewTextBoxColumn57.Width = 83;
            // 
            // dataGridViewTextBoxColumn58
            // 
            this.dataGridViewTextBoxColumn58.DataPropertyName = "custMobile";
            this.dataGridViewTextBoxColumn58.HeaderText = "Mobile No";
            this.dataGridViewTextBoxColumn58.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.ReadOnly = true;
            this.dataGridViewTextBoxColumn58.Width = 118;
            // 
            // dataGridViewTextBoxColumn59
            // 
            this.dataGridViewTextBoxColumn59.DataPropertyName = "sample";
            this.dataGridViewTextBoxColumn59.HeaderText = "Sample";
            this.dataGridViewTextBoxColumn59.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            this.dataGridViewTextBoxColumn59.ReadOnly = true;
            this.dataGridViewTextBoxColumn59.Width = 93;
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.DataPropertyName = "goldPurity";
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dataGridViewTextBoxColumn60.DefaultCellStyle = dataGridViewCellStyle39;
            this.dataGridViewTextBoxColumn60.HeaderText = "Purity";
            this.dataGridViewTextBoxColumn60.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.ReadOnly = true;
            this.dataGridViewTextBoxColumn60.Width = 82;
            // 
            // dataGridViewTextBoxColumn61
            // 
            this.dataGridViewTextBoxColumn61.DataPropertyName = "goldKarat";
            this.dataGridViewTextBoxColumn61.HeaderText = "Karat";
            this.dataGridViewTextBoxColumn61.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            this.dataGridViewTextBoxColumn61.ReadOnly = true;
            this.dataGridViewTextBoxColumn61.Width = 77;
            // 
            // dataGridViewTextBoxColumn62
            // 
            this.dataGridViewTextBoxColumn62.DataPropertyName = "orgWeight";
            this.dataGridViewTextBoxColumn62.HeaderText = "Weight";
            this.dataGridViewTextBoxColumn62.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn62.Name = "dataGridViewTextBoxColumn62";
            this.dataGridViewTextBoxColumn62.ReadOnly = true;
            this.dataGridViewTextBoxColumn62.Width = 92;
            // 
            // dataGridViewTextBoxColumn63
            // 
            this.dataGridViewTextBoxColumn63.DataPropertyName = "tounchweight";
            this.dataGridViewTextBoxColumn63.HeaderText = "Tounch Weight";
            this.dataGridViewTextBoxColumn63.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
            this.dataGridViewTextBoxColumn63.ReadOnly = true;
            this.dataGridViewTextBoxColumn63.Width = 152;
            // 
            // dataGridViewTextBoxColumn64
            // 
            this.dataGridViewTextBoxColumn64.DataPropertyName = "goldFine";
            this.dataGridViewTextBoxColumn64.HeaderText = "Gold Fine";
            this.dataGridViewTextBoxColumn64.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn64.Name = "dataGridViewTextBoxColumn64";
            this.dataGridViewTextBoxColumn64.ReadOnly = true;
            this.dataGridViewTextBoxColumn64.Width = 110;
            // 
            // dataGridViewTextBoxColumn65
            // 
            this.dataGridViewTextBoxColumn65.DataPropertyName = "Rate";
            this.dataGridViewTextBoxColumn65.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn65.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn65.Name = "dataGridViewTextBoxColumn65";
            this.dataGridViewTextBoxColumn65.ReadOnly = true;
            this.dataGridViewTextBoxColumn65.Width = 72;
            // 
            // dataGridViewTextBoxColumn66
            // 
            this.dataGridViewTextBoxColumn66.DataPropertyName = "customerAddress";
            this.dataGridViewTextBoxColumn66.HeaderText = "Address";
            this.dataGridViewTextBoxColumn66.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn66.Name = "dataGridViewTextBoxColumn66";
            this.dataGridViewTextBoxColumn66.ReadOnly = true;
            this.dataGridViewTextBoxColumn66.Width = 97;
            // 
            // sendSample
            // 
            this.sendSample.BorderColorFocused = System.Drawing.Color.Red;
            this.sendSample.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendSample.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.sendSample.BorderThickness = 1;
            this.sendSample.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.sendSample.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.sendSample.Enabled = false;
            this.sendSample.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.sendSample.ForeColor = System.Drawing.Color.White;
            this.sendSample.isPassword = false;
            this.sendSample.Location = new System.Drawing.Point(30, 216);
            this.sendSample.Margin = new System.Windows.Forms.Padding(4);
            this.sendSample.MaxLength = 32767;
            this.sendSample.Name = "sendSample";
            this.sendSample.Size = new System.Drawing.Size(277, 33);
            this.sendSample.TabIndex = 250;
            this.sendSample.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuLabel104
            // 
            this.bunifuLabel104.AutoEllipsis = false;
            this.bunifuLabel104.CursorType = null;
            this.bunifuLabel104.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel104.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel104.Location = new System.Drawing.Point(29, 191);
            this.bunifuLabel104.Name = "bunifuLabel104";
            this.bunifuLabel104.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel104.Size = new System.Drawing.Size(55, 23);
            this.bunifuLabel104.TabIndex = 249;
            this.bunifuLabel104.TabStop = false;
            this.bunifuLabel104.Text = "Sample";
            this.bunifuLabel104.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel104.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // sendCustomer
            // 
            this.sendCustomer.BorderColorFocused = System.Drawing.Color.Red;
            this.sendCustomer.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendCustomer.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.sendCustomer.BorderThickness = 1;
            this.sendCustomer.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.sendCustomer.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.sendCustomer.Enabled = false;
            this.sendCustomer.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.sendCustomer.ForeColor = System.Drawing.Color.White;
            this.sendCustomer.isPassword = false;
            this.sendCustomer.Location = new System.Drawing.Point(29, 152);
            this.sendCustomer.Margin = new System.Windows.Forms.Padding(4);
            this.sendCustomer.MaxLength = 32767;
            this.sendCustomer.Name = "sendCustomer";
            this.sendCustomer.Size = new System.Drawing.Size(277, 33);
            this.sendCustomer.TabIndex = 248;
            this.sendCustomer.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuLabel90
            // 
            this.bunifuLabel90.AutoEllipsis = false;
            this.bunifuLabel90.CursorType = null;
            this.bunifuLabel90.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel90.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel90.Location = new System.Drawing.Point(517, 191);
            this.bunifuLabel90.Name = "bunifuLabel90";
            this.bunifuLabel90.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel90.Size = new System.Drawing.Size(69, 23);
            this.bunifuLabel90.TabIndex = 245;
            this.bunifuLabel90.TabStop = false;
            this.bunifuLabel90.Text = "Gold Fine";
            this.bunifuLabel90.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel90.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // sendSerial
            // 
            this.sendSerial.AutoEllipsis = false;
            this.sendSerial.CursorType = null;
            this.sendSerial.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendSerial.ForeColor = System.Drawing.Color.White;
            this.sendSerial.Location = new System.Drawing.Point(837, 11);
            this.sendSerial.Name = "sendSerial";
            this.sendSerial.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.sendSerial.Size = new System.Drawing.Size(182, 39);
            this.sendSerial.TabIndex = 244;
            this.sendSerial.TabStop = false;
            this.sendSerial.Text = "Serial number";
            this.sendSerial.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.sendSerial.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel92
            // 
            this.bunifuLabel92.AutoEllipsis = false;
            this.bunifuLabel92.CursorType = null;
            this.bunifuLabel92.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel92.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel92.Location = new System.Drawing.Point(736, 53);
            this.bunifuLabel92.Name = "bunifuLabel92";
            this.bunifuLabel92.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel92.Size = new System.Drawing.Size(34, 23);
            this.bunifuLabel92.TabIndex = 241;
            this.bunifuLabel92.TabStop = false;
            this.bunifuLabel92.Text = "Rate";
            this.bunifuLabel92.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel92.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel94
            // 
            this.bunifuLabel94.AutoEllipsis = false;
            this.bunifuLabel94.CursorType = null;
            this.bunifuLabel94.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel94.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel94.Location = new System.Drawing.Point(734, 121);
            this.bunifuLabel94.Name = "bunifuLabel94";
            this.bunifuLabel94.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel94.Size = new System.Drawing.Size(108, 23);
            this.bunifuLabel94.TabIndex = 240;
            this.bunifuLabel94.TabStop = false;
            this.bunifuLabel94.Text = "Tounch Weight";
            this.bunifuLabel94.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel94.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel96
            // 
            this.bunifuLabel96.AutoEllipsis = false;
            this.bunifuLabel96.CursorType = null;
            this.bunifuLabel96.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel96.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel96.Location = new System.Drawing.Point(519, 122);
            this.bunifuLabel96.Name = "bunifuLabel96";
            this.bunifuLabel96.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel96.Size = new System.Drawing.Size(39, 23);
            this.bunifuLabel96.TabIndex = 243;
            this.bunifuLabel96.TabStop = false;
            this.bunifuLabel96.Text = "Karat";
            this.bunifuLabel96.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel96.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel97
            // 
            this.bunifuLabel97.AutoEllipsis = false;
            this.bunifuLabel97.CursorType = null;
            this.bunifuLabel97.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel97.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel97.Location = new System.Drawing.Point(315, 123);
            this.bunifuLabel97.Name = "bunifuLabel97";
            this.bunifuLabel97.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel97.Size = new System.Drawing.Size(44, 23);
            this.bunifuLabel97.TabIndex = 239;
            this.bunifuLabel97.TabStop = false;
            this.bunifuLabel97.Text = "Purity";
            this.bunifuLabel97.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel97.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel98
            // 
            this.bunifuLabel98.AutoEllipsis = false;
            this.bunifuLabel98.CursorType = null;
            this.bunifuLabel98.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel98.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel98.Location = new System.Drawing.Point(733, 186);
            this.bunifuLabel98.Name = "bunifuLabel98";
            this.bunifuLabel98.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel98.Size = new System.Drawing.Size(53, 23);
            this.bunifuLabel98.TabIndex = 238;
            this.bunifuLabel98.TabStop = false;
            this.bunifuLabel98.Text = "Weight";
            this.bunifuLabel98.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel98.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel99
            // 
            this.bunifuLabel99.AutoEllipsis = false;
            this.bunifuLabel99.CursorType = null;
            this.bunifuLabel99.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel99.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel99.Location = new System.Drawing.Point(401, 52);
            this.bunifuLabel99.Name = "bunifuLabel99";
            this.bunifuLabel99.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel99.Size = new System.Drawing.Size(35, 23);
            this.bunifuLabel99.TabIndex = 237;
            this.bunifuLabel99.TabStop = false;
            this.bunifuLabel99.Text = "Date";
            this.bunifuLabel99.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel99.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.bunifuLabel99.Click += new System.EventHandler(this.bunifuLabel99_Click);
            // 
            // bunifuLabel101
            // 
            this.bunifuLabel101.AutoEllipsis = false;
            this.bunifuLabel101.CursorType = null;
            this.bunifuLabel101.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel101.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel101.Location = new System.Drawing.Point(31, 123);
            this.bunifuLabel101.Name = "bunifuLabel101";
            this.bunifuLabel101.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel101.Size = new System.Drawing.Size(117, 23);
            this.bunifuLabel101.TabIndex = 235;
            this.bunifuLabel101.TabStop = false;
            this.bunifuLabel101.Text = "Customer Name";
            this.bunifuLabel101.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel101.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel102
            // 
            this.bunifuLabel102.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.bunifuLabel102.AutoEllipsis = false;
            this.bunifuLabel102.CursorType = null;
            this.bunifuLabel102.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.bunifuLabel102.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel102.Location = new System.Drawing.Point(29, 52);
            this.bunifuLabel102.Name = "bunifuLabel102";
            this.bunifuLabel102.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel102.Size = new System.Drawing.Size(70, 23);
            this.bunifuLabel102.TabIndex = 234;
            this.bunifuLabel102.TabStop = false;
            this.bunifuLabel102.Text = "Series No";
            this.bunifuLabel102.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel102.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // sendseries
            // 
            this.sendseries.BorderColorFocused = System.Drawing.Color.Red;
            this.sendseries.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendseries.BorderColorMouseHover = System.Drawing.Color.Goldenrod;
            this.sendseries.BorderThickness = 1;
            this.sendseries.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.sendseries.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.sendseries.Enabled = false;
            this.sendseries.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.sendseries.ForeColor = System.Drawing.Color.White;
            this.sendseries.isPassword = false;
            this.sendseries.Location = new System.Drawing.Point(28, 83);
            this.sendseries.Margin = new System.Windows.Forms.Padding(4);
            this.sendseries.MaxLength = 32767;
            this.sendseries.Name = "sendseries";
            this.sendseries.Size = new System.Drawing.Size(366, 33);
            this.sendseries.TabIndex = 221;
            this.sendseries.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuLabel103
            // 
            this.bunifuLabel103.AutoEllipsis = false;
            this.bunifuLabel103.CursorType = null;
            this.bunifuLabel103.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel103.ForeColor = System.Drawing.Color.White;
            this.bunifuLabel103.Location = new System.Drawing.Point(761, 22);
            this.bunifuLabel103.Name = "bunifuLabel103";
            this.bunifuLabel103.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel103.Size = new System.Drawing.Size(70, 23);
            this.bunifuLabel103.TabIndex = 233;
            this.bunifuLabel103.TabStop = false;
            this.bunifuLabel103.Text = "Serial No.";
            this.bunifuLabel103.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel103.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label16.Location = new System.Drawing.Point(22, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(161, 35);
            this.label16.TabIndex = 232;
            this.label16.Text = "Send Report";
            // 
            // sendFine
            // 
            this.sendFine.AcceptsReturn = false;
            this.sendFine.AcceptsTab = false;
            this.sendFine.AnimationSpeed = 200;
            this.sendFine.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.sendFine.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.sendFine.BackColor = System.Drawing.Color.Transparent;
            this.sendFine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sendFine.BackgroundImage")));
            this.sendFine.BorderColorActive = System.Drawing.Color.Goldenrod;
            this.sendFine.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendFine.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.sendFine.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendFine.BorderRadius = 1;
            this.sendFine.BorderThickness = 1;
            this.sendFine.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.sendFine.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.sendFine.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold);
            this.sendFine.DefaultText = "";
            this.sendFine.Enabled = false;
            this.sendFine.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.sendFine.ForeColor = System.Drawing.Color.White;
            this.sendFine.HideSelection = true;
            this.sendFine.IconLeft = null;
            this.sendFine.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.sendFine.IconPadding = 3;
            this.sendFine.IconRight = global::GoldRaven.Properties.Resources.Layer_1;
            this.sendFine.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.sendFine.Lines = new string[0];
            this.sendFine.Location = new System.Drawing.Point(515, 217);
            this.sendFine.MaxLength = 32767;
            this.sendFine.MinimumSize = new System.Drawing.Size(0, 33);
            this.sendFine.Modified = false;
            this.sendFine.Multiline = false;
            this.sendFine.Name = "sendFine";
            stateProperties155.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties155.FillColor = System.Drawing.Color.Empty;
            stateProperties155.ForeColor = System.Drawing.Color.Empty;
            stateProperties155.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendFine.OnActiveState = stateProperties155;
            stateProperties156.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties156.FillColor = System.Drawing.Color.White;
            stateProperties156.ForeColor = System.Drawing.Color.Empty;
            stateProperties156.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.sendFine.OnDisabledState = stateProperties156;
            stateProperties157.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties157.FillColor = System.Drawing.Color.Empty;
            stateProperties157.ForeColor = System.Drawing.Color.Empty;
            stateProperties157.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendFine.OnHoverState = stateProperties157;
            stateProperties158.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties158.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties158.ForeColor = System.Drawing.Color.White;
            stateProperties158.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendFine.OnIdleState = stateProperties158;
            this.sendFine.PasswordChar = '\0';
            this.sendFine.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.sendFine.PlaceholderText = "Fine";
            this.sendFine.ReadOnly = true;
            this.sendFine.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.sendFine.SelectedText = "";
            this.sendFine.SelectionLength = 0;
            this.sendFine.SelectionStart = 0;
            this.sendFine.ShortcutsEnabled = true;
            this.sendFine.Size = new System.Drawing.Size(187, 33);
            this.sendFine.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.sendFine.TabIndex = 219;
            this.sendFine.TabStop = false;
            this.sendFine.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sendFine.TextMarginBottom = 0;
            this.sendFine.TextMarginLeft = 5;
            this.sendFine.TextMarginTop = 0;
            this.sendFine.TextPlaceholder = "Fine";
            this.sendFine.UseSystemPasswordChar = false;
            this.sendFine.WordWrap = true;
            // 
            // sendAmount
            // 
            this.sendAmount.AcceptsReturn = false;
            this.sendAmount.AcceptsTab = false;
            this.sendAmount.AnimationSpeed = 200;
            this.sendAmount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.sendAmount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.sendAmount.BackColor = System.Drawing.Color.Transparent;
            this.sendAmount.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sendAmount.BackgroundImage")));
            this.sendAmount.BorderColorActive = System.Drawing.Color.Red;
            this.sendAmount.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendAmount.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.sendAmount.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendAmount.BorderRadius = 1;
            this.sendAmount.BorderThickness = 1;
            this.sendAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.sendAmount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.sendAmount.DefaultFont = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.sendAmount.DefaultText = "";
            this.sendAmount.Enabled = false;
            this.sendAmount.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.sendAmount.ForeColor = System.Drawing.Color.White;
            this.sendAmount.HideSelection = true;
            this.sendAmount.IconLeft = null;
            this.sendAmount.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.sendAmount.IconPadding = 3;
            this.sendAmount.IconRight = null;
            this.sendAmount.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.sendAmount.Lines = new string[0];
            this.sendAmount.Location = new System.Drawing.Point(733, 83);
            this.sendAmount.MaxLength = 32767;
            this.sendAmount.MinimumSize = new System.Drawing.Size(0, 33);
            this.sendAmount.Modified = false;
            this.sendAmount.Multiline = false;
            this.sendAmount.Name = "sendAmount";
            stateProperties159.BorderColor = System.Drawing.Color.Red;
            stateProperties159.FillColor = System.Drawing.Color.Empty;
            stateProperties159.ForeColor = System.Drawing.Color.Empty;
            stateProperties159.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendAmount.OnActiveState = stateProperties159;
            stateProperties160.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties160.FillColor = System.Drawing.Color.White;
            stateProperties160.ForeColor = System.Drawing.Color.Empty;
            stateProperties160.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.sendAmount.OnDisabledState = stateProperties160;
            stateProperties161.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties161.FillColor = System.Drawing.Color.Empty;
            stateProperties161.ForeColor = System.Drawing.Color.Empty;
            stateProperties161.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendAmount.OnHoverState = stateProperties161;
            stateProperties162.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties162.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties162.ForeColor = System.Drawing.Color.White;
            stateProperties162.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendAmount.OnIdleState = stateProperties162;
            this.sendAmount.PasswordChar = '\0';
            this.sendAmount.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.sendAmount.PlaceholderText = "₹";
            this.sendAmount.ReadOnly = false;
            this.sendAmount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.sendAmount.SelectedText = "";
            this.sendAmount.SelectionLength = 0;
            this.sendAmount.SelectionStart = 0;
            this.sendAmount.ShortcutsEnabled = true;
            this.sendAmount.Size = new System.Drawing.Size(285, 33);
            this.sendAmount.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.sendAmount.TabIndex = 228;
            this.sendAmount.TabStop = false;
            this.sendAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sendAmount.TextMarginBottom = 0;
            this.sendAmount.TextMarginLeft = 5;
            this.sendAmount.TextMarginTop = 0;
            this.sendAmount.TextPlaceholder = "₹";
            this.sendAmount.UseSystemPasswordChar = false;
            this.sendAmount.WordWrap = true;
            // 
            // sendTweight
            // 
            this.sendTweight.AcceptsReturn = false;
            this.sendTweight.AcceptsTab = false;
            this.sendTweight.AnimationSpeed = 200;
            this.sendTweight.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.sendTweight.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.sendTweight.BackColor = System.Drawing.Color.Transparent;
            this.sendTweight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sendTweight.BackgroundImage")));
            this.sendTweight.BorderColorActive = System.Drawing.Color.Red;
            this.sendTweight.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendTweight.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.sendTweight.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendTweight.BorderRadius = 1;
            this.sendTweight.BorderThickness = 1;
            this.sendTweight.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.sendTweight.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.sendTweight.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendTweight.DefaultText = "";
            this.sendTweight.Enabled = false;
            this.sendTweight.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.sendTweight.ForeColor = System.Drawing.Color.White;
            this.sendTweight.HideSelection = true;
            this.sendTweight.IconLeft = null;
            this.sendTweight.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.sendTweight.IconPadding = 3;
            this.sendTweight.IconRight = global::GoldRaven.Properties.Resources.Layer_11;
            this.sendTweight.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.sendTweight.Lines = new string[0];
            this.sendTweight.Location = new System.Drawing.Point(733, 151);
            this.sendTweight.MaxLength = 32767;
            this.sendTweight.MinimumSize = new System.Drawing.Size(0, 33);
            this.sendTweight.Modified = false;
            this.sendTweight.Multiline = false;
            this.sendTweight.Name = "sendTweight";
            stateProperties163.BorderColor = System.Drawing.Color.Red;
            stateProperties163.FillColor = System.Drawing.Color.Empty;
            stateProperties163.ForeColor = System.Drawing.Color.Empty;
            stateProperties163.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendTweight.OnActiveState = stateProperties163;
            stateProperties164.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties164.FillColor = System.Drawing.Color.White;
            stateProperties164.ForeColor = System.Drawing.Color.Empty;
            stateProperties164.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.sendTweight.OnDisabledState = stateProperties164;
            stateProperties165.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties165.FillColor = System.Drawing.Color.Empty;
            stateProperties165.ForeColor = System.Drawing.Color.Empty;
            stateProperties165.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendTweight.OnHoverState = stateProperties165;
            stateProperties166.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties166.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties166.ForeColor = System.Drawing.Color.White;
            stateProperties166.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendTweight.OnIdleState = stateProperties166;
            this.sendTweight.PasswordChar = '\0';
            this.sendTweight.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.sendTweight.PlaceholderText = "Enter Weight";
            this.sendTweight.ReadOnly = false;
            this.sendTweight.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.sendTweight.SelectedText = "";
            this.sendTweight.SelectionLength = 0;
            this.sendTweight.SelectionStart = 0;
            this.sendTweight.ShortcutsEnabled = true;
            this.sendTweight.Size = new System.Drawing.Size(285, 33);
            this.sendTweight.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.sendTweight.TabIndex = 224;
            this.sendTweight.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sendTweight.TextMarginBottom = 0;
            this.sendTweight.TextMarginLeft = 5;
            this.sendTweight.TextMarginTop = 0;
            this.sendTweight.TextPlaceholder = "Enter Weight";
            this.sendTweight.UseSystemPasswordChar = false;
            this.sendTweight.WordWrap = true;
            this.sendTweight.TextChanged += new System.EventHandler(this.bunifuTextBox3_TextChanged_1);
            // 
            // sendOweight
            // 
            this.sendOweight.AcceptsReturn = false;
            this.sendOweight.AcceptsTab = false;
            this.sendOweight.AnimationSpeed = 200;
            this.sendOweight.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.sendOweight.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.sendOweight.BackColor = System.Drawing.Color.Transparent;
            this.sendOweight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sendOweight.BackgroundImage")));
            this.sendOweight.BorderColorActive = System.Drawing.Color.Red;
            this.sendOweight.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendOweight.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.sendOweight.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendOweight.BorderRadius = 1;
            this.sendOweight.BorderThickness = 1;
            this.sendOweight.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.sendOweight.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.sendOweight.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendOweight.DefaultText = "";
            this.sendOweight.Enabled = false;
            this.sendOweight.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.sendOweight.ForeColor = System.Drawing.Color.White;
            this.sendOweight.HideSelection = true;
            this.sendOweight.IconLeft = null;
            this.sendOweight.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.sendOweight.IconPadding = 3;
            this.sendOweight.IconRight = global::GoldRaven.Properties.Resources.Layer_11;
            this.sendOweight.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.sendOweight.Lines = new string[0];
            this.sendOweight.Location = new System.Drawing.Point(733, 216);
            this.sendOweight.MaxLength = 32767;
            this.sendOweight.MinimumSize = new System.Drawing.Size(0, 33);
            this.sendOweight.Modified = false;
            this.sendOweight.Multiline = false;
            this.sendOweight.Name = "sendOweight";
            stateProperties167.BorderColor = System.Drawing.Color.Red;
            stateProperties167.FillColor = System.Drawing.Color.Empty;
            stateProperties167.ForeColor = System.Drawing.Color.Empty;
            stateProperties167.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendOweight.OnActiveState = stateProperties167;
            stateProperties168.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties168.FillColor = System.Drawing.Color.White;
            stateProperties168.ForeColor = System.Drawing.Color.Empty;
            stateProperties168.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.sendOweight.OnDisabledState = stateProperties168;
            stateProperties169.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties169.FillColor = System.Drawing.Color.Empty;
            stateProperties169.ForeColor = System.Drawing.Color.Empty;
            stateProperties169.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendOweight.OnHoverState = stateProperties169;
            stateProperties170.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties170.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties170.ForeColor = System.Drawing.Color.White;
            stateProperties170.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendOweight.OnIdleState = stateProperties170;
            this.sendOweight.PasswordChar = '\0';
            this.sendOweight.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.sendOweight.PlaceholderText = "Enter Weight";
            this.sendOweight.ReadOnly = false;
            this.sendOweight.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.sendOweight.SelectedText = "";
            this.sendOweight.SelectionLength = 0;
            this.sendOweight.SelectionStart = 0;
            this.sendOweight.ShortcutsEnabled = true;
            this.sendOweight.Size = new System.Drawing.Size(285, 33);
            this.sendOweight.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.sendOweight.TabIndex = 225;
            this.sendOweight.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sendOweight.TextMarginBottom = 0;
            this.sendOweight.TextMarginLeft = 5;
            this.sendOweight.TextMarginTop = 0;
            this.sendOweight.TextPlaceholder = "Enter Weight";
            this.sendOweight.UseSystemPasswordChar = false;
            this.sendOweight.WordWrap = true;
            // 
            // sendKarat
            // 
            this.sendKarat.AcceptsReturn = false;
            this.sendKarat.AcceptsTab = false;
            this.sendKarat.AnimationSpeed = 200;
            this.sendKarat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.sendKarat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.sendKarat.BackColor = System.Drawing.Color.Transparent;
            this.sendKarat.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sendKarat.BackgroundImage")));
            this.sendKarat.BorderColorActive = System.Drawing.Color.Goldenrod;
            this.sendKarat.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendKarat.BorderColorHover = System.Drawing.Color.Goldenrod;
            this.sendKarat.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendKarat.BorderRadius = 1;
            this.sendKarat.BorderThickness = 1;
            this.sendKarat.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.sendKarat.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.sendKarat.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold);
            this.sendKarat.DefaultText = "";
            this.sendKarat.Enabled = false;
            this.sendKarat.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.sendKarat.ForeColor = System.Drawing.Color.White;
            this.sendKarat.HideSelection = true;
            this.sendKarat.IconLeft = null;
            this.sendKarat.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.sendKarat.IconPadding = 10;
            this.sendKarat.IconRight = global::GoldRaven.Properties.Resources.icons8_k_45px_1;
            this.sendKarat.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.sendKarat.Lines = new string[0];
            this.sendKarat.Location = new System.Drawing.Point(517, 152);
            this.sendKarat.MaxLength = 32767;
            this.sendKarat.MinimumSize = new System.Drawing.Size(0, 33);
            this.sendKarat.Modified = false;
            this.sendKarat.Multiline = false;
            this.sendKarat.Name = "sendKarat";
            stateProperties171.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties171.FillColor = System.Drawing.Color.Empty;
            stateProperties171.ForeColor = System.Drawing.Color.Empty;
            stateProperties171.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendKarat.OnActiveState = stateProperties171;
            stateProperties172.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties172.FillColor = System.Drawing.Color.White;
            stateProperties172.ForeColor = System.Drawing.Color.Empty;
            stateProperties172.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.sendKarat.OnDisabledState = stateProperties172;
            stateProperties173.BorderColor = System.Drawing.Color.Goldenrod;
            stateProperties173.FillColor = System.Drawing.Color.Empty;
            stateProperties173.ForeColor = System.Drawing.Color.Empty;
            stateProperties173.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendKarat.OnHoverState = stateProperties173;
            stateProperties174.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            stateProperties174.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            stateProperties174.ForeColor = System.Drawing.Color.White;
            stateProperties174.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.sendKarat.OnIdleState = stateProperties174;
            this.sendKarat.PasswordChar = '\0';
            this.sendKarat.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.sendKarat.PlaceholderText = "Karat";
            this.sendKarat.ReadOnly = true;
            this.sendKarat.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.sendKarat.SelectedText = "";
            this.sendKarat.SelectionLength = 0;
            this.sendKarat.SelectionStart = 0;
            this.sendKarat.ShortcutsEnabled = true;
            this.sendKarat.Size = new System.Drawing.Size(187, 33);
            this.sendKarat.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.sendKarat.TabIndex = 231;
            this.sendKarat.TabStop = false;
            this.sendKarat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sendKarat.TextMarginBottom = 0;
            this.sendKarat.TextMarginLeft = 5;
            this.sendKarat.TextMarginTop = 0;
            this.sendKarat.TextPlaceholder = "Karat";
            this.sendKarat.UseSystemPasswordChar = false;
            this.sendKarat.WordWrap = true;
            // 
            // sendDate
            // 
            this.sendDate.BackColor = System.Drawing.Color.Transparent;
            this.sendDate.BorderRadius = 1;
            this.sendDate.Color = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendDate.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.sendDate.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Right;
            this.sendDate.DisabledColor = System.Drawing.Color.Gray;
            this.sendDate.DisplayWeekNumbers = false;
            this.sendDate.DPHeight = 0;
            this.sendDate.Enabled = false;
            this.sendDate.FillDatePicker = false;
            this.sendDate.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.sendDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendDate.Icon = ((System.Drawing.Image)(resources.GetObject("sendDate.Icon")));
            this.sendDate.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(212)))), ((int)(((byte)(169)))));
            this.sendDate.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Left;
            this.sendDate.Location = new System.Drawing.Point(401, 83);
            this.sendDate.MinimumSize = new System.Drawing.Size(301, 33);
            this.sendDate.Name = "sendDate";
            this.sendDate.Size = new System.Drawing.Size(301, 33);
            this.sendDate.TabIndex = 230;
            this.sendDate.TabStop = false;
            // 
            // bunifuButton17
            // 
            this.bunifuButton17.AllowToggling = false;
            this.bunifuButton17.AnimationSpeed = 200;
            this.bunifuButton17.AutoGenerateColors = false;
            this.bunifuButton17.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton17.BackColor1 = System.Drawing.Color.Goldenrod;
            this.bunifuButton17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton17.BackgroundImage")));
            this.bunifuButton17.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton17.ButtonText = "Get Data";
            this.bunifuButton17.ButtonTextMarginLeft = 0;
            this.bunifuButton17.ColorContrastOnClick = 45;
            this.bunifuButton17.ColorContrastOnHover = 45;
            this.bunifuButton17.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges42.BottomLeft = true;
            borderEdges42.BottomRight = true;
            borderEdges42.TopLeft = true;
            borderEdges42.TopRight = true;
            this.bunifuButton17.CustomizableEdges = borderEdges42;
            this.bunifuButton17.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton17.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton17.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton17.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton17.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton17.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton17.ForeColor = System.Drawing.Color.White;
            this.bunifuButton17.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton17.IconMarginLeft = 11;
            this.bunifuButton17.IconPadding = 10;
            this.bunifuButton17.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton17.IdleBorderColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton17.IdleBorderRadius = 3;
            this.bunifuButton17.IdleBorderThickness = 1;
            this.bunifuButton17.IdleFillColor = System.Drawing.Color.Goldenrod;
            this.bunifuButton17.IdleIconLeftImage = null;
            this.bunifuButton17.IdleIconRightImage = null;
            this.bunifuButton17.IndicateFocus = false;
            this.bunifuButton17.Location = new System.Drawing.Point(355, 334);
            this.bunifuButton17.Name = "bunifuButton17";
            stateProperties175.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties175.BorderRadius = 3;
            stateProperties175.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties175.BorderThickness = 1;
            stateProperties175.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties175.ForeColor = System.Drawing.Color.White;
            stateProperties175.IconLeftImage = null;
            stateProperties175.IconRightImage = null;
            this.bunifuButton17.onHoverState = stateProperties175;
            stateProperties176.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties176.BorderRadius = 3;
            stateProperties176.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties176.BorderThickness = 1;
            stateProperties176.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties176.ForeColor = System.Drawing.Color.White;
            stateProperties176.IconLeftImage = null;
            stateProperties176.IconRightImage = null;
            this.bunifuButton17.OnPressedState = stateProperties176;
            this.bunifuButton17.Size = new System.Drawing.Size(203, 33);
            this.bunifuButton17.TabIndex = 218;
            this.bunifuButton17.TabStop = false;
            this.bunifuButton17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton17.TextMarginLeft = 0;
            this.bunifuButton17.UseDefaultRadiusAndThickness = true;
            this.bunifuButton17.Click += new System.EventHandler(this.bunifuButton17_Click_1);
            // 
            // bunifuButton31
            // 
            this.bunifuButton31.AllowToggling = false;
            this.bunifuButton31.AnimationSpeed = 200;
            this.bunifuButton31.AutoGenerateColors = false;
            this.bunifuButton31.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton31.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton31.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton31.BackgroundImage")));
            this.bunifuButton31.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton31.ButtonText = "Send Report";
            this.bunifuButton31.ButtonTextMarginLeft = 0;
            this.bunifuButton31.ColorContrastOnClick = 45;
            this.bunifuButton31.ColorContrastOnHover = 45;
            this.bunifuButton31.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges43.BottomLeft = true;
            borderEdges43.BottomRight = true;
            borderEdges43.TopLeft = true;
            borderEdges43.TopRight = true;
            this.bunifuButton31.CustomizableEdges = borderEdges43;
            this.bunifuButton31.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton31.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton31.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton31.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton31.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton31.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton31.ForeColor = System.Drawing.Color.White;
            this.bunifuButton31.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton31.IconMarginLeft = 11;
            this.bunifuButton31.IconPadding = 10;
            this.bunifuButton31.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton31.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton31.IdleBorderRadius = 3;
            this.bunifuButton31.IdleBorderThickness = 1;
            this.bunifuButton31.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.bunifuButton31.IdleIconLeftImage = null;
            this.bunifuButton31.IdleIconRightImage = null;
            this.bunifuButton31.IndicateFocus = false;
            this.bunifuButton31.Location = new System.Drawing.Point(733, 269);
            this.bunifuButton31.Name = "bunifuButton31";
            stateProperties177.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties177.BorderRadius = 3;
            stateProperties177.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties177.BorderThickness = 1;
            stateProperties177.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties177.ForeColor = System.Drawing.Color.White;
            stateProperties177.IconLeftImage = null;
            stateProperties177.IconRightImage = null;
            this.bunifuButton31.onHoverState = stateProperties177;
            stateProperties178.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties178.BorderRadius = 3;
            stateProperties178.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties178.BorderThickness = 1;
            stateProperties178.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties178.ForeColor = System.Drawing.Color.White;
            stateProperties178.IconLeftImage = null;
            stateProperties178.IconRightImage = null;
            this.bunifuButton31.OnPressedState = stateProperties178;
            this.bunifuButton31.Size = new System.Drawing.Size(126, 33);
            this.bunifuButton31.TabIndex = 229;
            this.bunifuButton31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton31.TextMarginLeft = 0;
            this.bunifuButton31.UseDefaultRadiusAndThickness = true;
            this.bunifuButton31.Click += new System.EventHandler(this.bunifuButton31_Click_1);
            // 
            // bunifuButton33
            // 
            this.bunifuButton33.AllowToggling = false;
            this.bunifuButton33.AnimationSpeed = 200;
            this.bunifuButton33.AutoGenerateColors = false;
            this.bunifuButton33.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton33.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton33.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton33.BackgroundImage")));
            this.bunifuButton33.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton33.ButtonText = "New";
            this.bunifuButton33.ButtonTextMarginLeft = 0;
            this.bunifuButton33.ColorContrastOnClick = 45;
            this.bunifuButton33.ColorContrastOnHover = 45;
            this.bunifuButton33.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges44.BottomLeft = true;
            borderEdges44.BottomRight = true;
            borderEdges44.TopLeft = true;
            borderEdges44.TopRight = true;
            this.bunifuButton33.CustomizableEdges = borderEdges44;
            this.bunifuButton33.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton33.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton33.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton33.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton33.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton33.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton33.ForeColor = System.Drawing.Color.White;
            this.bunifuButton33.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton33.IconMarginLeft = 11;
            this.bunifuButton33.IconPadding = 10;
            this.bunifuButton33.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton33.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton33.IdleBorderRadius = 3;
            this.bunifuButton33.IdleBorderThickness = 1;
            this.bunifuButton33.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton33.IdleIconLeftImage = null;
            this.bunifuButton33.IdleIconRightImage = null;
            this.bunifuButton33.IndicateFocus = false;
            this.bunifuButton33.Location = new System.Drawing.Point(893, 269);
            this.bunifuButton33.Name = "bunifuButton33";
            stateProperties179.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties179.BorderRadius = 3;
            stateProperties179.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties179.BorderThickness = 1;
            stateProperties179.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties179.ForeColor = System.Drawing.Color.White;
            stateProperties179.IconLeftImage = null;
            stateProperties179.IconRightImage = null;
            this.bunifuButton33.onHoverState = stateProperties179;
            stateProperties180.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties180.BorderRadius = 3;
            stateProperties180.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties180.BorderThickness = 1;
            stateProperties180.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties180.ForeColor = System.Drawing.Color.White;
            stateProperties180.IconLeftImage = null;
            stateProperties180.IconRightImage = null;
            this.bunifuButton33.OnPressedState = stateProperties180;
            this.bunifuButton33.Size = new System.Drawing.Size(126, 33);
            this.bunifuButton33.TabIndex = 217;
            this.bunifuButton33.TabStop = false;
            this.bunifuButton33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton33.TextMarginLeft = 0;
            this.bunifuButton33.UseDefaultRadiusAndThickness = true;
            this.bunifuButton33.Click += new System.EventHandler(this.bunifuButton33_Click);
            // 
            // bunifuFormDock1
            // 
            this.bunifuFormDock1.AllowFormDragging = true;
            this.bunifuFormDock1.AllowFormDropShadow = true;
            this.bunifuFormDock1.AllowFormResizing = true;
            this.bunifuFormDock1.AllowHidingBottomRegion = true;
            this.bunifuFormDock1.AllowOpacityChangesWhileDragging = false;
            this.bunifuFormDock1.BorderOptions.BottomBorder.BorderColor = System.Drawing.Color.Silver;
            this.bunifuFormDock1.BorderOptions.BottomBorder.BorderThickness = 1;
            this.bunifuFormDock1.BorderOptions.BottomBorder.ShowBorder = true;
            this.bunifuFormDock1.BorderOptions.LeftBorder.BorderColor = System.Drawing.Color.Silver;
            this.bunifuFormDock1.BorderOptions.LeftBorder.BorderThickness = 1;
            this.bunifuFormDock1.BorderOptions.LeftBorder.ShowBorder = true;
            this.bunifuFormDock1.BorderOptions.RightBorder.BorderColor = System.Drawing.Color.Silver;
            this.bunifuFormDock1.BorderOptions.RightBorder.BorderThickness = 1;
            this.bunifuFormDock1.BorderOptions.RightBorder.ShowBorder = true;
            this.bunifuFormDock1.BorderOptions.TopBorder.BorderColor = System.Drawing.Color.Silver;
            this.bunifuFormDock1.BorderOptions.TopBorder.BorderThickness = 1;
            this.bunifuFormDock1.BorderOptions.TopBorder.ShowBorder = true;
            this.bunifuFormDock1.ContainerControl = this;
            this.bunifuFormDock1.DockingIndicatorsColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(215)))), ((int)(((byte)(233)))));
            this.bunifuFormDock1.DockingIndicatorsOpacity = 0.5D;
            this.bunifuFormDock1.DockingOptions.DockAll = true;
            this.bunifuFormDock1.DockingOptions.DockBottomLeft = true;
            this.bunifuFormDock1.DockingOptions.DockBottomRight = true;
            this.bunifuFormDock1.DockingOptions.DockFullScreen = true;
            this.bunifuFormDock1.DockingOptions.DockLeft = true;
            this.bunifuFormDock1.DockingOptions.DockRight = true;
            this.bunifuFormDock1.DockingOptions.DockTopLeft = true;
            this.bunifuFormDock1.DockingOptions.DockTopRight = true;
            this.bunifuFormDock1.FormDraggingOpacity = 0.9D;
            this.bunifuFormDock1.ParentForm = this;
            this.bunifuFormDock1.ShowCursorChanges = true;
            this.bunifuFormDock1.ShowDockingIndicators = false;
            this.bunifuFormDock1.TitleBarOptions.AllowFormDragging = true;
            this.bunifuFormDock1.TitleBarOptions.BunifuFormDock = this.bunifuFormDock1;
            this.bunifuFormDock1.TitleBarOptions.DoubleClickToExpandWindow = true;
            this.bunifuFormDock1.TitleBarOptions.TitleBarControl = null;
            this.bunifuFormDock1.TitleBarOptions.UseBackColorOnDockingIndicators = true;
            this.bunifuFormDock1.FormDragging += new System.EventHandler<Bunifu.UI.WinForms.BunifuFormDock.FormDraggingEventArgs>(this.bunifuFormDock1_FormDragging);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this.customerDataGridView;
            // 
            // timer2
            // 
            this.timer2.Interval = 15;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick_1);
            // 
            // timer3
            // 
            this.timer3.Interval = 1000;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // bunifuButton6
            // 
            this.bunifuButton6.AllowToggling = false;
            this.bunifuButton6.AnimationSpeed = 200;
            this.bunifuButton6.AutoGenerateColors = false;
            this.bunifuButton6.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton6.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton6.BackgroundImage")));
            this.bunifuButton6.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton6.ButtonText = "-";
            this.bunifuButton6.ButtonTextMarginLeft = 0;
            this.bunifuButton6.ColorContrastOnClick = 45;
            this.bunifuButton6.ColorContrastOnHover = 45;
            this.bunifuButton6.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges45.BottomLeft = true;
            borderEdges45.BottomRight = true;
            borderEdges45.TopLeft = true;
            borderEdges45.TopRight = true;
            this.bunifuButton6.CustomizableEdges = borderEdges45;
            this.bunifuButton6.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton6.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton6.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton6.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton6.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton6.Font = new System.Drawing.Font("Segoe UI Semibold", 15F, System.Drawing.FontStyle.Bold);
            this.bunifuButton6.ForeColor = System.Drawing.Color.White;
            this.bunifuButton6.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton6.IconMarginLeft = 11;
            this.bunifuButton6.IconPadding = 10;
            this.bunifuButton6.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton6.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton6.IdleBorderRadius = 3;
            this.bunifuButton6.IdleBorderThickness = 1;
            this.bunifuButton6.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(255)))));
            this.bunifuButton6.IdleIconLeftImage = null;
            this.bunifuButton6.IdleIconRightImage = null;
            this.bunifuButton6.IndicateFocus = false;
            this.bunifuButton6.Location = new System.Drawing.Point(1275, 7);
            this.bunifuButton6.Name = "bunifuButton6";
            stateProperties181.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties181.BorderRadius = 3;
            stateProperties181.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties181.BorderThickness = 1;
            stateProperties181.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties181.ForeColor = System.Drawing.Color.White;
            stateProperties181.IconLeftImage = null;
            stateProperties181.IconRightImage = null;
            this.bunifuButton6.onHoverState = stateProperties181;
            stateProperties182.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties182.BorderRadius = 3;
            stateProperties182.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties182.BorderThickness = 1;
            stateProperties182.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties182.ForeColor = System.Drawing.Color.White;
            stateProperties182.IconLeftImage = null;
            stateProperties182.IconRightImage = null;
            this.bunifuButton6.OnPressedState = stateProperties182;
            this.bunifuButton6.Size = new System.Drawing.Size(28, 24);
            this.bunifuButton6.TabIndex = 3;
            this.bunifuButton6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton6.TextMarginLeft = 0;
            this.bunifuButton6.UseDefaultRadiusAndThickness = true;
            this.bunifuButton6.Click += new System.EventHandler(this.bunifuButton6_Click);
            // 
            // bunifuButton5
            // 
            this.bunifuButton5.AllowToggling = false;
            this.bunifuButton5.AnimationSpeed = 200;
            this.bunifuButton5.AutoGenerateColors = false;
            this.bunifuButton5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton5.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.bunifuButton5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton5.BackgroundImage")));
            this.bunifuButton5.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton5.ButtonText = "X";
            this.bunifuButton5.ButtonTextMarginLeft = 0;
            this.bunifuButton5.ColorContrastOnClick = 45;
            this.bunifuButton5.ColorContrastOnHover = 45;
            this.bunifuButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges46.BottomLeft = true;
            borderEdges46.BottomRight = true;
            borderEdges46.TopLeft = true;
            borderEdges46.TopRight = true;
            this.bunifuButton5.CustomizableEdges = borderEdges46;
            this.bunifuButton5.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton5.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton5.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton5.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton5.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton5.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.bunifuButton5.ForeColor = System.Drawing.Color.White;
            this.bunifuButton5.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton5.IconMarginLeft = 11;
            this.bunifuButton5.IconPadding = 10;
            this.bunifuButton5.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton5.IdleBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton5.IdleBorderRadius = 3;
            this.bunifuButton5.IdleBorderThickness = 1;
            this.bunifuButton5.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.bunifuButton5.IdleIconLeftImage = null;
            this.bunifuButton5.IdleIconRightImage = null;
            this.bunifuButton5.IndicateFocus = false;
            this.bunifuButton5.Location = new System.Drawing.Point(1309, 7);
            this.bunifuButton5.Name = "bunifuButton5";
            stateProperties183.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties183.BorderRadius = 3;
            stateProperties183.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties183.BorderThickness = 1;
            stateProperties183.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties183.ForeColor = System.Drawing.Color.White;
            stateProperties183.IconLeftImage = null;
            stateProperties183.IconRightImage = null;
            this.bunifuButton5.onHoverState = stateProperties183;
            stateProperties184.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties184.BorderRadius = 3;
            stateProperties184.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties184.BorderThickness = 1;
            stateProperties184.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties184.ForeColor = System.Drawing.Color.White;
            stateProperties184.IconLeftImage = null;
            stateProperties184.IconRightImage = null;
            this.bunifuButton5.OnPressedState = stateProperties184;
            this.bunifuButton5.Size = new System.Drawing.Size(28, 24);
            this.bunifuButton5.TabIndex = 2;
            this.bunifuButton5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton5.TextMarginLeft = 0;
            this.bunifuButton5.UseDefaultRadiusAndThickness = true;
            this.bunifuButton5.Click += new System.EventHandler(this.bunifuButton5_Click);
            // 
            // Dashboard
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.ClientSize = new System.Drawing.Size(1350, 802);
            this.Controls.Add(this.pagesForm);
            this.Controls.Add(this.bunifuButton6);
            this.Controls.Add(this.bunifuButton5);
            this.Controls.Add(this.bunifuShadowPanel1);
            this.ForeColor = System.Drawing.Color.Transparent;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(1350, 802);
            this.MinimumSize = new System.Drawing.Size(1350, 802);
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Dashboard_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Dashboard_KeyDown);
            this.Resize += new System.EventHandler(this.Dashboard_Resize);
            this.bunifuShadowPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.indicator)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox2)).EndInit();
            this.pagesForm.ResumeLayout(false);
            this.dashbaordPage.ResumeLayout(false);
            this.dashbaordPage.PerformLayout();
            this.bunifuCards11.ResumeLayout(false);
            this.bunifuCards11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.bunifuCards12.ResumeLayout(false);
            this.bunifuCards12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.bunifuCards8.ResumeLayout(false);
            this.bunifuCards8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.bunifuCards7.ResumeLayout(false);
            this.bunifuCards7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.bunifuCards6.ResumeLayout(false);
            this.bunifuCards6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.bunifuCards5.ResumeLayout(false);
            this.bunifuCards5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.bunifuCards4.ResumeLayout(false);
            this.bunifuCards4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.bunifuCards3.ResumeLayout(false);
            this.bunifuCards3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.bunifuCards2.ResumeLayout(false);
            this.bunifuCards2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.bunifuCards1.ResumeLayout(false);
            this.bunifuCards1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.customerMasterPage.ResumeLayout(false);
            this.customerMasterPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customerDataGridView)).EndInit();
            this.sellItemPage.ResumeLayout(false);
            this.sellItemPage.PerformLayout();
            this.panelAddDetail.ResumeLayout(false);
            this.panelAddDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataTableGirdView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportImageArea)).EndInit();
            this.reportPage.ResumeLayout(false);
            this.reportPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allReportDataGrid)).EndInit();
            this.tokenPage.ResumeLayout(false);
            this.tokenPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tokenListDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tokenSampleDataGrid)).EndInit();
            this.testPage.ResumeLayout(false);
            this.testPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testDatagridView)).EndInit();
            this.expenses.ResumeLayout(false);
            this.expenses.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expensesDataGridView)).EndInit();
            this.backup.ResumeLayout(false);
            this.backup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox1)).EndInit();
            this.sendReport.ResumeLayout(false);
            this.sendReport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sendreportDataTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel bunifuShadowPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.UI.WinForms.BunifuPages pagesForm;
        private System.Windows.Forms.TabPage dashbaordPage;
        private System.Windows.Forms.TabPage customerMasterPage;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton1;
        private System.Windows.Forms.PictureBox indicator;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton4;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton3;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton2;
        private Bunifu.UI.WinForms.BunifuFormDock bunifuFormDock1;
        private System.Windows.Forms.TabPage sellItemPage;
        private System.Windows.Forms.TabPage reportPage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton6;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton5;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel2;
        private Bunifu.Framework.UI.BunifuMetroTextbox seriesNoTexBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel1;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel12;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel10;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel9;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel8;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel7;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel6;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel4;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel3;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel13;
        private Bunifu.Framework.UI.BunifuMetroTextbox customerAddressTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel11;
        private Bunifu.Framework.UI.BunifuMetroTextbox customerMobileTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel14;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton reportNewButton;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton reportPrintButton;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton reportSaveButton;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton reportModifyButton;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton adddetailButton;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton reportSelectButton;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton reportScanImageButton;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel15;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel16;
        private Bunifu.Framework.UI.BunifuMetroTextbox custMastMobiletextbox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel17;
        private Bunifu.Framework.UI.BunifuMetroTextbox custMastAddresstextbox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel20;
        private Bunifu.Framework.UI.BunifuMetroTextbox custMastNametextbox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel21;
        private Bunifu.Framework.UI.BunifuMetroTextbox custMastFirmNametextbox;
        private System.Windows.Forms.Timer timer1;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.DateTimePicker currentTimebox;
        private System.Windows.Forms.ComboBox customerTextBox;
        private System.Windows.Forms.ComboBox sampleTextBox;
        private Bunifu.Framework.UI.BunifuMetroTextbox bunifuMetroTextbox4;
        private Bunifu.Framework.UI.BunifuMetroTextbox bunifuLabel18;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton reportDetailSaveButton;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton reportDetailPrintButton;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton10;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton9;
        private Bunifu.Framework.UI.BunifuCustomDataGrid customerDataGridView;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuCustomDataGrid reportDataTableGirdView;
        private Bunifu.UI.WinForms.BunifuDatePicker reportDatePicker;
        private System.Windows.Forms.PictureBox reportImageArea;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox goldPurityTextBox;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox goldKaratTextBox;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox originalWeightTextBox;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox tounchWeightTextBox;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox customerRate;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox custMastRatetextbox;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Panel panelAddDetail;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel45;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel46;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailGalliumTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel47;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailRehniumTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel48;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailOsmiumTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel49;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailManganeseTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel50;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailTungustanTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel51;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailRutheniumTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel52;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailTitaniumTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel53;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailIndiumTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel54;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailNickelTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel55;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailBismuthTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel56;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailChromiumTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel57;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailLeadTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel58;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailTinTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel59;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailIronTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel60;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailAntimonyTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel61;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailPalladiumTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel62;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailPlatinumTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel63;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailCobaltTextBox;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailRhodiumTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel64;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailIridiumTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel65;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailCadmiumTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel66;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailZincTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel67;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailCopperTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel68;
        public Bunifu.Framework.UI.BunifuMetroTextbox reportDetailSilverTextBox;
        private Bunifu.UI.WinForms.BunifuLabel serialNoTextBox;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox goldFineTextBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel19;
        private Bunifu.UI.WinForms.BunifuLabel totalPercentageLabel;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel23;
        private System.Windows.Forms.Timer timer3;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton startStopTimerButton;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton startButton;
        private Bunifu.Framework.UI.BunifuCards bunifuCards4;
        private Bunifu.Framework.UI.BunifuCards bunifuCards3;
        private Bunifu.Framework.UI.BunifuCards bunifuCards2;
        private Bunifu.Framework.UI.BunifuCards bunifuCards1;
        private Bunifu.Framework.UI.BunifuDatepicker bunifuDatepicker3;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel22;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel26;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel25;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel24;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Bunifu.UI.WinForms.BunifuLabel savingText;
        private Bunifu.UI.WinForms.BunifuLabel investAmontText;
        private Bunifu.UI.WinForms.BunifuLabel saleAmountText;
        private Bunifu.UI.WinForms.BunifuLabel totalReportText;
        private System.Windows.Forms.ComboBox fullcustomerReportBox;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel27;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel29;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel28;
        private Bunifu.UI.WinForms.BunifuDatePicker fullReportStartDatepicker;
        private Bunifu.UI.WinForms.BunifuDatePicker fullReportEndDatePicker;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel30;
        private Bunifu.Framework.UI.BunifuMetroTextbox fullfilterMobileNo;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton12;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton11;
        private System.Windows.Forms.TabPage testPage;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private ADGV.AdvancedDataGridView allReportDataGrid;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton13;
        private System.Windows.Forms.TabPage tokenPage;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel40;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox tokenTotal;
        private Bunifu.Framework.UI.BunifuCustomDataGrid tokenSampleDataGrid;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton tokenAddButton;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton16;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton18;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton tokenSampleDeletebutton;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox tokenSample;
        private System.Windows.Forms.DateTimePicker tokenDateTimePicker;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel31;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel32;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel33;
        private Bunifu.Framework.UI.BunifuMetroTextbox tokenAddress;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel34;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel35;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel36;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel37;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel38;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel39;
        private Bunifu.Framework.UI.BunifuMetroTextbox tokenSeriesno;
        private Bunifu.Framework.UI.BunifuMetroTextbox tokenMobile;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox tokenRate;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox tokenWeightText;
        private Bunifu.UI.WinForms.BunifuDatePicker tokenDatePicker;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuCustomDataGrid tokenListDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column38;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column40;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column39;
        private Bunifu.Framework.UI.BunifuCustomDataGrid testDatagridView;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox testgoldFine;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel41;
        private Bunifu.UI.WinForms.BunifuLabel testId;
        private System.Windows.Forms.ComboBox testSmaplecomboBox;
        private System.Windows.Forms.ComboBox testCustcombo;
        private System.Windows.Forms.DateTimePicker testTime;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel43;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel70;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel71;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel72;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel73;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel74;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel75;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel77;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel78;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel79;
        private Bunifu.Framework.UI.BunifuMetroTextbox testSeries;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel80;
        private System.Windows.Forms.Label label11;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox testAmount;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox testtounchweight;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox testorgweight;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox testKarat;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox testPurity;
        private Bunifu.UI.WinForms.BunifuDatePicker testDate;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton22;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton23;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton24;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton14;
        private System.Windows.Forms.TabPage expenses;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton15;
        private System.Windows.Forms.TabPage backup;
        private System.Windows.Forms.Label label7;
        private Bunifu.Framework.UI.BunifuCustomDataGrid expensesDataGridView;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox expensesAmount;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel82;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel85;
        private Bunifu.Framework.UI.BunifuMetroTextbox expenseRemark;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton20;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton25;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton26;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton27;
        private System.Windows.Forms.DateTimePicker expenseTime;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel81;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel84;
        private Bunifu.UI.WinForms.BunifuDatePicker expenseDate;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel86;
        private Bunifu.Framework.UI.BunifuMetroTextbox expenseId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private Bunifu.Framework.UI.BunifuCards bunifuCards5;
        private System.Windows.Forms.PictureBox pictureBox7;
        private Bunifu.UI.WinForms.BunifuLabel testingCountText;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel87;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton21;
        private Bunifu.UI.WinForms.BunifuLabel tokenSeries;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel44;
        private System.Windows.Forms.ComboBox tokecustcomboBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column76;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column41;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column42;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column46;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column47;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column45;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column44;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column43;
        private System.Windows.Forms.Label label12;
        private Bunifu.Framework.UI.BunifuMetroTextbox backUppathtext;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton19;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton28;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton29;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton30;
        private System.Windows.Forms.Label rowCountTotal;
        private System.Windows.Forms.Label totalrow;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton getDashboardDataButton;
        private Bunifu.Framework.UI.BunifuCards bunifuCards6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private Bunifu.UI.WinForms.BunifuLabel totaltestingAmountText;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel69;
        private Bunifu.Framework.UI.BunifuCards bunifuCards7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private Bunifu.UI.WinForms.BunifuLabel totalUnpaidReportText;
        private Bunifu.UI.WinForms.BunifuLabel unpaidAmount;
        private Bunifu.Framework.UI.BunifuCards bunifuCards8;
        private System.Windows.Forms.PictureBox pictureBox8;
        private Bunifu.UI.WinForms.BunifuLabel totalUnpaidTesting;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel83;
        private Bunifu.Framework.UI.BunifuCards bunifuCards11;
        private System.Windows.Forms.PictureBox pictureBox11;
        private Bunifu.UI.WinForms.BunifuLabel unpaidTestingAmount;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel93;
        private Bunifu.Framework.UI.BunifuCards bunifuCards12;
        private System.Windows.Forms.PictureBox pictureBox12;
        private Bunifu.UI.WinForms.BunifuLabel totalUnpaidReportAmount;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel95;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel42;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel76;
        private Bunifu.UI.WinForms.BunifuDatePicker dashboardEndDatePicker;
        private Bunifu.UI.WinForms.BunifuDatePicker dashboardStartDatePicker;
        private Bunifu.UI.WinForms.BunifuPictureBox bunifuPictureBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private Bunifu.UI.WinForms.BunifuPictureBox bunifuPictureBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel88;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel89;
        private Bunifu.UI.WinForms.BunifuDropdown testingDropdown;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column35;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column78;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column33;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column34;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column36;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column79;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton8;
        private System.Windows.Forms.TabPage sendReport;
        private Bunifu.Framework.UI.BunifuCustomDataGrid sendreportDataTable;
        private Bunifu.Framework.UI.BunifuMetroTextbox sendSample;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel104;
        private Bunifu.Framework.UI.BunifuMetroTextbox sendCustomer;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel90;
        private Bunifu.UI.WinForms.BunifuLabel sendSerial;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel92;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel94;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel96;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel97;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel98;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel99;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel101;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel102;
        private Bunifu.Framework.UI.BunifuMetroTextbox sendseries;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel103;
        private System.Windows.Forms.Label label16;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox sendFine;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox sendAmount;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox sendTweight;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox sendOweight;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox sendKarat;
        private Bunifu.UI.WinForms.BunifuDatePicker sendDate;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton17;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton31;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton33;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox sendGoldPurity;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel100;
        private Bunifu.UI.WinForms.BunifuDatePicker selectDate;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column77;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column74;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column75;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column48;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column49;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column50;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column51;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column52;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column53;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column54;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column55;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column56;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column57;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column58;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column59;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column60;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column61;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column62;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column63;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column64;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column65;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column66;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column67;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column68;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column69;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column70;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column71;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column72;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column73;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column202;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn62;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn64;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn65;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn66;
        private Bunifu.UI.WinForms.BunifuLabel sendMobile;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel91;
    }
}