﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace GoldRaven
{
    public partial class Toaster : Form
    {
        public Toaster()
        {
            InitializeComponent();
            TopMost = true;
           
          
        }
        
        public enum enmAction
        {
            wait,
            start,
            close
        }

        public enum enmType
        {
            Success,
            Warning,
            Error,
            Info
        }
        private Toaster.enmAction action;

        private int x, y;
        public void showAlert(string msg, enmType type)
        {
            this.Opacity = 0.0;
            this.StartPosition = FormStartPosition.Manual;
            string fname;

            for (int i = 1; i < 10; i++)
            {
                fname = "alert" + i.ToString();
                Toaster frm = (Toaster)Application.OpenForms[fname];

                if (frm == null)
                {
                    this.Name = fname;
                    this.x = Screen.PrimaryScreen.WorkingArea.Width - this.Width + 15;
                    this.y = Screen.PrimaryScreen.WorkingArea.Height - this.Height * i - 5 * i;
                    this.Location = new Point(this.x, this.y);
                    break;

                }

            }
            this.x = Screen.PrimaryScreen.WorkingArea.Width - base.Width - 5;

            switch (type)
            {
                case enmType.Success:
                    this.pictureBox1.Image = Properties.Resources.icons8_ok_45px_2;
                    this.BackColor = Color.SeaGreen;
                    break;
                case enmType.Error:
                    this.pictureBox1.Image = Properties.Resources.icons8_sad_cloud_45px;
                    this.BackColor = Color.DarkRed;
                    break;
                case enmType.Info:
                    this.pictureBox1.Image = Properties.Resources.icons8_info_45px;
                    this.BackColor = Color.RoyalBlue;
                    break;
                case enmType.Warning:
                    this.pictureBox1.Image = Properties.Resources.icons8_warning_shield_45px;
                    this.BackColor = Color.DarkOrange;
                    break;
            }


            this.toastMessagelbl.Text = msg;

            this.Show();
            this.action = enmAction.start;
            this.toastertimer.Interval = 1;
            this.toastertimer.Start();
        }
        private void Toaster_Load(object sender, EventArgs e)
        {
           
        }

        private void toastertimer_Tick(object sender, EventArgs e)
        {
            switch (this.action)
            {
                case Toaster.enmAction.wait:
                    toastertimer.Interval = 2000;
                    action = enmAction.close;
                    break;
                case Toaster.enmAction.start:
                    this.toastertimer.Interval = 1;
                    this.Opacity += 0.1;
                    if (this.x < this.Location.X)
                    {
                        this.Left--;
                    }
                    else
                    {
                        if (this.Opacity == 1.0)
                        {
                            action = Toaster.enmAction.wait;
                        }
                    }
                    break;
                case Toaster.enmAction.close:
                  toastertimer.Interval = 1;
                    this.Opacity = 0.0;

                       this.Left -= 3;
                    if (base.Opacity == 0.0)
                    {
                        base.Close();


                    }
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void toastMessagelbl_Click(object sender, EventArgs e)
        {

        }
    }
}
