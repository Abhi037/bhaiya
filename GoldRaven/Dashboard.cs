﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Globalization;
using System.IO;
using Utilities;
using WpfScanner.Services;
using WIA;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using System.Drawing.Printing;
using GoldRaven.Properties;


namespace GoldRaven
{
    public partial class Dashboard : Form
    {

        public Dashboard()

        {
            
            InitializeComponent();
            //Control.SetIntial(this);
            bunifuFormDock1.SubscribeControlToDragEvents(panel1);
            bunifuFormDock1.SubscribeControlToDragEvents(panel2);
            // var deviceManager = new DeviceManager();
           // bunifuFormDock1.WindowState = Bunifu.UI.WinForms.BunifuFormDock.FormWindowStates.Minimized;
            FillCustomerdata();
            FillCustomertOKENdata();
            FillCustomerTestingdata();
            FillSampledata();
            FillTokenSampledata();
            FillTestingSampledata();
            getReportlastIdSeriesId();
            timer3.Start();
            this.dashBoardLoad();
            this.setDefaultReportValueFunc();



            // this.tokenCustomerBox.SelectedIndex = -1;
        }
        private static readonly string filePath = Environment.CurrentDirectory;
        public bool isMinimized = false;
        public ReportModel reportDetail;
        private customerMaster customerMaster;
        private PrintFullReport.FullReprtForm fullReprt_Form;
        private PrintFolder.PrintRdlc_Form printRdlc_Form;
        private PrintTestingReport.PrintTestingForm printTestingRdlc_Form;
        private PrintTokenSlip.Print_tokenForm print_tokenForm;
        TextInfo info = new CultureInfo("en-US", false).TextInfo;

        int reportDefaultRate = 50;
        int testingDefaultRate = 30;
        int defaultMobileNo = 1111111111;
        string defaultAddress = "India";
        string defaultFirmName = "Self";
        private string modifyId = null;

        //private savePrint savePrint;
        private bool isCollapsed = true;
        //private CustomerDeleteDialog CustomerDeleteDialog;


        private string latestReportId;
        private int sampleItemlastId;
        private string tokenlatestReportId;
        private string latestTestingId;
        private int genratedCustId;
        private int genratedTokenCustId;
        private string getCustIdToModify;
        private int itemId;
        private string lastExensesId;
        private string modifyTestingId;
        public bool allowDate;
        public bool startButtonView = false;


        class customer
        {
            public int date { get; set; }
            public string time { get; set; }
            public int customerId { get; set; }
            public int weight { get; set; }
            public int sSample { get; set; }


        }

     

        private void updatePaid(string paidValue, string Id)
        {
            if (paidValue != "" && Id != "")
            {
                Database databaseObject = new Database();
                string query = "UPDATE reportTable set payment = @payment where id = @id";

                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

                try
                {
                    databaseObject.OpenConnection();
                    command.Parameters.Add(new SQLiteParameter("@id", Id));
                    command.Parameters.Add(new SQLiteParameter("@payment", paidValue));

                    SQLiteDataReader result = command.ExecuteReader();


                    while (result.Read())
                    {


                    }
                    databaseObject.CloseConnection();


                    this.Alert("Report Record Updated!", Toaster.enmType.Success);
                    //    this.getReportTableDatagrid();


                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }

            }
            else
            {

                this.Alert("Please Select The Report Cell Data First", Toaster.enmType.Warning);

            }

        }


        private void createReportDetailId(string Id)
        {
            Database databaseObject = new Database();
            string query = "INSERT INTO reportDetailTable (Silver,Cobalt,Lead,Ruthenium,Copper,Platinum,Chromium,Tungustan,Zinc,Palladium,Bismuth,Manganese,Cadmium,Antimony,Nickel,Osmium,Iridium,Iron,Indium,Rehnium,Rhodium,Tin,Titanium,Gallium,reportId)" +
                "VALUES('" + this.reportDetailSilverTextBox.Text + "','" + this.reportDetailCobaltTextBox.Text + "','" + this.reportDetailLeadTextBox.Text + "','" + this.reportDetailRutheniumTextBox.Text + "','" + this.reportDetailCopperTextBox.Text + "','" + this.reportDetailPlatinumTextBox.Text + "','" + this.reportDetailChromiumTextBox.Text + "','" + this.reportDetailTungustanTextBox.Text + "','" + this.reportDetailZincTextBox.Text + "','" + this.reportDetailPalladiumTextBox.Text + "','" + this.reportDetailBismuthTextBox.Text + "','" + this.reportDetailManganeseTextBox.Text + "','" + this.reportDetailCadmiumTextBox.Text + "','" + this.reportDetailAntimonyTextBox.Text + "','" + this.reportDetailNickelTextBox.Text + "','" + this.reportDetailOsmiumTextBox.Text + "','" + this.reportDetailIridiumTextBox.Text + "','" + this.reportDetailIronTextBox.Text + "','" + this.reportDetailIndiumTextBox.Text + "','" + this.reportDetailRehniumTextBox.Text + "','" + this.reportDetailRhodiumTextBox.Text + "','" + this.reportDetailTinTextBox.Text + "','" + this.reportDetailTitaniumTextBox.Text + "','" + this.reportDetailGalliumTextBox.Text + "' ,@reportId)";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);


            try
            {

                databaseObject.OpenConnection();
                command.Parameters.Add(new SQLiteParameter("@reportId", Id));
                SQLiteDataReader result = command.ExecuteReader();
                
                this.Alert("Report Data Added Successfully!", Toaster.enmType.Success);
                while (result.Read())
                {


                }


                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }

        private void clearCustomerReport()
        {
            this.custMastNametextbox.Text = "";
            this.custMastMobiletextbox.Text = this.defaultMobileNo.ToString();
            this.custMastRatetextbox.Text = this.reportDefaultRate.ToString();
            this.custMastAddresstextbox.Text = this.defaultAddress.ToString();
            this.custMastFirmNametextbox.Text = this.defaultFirmName.ToString();
            
        }

        private void clearExpensesReport()
        {
            this.expenseRemark.Text = "";
            this.expensesAmount.Text = "";
            this.getExpensestlastId();


        }

        private void clearTokenSample()
        {
            this.tokenSample.ResetText();
            this.tokenWeightText.Text = "";
            this.getSampleTableItemslastId();

        }


        private void clearTestingControl()
        {
            this.serialNoTextBox.Text = "";
            this.testSeries.Text = "";
            this.testCustcombo.ResetText();
            //this.testCustcombo.Text = "";
            this.testPurity.Text = "";
            this.testKarat.Text = "";
            this.testSmaplecomboBox.ResetText();
            //this.testSmaplecomboBox.Text = "";
            this.testorgweight.Text = "";
            this.testtounchweight.Text = "";
            this.testAmount.Text = this.testingDefaultRate.ToString();
            this.testgoldFine.Text = "";
            //this.testCustcombo.SelectedValue = 0;
            this.gettestinglastId();
            this.testingDataGridDbData();
            this.FillTestingSampledata();
            this.testCustcombo.Focus();


        }
        private void clearReportControl()
        {
            this.serialNoTextBox.Text = "";
            this.seriesNoTexBox.Text = "";
            this.customerTextBox.ResetText();
            this.goldPurityTextBox.Text = "";
            this.goldKaratTextBox.Text = "";
            this.sampleTextBox.ResetText();
           // this.iconshowareabox.Image = Properties.Resources.add_image;
            this.originalWeightTextBox.Text = "";
            this.tounchWeightTextBox.Text = "";
            this.customerAddressTextBox.Text = this.defaultAddress.ToString();
            this.customerMobileTextBox.Text = this.defaultMobileNo.ToString();
            this.customerRate.Text = this.reportDefaultRate.ToString();
            this.goldFineTextBox.Text = "";
            this.totalPercentageLabel.Text = "";
            this.genratedCustId = 0;
            this.reportImageArea.Image = Properties.Resources.add_image;
            this.reportDetailSilverTextBox.Text = "0.0";
            this.reportDetailCobaltTextBox.Text = "0.0";
            this.reportDetailLeadTextBox.Text = "0.0";
            this.reportDetailRutheniumTextBox.Text = "0.0";
            this.reportDetailCopperTextBox.Text = "0.0";
            this.reportDetailPlatinumTextBox.Text = "0.0";
            this.reportDetailChromiumTextBox.Text = "0.0";
            this.reportDetailTungustanTextBox.Text = "0.0";
            this.reportDetailZincTextBox.Text = "0.0";
            this.reportDetailPalladiumTextBox.Text = "0.0";
            this.reportDetailBismuthTextBox.Text = "0.0";
            this.reportDetailManganeseTextBox.Text = "0.0";
            this.reportDetailCadmiumTextBox.Text = "0.0";
            this.reportDetailAntimonyTextBox.Text = "0.0";
            this.reportDetailNickelTextBox.Text = "0.0";
            this.reportDetailOsmiumTextBox.Text = "0.0";
            this.reportDetailIridiumTextBox.Text = "0.0";
            this.reportDetailIronTextBox.Text = "0.0";
            this.reportDetailIndiumTextBox.Text = "0.0";
            this.reportDetailRehniumTextBox.Text = "0.0";
            this.reportDetailRhodiumTextBox.Text = "0.0";
            this.reportDetailTinTextBox.Text = "0.0";
            this.reportDetailTitaniumTextBox.Text = "0.0";
            this.reportDetailGalliumTextBox.Text = "0.0";
           // this.firstIconText.Text = "";
            //this.lastIconText.Text = "";
            

            this.getReportlastIdSeriesId();
            this.getReportTableDatagrid();
            this.FillCustomerdata();
            this.FillSampledata();
            this.customerTextBox.Focus();



        }



        private void FillCustomerdata()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM customerTable";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            DataRow dr;
            try
            {
                databaseObject.OpenConnection();
                //  SQLiteDataReader result = command.ExecuteReader();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                //   while (result.Read())
                //   {
                dr = dt.NewRow();
                dr.ItemArray = new object[] { 0, "Select Customer" };
                dt.Rows.InsertAt(dr, 0);
                dt.Columns.Add("FullName", typeof(string), "customerName + ' ' + custMobile + ' ' + customerAddress");
                this.customerTextBox.ValueMember = "id";

                this.customerTextBox.DisplayMember = "FullName";
                this.customerTextBox.DataSource = dt;

                

                //    TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                //   this.customerTextBox.Items.Add(textInfo.ToTitleCase(result["customerName"] + result["id"] .ToString())); 

                // /  }



                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }

        private void FillCustomertOKENdata()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM customerTable";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            DataRow dr;
            try
            {
                databaseObject.OpenConnection();
                //  SQLiteDataReader result = command.ExecuteReader();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                //   while (result.Read())
                //   {
               dr = dt.NewRow();
               dr.ItemArray = new object[] { 0, "Select Customer" };
               dt.Rows.InsertAt(dr, 0);
                dt.Columns.Add("FullName", typeof(string), "customerName + ' ' + custMobile + ' ' + customerAddress");
          

                this.tokecustcomboBox2.ValueMember = "id";
                this.tokecustcomboBox2.DisplayMember = "FullName";
                this.tokecustcomboBox2.DataSource = dt;


               

                //    TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                //   this.customerTextBox.Items.Add(textInfo.ToTitleCase(result["customerName"] + result["id"] .ToString())); 

                // /  }



                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }

        private void FillCustomerTestingdata()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM customerTable";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            DataRow dr;
            try
            {
                databaseObject.OpenConnection();
                //  SQLiteDataReader result = command.ExecuteReader();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                //   while (result.Read())
                //   {
                dr = dt.NewRow();
                dr.ItemArray = new object[] { 0, "Select Customer" };
                dt.Rows.InsertAt(dr, 0);
                dt.Columns.Add("FullName", typeof(string), "customerName + ' ' + custMobile + ' ' + customerAddress");
            


                this.testCustcombo.ValueMember = "id";
                this.testCustcombo.DisplayMember = "FullName";
                this.testCustcombo.DataSource = dt;

                //    TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                //   this.customerTextBox.Items.Add(textInfo.ToTitleCase(result["customerName"] + result["id"] .ToString())); 

                // /  }



                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }


        private void createCustomerData()
        {
            if (this.customerTextBox.Text.ToString() != "" && this.customerAddressTextBox.Text.ToString() != "" && this.customerAddressTextBox.Text.ToString() != "" && this.customerRate.Text.ToString() != "")
            {
                Database databaseObject = new Database();

                string query = "INSERT INTO customerTable (customerName,customerAddress,custMobile,Rate ) VALUES('" + info.ToTitleCase(this.customerTextBox.Text.ToLower()) + "','" + info.ToTitleCase(this.customerAddressTextBox.Text.ToLower()) + "','" + this.customerMobileTextBox.Text.ToString() + "','" + this.customerRate.Text.ToString() + "')";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
                try
                {
                    databaseObject.OpenConnection();
                    SQLiteDataReader result = command.ExecuteReader();



                    this.getCustomerlastdataId();
                    databaseObject.CloseConnection();

                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }
            else
            {
                this.Alert("Fill The Customer Data First!", Toaster.enmType.Info);
                //MessageBox.Show("Please Fill The Customer Data First");
            }
        }

        private void getSelectedCustomerData()
        {
            {

                Database databaseObject = new Database();
                string query = "SELECT customerName,customerAddress,custMobile,Rate FROM customerTable WHERE id=" + this.customerTextBox.SelectedValue + "";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);



                try
                {
                    databaseObject.OpenConnection();
                    SQLiteDataReader result = command.ExecuteReader();
                    if (result.HasRows)
                    {
                        while (result.Read())
                        {

                            this.customerMobileTextBox.Text = result["custMobile"].ToString();
                            this.customerAddressTextBox.Text = result["customerAddress"].ToString();
                            this.customerRate.Text = result["Rate"].ToString();

                        }
                    }

                    databaseObject.CloseConnection();
                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }

        }

        private void getCustomerlastdataId()
        {
            if (this.customerTextBox.Text.ToString() != "" && this.customerAddressTextBox.Text.ToString() != "" && this.customerAddressTextBox.Text.ToString() != "" && this.customerRate.Text.ToString() != "")
            {
                Database databaseObject = new Database();
                string query = "SELECT * FROM customerTable WHERE id = (SELECT MAX(id) FROM customerTable)";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
                try
                {
                    databaseObject.OpenConnection();
                    SQLiteDataReader result = command.ExecuteReader();

                    while (result.Read())
                    {
                        int lastId = Int32.Parse(result["id"].ToString());

                        this.genratedCustId = lastId;
                    }


                    databaseObject.CloseConnection();
                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }
            // else
            //{
            //  this.Alert("Fill The Customer Data First!", Toaster.enmType.Info);
            // MessageBox.Show("Please Fill The Customer Data First");
            //}
        }
        private void FillSampledata()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM reportTable";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataReader result = command.ExecuteReader();
                IList<string> listName = new List<string>();
                while (result.Read())
                {
                    listName.Add(result["sample"].ToString());


                }
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                listName = listName.Distinct().ToList();
                this.sampleTextBox.DataSource = listName;



                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }

        private void FillTokenSampledata()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM sampleTable";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataReader result = command.ExecuteReader();
                IList<string> listName = new List<string>();
                while (result.Read())
                {
                    listName.Add(result["sSample"].ToString());


                }
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                listName = listName.Distinct().ToList();

                this.tokenSample.DataSource = listName;



                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }

        private void FillTestingSampledata()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM testingTable";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataReader result = command.ExecuteReader();
                IList<string> listName = new List<string>();
                while (result.Read())
                {
                    listName.Add(result["sample"].ToString());


                }
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                listName = listName.Distinct().ToList();
                this.testSmaplecomboBox.DataSource = listName;


                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }


        private void cutomerMaster_Click(object sender, EventArgs e)
        {
            customerMaster = new customerMaster();
            customerMaster.Show();
        }
        private newSell newSell;
        private void newSellButton_Click(object sender, EventArgs e)
        {
            newSell = new newSell();
            newSell.Show();
        }
        private reportForm reportForm;
        private void reportButton_Click(object sender, EventArgs e)
        {
            reportForm = new reportForm();
            reportForm.Show();
        }

        private void bunifuLabel1_Click(object sender, EventArgs e)
        {

        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
         

          
        }

        private void bunifuCustomLabel1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton1_Click(object sender, EventArgs e)
        {
            this.SuspendLayout();
            indicator.Location = new Point(3, 184);
            this.ResumeLayout();
            pagesForm.SetPage("Dashboard");
            this.bunifuButton31_Click(sender, e);

        }

        private void getCustomerDataFormdb()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM customerTable ORDER BY id DESC";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                this.customerDataGridView.DataSource = dt;

                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }

        private void getExpensesDataFormdb()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM expensesTable WHERE expensesTable.date = '" + this.expenseDate.Value.Date.ToString("yyyy-MM-dd") + "' ORDER BY id DESC";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                this.expensesDataGridView.DataSource = dt;

                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }

        private void bunifuButton2_Click(object sender, EventArgs e)
        {
            this.SuspendLayout();
            indicator.Location = new Point(3, 229);
            this.ResumeLayout();
            pagesForm.SetPage("customerMaster");
            this.getCustomerDataFormdb();


        }

        private void getReportlastIdSeriesId()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM reportTable WHERE id = (SELECT MAX(id) FROM reportTable)";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataReader result = command.ExecuteReader();

                while (result.Read())
                {
                    //this.customerTextBox.Items.Add(result["customerName"]);
                    int lastId = Int32.Parse(result["id"].ToString());
                    int seriesId = Int32.Parse(result["seriesNo"].ToString());
                    this.latestReportId = (lastId + 1).ToString();
                    this.serialNoTextBox.Text = (++lastId).ToString();
                    this.seriesNoTexBox.Text = (++seriesId).ToString();

                }



                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

        private void gettestinglastId()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM testingTable WHERE id = (SELECT MAX(id) FROM testingTable)";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataReader result = command.ExecuteReader();

                while (result.Read())
                {
                    //this.customerTextBox.Items.Add(result["customerName"]);
                    int lastId = Int32.Parse(result["id"].ToString());
                    int seriesId = Int32.Parse(result["series"].ToString());
                    this.latestTestingId = (lastId + 1).ToString();
                    this.testSeries.Text = (++seriesId).ToString();
                    this.testId.Text = (++lastId).ToString();

                }



                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

        private void getExpensestlastId()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM expensesTable WHERE id = (SELECT MAX(id) FROM expensesTable)";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataReader result = command.ExecuteReader();

                while (result.Read())
                {
                    //this.customerTextBox.Items.Add(result["customerName"]);
                    int ExlastId = Int32.Parse(result["id"].ToString());

                    this.lastExensesId = (ExlastId + 1).ToString();
                    this.expenseId.Text = this.lastExensesId;



                }



                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }
        private void getReportTableDatagrid()
        {
            Database databaseObject = new Database();
            ;
            // string query = " SELECT * FROM reportTable INNER JOIN customerTable ON reportTable.customerId = customerTable.id ORDER BY id DESC";
            string query = "SELECT * FROM reportTable INNER JOIN customerTable ON reportTable.customerId = customerTable.id INNER JOIN reportDetailTable ON reportTable.id = reportDetailTable.reportId WHERE reportTable.currentDate = '" + this.reportDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'ORDER BY id DESC";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                this.reportDataTableGirdView.AutoGenerateColumns = false;
                this.reportDataTableGirdView.DataSource = dt;
                databaseObject.CloseConnection();
                int count = this.reportDataTableGirdView.RowCount - 1;

                foreach (DataRow pRow in dt.Rows)
                {


                }

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }


        private void getAllReportDatagrid()
        {
          
            if (this.fullcustomerReportBox.Text !=  "" && this.fullfilterMobileNo.Text == "")
            {
                Database databaseObject = new Database();
                string query = "SELECT * FROM reportTable INNER JOIN customerTable ON reportTable.customerId = customerTable.id WHERE  DATE(reportTable.currentDate) BETWEEN '" + this.fullReportStartDatepicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.fullReportEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'   AND reportTable.customerId = '" + this.fullcustomerReportBox.SelectedValue + "' ";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
                try
                {
                    databaseObject.OpenConnection();
                    SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    this.allReportDataGrid.AutoGenerateColumns = false;
                    this.allReportDataGrid.DataSource = dt;
                    databaseObject.CloseConnection();
                    int count = this.allReportDataGrid.RowCount;
                    this.rowCountTotal.Text = count.ToString();
                    foreach (DataRow pRow in dt.Rows)
                    {


                    }

                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }
            else if (this.fullfilterMobileNo.Text != "" && this.fullcustomerReportBox.Text == "")
            {
                Database databaseObject = new Database();
                string query = "SELECT * FROM reportTable INNER JOIN customerTable ON reportTable.customerId = customerTable.id WHERE  DATE(reportTable.currentDate) BETWEEN '" + this.fullReportStartDatepicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.fullReportEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'   AND customerTable.custMobile =  '" + this.fullfilterMobileNo.Text + "' ";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
                try
                {
                    databaseObject.OpenConnection();
                    SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    this.allReportDataGrid.AutoGenerateColumns = false;
                    this.allReportDataGrid.DataSource = dt;
                    databaseObject.CloseConnection();
                    int count = this.allReportDataGrid.RowCount;
                    this.rowCountTotal.Text = count.ToString();
                    foreach (DataRow pRow in dt.Rows)
                    {


                    }

                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }
           else  if (this.fullfilterMobileNo.Text == "" && this.fullcustomerReportBox.Text == "")
            {
                Database databaseObject = new Database();
                string query = "SELECT * FROM reportTable INNER JOIN customerTable ON reportTable.customerId = customerTable.id WHERE  DATE(reportTable.currentDate) BETWEEN '" + this.fullReportStartDatepicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.fullReportEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
                try
                {
                    databaseObject.OpenConnection();
                    SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    this.allReportDataGrid.AutoGenerateColumns = false;
                    this.allReportDataGrid.DataSource = dt;
                    databaseObject.CloseConnection();
                    int count = this.allReportDataGrid.RowCount;
                    this.rowCountTotal.Text = count.ToString();

                    foreach (DataRow pRow in dt.Rows)
                    {


                    }

                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }
            else if (this.fullfilterMobileNo.Text != "" && this.fullcustomerReportBox.Text != "")
            {
                Database databaseObject = new Database();
                string query = "SELECT * FROM reportTable INNER JOIN customerTable ON reportTable.customerId = customerTable.id WHERE  DATE(reportTable.currentDate) BETWEEN '" + this.fullReportStartDatepicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.fullReportEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "' AND reportTable.customerId = '"+this.fullcustomerReportBox.SelectedValue.ToString()+"' AND customerTable.custMobile = '" + this.fullfilterMobileNo.Text+"'";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
                try
                {
                    databaseObject.OpenConnection();
                    SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    this.allReportDataGrid.AutoGenerateColumns = false;
                    this.allReportDataGrid.DataSource = dt;
                    databaseObject.CloseConnection();
                    int count = this.allReportDataGrid.RowCount;
                    this.rowCountTotal.Text = count.ToString();
                    foreach (DataRow pRow in dt.Rows)
                    {


                    }

                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }



        }

        private void bunifuButton3_Click(object sender, EventArgs e)
        {
            this.SuspendLayout();
            indicator.Location = new Point(3, 274);
            this.ResumeLayout();
            pagesForm.SetPage("sellItem");
            
            this.getReportlastIdSeriesId();
            this.getReportTableDatagrid();
            this.FillCustomerdata();






        }

        private void bunifuButton4_Click(object sender, EventArgs e)
        {
            this.SuspendLayout();
            indicator.Location = new Point(3, 319);
            this.ResumeLayout();
            pagesForm.SetPage("Report");
            this.getAllReportDatagrid();
            this.fillCustomerDataForReport();


        }
        private void fillCustomerDataForReport()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM customerTable";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            DataRow dr;
            try
            {
                databaseObject.OpenConnection();
                //  SQLiteDataReader result = command.ExecuteReader();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                //   while (result.Read())
                //   {
                dr = dt.NewRow();
                dr.ItemArray = new object[] { 0, "Select Customer" };
                dt.Rows.InsertAt(dr, 0);
                dt.Columns.Add("FullName", typeof(string), "customerName + ' ' + custMobile + ' ' + customerAddress");
                this.fullcustomerReportBox.ValueMember = "id";

                this.fullcustomerReportBox.DisplayMember = "FullName";
                this.fullcustomerReportBox.DataSource = dt;



                //    TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                //   this.customerTextBox.Items.Add(textInfo.ToTitleCase(result["customerName"] + result["id"] .ToString())); 

                // /  }



                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

private void sellItemPage_Click(object sender, EventArgs e)
        {

        }

        private void bunifuShadowPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void indicator_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pagesForm_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dashbaordPage_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void customerMasterPage_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel12_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox13_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel11_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox12_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel10_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox11_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel9_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox10_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel8_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox9_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel7_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox8_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel6_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox7_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel5_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox6_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox5_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel4_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox4_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel3_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox3_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel2_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox2_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel1_Click_1(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox1_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void reportPage_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void bunifuFormDock1_FormDragging(object sender, Bunifu.UI.WinForms.BunifuFormDock.FormDraggingEventArgs e)
        {

        }

        private void bunifuButton5_Click(object sender, EventArgs e)
        {
            this.Close();
            System.Windows.Forms.Application.Exit();
        }

        private void bunifuButton6_Click(object sender, EventArgs e)
        {
            try
            {
                panel1.Dock = DockStyle.None; // Un-dock
                this.WindowState = FormWindowState.Minimized;
                isMinimized = true;
            }
            catch (Exception)
            {
                //...
            }
        }

        private void bunifuPictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton7_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton8_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton9_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton10_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton11_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton12_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel13_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox14_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel11_Click_1(object sender, EventArgs e)
        {

        }

        private void bunifuLabel23_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel21_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel27_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel36_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel33_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel32_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton13_Click(object sender, EventArgs e)
        {


        }

        private void bunifuDropdown1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel14_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox15_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuButton7_Click_1(object sender, EventArgs e)
        {
            // reportDetail = new ReportModel();
            // reportDetail.Show();
            DialogResult det = MessageBox.Show("Do you want to Open or close Detail Page", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (det == DialogResult.Yes)
            {
                timer2.Start();
                //this.reportDetailSilverTextBox.Focus();
                this.panelAddDetail.Focus();
                
                

                //this.goldPurityTextBox.Select();

            }
            else if (det == DialogResult.No)
            {
                this.reportSaveButton.Select();

            }
        }

        private void reportNewButton_Click(object sender, EventArgs e)
        {
            // this.Alert("Success Alert", Toaster.enmType.Success);
            this.clearReportControl();
            this.modifyId = null;
        }

        public void Alert(string msg, Toaster.enmType type)
        {
            
            Toaster frm = new Toaster();
         
            frm.showAlert(msg, type);
            
           
        }

        private void bunifuMetroTextbox8_OnValueChanged_1(object sender, EventArgs e)
        {

        }

        private void bunifuLabel21_Click_1(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox9_OnValueChanged_1(object sender, EventArgs e)
        {

        }

        private void reportSaveButton_Click(object sender, EventArgs e)
        {
            if (this.modifyId == null)
            {
                // MessageBox.Show("aa gaya if");
                DialogResult resP = MessageBox.Show("Are you sure want to Print", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (resP == DialogResult.No)
                {
                    byte[] imageBt = null;
                    byte[] imageNewBt = (byte[])(new ImageConverter()).ConvertTo(this.reportImageArea.Image, typeof(byte[]));
                    byte[] imageDefault = (byte[])(new ImageConverter()).ConvertTo(Properties.Resources.add_image, typeof(byte[]));
                    if (imageNewBt.Length != imageDefault.Length)
                    {
                        //FileStream fStream = new FileStream(this.bunifuLabel18.Text, FileMode.Open, FileAccess.Read);
                        //BinaryReader br = new BinaryReader(fStream);
                        imageBt = (byte[])(new ImageConverter()).ConvertTo(this.reportImageArea.Image, typeof(byte[]));
                    }
                    else if (imageNewBt.Length == imageDefault.Length)
                    {
                        imageBt = null;
                    }





                 /*   byte[] imageICON = null;
                    byte[] imageICONNewBt = (byte[])(new ImageConverter()).ConvertTo(this.iconshowareabox.Image, typeof(byte[]));
                    byte[] imageICONDefault = (byte[])(new ImageConverter()).ConvertTo(Properties.Resources.add_image, typeof(byte[]));
                    if (imageICONNewBt.Length != imageICONDefault.Length)
                    {
                       // FileStream NEWStream = new FileStream(this.bunifuMetroTextbox4.Text, FileMode.Open, FileAccess.Read);
                        //BinaryReader brR = new BinaryReader(NEWStream);
                        //imageICON = brR.ReadBytes((int)NEWStream.Length);
                        imageICON = (byte[])(new ImageConverter()).ConvertTo(this.iconshowareabox.Image, typeof(byte[]));
                    }
                    else if (imageICONNewBt.Length == imageICONDefault.Length)
                    {
                        imageICON = null;

                    } */


                    if (this.seriesNoTexBox.Text != "")
                    {

                    }
                    else
                    {
                        this.Alert("Series Area Not Empty!", Toaster.enmType.Warning);

                    }


                    if (this.customerTextBox.SelectedIndex == -1)
                    {
                        this.createCustomerData();

                    }
                    else
                    {
                        this.genratedCustId = Int32.Parse(this.customerTextBox.SelectedValue.ToString());
                    }
                    if (this.genratedCustId > 0)
                    {

                        Database databaseObject = new Database();
                        string query = "INSERT INTO reportTable (seriesNo,currentDate,goldPurity,goldKarat,sample,orgWeight,customerId,tounchweight,currentTime,goldFine,payment,amount,relatedImage) " +
                            "VALUES('" + this.seriesNoTexBox.Text + "','" + this.reportDatePicker.Value.Date.ToString("yyyy-MM-dd") + "','" + this.goldPurityTextBox.Text + "','" + this.goldKaratTextBox.Text + "','" + info.ToTitleCase(this.sampleTextBox.Text.ToLower()) + "','" + this.originalWeightTextBox.Text + "','" + this.genratedCustId.ToString() + "','" + this.tounchWeightTextBox.Text + "','" + this.currentTimebox.Value.ToShortTimeString() + "','" + this.goldFineTextBox.Text + "','" + "True" + "','" + this.customerRate.Text.ToString() + "' ,@imgae )";
                        SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);


                        try
                        {

                            databaseObject.OpenConnection();

                            command.Parameters.Add(new SQLiteParameter("@imgae", imageBt));

                           // command.Parameters.Add(new SQLiteParameter("@iconImage", imageICON));

                            SQLiteDataReader result = command.ExecuteReader();

                            while (result.Read())
                            {


                            }


                            databaseObject.CloseConnection();

                            this.createReportDetailId(this.latestReportId);
                            this.clearReportControl();


                        }
                        catch (Exception es)
                        {
                            MessageBox.Show(es.Message);
                        }



                    }
                    else
                    {
                        
                        this.Alert("Please Fill Data First", Toaster.enmType.Info);
                        this.customerTextBox.Select();
                    }
                }
                else if (resP == DialogResult.Yes)
                {
                    this.saveAndPrintReport();

                }
            }
            else
            {
                DialogResult resM = MessageBox.Show("Are you sure Want to Modify & Print", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (resM == DialogResult.No)
                {
                    //MessageBox.Show("aa gaya eles");
                    this.modifyReportData();
                    this.modifyReportDetailData();
                    // this.createReportDetailId(this.latestReportId);
                    this.clearReportControl();
                    this.modifyId = null;
                }
                else if (resM == DialogResult.Yes)
                {
                    this.modifyReportData();
                    this.modifyReportDetailData();
                    printRdlc_Form = new PrintFolder.PrintRdlc_Form();
                    printRdlc_Form.calldatda(this.modifyId.ToString());
                    //this.createReportDetailId(this.latestReportId);
                    this.clearReportControl();
                    this.modifyId = null;
                }
            }


        }

        private void modifyReportDetailData()
        {
            Database databaseObject = new Database();

            string query = "UPDATE reportDetailTable SET Silver = @Silver,Cobalt = @Cobalt ,Lead = @Lead ,Ruthenium = @Ruthenium,Copper = @Copper ,Platinum = @Platinum, Chromium = @Chromium ,Tungustan = @Tungustan ,Zinc = @Zinc ,Palladium =@Palladium,Bismuth= @Bismuth,Manganese = @Manganese ,Cadmium = @Cadmium ,Antimony = @Antimony ,Nickel = @Nickel ,Osmium = @Osmium ,Iridium = @Iridium ,Iron = @Iron,Indium = @Indium,Rehnium = @Rehnium,Rhodium = @Rhodium ,Tin = @Tin,Titanium = @Titanium,Gallium = @Gallium WHERE reportId = @reportId";
            //  string query = "UPDATE customerTable (customerName,customerAddress,custMobile,Rate,custFirm) VALUES('" + this.custMastNametextbox.Text.ToString() + "','" + this.custMastAddresstextbox.Text.ToString() + "','" + this.custMastMobiletextbox.Text.ToString() + "','" + this.custMastRatetextbox.Text.ToString() + "','" + this.custMastFirmNametextbox.Text.ToString() + "')";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                command.Parameters.Add(new SQLiteParameter("@reportId", this.modifyId.ToString()));
                command.Parameters.Add(new SQLiteParameter("@Silver", this.reportDetailSilverTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Cobalt", this.reportDetailCobaltTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Lead", this.reportDetailLeadTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Ruthenium", this.reportDetailRutheniumTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Copper", this.reportDetailCopperTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Platinum", this.reportDetailPlatinumTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Chromium", this.reportDetailChromiumTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Tungustan", this.reportDetailTungustanTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Zinc", this.reportDetailZincTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Palladium", this.reportDetailPalladiumTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Bismuth", this.reportDetailBismuthTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Manganese", this.reportDetailManganeseTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Cadmium", this.reportDetailCadmiumTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Antimony", this.reportDetailAntimonyTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Nickel", this.reportDetailNickelTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Osmium", this.reportDetailOsmiumTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Iridium", this.reportDetailIridiumTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Iron", this.reportDetailIronTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Indium", this.reportDetailIndiumTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Rehnium", this.reportDetailRehniumTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Rhodium", this.reportDetailRhodiumTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Tin", this.reportDetailTinTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Titanium", this.reportDetailTitaniumTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@Gallium", this.reportDetailGalliumTextBox.Text));


                SQLiteDataReader result = command.ExecuteReader();
               this.Alert("Report Data Modified Successfully!", Toaster.enmType.Success);


                while (result.Read())
                {


                }
                databaseObject.CloseConnection();

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

        private void modifyReportData()
        {
            byte[] imageBt = null;
            byte[] imageNewBt = (byte[])(new ImageConverter()).ConvertTo(this.reportImageArea.Image, typeof(byte[]));
            byte[] imageDefault = (byte[])(new ImageConverter()).ConvertTo(Properties.Resources.add_image, typeof(byte[]));
            if (imageNewBt.Length != imageDefault.Length)
            {
                //FileStream fStream = new FileStream(this.bunifuLabel18.Text, FileMode.Open, FileAccess.Read);
                //BinaryReader br = new BinaryReader(fStream);
                imageBt = (byte[])(new ImageConverter()).ConvertTo(this.reportImageArea.Image, typeof(byte[]));
            }
            else if (imageNewBt.Length == imageDefault.Length)
            {
                imageBt = null;
            }

           /* byte[] imageICON = null;
            byte[] imageICONNewBt = (byte[])(new ImageConverter()).ConvertTo(this.iconshowareabox.Image, typeof(byte[]));
            byte[] imageICONDefault = (byte[])(new ImageConverter()).ConvertTo(Properties.Resources.add_image, typeof(byte[]));
            if (imageICONNewBt.Length != imageICONDefault.Length)
            {
                // FileStream NEWStream = new FileStream(this.bunifuMetroTextbox4.Text, FileMode.Open, FileAccess.Read);
                //BinaryReader brR = new BinaryReader(NEWStream);
                //imageICON = brR.ReadBytes((int)NEWStream.Length);
                imageICON = (byte[])(new ImageConverter()).ConvertTo(this.iconshowareabox.Image, typeof(byte[]));
            }
            else if (imageICONNewBt.Length == imageICONDefault.Length)
            {
                imageICON = null;

            }*/
           /* byte[] imageBt = null;
            if (this.bunifuLabel18.Text != "")
            {
                FileStream fStream = new FileStream(this.bunifuLabel18.Text, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fStream);
                imageBt = br.ReadBytes((int)fStream.Length);
            }
            else
            {
                imageBt = (byte[])(new ImageConverter()).ConvertTo(this.reportImageArea.Image, typeof(byte[]));
            }





            byte[] imageICON = null;
            if (this.bunifuMetroTextbox4.Text != "")
            {
                FileStream NEWStream = new FileStream(this.bunifuMetroTextbox4.Text, FileMode.Open, FileAccess.Read);
                BinaryReader brR = new BinaryReader(NEWStream);
                imageICON = brR.ReadBytes((int)NEWStream.Length);
            }
           */
            Database databaseObject = new Database();

            string query = "UPDATE reportTable SET goldFine = @goldFine,goldPurity = @goldPurity,goldKarat = @goldKarat, relatedImage = @relatedImage, sample =@sample, orgWeight = @orgWeight , tounchweight = @tounchweight WHERE id = @id";
            //  string query = "UPDATE customerTable (customerName,customerAddress,custMobile,Rate,custFirm) VALUES('" + this.custMastNametextbox.Text.ToString() + "','" + this.custMastAddresstextbox.Text.ToString() + "','" + this.custMastMobiletextbox.Text.ToString() + "','" + this.custMastRatetextbox.Text.ToString() + "','" + this.custMastFirmNametextbox.Text.ToString() + "')";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();

                command.Parameters.Add(new SQLiteParameter("@relatedImage", imageBt));
                //command.Parameters.Add(new SQLiteParameter("@iconImage", imageICON));
                command.Parameters.Add(new SQLiteParameter("@id", this.modifyId.ToString()));
                command.Parameters.Add(new SQLiteParameter("@goldFine", this.goldFineTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@goldPurity", this.goldPurityTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@goldKarat", this.goldKaratTextBox.Text));
                //command.Parameters.Add(new SQLiteParameter("@iconFirst", info.ToTitleCase(this.firstIconText.Text.ToUpper())));
                //command.Parameters.Add(new SQLiteParameter("@iconLast", info.ToTitleCase(this.lastIconText.Text.ToUpper())));
                command.Parameters.Add(new SQLiteParameter("@sample", info.ToTitleCase(this.sampleTextBox.Text.ToLower())));
                command.Parameters.Add(new SQLiteParameter("@orgWeight", this.originalWeightTextBox.Text));
                command.Parameters.Add(new SQLiteParameter("@tounchweight", this.tounchWeightTextBox.Text));
                //command.Parameters.Add(new SQLiteParameter("@amount", this.customerRate.ToString()));

                SQLiteDataReader result = command.ExecuteReader();



                while (result.Read())
                {


                }
                databaseObject.CloseConnection();

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

        private void modifyTestingData()
        {
            Database databaseObject = new Database();

            string query = "UPDATE testingTable SET goldFine = @goldFine,purity = @purity,karat = @karat,amount = @amount, orgweight = @orgweight, tounchweight = @tounchweight, sample = @sample , type = @type WHERE id = @id";
            //  string query = "UPDATE customerTable (customerName,customerAddress,custMobile,Rate,custFirm) VALUES('" + this.custMastNametextbox.Text.ToString() + "','" + this.custMastAddresstextbox.Text.ToString() + "','" + this.custMastMobiletextbox.Text.ToString() + "','" + this.custMastRatetextbox.Text.ToString() + "','" + this.custMastFirmNametextbox.Text.ToString() + "')";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                command.Parameters.Add(new SQLiteParameter("@id", this.modifyTestingId.ToString()));
                command.Parameters.Add(new SQLiteParameter("@goldFine", this.testgoldFine.Text));
                command.Parameters.Add(new SQLiteParameter("@purity", this.testPurity.Text));
                command.Parameters.Add(new SQLiteParameter("@karat", this.testKarat.Text));
                command.Parameters.Add(new SQLiteParameter("@amount", this.testAmount.Text));
                command.Parameters.Add(new SQLiteParameter("@orgweight", this.testorgweight.Text));
                command.Parameters.Add(new SQLiteParameter("@tounchweight", this.testtounchweight.Text));
                command.Parameters.Add(new SQLiteParameter("@sample", this.testSmaplecomboBox.Text));
                command.Parameters.Add(new SQLiteParameter("@type", this.testingDropdown.Text.ToString()));


                SQLiteDataReader result = command.ExecuteReader();



                while (result.Read())
                {


                }
                databaseObject.CloseConnection();

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }
        private void saveAndPrintReport()
        {

            byte[] imageBt = null;
            byte[] imageNewBt = (byte[])(new ImageConverter()).ConvertTo(this.reportImageArea.Image, typeof(byte[]));
            byte[] imageDefault = (byte[])(new ImageConverter()).ConvertTo(Properties.Resources.add_image, typeof(byte[]));
            if (imageNewBt.Length != imageDefault.Length)
            {
                //FileStream fStream = new FileStream(this.bunifuLabel18.Text, FileMode.Open, FileAccess.Read);
                //BinaryReader br = new BinaryReader(fStream);
                imageBt = (byte[])(new ImageConverter()).ConvertTo(this.reportImageArea.Image, typeof(byte[]));
            }
            else if (imageNewBt.Length == imageDefault.Length)
            {
                imageBt = null;
            }

          /*  byte[] imageICON = null;
            byte[] imageICONNewBt = (byte[])(new ImageConverter()).ConvertTo(this.iconshowareabox.Image, typeof(byte[]));
            byte[] imageICONDefault = (byte[])(new ImageConverter()).ConvertTo(Properties.Resources.add_image, typeof(byte[]));
            if (imageICONNewBt.Length != imageICONDefault.Length)
            {
                // FileStream NEWStream = new FileStream(this.bunifuMetroTextbox4.Text, FileMode.Open, FileAccess.Read);
                //BinaryReader brR = new BinaryReader(NEWStream);
                //imageICON = brR.ReadBytes((int)NEWStream.Length);
                imageICON = (byte[])(new ImageConverter()).ConvertTo(this.iconshowareabox.Image, typeof(byte[]));
            }
            else if (imageICONNewBt.Length == imageICONDefault.Length)
            {
                imageICON = null;

            }
          */



            /*byte[] imageBt = null;
            if (this.bunifuLabel18.Text != "")
            {
                FileStream fStream = new FileStream(this.bunifuLabel18.Text, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fStream);
                imageBt = br.ReadBytes((int)fStream.Length);
            }
            else
            {
                //this.reportImageArea.Image = Properties.Resources.add_image;
                // imageBt = (byte[])(new ImageConverter()).ConvertTo(this.reportImageArea.Image, typeof(byte[]));
            }





            byte[] imageICON = null;
            if (this.bunifuMetroTextbox4.Text != "")
            {
                FileStream NEWStream = new FileStream(this.bunifuMetroTextbox4.Text, FileMode.Open, FileAccess.Read);
                BinaryReader brR = new BinaryReader(NEWStream);
                imageICON = brR.ReadBytes((int)NEWStream.Length);
            }
            */


            if (this.seriesNoTexBox.Text != "")
            {

            }
            else
            {
                this.Alert("Series Area Not Empty!", Toaster.enmType.Warning);

            }


            if (this.customerTextBox.SelectedIndex == -1)
            {
                this.createCustomerData();

            }
            else
            {
                this.genratedCustId = Int32.Parse(this.customerTextBox.SelectedValue.ToString());
            }
            if (this.genratedCustId > 0)
            {

                Database databaseObject = new Database();
                string query = "INSERT INTO reportTable (seriesNo,currentDate,goldPurity,goldKarat,sample,orgWeight,customerId,tounchweight,currentTime,goldFine,payment,amount,relatedImage) " +
                    "VALUES('" + this.seriesNoTexBox.Text + "','" + this.reportDatePicker.Value.Date.ToString("yyyy-MM-dd") + "','" + this.goldPurityTextBox.Text + "','" + this.goldKaratTextBox.Text + "','" + info.ToTitleCase(this.sampleTextBox.Text.ToLower())+ "','" + this.originalWeightTextBox.Text + "','" + this.genratedCustId.ToString() + "','" + this.tounchWeightTextBox.Text + "','" + this.currentTimebox.Value.ToShortTimeString() + "','" + this.goldFineTextBox.Text + "','" + "True" + "','" + this.customerRate.Text.ToString() + "' ,@imgae)";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);


                try
                {

                    databaseObject.OpenConnection();

                    command.Parameters.Add(new SQLiteParameter("@imgae", imageBt));

                   // command.Parameters.Add(new SQLiteParameter("@iconImage", imageICON));

                    SQLiteDataReader result = command.ExecuteReader();

                    while (result.Read())
                    {


                    }


                    databaseObject.CloseConnection();
                    this.createReportDetailId(this.latestReportId);
                    printRdlc_Form = new PrintFolder.PrintRdlc_Form();
                    printRdlc_Form.calldatda(this.latestReportId);
                    this.clearReportControl();



                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }



            }
            else
            {
                this.Alert("Please Fill Data First", Toaster.enmType.Info);
                this.customerTextBox.Select();
            }
        }

        private void saveAndPrintTestingReoport()
        {

            if (this.customerTextBox.SelectedIndex == -1)
            {
                this.Alert("Please the select Customer!", Toaster.enmType.Warning);
            }





            if (this.testSeries.Text != "")
            {

            }
            else
            {
                this.Alert("Series Area Not Empty!", Toaster.enmType.Warning);

            }







            Database databaseObject = new Database();
            string query = "INSERT INTO testingTable (series,date,purity,karat,sample,orgweight,testcustId,tounchweight,time,goldFine,amount,payment,type) " +
                "VALUES('" + this.testSeries.Text + "','" + this.testDate.Value.Date.ToString("yyyy-MM-dd") + "','" + this.testPurity.Text + "','" + this.testKarat.Text + "','" + this.testSmaplecomboBox.Text + "','" + this.testorgweight.Text + "','" + this.testCustcombo.SelectedValue.ToString() + "','" + this.testtounchweight.Text + "','" + this.testTime.Value.ToShortTimeString() + "','" + this.testgoldFine.Text + "','" + this.testAmount.Text + "','" + "true" + "','" + this.testingDropdown.Text.ToString() +  "')";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);


            try
            {

                databaseObject.OpenConnection();
                SQLiteDataReader result = command.ExecuteReader();

                while (result.Read())
                {


                }


                databaseObject.CloseConnection();
                printTestingRdlc_Form = new PrintTestingReport.PrintTestingForm();
                printTestingRdlc_Form.callTestingdata(this.latestTestingId);

                this.clearTestingControl();
                this.modifyTestingId = null;


            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }


        }

        private void seriesNoTexBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                    (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Number Only!");
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }



        }

        private void seriesNoTexBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }
        }

        private void customerTextBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void goldPurityTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                  (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }


        }

        private void goldPurityTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                //MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void bunifuTileButton1_Click(object sender, EventArgs e)
        {

        }

        private void goldPurityTextBox_Leave(object sender, EventArgs e)
        {
            if (this.goldPurityTextBox.Text != "")
            {
                if (this.goldPurityTextBox.Text.ToString() != "100")
                {
                    string your_String = this.goldPurityTextBox.Text.ToString();
                    string my_String = Regex.Replace(your_String, @"[^0-9]+", "");
                    int my_newlength = my_String.Length;
                    if (my_String.Length > 3)
                    {
                        string resultValue = my_String.Substring(0, 3);
                        int parseNo = int.Parse(resultValue);
                        double karatValue = Math.Round((parseNo / 1000.0) * 24, 1);
                        this.goldKaratTextBox.Text = karatValue.ToString();
                    }
                    else if (my_String.Length < 3)
                    {

                        string resultValue = my_String + "0";
                        int parseNo = int.Parse(resultValue);
                        double karatValue = Math.Round((parseNo / 1000.0) * 24, 1);
                        this.goldKaratTextBox.Text = karatValue.ToString();
                    }
                    else if (my_String.Length == 3)
                    {
                        int parseNo = int.Parse(my_String);
                        double karatValue = Math.Round((parseNo / 1000.0) * 24, 1);
                        this.goldKaratTextBox.Text = karatValue.ToString();

                    }
                }
                else
                {

                    int karatValue = 24;
                    this.goldKaratTextBox.Text = karatValue.ToString();

                }



            }

            if (this.goldPurityTextBox.Text != "" && this.originalWeightTextBox.Text != "")
            {
                string goldper_string = this.goldPurityTextBox.Text.ToString();
                string goldWeight_stirng = this.originalWeightTextBox.Text.ToString();
                float parseNogoldPer = float.Parse(goldper_string);
                float parseGoldweight = float.Parse(goldWeight_stirng);
                double goldFine = Math.Round(((parseGoldweight * parseNogoldPer) / 100), 3);
                this.goldFineTextBox.Text = goldFine.ToString();
            }

            if (goldPurityTextBox.Text.Length == 4)
            {
                string myString = this.goldPurityTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.goldPurityTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bunifuButton8_Click_1(object sender, EventArgs e)
        {
            // open file dialog   

        }

        private void customerMobileTextBox_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuPictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void sampleTextBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void sampleTextBox_Leave(object sender, EventArgs e)
        {

        }

        private void customerTextBox_Leave(object sender, EventArgs e)
        {

            if (this.customerTextBox.SelectedIndex == -1)
            {
                this.customerMobileTextBox.Text = this.defaultMobileNo.ToString();
                this.customerAddressTextBox.Text = this.defaultAddress.ToString() ;
                this.customerRate.Text = this.reportDefaultRate.ToString();
            }
            if (this.customerTextBox.SelectedIndex == 0)
            {
                this.Alert("Customer Name Not Be Null!", Toaster.enmType.Warning);
            }
        }

        private void customerTextBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (this.customerTextBox.SelectedIndex != -1)
            {
                this.getSelectedCustomerData();

            }

        }

        private void customerTextBox_KeyDown(object sender, KeyEventArgs e)
        {
           // if (e.Control == true)
            //{
             //   this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                //   MessageBox.Show("Cut/Copy and Paste Options are disabled !");
           // }
        }

        private void customerTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && char.IsDigit(e.KeyChar) &&
                  (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Character Only!");

            }
          //  if (e.KeyChar == '.')
            //{
              //  e.Handled = true;
                //this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //      MessageBox.Show("This Field Accepts Character Only!");
            //}
        }

        private void currentTimebox_MouseDown(object sender, MouseEventArgs e)
        {
            this.currentTimebox.CustomFormat = "hh:mm";
        }

        private void reportSelectButton_Click(object sender, EventArgs e)
        {
            // open file dialog   
            OpenFileDialog open = new OpenFileDialog();
            // image filters  
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";
            if (open.ShowDialog() == DialogResult.OK)
            {
                // display image in picture box  
                this.reportImageArea.Image = new Bitmap(open.FileName);
                // image file path  
                this.bunifuLabel18.Text = open.FileName;
            }
        }

        private void bunifuLabel18_Click(object sender, EventArgs e)
        {

        }

        private void iconshowareabox_Click(object sender, EventArgs e)
        {

        }

        private void reportModifyButton_Click(object sender, EventArgs e)
        {
            // this.Alert("Warning Alert", Toaster.enmType.Warning);
            //this.savePrint = new savePrint();
            //this.savePrint.Show("open new box UIYUIY", "new one", savePrint.MessageBoxButtons.YesNo);
            //  savePrint saveP = new savePrint();
            //saveP.Hide();
            // var result =  saveP.Show("open new box", "new one", savePrint.MessageBoxButtons.YesNo);
            //var result = saveP.ShowDialog();
            //  using (savePrint saveRp = new savePrint())
            //{
            //  if (saveRp.ShowDialog() == DialogResult.Yes)
            //{
            //  MessageBox.Show("OK CLICK", DialogResult.OK.ToString());
            //}
            //}
           // DialogResult dialogResult = MessageBox.Show("Sure NEW ", "Some Title TEXT", MessageBoxButtons.YesNo);
            //if (dialogResult == DialogResult.Yes)
           // {
                //do something
           // }
           // else if (dialogResult == DialogResult.No)
          //  {
                //do something else
          //  }



            //  {
            //    MessageBox.Show("OK CLICK", DialogResult.OK.ToString());
            //            }
            //   if (result == DialogResult.Cancel)
            //      {
            //    MessageBox.Show("CANCEL CLICK", DialogResult.Cancel.ToString());
            //}



        }

        private void bunifuButton9_Click_1(object sender, EventArgs e)
        {
            this.clearCustomerReport();

            this.custMastNametextbox.Focus();

        }

        private void reportDetailPrintButton_Click(object sender, EventArgs e)
        {
            if (this.custMastNametextbox.Text.ToString() != "" && this.custMastMobiletextbox.Text.ToString() != "" && this.custMastAddresstextbox.Text.ToString() != "" && this.custMastRatetextbox.Text.ToString() != "")
            {
                DialogResult res = MessageBox.Show("Are you sure want to Delete this Record", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (res == DialogResult.OK)
                {

                    Database databaseObject = new Database();
                    string query = "DELETE FROM customerTable  where id = @id";
                    //  string query = "UPDATE customerTable (customerName,customerAddress,custMobile,Rate,custFirm) VALUES('" + this.custMastNametextbox.Text.ToString() + "','" + this.custMastAddresstextbox.Text.ToString() + "','" + this.custMastMobiletextbox.Text.ToString() + "','" + this.custMastRatetextbox.Text.ToString() + "','" + this.custMastFirmNametextbox.Text.ToString() + "')";
                    SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

                    try
                    {
                        databaseObject.OpenConnection();
                        command.Parameters.Add(new SQLiteParameter("@id", this.getCustIdToModify));

                        SQLiteDataReader result = command.ExecuteReader();


                        while (result.Read())
                        {


                        }
                        databaseObject.CloseConnection();

                        this.clearCustomerReport();
                        this.Alert("Customer Record Deleted!", Toaster.enmType.Success);
                        this.getCustomerDataFormdb();
                        this.custMastNametextbox.Focus();

                    }
                    catch (Exception es)
                    {
                        MessageBox.Show(es.Message);
                    }
                }
                else if (res == DialogResult.Cancel)
                {

                }

            }
            else
            {

                this.Alert("Please Select The Customer Data First", Toaster.enmType.Warning);
                this.custMastNametextbox.Select();
            }

        }

        private void reportDetailSaveButton_Click(object sender, EventArgs e)
        {

            if (this.custMastNametextbox.Text.ToString() != "" && this.custMastMobiletextbox.Text.ToString() != "" && this.custMastAddresstextbox.Text.ToString() != "" && this.custMastRatetextbox.Text.ToString() != "" && this.custMastFirmNametextbox.Text.ToString() != "")
            {
                DialogResult sav = MessageBox.Show("Are you sure Want to Save", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (sav == DialogResult.OK)
                {

                    Database databaseObject = new Database();
                    string query = "INSERT INTO customerTable (customerName,customerAddress,custMobile,Rate,custFirm) VALUES('" + info.ToTitleCase(this.custMastNametextbox.Text.ToLower()) + "','" + info.ToTitleCase(this.custMastAddresstextbox.Text.ToLower()) + "','" + this.custMastMobiletextbox.Text.ToString() + "','" + this.custMastRatetextbox.Text.ToString() + "','" + info.ToTitleCase(this.custMastFirmNametextbox.Text.ToLower()) + "')";
                    SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
                    try
                    {
                        databaseObject.OpenConnection();
                        SQLiteDataReader result = command.ExecuteReader();


                        while (result.Read())
                        {


                        }
                        databaseObject.CloseConnection();

                        this.clearCustomerReport();
                        this.Alert("Customer Record Added!", Toaster.enmType.Success);
                        this.getCustomerDataFormdb();
                        this.custMastNametextbox.Focus();

                    }
                    catch (Exception es)
                    {
                        MessageBox.Show(es.Message);
                    }

                }
                else if (sav == DialogResult.Cancel)
                {

                }

            }
            else
            {

                this.Alert("Please Fill The Customer Data First", Toaster.enmType.Warning);
                this.custMastNametextbox.Select();
            }






        }
        private BitmapSource _scannedImage;
        private PrintDocument printDocument1;
       

        public BitmapSource ScannedImage
        {
            get { return _scannedImage; }
            set { _scannedImage = value; }
        }
        public bool CanScan
        {
            get { return true; }
        }

        private void reportPrintButton_Click(object sender, EventArgs e)
        {
            if (this.modifyId != null)
            {
                DialogResult resP = MessageBox.Show("Do you want to Print", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (resP == DialogResult.Yes)
                {


                    printRdlc_Form = new PrintFolder.PrintRdlc_Form();
                    printRdlc_Form.calldatda(this.modifyId.ToString());
                    this.clearReportControl();
                    this.modifyId = null;


                }
                else if (resP == DialogResult.No)
                {
                    this.clearReportControl();
                    this.modifyId = null;
                }
            }

            else
            {
                this.Alert("Please Select Data From Report Table! ", Toaster.enmType.Info);
            }
            // MessageBox.Show(this.reportDetailSilverTextBox.Text);
            // this.createReportDetailId(this.serialNoTextBox.Text.ToString());
        }

        private void clearImage()
        {
            //this.reportImageArea.Image = Properties.Resources.add_image;
            this.reportImageArea.Refresh();
            this.bunifuLabel18.Text = "";
        }
        private void reportScanImageButton_Click(object sender, EventArgs e)
        {
            this.clearImage();
            var imageScanObj = new ImageScanner();


            try
            {
                ImageFile file = imageScanObj.Scan();

                if (file != null)
                {
                    var converter = new ScannerImageConverter();

                    ScannedImage = converter.ConvertScannedImage(file);
                    //var path = @"C:\Users\abhis\Desktop\SCANNER IMAGE\scan.jpeg";
                    // System.Drawing.Image image1 = System.Drawing.Image.FromStream(ScannedImage);
                    //ScannedImage = converter.InMemoryConvertScannedImage(file);

                    var imageBytes = (byte[])file.FileData.get_BinaryData();
                    var ms = new MemoryStream(imageBytes);
                    this.reportImageArea.Image = Image.FromStream(ms);
                    //this.bunifuLabel18.Text ="" 






                }
                else
                {
                    ScannedImage = null;
                }

            }
            catch (ScannerException ex)
            {
                // yeah, I know. Showing UI from the VM. Shoot me now.
                MessageBox.Show(ex.Message, "Unable to Scan Image");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            // this.Alert("Info Alert", Toaster.enmType.Info);
        }

        private void bunifuCustomDataGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.customerDataGridView.Rows[e.RowIndex];
                this.custMastNametextbox.Text = row.Cells["Column1"].Value.ToString();
                this.custMastMobiletextbox.Text = row.Cells["Column2"].Value.ToString();
                this.custMastRatetextbox.Text = row.Cells["Column4"].Value.ToString();
                this.custMastAddresstextbox.Text = row.Cells["Column5"].Value.ToString();
                this.custMastFirmNametextbox.Text = row.Cells["Column3"].Value.ToString();
                this.getCustIdToModify = row.Cells["column6"].Value.ToString();
            }
        }

        private void bunifuDatePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click_1(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox1_OnValueChanged_1(object sender, EventArgs e)
        {

        }

        private void bunifuTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void originalWeightTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }
        }

        private void originalWeightTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                 (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void tounchWeightTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }
        }

        private void tounchWeightTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void customerRate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }
        }

        private void customerRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
              (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Number Only!");
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }

        }

        private void customerMobileTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
             
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void bunifuButton1_ForeColorChanged(object sender, EventArgs e)
        {

        }

        private void custMastNametextbox_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Control == true)
           // {
               // this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                //   MessageBox.Show("Cut/Copy and Paste Options are disabled !");
           // }
        }

        private void custMastNametextbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && char.IsDigit(e.KeyChar) &&
                 (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Character Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //      MessageBox.Show("This Field Accepts Character Only!");
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void custMastRatetextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }
        }

        private void custMastRatetextbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
           (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Number Only!");
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void custMastMobiletextbox_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void reportDataTableGirdView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow reportRow = this.reportDataTableGirdView.Rows[e.RowIndex];

                this.serialNoTextBox.Text = reportRow.Cells["Column7"].Value.ToString();
                this.modifyId = reportRow.Cells["Column7"].Value.ToString();
                this.seriesNoTexBox.Text = reportRow.Cells["Column8"].Value.ToString();
                this.customerTextBox.Text = reportRow.Cells["Column11"].Value.ToString();
                if (reportRow.Cells["Column14"].Value.ToString() != null)
                {
                    this.goldPurityTextBox.Text = reportRow.Cells["Column14"].Value.ToString();
                }
                //this.goldKaratTextBox.Text = reportRow.Cells["Column1"].Value.ToString();
                this.sampleTextBox.Text = reportRow.Cells["Column13"].Value.ToString();
                // this.iconshowareabox.Image = reportRow.Cells["Column1"].Value.ToString();
                if (reportRow.Cells["Column16"].Value.ToString() != null)
                {
                    this.originalWeightTextBox.Text = reportRow.Cells["Column16"].Value.ToString();
                }
                if (reportRow.Cells["Column17"].Value.ToString() != null)
                {
                    this.tounchWeightTextBox.Text = reportRow.Cells["Column17"].Value.ToString();
                }
                if (reportRow.Cells["Column19"].Value.ToString() != null)
                {
                    this.customerAddressTextBox.Text = reportRow.Cells["Column19"].Value.ToString();
                }
                if (reportRow.Cells["Column48"].Value is System.DBNull)
                {
                    this.reportImageArea.Image = Properties.Resources.add_image;
                }
                else
                {
                    Byte[] data = new Byte[0];
                    data = (Byte[])(reportRow.Cells["Column48"].Value);
                    MemoryStream mem = new MemoryStream(data);
                    this.reportImageArea.Image = Image.FromStream(mem);
                }
               /* if (reportRow.Cells["Column49"].Value is System.DBNull)
                {
                    this.iconshowareabox.Image = Properties.Resources.add_image;
                }
                else
                {
                    Byte[] newdata = new Byte[0];
                    newdata = (Byte[])(reportRow.Cells["Column49"].Value);
                    MemoryStream newmem = new MemoryStream(newdata);
                    this.iconshowareabox.Image = Image.FromStream(newmem);
                }*/
                this.customerMobileTextBox.Text = reportRow.Cells["Column12"].Value.ToString();
                this.customerRate.Text = reportRow.Cells["Column18"].Value.ToString();
                this.goldFineTextBox.Text = reportRow.Cells["Column20"].Value.ToString();
                this.goldKaratTextBox.Text = reportRow.Cells["Column15"].Value.ToString();
                //this.totalPercentageLabel.Text = reportRow.Cells["Column1"].Value.ToString();
                //this.genratedCustId = 0;
                //this.customerTextBox.SelectedValue = 0;
                //this.reportImageArea.Image = reportRow.Cells["Column1"].Value.ToString();
                this.reportDetailSilverTextBox.Text = reportRow.Cells["Column50"].Value.ToString();
                this.reportDetailCobaltTextBox.Text = reportRow.Cells["Column51"].Value.ToString();
                this.reportDetailLeadTextBox.Text = reportRow.Cells["Column52"].Value.ToString();
                this.reportDetailRutheniumTextBox.Text = reportRow.Cells["Column53"].Value.ToString();
                this.reportDetailCopperTextBox.Text = reportRow.Cells["Column54"].Value.ToString();
                this.reportDetailPlatinumTextBox.Text = reportRow.Cells["Column55"].Value.ToString();
                this.reportDetailChromiumTextBox.Text = reportRow.Cells["Column56"].Value.ToString();
                this.reportDetailTungustanTextBox.Text = reportRow.Cells["Column57"].Value.ToString();
                this.reportDetailZincTextBox.Text = reportRow.Cells["Column58"].Value.ToString();
                this.reportDetailPalladiumTextBox.Text = reportRow.Cells["Column59"].Value.ToString();
                this.reportDetailBismuthTextBox.Text = reportRow.Cells["Column60"].Value.ToString();
                this.reportDetailManganeseTextBox.Text = reportRow.Cells["Column61"].Value.ToString();
                this.reportDetailCadmiumTextBox.Text = reportRow.Cells["Column62"].Value.ToString();
                this.reportDetailAntimonyTextBox.Text = reportRow.Cells["Column63"].Value.ToString();
                this.reportDetailNickelTextBox.Text = reportRow.Cells["Column64"].Value.ToString();
                this.reportDetailOsmiumTextBox.Text = reportRow.Cells["Column65"].Value.ToString();
                this.reportDetailIridiumTextBox.Text = reportRow.Cells["Column66"].Value.ToString();
                this.reportDetailIronTextBox.Text = reportRow.Cells["Column67"].Value.ToString();
                this.reportDetailIndiumTextBox.Text = reportRow.Cells["Column68"].Value.ToString();
                this.reportDetailRehniumTextBox.Text = reportRow.Cells["Column69"].Value.ToString();
                this.reportDetailRhodiumTextBox.Text = reportRow.Cells["Column70"].Value.ToString();
                this.reportDetailTinTextBox.Text = reportRow.Cells["Column71"].Value.ToString();
                this.reportDetailTitaniumTextBox.Text = reportRow.Cells["Column72"].Value.ToString();
                this.reportDetailGalliumTextBox.Text = reportRow.Cells["Column73"].Value.ToString();
                //this.firstIconText.Text = reportRow.Cells["Column74"].Value.ToString();
                //this.lastIconText.Text = reportRow.Cells["Column75"].Value.ToString();
                //this.custMastNametextbox.Text = row.Cells["Column1"].Value.ToString();
                //this.custMastMobiletextbox.Text = row.Cells["Column2"].Value.ToString();
                //this.custMastRatetextbox.Text = row.Cells["Column4"].Value.ToString();
                //this.custMastAddresstextbox.Text = row.Cells["Column5"].Value.ToString();
                //this.custMastFirmNametextbox.Text = row.Cells["Column3"].Value.ToString();
                //this.getCustIdToModify = row.Cells["column6"].Value.ToString(); */
            }
        }

        private void reportDataTableGirdView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //Check to ensure that the row CheckBox is clicked.
         //   DialogResult paidResRep = MessageBox.Show("Are you sure want to Change Payment Value", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
           // if (paidResRep == DialogResult.Yes)
//            {//
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    //Reference the GridView Row.
                    DataGridViewRow row = reportDataTableGirdView.Rows[e.RowIndex];

                    //Set the CheckBox selection.
                    row.Cells["Column21"].Value = !Convert.ToBoolean(row.Cells["Column21"].EditedFormattedValue);

                    //If CheckBox is checked, display Message Box.
                    if (Convert.ToBoolean(row.Cells["Column21"].Value))
                    {
                        this.updatePaid(row.Cells["Column21"].Value.ToString(), row.Cells["Column7"].Value.ToString());

                    }
                    else
                    {
                        this.updatePaid(row.Cells["Column21"].Value.ToString(), row.Cells["Column7"].Value.ToString());

                    }
                }
            //} if (paidResRep == DialogResult.No)
              //  {
                //    this.Alert("Testing Record Updated!", Toaster.enmType.Success);
                    //MessageBox.Show(reportDataTableGirdView.CurrentRow.Cells["id"].Value.ToString());
                //}
        }

        private void bunifuButton11_Click_1(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {

        }

        private void timer2_Tick_1(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                this.adddetailButton.RightIcon.Image = Properties.Resources.icons8_expand_arrow_45px;
                panelAddDetail.Height += 10;
                if (panelAddDetail.Size == panelAddDetail.MaximumSize)
                {
                    timer2.Stop();
                    isCollapsed = false;
                    //this.reportDetailSilverTextBox.Select();
                    // this.ActiveControl = this.reportDetailSilverTextBox;
                }
            }
            else
            {
                this.adddetailButton.RightIcon.Image = Properties.Resources.icons8_collapse_arrow_45px;
                panelAddDetail.Height -= 10;
                if (panelAddDetail.Size == panelAddDetail.MinimumSize)
                {
                    timer2.Stop();
                    isCollapsed = true;
                }
            }
        }

        private void panelAddDetail_Paint(object sender, PaintEventArgs e)
        {

        }

        private void currentTimebox_ValueChanged(object sender, EventArgs e)
        {

        }

        private void serialNoTextBox_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel19_Click(object sender, EventArgs e)
        {

        }

        private void originalWeightTextBox_Leave(object sender, EventArgs e)
        {


            /* string your_String = this.goldPurityTextBox.Text.ToString();
             string my_String = Regex.Replace(your_String, @"[^0-9]+", "");
             int my_newlength = my_String.Length;
             if (my_String.Length > 3)
             {
                 string resultValue = my_String.Substring(0, 3);
                 int parseNo = int.Parse(resultValue);
                 double karatValue = Math.Round((parseNo / 1000.0) * 24, 1);
                 this.goldKaratTextBox.Text = karatValue.ToString();
             }
             else if (my_String.Length < 3)
             {
                 string resultValue = my_String + "0";
                 int parseNo = int.Parse(resultValue);
                 double karatValue = Math.Round((parseNo / 1000.0) * 24, 1);
                 this.goldKaratTextBox.Text = karatValue.ToString();
             }
             else if (my_String.Length == 3)
             {
                 int parseNo = int.Parse(my_String);
                 double karatValue = Math.Round((parseNo / 1000.0) * 24, 1);
                 this.goldKaratTextBox.Text = karatValue.ToString();
             }*/

            /*  else
              {
                  int karatValue = 24;
                  this.goldKaratTextBox.Text = karatValue.ToString();
              } */

        }

        private void bunifuLabel22_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel23_Click_1(object sender, EventArgs e)
        {

        }

        private void reportDetailSilverTextBox_Leave(object sender, EventArgs e)
        {
            this.calculteTotalPercantage();
            if (reportDetailSilverTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailSilverTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailSilverTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
        }
        private void calculteTotalPercantage()
        {
            decimal v1 = 0;
            decimal v2 = 0;
            decimal v3 = 0;
            decimal v4 = 0;
            decimal v5 = 0;
            decimal v6 = 0;
            decimal v7 = 0;
            decimal v8 = 0;
            decimal v9 = 0;
            decimal v10 = 0;
            decimal v11 = 0;
            decimal v12 = 0;
            decimal v13 = 0;
            decimal v14 = 0;
            decimal v15 = 0;
            decimal v16 = 0;
            decimal v17 = 0;
            decimal v18 = 0;
            decimal v19 = 0;
            decimal v20 = 0;
            decimal v21 = 0;
            decimal v22 = 0;
            decimal v23 = 0;
            decimal v24 = 0;
            decimal v25 = 0;

            if (decimal.TryParse(this.reportDetailSilverTextBox.Text, out v1) &
                decimal.TryParse(this.reportDetailCobaltTextBox.Text, out v2) &
                decimal.TryParse(this.reportDetailLeadTextBox.Text, out v3) &
                decimal.TryParse(this.reportDetailRutheniumTextBox.Text, out v4) &
                decimal.TryParse(this.reportDetailIridiumTextBox.Text, out v5) &
                decimal.TryParse(this.reportDetailRhodiumTextBox.Text, out v6) &
                decimal.TryParse(this.reportDetailCopperTextBox.Text, out v7) &
                decimal.TryParse(this.reportDetailPlatinumTextBox.Text, out v8) &
                decimal.TryParse(this.reportDetailChromiumTextBox.Text, out v9) &
                decimal.TryParse(this.reportDetailTungustanTextBox.Text, out v10) &
                decimal.TryParse(this.reportDetailIronTextBox.Text, out v11) &
                decimal.TryParse(this.reportDetailTinTextBox.Text, out v12) &
                decimal.TryParse(this.reportDetailZincTextBox.Text, out v13) &
                decimal.TryParse(this.reportDetailPalladiumTextBox.Text, out v14) &
                decimal.TryParse(this.reportDetailBismuthTextBox.Text, out v15) &
                decimal.TryParse(this.reportDetailManganeseTextBox.Text, out v16) &
                decimal.TryParse(this.reportDetailIndiumTextBox.Text, out v17) &
                decimal.TryParse(this.reportDetailTitaniumTextBox.Text, out v18) &
                decimal.TryParse(this.reportDetailCadmiumTextBox.Text, out v19) &
                decimal.TryParse(this.reportDetailAntimonyTextBox.Text, out v20) &
                decimal.TryParse(this.reportDetailNickelTextBox.Text, out v21) &
                decimal.TryParse(this.reportDetailOsmiumTextBox.Text, out v22) &
                decimal.TryParse(this.reportDetailRehniumTextBox.Text, out v23) &
                decimal.TryParse(this.reportDetailGalliumTextBox.Text, out v24) &
                decimal.TryParse(this.goldPurityTextBox.Text, out v25)
                )
            {
                decimal totalPercentage = (v1 + v2 + v3 + v4 + v5 + v6 + v7 + v8 + v9 + v10 + v11 + v12 + v13 + v14 + v15 + v16 + v17 + v18 + v19 + v20
                    + v21 + v22 + v23 + v24 + v25);
                this.totalPercentageLabel.Text = totalPercentage.ToString("0.00");

            }
        }

        private void reportDetailCobaltTextBox_Leave(object sender, EventArgs e)
        {
            this.calculteTotalPercantage();
            if (reportDetailCobaltTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailCobaltTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailCobaltTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
        }

        private void reportDetailLeadTextBox_Leave(object sender, EventArgs e)
        {
            this.calculteTotalPercantage();
            if (reportDetailLeadTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailLeadTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailLeadTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
        }

        private void reportDetailRutheniumTextBox_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void reportDetailRutheniumTextBox_Leave(object sender, EventArgs e)
        {
            this.calculteTotalPercantage();
            if (reportDetailRutheniumTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailRutheniumTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailRutheniumTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
        }

        private void reportDetailIridiumTextBox_Leave(object sender, EventArgs e)
        {
            this.calculteTotalPercantage();
            if (reportDetailIridiumTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailIridiumTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailIridiumTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
        }

        private void reportDetailRhodiumTextBox_Leave(object sender, EventArgs e)
        {
            this.calculteTotalPercantage();
            if (reportDetailRhodiumTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailRhodiumTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailRhodiumTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
        }

        private void reportDetailCopperTextBox_Leave(object sender, EventArgs e)
        {
            this.calculteTotalPercantage();
            if (reportDetailCopperTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailCopperTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailCopperTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
        }

        private void reportDetailPlatinumTextBox_Leave(object sender, EventArgs e)
        {
            this.calculteTotalPercantage();
            if (reportDetailPlatinumTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailPlatinumTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailPlatinumTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
        }

        private void reportDetailChromiumTextBox_Leave(object sender, EventArgs e)
        {
            this.calculteTotalPercantage();
            if (reportDetailChromiumTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailChromiumTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailChromiumTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
        }

        private void reportDetailTungustanTextBox_Leave(object sender, EventArgs e)
        {
            this.calculteTotalPercantage();
            if (reportDetailTungustanTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailTungustanTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailTungustanTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }

        }

        private void reportDetailIronTextBox_Leave(object sender, EventArgs e)
        {
            this.calculteTotalPercantage();
            if (reportDetailIronTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailIronTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailIronTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
        }

        private void reportDetailTinTextBox_Leave(object sender, EventArgs e)
        {
            this.calculteTotalPercantage();
            if (reportDetailTinTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailTinTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailTinTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
        }

        private void reportDetailZincTextBox_Leave(object sender, EventArgs e)
        {
            if (reportDetailZincTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailZincTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailZincTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
            this.calculteTotalPercantage();
        }

        private void reportDetailPalladiumTextBox_Leave(object sender, EventArgs e)
        {
            if (reportDetailPalladiumTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailPalladiumTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailPalladiumTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
            this.calculteTotalPercantage();
        }

        private void reportDetailBismuthTextBox_Leave(object sender, EventArgs e)
        {
            if (reportDetailBismuthTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailBismuthTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailBismuthTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
            this.calculteTotalPercantage();
        }

        private void reportDetailManganeseTextBox_Leave(object sender, EventArgs e)
        {
            if (reportDetailManganeseTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailManganeseTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailManganeseTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
            this.calculteTotalPercantage();
        }

        private void reportDetailIndiumTextBox_Leave(object sender, EventArgs e)
        {
            if (reportDetailIndiumTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailIndiumTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailIndiumTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
            this.calculteTotalPercantage();
        }

        private void reportDetailTitaniumTextBox_Leave(object sender, EventArgs e)
        {
            if (reportDetailTitaniumTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailTitaniumTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailTitaniumTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
            this.calculteTotalPercantage();
        }

        private void reportDetailCadmiumTextBox_Leave(object sender, EventArgs e)
        {
            if (reportDetailCadmiumTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailCadmiumTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailCadmiumTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
            this.calculteTotalPercantage();
        }

        private void reportDetailAntimonyTextBox_Leave(object sender, EventArgs e)
        {
            if (reportDetailAntimonyTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailAntimonyTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailAntimonyTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
            this.calculteTotalPercantage();
        }

        private void reportDetailNickelTextBox_Leave(object sender, EventArgs e)
        {
            if (reportDetailNickelTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailNickelTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailNickelTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
            this.calculteTotalPercantage();
        }

        private void reportDetailOsmiumTextBox_Leave(object sender, EventArgs e)
        {
            if (reportDetailOsmiumTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailOsmiumTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailOsmiumTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
            this.calculteTotalPercantage();
        }

        private void reportDetailRehniumTextBox_Leave(object sender, EventArgs e)
        {
            if (reportDetailRehniumTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailRehniumTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailRehniumTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
            this.calculteTotalPercantage();
        }

        private void reportDetailGalliumTextBox_Leave(object sender, EventArgs e)
        {
            if (reportDetailGalliumTextBox.Text.Length == 4)
            {
                string myString = this.reportDetailGalliumTextBox.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
                //  newValue.ToString("#.##");
                this.reportDetailGalliumTextBox.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }
            this.calculteTotalPercantage();
            
            this.timer2.Start();
            this.reportSaveButton.Select();
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            DateTime dateTime = DateTime.Now;
            this.reportDatePicker.Text = dateTime.ToString();
            this.currentTimebox.Text = dateTime.ToString();
            this.tokenDatePicker.Text = dateTime.ToString();
            this.tokenDateTimePicker.Text = dateTime.ToString();
            this.expenseDate.Text = dateTime.ToString();
            this.expenseTime.Text = dateTime.ToString();
            this.testDate.Text = dateTime.ToString();
            this.testTime.Text = dateTime.ToString();
            this.selectDate.Text = dateTime.ToString();
            this.startButton.Visible = false;
        }

        private void currentTimebox_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void currentTimebox_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void currentTimebox_DragDrop(object sender, DragEventArgs e)
        {

        }

        private void currentTimebox_MouseEnter(object sender, EventArgs e)
        {

        }

        private void currentTimebox_DropDown(object sender, EventArgs e)
        {

        }

        private void currentTimebox_TabStopChanged(object sender, EventArgs e)
        {
            ;
        }

        private void currentTimebox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

        }

        private void bunifuButton7_Click_2(object sender, EventArgs e)
        {

            timer3.Stop();
            this.startStopTimerButton.Visible = false;
            this.startButton.Visible = true;
            this.startButtonView = true;
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            timer3.Start();
            this.startStopTimerButton.Visible = true;
            this.startButton.Visible = false;
            this.startButtonView = false;
        }

        private void bunifuDatepicker1_onValueChanged(object sender, EventArgs e)
        {
        }

        private void bunifuLabel22_Click_1(object sender, EventArgs e)
        {

        }

        private void bunifuLabel24_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel28_Click(object sender, EventArgs e)
        {

        }

        private void Dashboard_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void bunifuLabel49_Click(object sender, EventArgs e)
        {

        }

        private void reportDetailOsmiumTextBox_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuLabel48_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel63_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel51_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton11_Click_2(object sender, EventArgs e)
        {
            this.SuspendLayout();
            indicator.Location = new Point(3, 364);
            this.ResumeLayout();
            pagesForm.SetPage("Token");
            this.getTokenReportlastId();
            this.getSampleTableItemslastId();
            this.CheckSampleUnwanttedData();
            this.getTokenListFromdb();

        }

        private void getTokenListFromdb()
        {
            Database databaseObject = new Database();
           
            string query = "SELECT * FROM tokenTable INNER JOIN customerTable ON tokenTable.customerId = customerTable.id WHERE tokenTable.date = '" + this.tokenDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'  ORDER BY id DESC";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                this.tokenListDataGrid.DataSource = dt;

                databaseObject.CloseConnection();

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }

        private void CheckSampleUnwanttedData()
        {
            if (this.sampleItemlastId == Int32.Parse(this.tokenSeries.Text))
            {
                this.tokenSampleDeletebutton.Enabled = true;
            }
            else
            {
                this.tokenSampleDeletebutton.Enabled = false;
            }

            this.gettokenItemDataFormdb();
        }

        private void getTokenReportlastId()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM tokenTable WHERE id = (SELECT MAX(id) FROM tokenTable)";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataReader result = command.ExecuteReader();

                while (result.Read())
                {

                    int tokenLastId = Int32.Parse(result["id"].ToString());
                    int tokenLastSeries = Int32.Parse(result["series"].ToString());
                    this.tokenSeriesno.Text = (tokenLastSeries + 1).ToString();
                    this.tokenlatestReportId = (tokenLastId + 1).ToString();
                    this.tokenSeries.Text = (++tokenLastId).ToString();


                }



                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

        private void bunifuButton12_Click_1(object sender, EventArgs e)
        {
            this.SuspendLayout();
            indicator.Location = new Point(3, 409);
            this.ResumeLayout();
            pagesForm.SetPage("Testing");
            this.gettestinglastId();
            this.testingDataGridDbData();
        }

        private void testingDataGridDbData()
        {
            Database databaseObject = new Database();
            ;
            // string query = " SELECT * FROM reportTable INNER JOIN customerTable ON reportTable.customerId = customerTable.id ORDER BY id DESC";
            string query = "SELECT * FROM testingTable INNER JOIN customerTable ON testingTable.testcustId = customerTable.id WHERE testingTable.date = '" + this.testDate.Value.Date.ToString("yyyy-MM-dd") + "'  ORDER BY id DESC";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                this.testDatagridView.AutoGenerateColumns = false;
                this.testDatagridView.DataSource = dt;
                databaseObject.CloseConnection();
                int count = this.testDatagridView.RowCount - 1;

                foreach (DataRow pRow in dt.Rows)
                {


                }

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }


        private void label6_Click_2(object sender, EventArgs e)
        {

        }

        private void originalWeightTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void tounchWeightTextBox_Leave(object sender, EventArgs e)
        {
            if (this.goldPurityTextBox.Text != "" && this.tounchWeightTextBox.Text != "")
            {
                string goldper_string = this.goldPurityTextBox.Text.ToString();
                string goldWeight_stirng = this.tounchWeightTextBox.Text.ToString();
                float parseNogoldPer = float.Parse(goldper_string);
                float parseGoldweight = float.Parse(goldWeight_stirng);
                double goldFine = Math.Round(((parseGoldweight * parseNogoldPer) / 100), 3);
                this.goldFineTextBox.Text = goldFine.ToString();
            }
        }

        private void bunifuButton10_Click_1(object sender, EventArgs e)
        {

            if (this.custMastNametextbox.Text.ToString() != "" && this.custMastMobiletextbox.Text.ToString() != "" && this.custMastAddresstextbox.Text.ToString() != "" && this.custMastRatetextbox.Text.ToString() != "" && this.custMastFirmNametextbox.Text.ToString() != "")
            {
                DialogResult mod = MessageBox.Show("Are you sure want to Modify this Record", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (mod == DialogResult.OK)
                {


                    Database databaseObject = new Database();
                    string query = "UPDATE customerTable set customerName = @customerName,custMobile = @custMobile,Rate = @Rate, customerAddress = @customerAddress,custFirm = @custFirm where id = @id";
                    //  string query = "UPDATE customerTable (customerName,customerAddress,custMobile,Rate,custFirm) VALUES('" + this.custMastNametextbox.Text.ToString() + "','" + this.custMastAddresstextbox.Text.ToString() + "','" + this.custMastMobiletextbox.Text.ToString() + "','" + this.custMastRatetextbox.Text.ToString() + "','" + this.custMastFirmNametextbox.Text.ToString() + "')";
                    SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

                    try
                    {
                        databaseObject.OpenConnection();
                        command.Parameters.Add(new SQLiteParameter("@id", this.getCustIdToModify));
                        command.Parameters.Add(new SQLiteParameter("@customerName", info.ToTitleCase(this.custMastNametextbox.Text.ToLower())));
                        command.Parameters.Add(new SQLiteParameter("@custMobile", this.custMastMobiletextbox.Text));
                        command.Parameters.Add(new SQLiteParameter("@Rate", this.custMastRatetextbox.Text));
                        command.Parameters.Add(new SQLiteParameter("@customerAddress", info.ToTitleCase(this.custMastAddresstextbox.Text.ToLower())));
                        command.Parameters.Add(new SQLiteParameter("@custFirm", info.ToTitleCase(this.custMastFirmNametextbox.Text.ToLower())));
                        SQLiteDataReader result = command.ExecuteReader();


                        while (result.Read())
                        {


                        }
                        databaseObject.CloseConnection();

                        this.clearCustomerReport();
                        this.Alert("Customer Record Updated!", Toaster.enmType.Success);
                        this.getCustomerDataFormdb();
                        this.custMastNametextbox.Focus();

                    }
                    catch (Exception es)
                    {
                        MessageBox.Show(es.Message);
                    }
                }
                else if (mod == DialogResult.Cancel)
                {

                }

            }
            else
            {

                this.Alert("Please Select The Customer Data First", Toaster.enmType.Warning);
                this.custMastNametextbox.Select();
            }

        }

        private void bunifuCustomDataGrid1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void bunifuCustomDataGrid1_CellContentClick_2(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void reportDataTableGirdView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void bunifuButton7_Click_3(object sender, EventArgs e)
        {
            //BindingSource bs = new BindingSource();
            // bs.DataSource = allReportDataGrid.DataSource;
            // bs.Filter = allReportDataGrid.Columns[6].HeaderText.ToString() + " LIKE '%" + filterMobileNo.Text + "%'";
            //allReportDataGrid.DataSource = bs;
            this.getAllReportDatagrid();
           
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void allReportDataGrid_SortStringChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = allReportDataGrid.DataSource;
            bs.Sort = this.allReportDataGrid.SortString;
            allReportDataGrid.DataSource = bs;
        }

        private void allReportDataGrid_FilterStringChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = allReportDataGrid.DataSource;
            bs.Filter = this.allReportDataGrid.FilterString;
            allReportDataGrid.DataSource = bs;
        }

        private void allReportDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }




        private void bunifuButton13_Click_1(object sender, EventArgs e)
        {
            this.clearImage();
           // printRdlc_Form = new PrintFolder.PrintRdlc_Form();
            //printRdlc_Form.calldatda("42");
         

        }

        private void bunifuButton14_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox3_OnValueChanged_1(object sender, EventArgs e)
        {

        }

        private void bunifuLabel32_Click_1(object sender, EventArgs e)
        {

        }

        private void label10_Click_1(object sender, EventArgs e)
        {

        }

        private void tokenPage_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel40_Click(object sender, EventArgs e)
        {

        }

        private void bunifuTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void tokenCustomerBox_SelectedIndexChanged(object sender, EventArgs e)
        {
           // if (this.tokenCustomerBox.SelectedIndex != -1)
            //{
              //  this.getTokenSelectedCustomerData();

            //}
        }

        private void getTokenSelectedCustomerData()
        {
            {

                Database databaseObject = new Database();
                string query = "SELECT customerName,customerAddress,custMobile,Rate FROM customerTable WHERE id=" + this.tokecustcomboBox2.SelectedValue + "";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);



                try
                {
                    databaseObject.OpenConnection();
                    SQLiteDataReader result = command.ExecuteReader();
                    if (result.HasRows)
                    {
                        while (result.Read())
                        {

                            this.tokenMobile.Text = result["custMobile"].ToString();
                            this.tokenAddress.Text = result["customerAddress"].ToString();
                            this.tokenRate.Text = result["Rate"].ToString();

                        }
                    }

                    databaseObject.CloseConnection();
                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }

        }

        private void tokenAddButton_Click(object sender, EventArgs e)
        {
            if ( (this.tokecustcomboBox2.SelectedItem == null || Int32.Parse(this.tokecustcomboBox2.SelectedValue.ToString()) == 0 ) && this.tokecustcomboBox2.Text == "")
            {
                this.Alert("Fill The Customer Data First!", Toaster.enmType.Warning);
            }
            else
            {


                if (this.tokenSample.Text.ToString() != "" && this.tokenWeightText.Text.ToString() != "")
                {
                    if (this.sampleItemlastId != Int32.Parse(this.tokenSeries.Text))
                    {
                        this.itemId = 1;
                    }
                    else
                    {
                        this.itemId = this.itemId + 1;
                    }

                    Database databaseObject = new Database();
                    string query = "INSERT INTO sampleTable (weight,sSample,tokenId,itemNo ) VALUES('" + this.tokenWeightText.Text.ToString() + "','" + info.ToTitleCase(this.tokenSample.Text.ToLower()) + "','" + this.tokenSeries.Text.ToString() + "','" + this.itemId.ToString() + "')";
                    SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
                    try
                    {
                        databaseObject.OpenConnection();
                        SQLiteDataReader result = command.ExecuteReader();


                        while (result.Read())
                        {


                        }
                        databaseObject.CloseConnection();

                        this.clearTokenSample();
                        this.Alert("Sample Record Added!", Toaster.enmType.Success);
                        this.gettokenItemDataFormdb();
                        this.CheckSampleUnwanttedData();
                        this.tokenTotal.Text = ((tokenSampleDataGrid.RowCount) * Int32.Parse(this.tokenRate.Text)).ToString();



                    }
                    catch (Exception es)
                    {
                        MessageBox.Show("TUM AAAA ARA ARAA", es.Message);
                    }

                }
            }


        }

        private void gettokenItemDataFormdb()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM sampleTable WHERE tokenId = " + this.tokenlatestReportId + "";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                this.tokenSampleDataGrid.DataSource = dt;

                databaseObject.CloseConnection();

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }
        private void getCustomerTokendataId()
        {
            if (this.tokecustcomboBox2.Text.ToString() != "" && this.tokenAddress.Text.ToString() != "" && this.tokenMobile.Text.ToString() != "" && this.tokenRate.Text.ToString() != "")
            {
                Database databaseObject = new Database();
                string query = "SELECT * FROM customerTable WHERE id = (SELECT MAX(id) FROM customerTable)";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
                try
                {
                    databaseObject.OpenConnection();
                    SQLiteDataReader result = command.ExecuteReader();

                    while (result.Read())
                    {
                        int lastId = Int32.Parse(result["id"].ToString());

                        this.genratedTokenCustId = lastId;
                    }


                    databaseObject.CloseConnection();
                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }
            // else
            //{
            //  this.Alert("Fill The Customer Data First!", Toaster.enmType.Info);
            // MessageBox.Show("Please Fill The Customer Data First");
            //}
        }

        private void createCustomerFromTokenData()
        {
            if (this.tokecustcomboBox2.Text.ToString() != "" && this.tokenMobile.Text.ToString() != "" && this.tokenAddress.Text.ToString() != "" && this.tokenRate.Text.ToString() != "")
            {
                Database databaseObject = new Database();
                string query = "INSERT INTO customerTable (customerName,customerAddress,custMobile,Rate ) VALUES('" + info.ToTitleCase(this.tokecustcomboBox2.Text.ToLower()) + "','" + info.ToTitleCase(this.tokenAddress.Text.ToLower()) + "','" + this.tokenMobile.Text.ToString() + "','" + this.tokenRate.Text.ToString() + "')";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
                try
                {
                    databaseObject.OpenConnection();
                    SQLiteDataReader result = command.ExecuteReader();



                    this.getCustomerTokendataId();
                    databaseObject.CloseConnection();

                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }
            else
            {
                this.Alert("Fill The Customer Data First!", Toaster.enmType.Info);
                //MessageBox.Show("Please Fill The Customer Data First");
            }
        }

        private void bunifuButton18_Click(object sender, EventArgs e)
        {
            int rowCount = this.tokenSampleDataGrid.RowCount;
            if (rowCount > 0 )
            {
                DialogResult resP = MessageBox.Show("Are you sure want to Print", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (resP == DialogResult.No)
            {
                if (this.tokecustcomboBox2.SelectedIndex == -1)
                {
                    this.createCustomerFromTokenData();

                }
                else
                {
                    this.genratedTokenCustId = Int32.Parse(this.tokecustcomboBox2.SelectedValue.ToString());
                }
                if (this.genratedTokenCustId > 0)
                {
                        
                        Database databaseObject = new Database();
                        string query = "INSERT INTO tokenTable (customerId,date,time,totalAmount,Quantity,series) " +
                            "VALUES('" + this.genratedTokenCustId.ToString() + "','" + this.tokenDatePicker.Value.Date.ToString("yyyy-MM-dd") + "','" + this.tokenDateTimePicker.Value.ToShortTimeString() + "','" + this.tokenTotal.Text + "','" + rowCount.ToString() + "','" + this.tokenSeriesno.Text + "')";
                        SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);


                        try
                        {

                            databaseObject.OpenConnection();
                            SQLiteDataReader result = command.ExecuteReader();
                            databaseObject.CloseConnection();
                            this.createReportFromToken(this.tokenSeries.Text);
                            this.clearTokenReportControl();
                            this.getTokenListFromdb();


                        }
                        catch (Exception es)
                        {
                            MessageBox.Show(es.Message);
                        }
                   
                }


            }
            else if (resP == DialogResult.Yes)
            {
                this.saveAndPrintTokenSlip();
            }
            }
            else
            {
                this.Alert("Please Add the Sample First!", Toaster.enmType.Info);
            }
        }

        private void saveAndPrintTokenSlip()
        {

            if (this.tokecustcomboBox2.SelectedIndex == -1)
            {
                this.createCustomerFromTokenData();

            }
            else
            {
                this.genratedTokenCustId = Int32.Parse(this.tokecustcomboBox2.SelectedValue.ToString());
            }
            if (this.genratedTokenCustId > 0)
            {
                int rowCount = this.tokenSampleDataGrid.RowCount;
              
                    Database databaseObject = new Database();
                    string query = "INSERT INTO tokenTable (customerId,date,time,totalAmount,Quantity,series) " +
                        "VALUES('" + this.genratedTokenCustId.ToString() + "','" + this.tokenDatePicker.Value.Date.ToString("yyyy-MM-dd") + "','" + this.tokenDateTimePicker.Value.ToShortTimeString() + "','" + this.tokenTotal.Text + "','" + rowCount.ToString() + "','" + this.tokenSeriesno.Text + "')";
                    SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);


                    try
                    {

                        databaseObject.OpenConnection();
                        SQLiteDataReader result = command.ExecuteReader();
                        databaseObject.CloseConnection();
                        print_tokenForm = new PrintTokenSlip.Print_tokenForm();
                        print_tokenForm.callTokendata(this.tokenSeries.Text);
                        this.createReportFromToken(this.tokenSeries.Text);
                        this.clearTokenReportControl();
                        this.getTokenListFromdb();


                    }
                    catch (Exception es)
                    {
                        MessageBox.Show(es.Message);
                    }
              
            }
        }
        private  void createReportFromToken(string LatestTokenId)
        {
          

            Database databaseObject = new Database();
            
            // string query = " SELECT * FROM reportTable INNER JOIN customerTable ON reportTable.customerId = customerTable.id ORDER BY id DESC";
            string query = "SELECT * FROM tokenTable INNER JOIN customerTable ON tokenTable.customerId = customerTable.id INNER JOIN sampleTable ON tokenTable.id = sampleTable.tokenId  WHERE tokenTable.id = @Id"; 
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                command.Parameters.Add(new SQLiteParameter("@Id", LatestTokenId ));
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                sda.Fill(dt);




                foreach (DataRow pRow in dt.Rows)
                {
                    //MessageBox.Show("AA ", pRow["sSample"].ToString());
                 
                    Database databaseObject2 = new Database();
                    string query2 = "INSERT INTO reportTable (seriesNo,currentDate,sample,customerId,tounchweight,currentTime,payment,amount) " +
                        "VALUES('" + this.seriesNoTexBox.Text + "','" + pRow["date"].ToString() + "','" + pRow["sSample"].ToString() + "','" + pRow["customerId"].ToString() + "','" + pRow["weight"].ToString() + "','" + pRow["time"].ToString() + "','" + "True" + "','" + pRow["rate"].ToString() + "')";
                    SQLiteCommand command2 = new SQLiteCommand(query2, databaseObject2.myConn);


                    try
                    {

                        databaseObject2.OpenConnection();
                        SQLiteDataReader result2 = command2.ExecuteReader();
                        
                        this.createReportDetailId(this.latestReportId);
                        this.clearReportControl();

                        while (result2.Read())
                        {
                            
                        }


                        databaseObject2.CloseConnection();

                       // this.createReportDetailId(this.latestReportId);
                       databaseObject.CloseConnection();
                    }
                    catch (Exception esE)
                    {
                        MessageBox.Show(esE.Message);
                    }

                }
            } 
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        


    }
        private void clearTokenReportControl()
        {
            this.tokecustcomboBox2.ResetText();
           // this.tokecustcomboBox2.Text = "";
           // this.tokecustcomboBox2.SelectedValue = 0;
            this.tokenRate.Text = this.reportDefaultRate.ToString();
            this.tokenMobile.Text = this.defaultMobileNo.ToString();
            this.tokenAddress.Text = this.defaultAddress.ToString();
            this.tokenTotal.Text = "";
            this.getTokenReportlastId();
            this.gettokenItemDataFormdb();
            this.tokecustcomboBox2.Focus();
        }


        private void getSampleTableItemslastId()
        {
            Database databaseObject = new Database();
            string query = "SELECT * FROM sampleTable WHERE id = (SELECT MAX(id) FROM sampleTable)";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataReader result = command.ExecuteReader();

                while (result.Read())
                {
                    //this.customerTextBox.Items.Add(result["customerName"]);
                    int sampleItemNolastId = Int32.Parse(result["tokenId"].ToString());
                    this.itemId = Int32.Parse(result["itemNo"].ToString());
                    this.sampleItemlastId = sampleItemNolastId;



                }



                databaseObject.CloseConnection();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

    


  
        private void bunifuButton19_Click(object sender, EventArgs e)
        {
            if (this.sampleItemlastId == Int32.Parse(this.tokenSeries.Text))
            {
                Database databaseObject = new Database();
                string query = "DELETE FROM sampleTable WHERE tokenId = @newId";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

                try
                {
                    databaseObject.OpenConnection();
                    command.Parameters.Add(new SQLiteParameter("@newId", tokenlatestReportId));
                    SQLiteDataReader result = command.ExecuteReader();




                    this.Alert("Sample Deleted Sucessfully!", Toaster.enmType.Success);
                    databaseObject.CloseConnection();
                    this.getSampleTableItemslastId();
                    this.getTokenReportlastId();
                    this.gettokenItemDataFormdb();
                    this.CheckSampleUnwanttedData();

                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }

        }

        private void custMastMobiletextbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)
               )
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void custMastMobiletextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }
        }

        private void custMastMobiletextbox_Leave(object sender, EventArgs e)
        {
            if (this.custMastMobiletextbox.Text.Length != 10)
            {
                this.Alert("Please Enter 10 Digit Mobile No.", Toaster.enmType.Info);
               // this.custMastMobiletextbox.Select();
            }
        }

        private void iconshowareabox_Click_1(object sender, EventArgs e)
        {
           /* OpenFileDialog open = new OpenFileDialog();
            // image filters  
            open.Filter = "Image Files(*.png)|*.png";
            if (open.ShowDialog() == DialogResult.OK)
            {
                // display image in picture box  
               // this.iconshowareabox.Image = new Bitmap(open.FileName);
                // image file path  
                this.bunifuMetroTextbox4.Text = open.FileName;
            }
            else
            {
                // MessageBox.Show("ni aya ");
            }*/
        }

        private void customerMobileTextBox_Leave(object sender, EventArgs e)
        {
            if (this.customerMobileTextBox.Text.Length != 10)
            {
                this.Alert("Please Enter 10 Digit Mobile No.", Toaster.enmType.Info);
               // this.customerMobileTextBox.Select();
            }
        }

        private void customerMobileTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }
        }

        private void reportDatePicker_ValueChanged(object sender, EventArgs e)
        {
            //this.getReportTableDatagrid();
        }

        private void bunifuButton16_Click(object sender, EventArgs e)
        {
            print_tokenForm = new PrintTokenSlip.Print_tokenForm();
            print_tokenForm.callTokendata("27");
            //this.createReportFromToken();

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton14_Click_1(object sender, EventArgs e)
        {
            this.SuspendLayout();
            indicator.Location = new Point(3, 454);
            this.ResumeLayout();
            pagesForm.SetPage("Expenses");
            this.getExpensestlastId();
            this.getExpensesDataFormdb();
        }

        private void bunifuButton15_Click(object sender, EventArgs e)
        {
            this.SuspendLayout();
            indicator.Location = new Point(3, 499);
            this.ResumeLayout();
            pagesForm.SetPage("sendReport");
            this.getsendReportTableDatagrid();
        }

        private void label7_Click_1(object sender, EventArgs e)
        {

        }

        private void bunifuLabel85_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel82_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox12_OnValueChanged_1(object sender, EventArgs e)
        {

        }

        private void bunifuLabel83_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox10_OnValueChanged_1(object sender, EventArgs e)
        {

        }

        private void bunifuButton26_Click(object sender, EventArgs e)
        {

            if (this.expenseRemark.Text != "" && this.expensesAmount.Text != "")
            {
                DialogResult sav = MessageBox.Show("Are you sure Want to Save", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (sav == DialogResult.OK)
                {

                    Database databaseObject = new Database();
                    string query = "INSERT INTO expensesTable (date,time,remarks,expenses) VALUES('" + this.expenseDate.Value.Date.ToString("yyyy-MM-dd") + "','" + this.expenseTime.Value.ToShortTimeString() + "','" + info.ToTitleCase(this.expenseRemark.Text.ToLower()) + "','" + this.expensesAmount.Text.ToString() + "')";
                    SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
                    try
                    {
                        databaseObject.OpenConnection();
                        SQLiteDataReader result = command.ExecuteReader();


                        while (result.Read())
                        {


                        }
                        databaseObject.CloseConnection();

                        this.clearExpensesReport();
                        this.Alert("Expenses Record Added!", Toaster.enmType.Success);
                        this.getExpensesDataFormdb();
                        this.expenseRemark.Focus();

                    }
                    catch (Exception es)
                    {
                        MessageBox.Show(es.Message);
                    }

                }
                else if (sav == DialogResult.Cancel)
                {

                }

            }
            else
            {

                this.Alert("Please Fill The Expenses Data First", Toaster.enmType.Warning);
                this.expenseRemark.Select();
            }


        }

        private void bunifuMetroTextbox9_OnValueChanged_2(object sender, EventArgs e)
        {

        }

        private void bunifuTextBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void expenses_Click(object sender, EventArgs e)
        {

        }

        private void expensesDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.expensesDataGridView.Rows[e.RowIndex];
                this.expenseId.Text = row.Cells["dataGridViewTextBoxColumn49"].Value.ToString();
                this.expenseRemark.Text = row.Cells["dataGridViewTextBoxColumn51"].Value.ToString();
                this.expensesAmount.Text = row.Cells["dataGridViewTextBoxColumn53"].Value.ToString();

            }
        }

        private void bunifuButton25_Click(object sender, EventArgs e)
        {
            this.clearExpensesReport();
            this.expenseRemark.Focus();
        }

        private void expensesAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }

        }

        private void expensesAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
             (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Number Only!");
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }

        }

        private void expenseRemark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                //   MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }

        }

        private void expenseRemark_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && char.IsDigit(e.KeyChar) &&
               (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Character Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //      MessageBox.Show("This Field Accepts Character Only!");
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }

        }

        private void bunifuButton20_Click(object sender, EventArgs e)
        {
            if (this.expenseRemark.Text.ToString() != "" && this.expensesAmount.Text.ToString() != "")
            {
                DialogResult mod = MessageBox.Show("Are you sure want to Modify this Record", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (mod == DialogResult.OK)
                {


                    Database databaseObject = new Database();
                    string query = "UPDATE expensesTable set remarks = @remarks, expenses = @expenses where id = @id";
                    //  string query = "UPDATE customerTable (customerName,customerAddress,custMobile,Rate,custFirm) VALUES('" + this.custMastNametextbox.Text.ToString() + "','" + this.custMastAddresstextbox.Text.ToString() + "','" + this.custMastMobiletextbox.Text.ToString() + "','" + this.custMastRatetextbox.Text.ToString() + "','" + this.custMastFirmNametextbox.Text.ToString() + "')";
                    SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

                    try
                    {
                        databaseObject.OpenConnection();
                        command.Parameters.Add(new SQLiteParameter("@id", this.expenseId.Text.ToString()));

                        command.Parameters.Add(new SQLiteParameter("@remarks", info.ToTitleCase(this.expenseRemark.Text)));
                        command.Parameters.Add(new SQLiteParameter("@expenses", this.expensesAmount.Text));

                        SQLiteDataReader result = command.ExecuteReader();


                        while (result.Read())
                        {


                        }
                        databaseObject.CloseConnection();

                        this.clearExpensesReport();
                        this.Alert("Expense Record Updated!", Toaster.enmType.Success);
                        this.getExpensesDataFormdb();
                        this.expenseRemark.Focus();

                    }
                    catch (Exception es)
                    {
                        MessageBox.Show(es.Message);
                    }
                }
                else if (mod == DialogResult.Cancel)
                {

                }

            }
            else
            {

                this.Alert("Please Select The Expense Data First", Toaster.enmType.Warning);
                this.expenseRemark.Select();
            }
        }

        private void bunifuButton27_Click(object sender, EventArgs e)
        {
            if (this.expenseRemark.Text.ToString() != "" && this.expensesAmount.Text.ToString() != "")
            {
                DialogResult Expres = MessageBox.Show("Are you sure want to Delete this Expense", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (Expres == DialogResult.OK)
                {

                    Database databaseObject = new Database();
                    string query = "DELETE FROM expensesTable  where id = @id";
                    //  string query = "UPDATE customerTable (customerName,customerAddress,custMobile,Rate,custFirm) VALUES('" + this.custMastNametextbox.Text.ToString() + "','" + this.custMastAddresstextbox.Text.ToString() + "','" + this.custMastMobiletextbox.Text.ToString() + "','" + this.custMastRatetextbox.Text.ToString() + "','" + this.custMastFirmNametextbox.Text.ToString() + "')";
                    SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

                    try
                    {
                        databaseObject.OpenConnection();
                        command.Parameters.Add(new SQLiteParameter("@id", this.expenseId.Text.ToString()));

                        SQLiteDataReader result = command.ExecuteReader();


                        while (result.Read())
                        {


                        }
                        databaseObject.CloseConnection();

                        this.clearExpensesReport();
                        this.Alert("Customer Record Deleted!", Toaster.enmType.Success);
                        this.getExpensesDataFormdb();
                        this.expenseRemark.Focus();

                    }
                    catch (Exception es)
                    {
                        MessageBox.Show(es.Message);
                    }
                }
                else if (Expres == DialogResult.Cancel)
                {

                }

            }
            else
            {

                this.Alert("Please Select The Expense Data First", Toaster.enmType.Warning);
                this.expenseRemark.Select();
            }

        }

        private void tokenMobile_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }

        }

        private void tokenMobile_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)
            )
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }

        }

        private void tokenMobile_Leave(object sender, EventArgs e)
        {
            if (this.tokenMobile.Text.Length != 10)
            {
                this.Alert("Please Enter 10 Digit Mobile No.", Toaster.enmType.Info);
                //this.tokenMobile.Select();
            }

        }

        private void tokenRate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }

        }

        private void tokenRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
           (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Number Only!");
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }

        }

        private void tokenAddress_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                //   MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }

        }

        private void tokenAddress_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && char.IsDigit(e.KeyChar) &&
               (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Character Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //      MessageBox.Show("This Field Accepts Character Only!");
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }

        }

        private void tokenWeightText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }

        }

        private void tokenWeightText_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
              (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void bunifuButton21_Click(object sender, EventArgs e)
        {
            if (this.modifyTestingId != null)
            {
                DialogResult resPtEST = MessageBox.Show("Do you want to Print", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (resPtEST == DialogResult.Yes)
                {


                    printTestingRdlc_Form = new PrintTestingReport.PrintTestingForm();
                    printTestingRdlc_Form.callTestingdata(this.modifyTestingId.ToString());
                    this.clearTestingControl();
                    this.modifyTestingId = null;
                }


                else if (resPtEST == DialogResult.No)
                {
                    this.clearTestingControl();
                    this.modifyTestingId = null;
                }
            }
            else
            {
                this.Alert("Please Select Data From Testing Table! ", Toaster.enmType.Info);
            }
        }

        private void bunifuTextBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void bunifuTextBox3_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void bunifuButton22_Click(object sender, EventArgs e)
        {
            if (this.modifyTestingId == null)
            {
                if (this.testSeries.Text != "" && this.testorgweight.Text != "")
                {
                    // MessageBox.Show("aa gaya if");
                    DialogResult resP = MessageBox.Show("Are you sure want to Print", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (resP == DialogResult.No)
                    {



                        if (this.customerTextBox.SelectedIndex == -1)
                        {
                            this.Alert("Please the select Customer!", Toaster.enmType.Warning);
                        }





                        if (this.testSeries.Text != "")
                        {

                        }
                        else
                        {
                            this.Alert("Series Area Not Empty!", Toaster.enmType.Warning);

                        }







                        Database databaseObject = new Database();
                        string query = "INSERT INTO testingTable (series,date,purity,karat,sample,orgweight,testcustId,tounchweight,time,goldFine,amount,payment,type) " +
                            "VALUES('" + this.testSeries.Text + "','" + this.testDate.Value.Date.ToString("yyyy-MM-dd") + "','" + this.testPurity.Text + "','" + this.testKarat.Text + "','" + info.ToTitleCase(this.testSmaplecomboBox.Text.ToLower()) + "','" + this.testorgweight.Text + "','" + this.testCustcombo.SelectedValue.ToString() + "','" + this.testtounchweight.Text + "','" + this.testTime.Value.ToShortTimeString() + "','" + this.testgoldFine.Text + "','" + this.testAmount.Text  +"','" + "true" + "','" + this.testingDropdown.Text.ToString()+  "')";
                        SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);


                        try
                        {

                            databaseObject.OpenConnection();
                            SQLiteDataReader result = command.ExecuteReader();

                            while (result.Read())
                            {


                            }


                            databaseObject.CloseConnection();


                            this.clearTestingControl();
                          
                            this.modifyTestingId = null;



                        }
                        catch (Exception es)
                        {
                            MessageBox.Show(es.Message);
                        }





                    }
                    else if (resP == DialogResult.Yes)
                    {
                        //this.saveAndPrintReport();
                        this.saveAndPrintTestingReoport();

                    }
                }else
                {
                    this.Alert("Please Fill The Detail First!", Toaster.enmType.Warning);
                }
            }
            else
            {
                DialogResult resTEST = MessageBox.Show("Are you sure Want to Modify & Print", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (resTEST == DialogResult.No)
                {

                    this.modifyTestingData();
                    this.clearTestingControl();
                    this.modifyTestingId = null;
                 
                }
                else if (resTEST == DialogResult.Yes)
                {

                    this.modifyTestingData();

                    printTestingRdlc_Form = new PrintTestingReport.PrintTestingForm();
                    printTestingRdlc_Form.callTestingdata(this.modifyTestingId.ToString());
                   
                    this.clearTestingControl();
                    this.modifyTestingId = null;
                  
                }
            }



        }

        private void testCustcombo_KeyDown(object sender, KeyEventArgs e)
        {
           // if (e.Control == true)
           // {
            //    this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                //   MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            //}

        }

        private void testCustcombo_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsControl(e.KeyChar) && char.IsDigit(e.KeyChar) &&
                             (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Character Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //      MessageBox.Show("This Field Accepts Character Only!");
            }


        }

        private void testCustcombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.testCustcombo.SelectedIndex != -1)
            {
                this.getTestSelectedCustomerData();

            }

        }

        private void getTestSelectedCustomerData()
        {
            {

                Database databaseObject = new Database();
                string query = "SELECT customerName,customerAddress,custMobile,Rate FROM customerTable WHERE id=" + this.testCustcombo.SelectedValue + "";
                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);



                try
                {
                    databaseObject.OpenConnection();
                    SQLiteDataReader result = command.ExecuteReader();
                    if (result.HasRows)
                    {
                        while (result.Read())
                        {



                        }
                    }

                    databaseObject.CloseConnection();
                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }

        }

        private void testPurity_Leave(object sender, EventArgs e)
        {
            if (this.testPurity.Text != "")
            {
                if (this.testPurity.Text.ToString() != "100")
                {
                    string your_String = this.testPurity.Text.ToString();
                    string my_String = Regex.Replace(your_String, @"[^0-9]+", "");
                    int my_newlength = my_String.Length;
                    if (my_String.Length > 3)
                    {
                        string resultValue = my_String.Substring(0, 3);
                        int parseNo = int.Parse(resultValue);
                        double karatValue = Math.Round((parseNo / 1000.0) * 24, 1);
                        this.testKarat.Text = karatValue.ToString();
                    }
                    else if (my_String.Length < 3)
                    {

                        string resultValue = my_String + "0";
                        int parseNo = int.Parse(resultValue);
                        double karatValue = Math.Round((parseNo / 1000.0) * 24, 1);
                        this.testKarat.Text = karatValue.ToString();
                    }
                    else if (my_String.Length == 3)
                    {
                        int parseNo = int.Parse(my_String);
                        double karatValue = Math.Round((parseNo / 1000.0) * 24, 1);
                        this.testKarat.Text = karatValue.ToString();

                    }
                }
                else
                {

                    int karatValue = 24;
                    this.testKarat.Text = karatValue.ToString();

                }



            }

            if (this.testPurity.Text != "" && this.testorgweight.Text != "")
            {
                string goldper_string = this.testPurity.Text.ToString();
                string goldWeight_stirng = this.testorgweight.Text.ToString();
                float parseNogoldPer = float.Parse(goldper_string);
                float parseGoldweight = float.Parse(goldWeight_stirng);
                double goldFine = Math.Round(((parseGoldweight * parseNogoldPer) / 100), 3);
                this.testgoldFine.Text = goldFine.ToString();
            }

            if (testPurity.Text.Length == 4)
            {
                string myString = this.testPurity.Text.ToString();
                float perValueNo = float.Parse(myString);
                float newValue = perValueNo / 100;
              //  newValue.ToString("#.##");
                this.testPurity.Text = String.Format("{0:N2}", double.Parse(newValue.ToString()));

            }



        }

        private void testPurity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                //MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }

        }

        private void testPurity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
             (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void testorgweight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }

        }

        private void testorgweight_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
               (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void testtounchweight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }

        }

        private void testtounchweight_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
              (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }

        }

        private void testAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }

        }

        private void testAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
            (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Number Only!");
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }

        }

        private void testSeries_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                // MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }

        }

        private void testSeries_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
           (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Number Only!");
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void bunifuButton8_Click_2(object sender, EventArgs e)
        {
            printTestingRdlc_Form = new PrintTestingReport.PrintTestingForm();
            printTestingRdlc_Form.callTestingdata("2");
        }

        private void testDatagridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.testDatagridView.Rows[e.RowIndex];
                this.testSeries.Text = row.Cells["dataGridViewTextBoxColumn36"].Value.ToString();
                this.testId.Text = row.Cells["dataGridViewTextBoxColumn35"].Value.ToString();
                this.testPurity.Text = row.Cells["dataGridViewTextBoxColumn42"].Value.ToString();
                this.testKarat.Text = row.Cells["dataGridViewTextBoxColumn43"].Value.ToString();
                this.testgoldFine.Text = row.Cells["dataGridViewTextBoxColumn46"].Value.ToString();
                this.testAmount.Text = row.Cells["dataGridViewTextBoxColumn47"].Value.ToString();
                this.testCustcombo.Text = row.Cells["dataGridViewTextBoxColumn39"].Value.ToString();
                this.testorgweight.Text = row.Cells["dataGridViewTextBoxColumn44"].Value.ToString();
                this.testtounchweight.Text = row.Cells["dataGridViewTextBoxColumn45"].Value.ToString();
                this.testSmaplecomboBox.Text = row.Cells["dataGridViewTextBoxColumn41"].Value.ToString();
                this.modifyTestingId = row.Cells["dataGridViewTextBoxColumn35"].Value.ToString();
                this.testingDropdown.Text = row.Cells["Column79"].Value.ToString();






            }


            //private void panel2_Paint(object sender, PaintEventArgs e)
            //{
            //    ControlPaint.DrawBorder(e.Graphics, panel2.ClientRectangle,
            //            Color.DimGray, 1, ButtonBorderStyle.Solid,
            //            Color.DimGray, 1, ButtonBorderStyle.None,
            //            Color.DimGray, 1, ButtonBorderStyle.Solid,
            //            Color.DimGray, 1, ButtonBorderStyle.Solid
            //        );


            //}

        }

        private void testPage_Click(object sender, EventArgs e)
        {

        }

        private void testorgweight_Leave(object sender, EventArgs e)
        {
            if (this.testPurity.Text != "")
            {
                if (this.testPurity.Text.ToString() != "100")
                {
                    string your_String = this.testPurity.Text.ToString();
                    string my_String = Regex.Replace(your_String, @"[^0-9]+", "");
                    int my_newlength = my_String.Length;
                    if (my_String.Length > 3)
                    {
                        string resultValue = my_String.Substring(0, 3);
                        int parseNo = int.Parse(resultValue);
                        double karatValue = Math.Round((parseNo / 1000.0) * 24, 1);
                        this.testKarat.Text = karatValue.ToString();
                    }
                    else if (my_String.Length < 3)
                    {

                        string resultValue = my_String + "0";
                        int parseNo = int.Parse(resultValue);
                        double karatValue = Math.Round((parseNo / 1000.0) * 24, 1);
                        this.testKarat.Text = karatValue.ToString();
                    }
                    else if (my_String.Length == 3)
                    {
                        int parseNo = int.Parse(my_String);
                        double karatValue = Math.Round((parseNo / 1000.0) * 24, 1);
                        this.testKarat.Text = karatValue.ToString();

                    }
                }
                else
                {

                    int karatValue = 24;
                    this.testKarat.Text = karatValue.ToString();

                }



            }

            if (this.testPurity.Text != "" && this.testorgweight.Text != "")
            {
                string goldper_string = this.testPurity.Text.ToString();
                string goldWeight_stirng = this.testorgweight.Text.ToString();
                float parseNogoldPer = float.Parse(goldper_string);
                float parseGoldweight = float.Parse(goldWeight_stirng);
                double goldFine = Math.Round(((parseGoldweight * parseNogoldPer) / 100), 3);
                this.testgoldFine.Text = goldFine.ToString();
            }
        }

        private void bunifuButton24_Click(object sender, EventArgs e)
        {
            this.clearTestingControl();
            this.modifyTestingId = null;
            this.testCustcombo.Select();
        }

        private void bunifuButton23_Click(object sender, EventArgs e)
        {
            //this.createReportFromToken();
          
        }

       

        private void tokenCustomerBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                //   MessageBox.Show("Cut/Copy and Paste Options are disabled !");
            }
        }

        private void tokenCustomerBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && char.IsDigit(e.KeyChar) &&
                 (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Character Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //      MessageBox.Show("This Field Accepts Character Only!");
            }
        }

        private void testCustcombo_Leave(object sender, EventArgs e)
        {
           
            if (this.testCustcombo.SelectedIndex == 0)
            {
                this.Alert("Customer Name Not Be Null!", Toaster.enmType.Warning);
            }
        }

        private void tokecustcomboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tokecustcomboBox2.SelectedIndex != -1)
            {
                this.getTokenSelectedCustomerData();

            }
        }

        private void tokecustcomboBox2_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Control == true)
           // {
           //     this.Alert("Cut/Copy and Paste Options are disabled!", Toaster.enmType.Info);
                //   MessageBox.Show("Cut/Copy and Paste Options are disabled !");
           // }
        }

        private void tokecustcomboBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && char.IsDigit(e.KeyChar) &&
               (e.KeyChar != '.'))
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //  MessageBox.Show("This Field Accepts Character Only!");

            }
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                this.Alert("This Field Accepts Character Only!", Toaster.enmType.Info);
                //      MessageBox.Show("This Field Accepts Character Only!");
            }
        }

        private void tokecustcomboBox2_Leave(object sender, EventArgs e)
        {
            if (this.tokecustcomboBox2.SelectedIndex == -1)
            {
                this.tokenAddress.Text = this.defaultAddress;
                this.tokenRate.Text = this.reportDefaultRate.ToString();
                this.tokenMobile.Text = this.defaultMobileNo.ToString() ;
            }
            if (this.tokecustcomboBox2.SelectedIndex == 0)
            {
                this.Alert("Customer Name Not Be Null!", Toaster.enmType.Warning);
            }
        }

        private void reportDatePicker_ValueChanged_1(object sender, EventArgs e)
        {
            if (this.startButtonView == true)
            {
                this.getReportTableDatagrid();
            }
            //MessageBox.Show("You are in the DateTimePickerVALUEChanged event.");
        }

        private void tokenDatePicker_ValueChanged(object sender, EventArgs e)
        {
            if (this.startButtonView == true)
            {
                this.getTokenListFromdb();
            }
        }

        private void testDate_ValueChanged(object sender, EventArgs e)
        {   if (this.startButtonView == true) { 
            this.testingDataGridDbData();
            }
        }

        private void bunifuButton8_Click_3(object sender, EventArgs e)
        {

         
                PrintDialog printDialog1 = new PrintDialog();
                printDialog1.Document = printDocument1;
                DialogResult result = printDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    printDocument1.Print();
                }

            /*  PrintDocument prtdoc = new PrintDocument();
              string strDefaultPrinter = prtdoc.PrinterSettings.PrinterName;
              foreach (String strPrinter in PrinterSettings.InstalledPrinters)
              {
                  comboBox2.Items.Add(strPrinter);
                  if (strPrinter == strDefaultPrinter)
                  {
                      comboBox2.SelectedIndex = comboBox2.Items.IndexOf(strPrinter);
                  }
              }*/
        }
        private void Dashboard_Paint(object sender, PaintEventArgs e)
        {

         // ControlPaint.DrawBorder(e.Graphics, this.panel1.ClientRectangle, Color.DarkRed, ButtonBorderStyle.Solid );
        }

        private void reportImageArea_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton19_Click_1(object sender, EventArgs e)
        {
            using (var folderDialog = new FolderBrowserDialog())
            {
                if(folderDialog.ShowDialog() == DialogResult.OK)
                {
                    backUppathtext.Text = folderDialog.SelectedPath;
                }
            }

        }

        private void bunifuButton28_Click(object sender, EventArgs e)
        {
            string sourcePath = Application.StartupPath;
            string tagetPath = this.backUppathtext.Text;
            string fileName = "NewBackUp.db";
            //var bkupFilename = Path.GetFileNameWithoutExtension(fileName) + ".bak";
            string targetPath = tagetPath;
            string sourceFile = System.IO.Path.Combine(sourcePath , fileName);
            string targetFile = System.IO.Path.Combine(targetPath, fileName);
            if (!System.IO.Directory.Exists(targetFile))
            {
                SQLiteConnection.CreateFile(targetFile);
                using (SQLiteConnection source = new SQLiteConnection(string.Format("Data Source = {0}", sourceFile)))
                using (SQLiteConnection destination = new SQLiteConnection(string.Format("Data Source = {0}", targetFile)))
                {
                    source.Open();
                    destination.Open();
                    source.BackupDatabase(destination, "main", "main", -1, null, 10000);
                    this.Alert("Data Backup Successfully!", Toaster.enmType.Success);
                }
                   
            }
        }
        

        private void bunifuButton17_Click(object sender, EventArgs e)
        {

        }

        private void backup_Click(object sender, EventArgs e)
        {

        }

        private void Dashboard_Resize(object sender, EventArgs e)
        {
            if (isMinimized == true)
            {
                panel1.Dock = DockStyle.Top;    // Re-dock
                panel1.Width = panel1.Width; // maintain desired width
                isMinimized = false;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
           
        }

        private void testDatagridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //Check to ensure that the row CheckBox is clicked.
          
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    //Reference the GridView Row.
                    DataGridViewRow row = testDatagridView.Rows[e.RowIndex];

                    //Set the CheckBox selection.
                    row.Cells["dataGridViewCheckBoxColumn1"].Value = !Convert.ToBoolean(row.Cells["dataGridViewCheckBoxColumn1"].EditedFormattedValue);

                    //If CheckBox is checked, display Message Box.
                    if (Convert.ToBoolean(row.Cells["dataGridViewCheckBoxColumn1"].Value))
                    {
                        this.updateTestingPaid(row.Cells["dataGridViewCheckBoxColumn1"].Value.ToString(), row.Cells["dataGridViewTextBoxColumn35"].Value.ToString());

                    }
                    else
                    {
                        this.updateTestingPaid(row.Cells["dataGridViewCheckBoxColumn1"].Value.ToString(), row.Cells["dataGridViewTextBoxColumn35"].Value.ToString());

                    }
                }
            
           
            //MessageBox.Show(reportDataTableGirdView.CurrentRow.Cells["id"].Value.ToString());
        }

        private void updateTestingPaid(string paidValue, string Id)
        {
            if (paidValue != "" && Id != "")
            {
                Database databaseObject = new Database();
                string query = "UPDATE testingTable set payment = @payment where id = @id";

                SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

                try
                {
                    databaseObject.OpenConnection();
                    command.Parameters.Add(new SQLiteParameter("@id", Id));
                    command.Parameters.Add(new SQLiteParameter("@payment", paidValue));

                    SQLiteDataReader result = command.ExecuteReader();


                    while (result.Read())
                    {


                    }
                    databaseObject.CloseConnection();


                    this.Alert("Testing Record Updated!", Toaster.enmType.Success);
                    //    this.getReportTableDatagrid();


                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }

            }
            else
            {

                this.Alert("Please Select The Testing Cell Data First", Toaster.enmType.Warning);

            }

        }

        private void reportDatePicker_VisibleChanged(object sender, EventArgs e)
        {
            this.getReportTableDatagrid();

            //MessageBox.Show("You are in the DateTimePicker.VISIBLEChanged event.");
        }

        private void testDate_VisibleChanged(object sender, EventArgs e)
        {
            this.testingDataGridDbData();

        }

        private void tokenDatePicker_VisibleChanged(object sender, EventArgs e)
        {
            this.getTokenListFromdb();

        }

        private void expenseDate_ValueChanged(object sender, EventArgs e)
        {
            if (this.startButtonView == true)
            {
                this.getExpensesDataFormdb();
            }
        }

        private void expenseDate_VisibleChanged(object sender, EventArgs e)
        {
            this.getExpensesDataFormdb();
        }

        private void bunifuDatePicker4_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void bunifuDatePicker5_ValueChanged(object sender, EventArgs e)
        {
            if ((this.fullReportStartDatepicker.Value) > (this.fullReportEndDatePicker.Value))
            {
                this.Alert("End Date Must Be Grater Than Start Date !", Toaster.enmType.Warning);
                this.allowDate = false;

            }
            else
            {
                this.allowDate = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void bunifuButton31_Click(object sender, EventArgs e)
        {
            this.dashBoardLoad();
        }

        private void dashBoardLoad()
        {
            this.getTotalReportCount();
            this.getTotalSaleAmount();
            this.getTotalExpenseAmount();
            this.getTotalTestingAmount();
            this.getTotalTestingCount();
            this.getTotalSavingAmount();
            this.getTotalUnpaidReport();
            this.getTotalUnpaidReportAmount();
            this.getTotalUnpaidTesting();
            this.getTotalUnpaidTestingAmount();
        }
        private void getTotalReportCount()
        {
            Database databaseObject = new Database();
            string query = "SELECT COUNT(*) FROM reportTable WHERE  DATE(reportTable.currentDate) BETWEEN '" + this.dashboardStartDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.dashboardEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                if (command.ExecuteScalar().ToString() != "")
                {
                    this.totalReportText.Text = Convert.ToInt32(command.ExecuteScalar()).ToString();
                } else
                {
                    this.totalReportText.Text = "0";
                }
                databaseObject.CloseConnection();
               

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }
        private void getTotalSaleAmount()
        {
            Database databaseObject = new Database();
          string query = "SELECT  SUM(amount) FROM reportTable WHERE  DATE(reportTable.currentDate) BETWEEN '" + this.dashboardStartDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.dashboardEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "' AND reportTable.payment = 'True' " ;
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                if (command.ExecuteScalar().ToString() != "")
                {
                    this.saleAmountText.Text = Convert.ToInt32(command.ExecuteScalar()).ToString();
                } else
                {
                    this.saleAmountText.Text = "0";
                }
                databaseObject.CloseConnection();


            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }
        private void getTotalExpenseAmount()
        {
            Database databaseObject = new Database();
            string query = "SELECT  SUM(expenses) FROM expensesTable  WHERE  DATE(expensesTable.date) BETWEEN '" + this.dashboardStartDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.dashboardEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                if (command.ExecuteScalar().ToString() != "")
                {
                    this.investAmontText.Text = Convert.ToInt32(command.ExecuteScalar()).ToString();
                } else
                {
                    this.investAmontText.Text = "0";
                }
                databaseObject.CloseConnection();


            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }
        private void getTotalTestingCount()
        {
            Database databaseObject = new Database();
            string query = "SELECT COUNT(*) FROM testingTable WHERE  DATE(date) BETWEEN '" + this.dashboardStartDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.dashboardEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                if (command.ExecuteScalar().ToString() != "")
                {
                    this.testingCountText.Text = Convert.ToInt32(command.ExecuteScalar()).ToString();
                } else
                {
                    this.testingCountText.Text = "0";
                }
                databaseObject.CloseConnection();


            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

        private void getTotalTestingAmount()
        {
            Database databaseObject = new Database();
            string query = "SELECT SUM(Amount) FROM testingTable WHERE  DATE(date) BETWEEN '" + this.dashboardStartDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.dashboardEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'AND testingTable.payment = 'True'";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                if (command.ExecuteScalar().ToString() != "")
                {
                    this.totaltestingAmountText.Text = Convert.ToInt32(command.ExecuteScalar()).ToString();
                } else
                {
                    this.totaltestingAmountText.Text = "0";
                }
                databaseObject.CloseConnection();


            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }

        private void getTotalUnpaidReport()
        {
            Database databaseObject = new Database();
            string query = "SELECT  COUNT(*) FROM reportTable    WHERE  DATE(reportTable.currentDate) BETWEEN '" + this.dashboardStartDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.dashboardEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "' AND reportTable.payment = 'False' ";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                if (command.ExecuteScalar().ToString() != "")
                {
                    this.totalUnpaidReportText.Text = Convert.ToInt32(command.ExecuteScalar()).ToString();
                }else
                {
                    this.totalUnpaidReportText.Text = "0";
                }
                databaseObject.CloseConnection();


            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }
        private void getTotalUnpaidReportAmount()
        {
            Database databaseObject = new Database();
            string query = "SELECT  SUM(customerTable.Rate) FROM reportTable  INNER JOIN customerTable ON reportTable.customerId = customerTable.id   WHERE  DATE(reportTable.currentDate) BETWEEN '" + this.dashboardStartDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.dashboardEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "' AND reportTable.payment = 'False' ";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            try
            {
                databaseObject.OpenConnection();

                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                if (command.ExecuteScalar().ToString() != "")
                {
                    this.totalUnpaidReportAmount.Text = Convert.ToInt32(command.ExecuteScalar()).ToString();
                } else
                {
                    this.totalUnpaidReportAmount.Text = "0";
                }
                databaseObject.CloseConnection();


            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

        }
        private void getTotalUnpaidTesting()
        {

            Database databaseObject = new Database();
            string query = "SELECT  COUNT(*) FROM testingTable  WHERE  DATE(date) BETWEEN '" + this.dashboardStartDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.dashboardEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "' AND testingTable.payment = 'False' ";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                if (command.ExecuteScalar().ToString() != "")
                {
                    this.totalUnpaidTesting.Text = Convert.ToInt32(command.ExecuteScalar()).ToString();
                }
                else
                {
                    this.totalUnpaidTesting.Text = "0";
                }
                databaseObject.CloseConnection();


            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }
        private void getTotalUnpaidTestingAmount()
        {

            Database databaseObject = new Database();
            string query = "SELECT  SUM(amount) FROM testingTable  WHERE  DATE(date) BETWEEN '" + this.dashboardStartDatePicker.Value.Date.ToString("yyyy-MM-dd") + "'  AND '" + this.dashboardEndDatePicker.Value.Date.ToString("yyyy-MM-dd") + "' AND testingTable.payment = 'False' ";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);
            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                if (command.ExecuteScalar().ToString() != "")
                {
                    this.unpaidTestingAmount.Text = Convert.ToInt32(command.ExecuteScalar()).ToString();
                }
                else
                {
                    this.unpaidTestingAmount.Text = "0";
                }
                databaseObject.CloseConnection();


            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }
        private void getTotalSavingAmount()
        {
       int Total = ((Convert.ToInt32(this.totaltestingAmountText.Text) + Convert.ToInt32(this.saleAmountText.Text)) - Convert.ToInt32(this.investAmontText.Text));
            this.savingText.Text = Total.ToString();
        }

        private void saleAmountText_Click(object sender, EventArgs e)
        {

        }

        private void bunifuLabel76_Click(object sender, EventArgs e)
        {

        }

        private void dashboardStartDatePicker_onValueChanged(object sender, EventArgs e)
        {

        }

        private void bunifuButton29_Click(object sender, EventArgs e)
        {
           if (this.fullcustomerReportBox.Text != "" && this.fullfilterMobileNo.Text !="")
            {
                DialogResult resP = MessageBox.Show("Are you sure want to Print", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (resP == DialogResult.Yes)
                {
                    fullReprt_Form = new PrintFullReport.FullReprtForm();
                    fullReprt_Form.callFullReportdata(this.fullcustomerReportBox.SelectedValue.ToString(), this.fullReportStartDatepicker.Value.Date.ToString("yyyy-MM-dd"), this.fullReportEndDatePicker.Value.Date.ToString("yyyy-MM-dd"), this.fullfilterMobileNo.Text);
                }
                } else
            {
                this.Alert("Please Select Customer and Mobile No !", Toaster.enmType.Warning);
            }
          
        }

        private void label5_Click_1(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click_1(object sender, EventArgs e)
        {

        }

        private void bunifuButton30_Click(object sender, EventArgs e)
        {
            this.clearFullreportData();
        }
        private void clearFullreportData()
        {
            this.fullcustomerReportBox.ResetText();
            this.fullfilterMobileNo.Text = "";
            this.fullReportStartDatepicker.Value = DateTime.Now;
            this.fullReportEndDatePicker.Value = DateTime.Now;


        }

        private void reportDetailGalliumTextBox_Enter(object sender, EventArgs e)
        {

        }

        private void reportDetailGalliumTextBox_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void firstIconText_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void firstIconText_KeyUp(object sender, KeyEventArgs e)
        {
          /*  if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
            }*/
        }

        private void lastIconText_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void customerAddressTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void adddetailButton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void reportSaveButton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void reportDetailSilverTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void reportDetailSilverTextBox_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void custMastAddresstextbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void custMastFirmNametextbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void reportDetailSaveButton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void fullfilterMobileNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))

            {
                e.Handled = true;
                this.Alert("This Field Accepts Number Only!", Toaster.enmType.Info);
                // MessageBox.Show("This Field Accepts Number Only!");

            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void tokenSeriesno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void testgoldFine_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void bunifuButton22_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void expenseId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void bunifuButton26_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void bunifuButton8_Click_4(object sender, EventArgs e)
        {

        }

        private void setDefaultReportValueFunc()
        {
         
            this.custMastMobiletextbox.Text = this.defaultMobileNo.ToString();
            this.custMastAddresstextbox.Text = this.defaultAddress.ToString();
            this.custMastFirmNametextbox.Text = this.defaultFirmName.ToString();
            this.customerMobileTextBox.Text = this.defaultMobileNo.ToString();
            this.customerAddressTextBox.Text = this.defaultAddress.ToString();
            this.tokenMobile.Text = this.defaultMobileNo.ToString();
            this.tokenAddress.Text = this.defaultAddress.ToString();
        }

        private void bunifuLabel89_Click(object sender, EventArgs e)
        {

        }

        private void testingDropdown_SelectedValueChanged(object sender, EventArgs e)
        {
            
            if (this.testingDropdown.Text.ToString() == "Gold")
            {
                this.testAmount.Text = this.testingDefaultRate.ToString();
            } else if (this.testingDropdown.Text.ToString() == "Silver")
            {
                this.testAmount.Text = this.reportDefaultRate.ToString();
            }
        }

        private void bunifuCards2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuButton8_Click_5(object sender, EventArgs e)
        {     
            string your_String = this.testPurity.Text.ToString();
        
            string resultValue = your_String + ".";
            
            float parseNo = float.Parse(resultValue);
            float karatValue = (parseNo / 100);

            //decimal value = decimal.Parse(resultValue);
            //MessageBox.Show("AA GAYA", karatValue.ToString());



        }

        private void testPurity_Enter(object sender, EventArgs e)
        {
          
        }

        private void bunifuLabel99_Click(object sender, EventArgs e)
        {

        }

        private void bunifuDatePicker4_ValueChanged_1(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }
        private void sendWhatsApp(string number, string message)

        {

            try

            {

                if (number == "")

                {

                    //this.Alert("Please ")

                }

                if (number.Length <= 10)

                {

                    //MessageBox.Show("Inidan Code added automatically");

                    number = "+91" + number;

                }

                number = number.Replace(" ", "");



                System.Diagnostics.Process.Start("http://api.whatsapp.com/send?phone=" + number + "&text=" + message);

            }

            catch (Exception ex)

            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void bunifuButton31_Click_1(object sender, EventArgs e)
        {
            if (this.sendGoldPurity.Text.Length != 0)
            {
                DialogResult send = MessageBox.Show("Do you want to send Report To Client ?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (send == DialogResult.OK)
                {
                    this.sendWhatsApp(this.sendMobile.Text.ToString(), "Dear " + "*" + this.sendCustomer.Text.ToString()+ "*" + "%0a" + "Greetings of the day," + "%0a" + "Your report has been generated for Bill No "+ "*"+ this.sendseries.Text.ToString() + " on " + this.sendDate.Text.ToString()+ "*" + " %0a" + "Your sample " + "*" +this.sendSample.Text.ToString() +  "* of *" + this.sendTweight.Text.ToString() +"GM* "+ "Weight contains *Gold Purity of " + this.sendGoldPurity.Text.ToString() +" %*" + "%0a" + "*Thank you for using Chetna Bullion Refinery*" + "%0a" + "_Note: 1- 0.35% (plus/minus) Difference may be possible" + "%0a" + "2- The report pertains to specific points and not responsible for other points or Melting issue");
                }
                else if (send == DialogResult.Cancel)
                {

                }
            }
            else
            {
                this.Alert("Please Select Data First !", Toaster.enmType.Warning);
            }
        }

        private void bunifuButton17_Click_1(object sender, EventArgs e)
        {
         
                    this.getsendReportTableDatagrid();
               
        }

        private void getsendReportTableDatagrid()
        {
            Database databaseObject = new Database();
            ;
            // string query = " SELECT * FROM reportTable INNER JOIN customerTable ON reportTable.customerId = customerTable.id ORDER BY id DESC";
            string query = "SELECT * FROM reportTable INNER JOIN customerTable ON reportTable.customerId = customerTable.id INNER JOIN reportDetailTable ON reportTable.id = reportDetailTable.reportId WHERE reportTable.currentDate = '" + this.selectDate.Value.Date.ToString("yyyy-MM-dd") + "'ORDER BY id DESC";
            SQLiteCommand command = new SQLiteCommand(query, databaseObject.myConn);

            try
            {
                databaseObject.OpenConnection();
                SQLiteDataAdapter sda = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                this.sendreportDataTable.AutoGenerateColumns = false;
                this.sendreportDataTable.DataSource = dt;
                databaseObject.CloseConnection();
                int count = this.sendreportDataTable.RowCount - 1;

                foreach (DataRow pRow in dt.Rows)
                {


                }

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

        private void bunifuButton8_Click_6(object sender, EventArgs e)
        {
            this.SuspendLayout();
            indicator.Location = new Point(3, 544);
            this.ResumeLayout();
            pagesForm.SetPage("BackUp");

        
            
        }

        private void sendreportDataTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow sendRow = this.sendreportDataTable.Rows[e.RowIndex];

                this.sendSerial.Text = sendRow.Cells["dataGridViewTextBoxColumn54"].Value.ToString();
               
                this.sendseries.Text = sendRow.Cells["dataGridViewTextBoxColumn55"].Value.ToString();
                this.sendCustomer.Text = sendRow.Cells["dataGridViewTextBoxColumn57"].Value.ToString();
                if (sendRow.Cells["dataGridViewTextBoxColumn60"].Value.ToString() != null)
                {
                    this.sendGoldPurity.Text = sendRow.Cells["dataGridViewTextBoxColumn60"].Value.ToString();
                }
                
                this.sendSample.Text = sendRow.Cells["dataGridViewTextBoxColumn59"].Value.ToString();
            
                if (sendRow.Cells["dataGridViewTextBoxColumn62"].Value.ToString() != null)
                {
                    this.sendOweight.Text = sendRow.Cells["dataGridViewTextBoxColumn62"].Value.ToString();
                }
                if (sendRow.Cells["dataGridViewTextBoxColumn63"].Value.ToString() != null)
                {
                    this.sendTweight.Text = sendRow.Cells["dataGridViewTextBoxColumn63"].Value.ToString();
                }


                this.sendMobile.Text = sendRow.Cells["dataGridViewTextBoxColumn58"].Value.ToString();
                this.sendAmount.Text = sendRow.Cells["Column202"].Value.ToString();
                this.sendFine.Text = sendRow.Cells["dataGridViewTextBoxColumn64"].Value.ToString();
                this.sendKarat.Text = sendRow.Cells["dataGridViewTextBoxColumn61"].Value.ToString();
                
            }
        }

        private void bunifuTextBox3_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void bunifuButton33_Click(object sender, EventArgs e)
        {
            this.sendCustomer.ResetText();
            this.sendAmount.ResetText();
            this.sendDate.ResetText();
            this.sendFine.ResetText();
            this.sendKarat.ResetText();
            this.sendMobile.ResetText();
            this.sendOweight.ResetText();
            this.sendTweight.ResetText();
            this.sendseries.ResetText();
            this.sendSample.ResetText();
            this.sendGoldPurity.ResetText();
        }
    }
}
