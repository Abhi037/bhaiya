﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoldRaven
{
    public partial class savePrint : Form
    {
      
       // public string ReturnValue2 { get; set; }
        public savePrint()
        {
            InitializeComponent();
            this.Show();
        }
        public enum MessageBoxButtons
        {
            OK=0,YesNo=1,YesNoCancel=2
        }
        /*    public void Show(string msg)
            {
                dialogLebel.Text = msg;
                yesButton.Visible = false;
                noButton.Visible = false;
                cancelButton.Visible = false;
            }
         public void Show(string msg, string Title)
            {
                dialogLebel.Text = msg;
                yesButton.Visible = false;
                noButton.Visible = false;
                cancelButton.Visible = false;
            }*/

        public void Show(string msg, string Title, MessageBoxButtons button)
        {
          
        dialogLebel.Text = msg;

            if (MessageBoxButtons.OK == button)
            {
                yesButton.Visible = false;
                noButton.Visible = false;
                cancelButton.Visible = false;
            }
            else if (MessageBoxButtons.YesNo == button)
            {
                yesButton.Visible = true;
                noButton.Visible = true;
                cancelButton.Visible = false;
                okButton.Visible = false;
              
            }
            else if (MessageBoxButtons.YesNoCancel == button)
            {
                yesButton.Visible = true;
                noButton.Visible = true;
                cancelButton.Visible = true;
                okButton.Visible = false;
            }
           
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void reportModifyButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
          
        }

        private void reportSaveButton_Click(object sender, EventArgs e)
        {
            {
               
                this.DialogResult = DialogResult.Cancel;
              
         }
        }

        private void bunifuLabel1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuButton5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bunifuButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
